package org.tinyjee;

import java.util.HashMap;

/**
 * Simple content creating source class that references a implementation from another module.
 *
 * @author Juergen_Kellerer, 2011-10-27
 */
public class SimpleContentClass extends HashMap<String, String> {

	private static final long serialVersionUID = -1672955066852006035L;

	public SimpleContentClass() {
		put("source-content", "Next Poisson " + new DummyDataGenerator().nextPoisson());
	}
}
