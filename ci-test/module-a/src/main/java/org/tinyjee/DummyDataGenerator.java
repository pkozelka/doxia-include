package org.tinyjee;

import org.apache.commons.math.random.RandomData;
import org.apache.commons.math.random.RandomDataImpl;

/**
 * Simple random data generator to test dependency resolution.
 *
 * @author Juergen_Kellerer, 2011-10-27
 */
public class DummyDataGenerator {

	RandomData randomData = new RandomDataImpl();

	public long nextPoisson() {
		return randomData.nextPoisson(10D);
	}
}
