This folder contains a "pom" that may be used to redistribute the
jgraphx library on Maven Central.

Usage:

1. Call

  ./local-release.sh

2. ZIP (*.jar/*.pom) inside "target"

3. Upload to "https://oss.sonatype.org/"