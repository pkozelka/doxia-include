/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package samples;

import com.jhlabs.image.GaussianFilter;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.io.IOException;
import java.util.Map;

import static java.awt.RenderingHints.KEY_ANTIALIASING;
import static java.awt.RenderingHints.VALUE_ANTIALIAS_ON;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
import static java.lang.Float.parseFloat;
import static java.lang.String.valueOf;

/**
 * Provides methods for dynamic image rendering.
 *
 * @author juergen kellerer, 2012-05-28
 */
public class ImageSample {

	//
	// Note: Take care on the total width, if should not be more than 80 chars
	// as the methods are included inside the documentation pages.
	//

	public JFrame getFrame(Map parameters) {
		JFrame frame = new JFrame();
		String text = valueOf(parameters.get("text"));
		frame.setContentPane(getPanel(text));

		frame.pack();

		return frame;
	}

	private JPanel getPanel() {
		return getPanel("Hello World");
	}

	private JPanel getPanel(String text) {
		JPanel contentPane = new JPanel(new FlowLayout());

		contentPane.add(new JLabel(text));
		contentPane.add(new JButton("Ok"));
		return contentPane;
	}

	public Image getImage() {
		BufferedImage image = new BufferedImage(128, 128, TYPE_INT_ARGB);
		Graphics2D graphics = image.createGraphics();
		try {
			graphics.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_ON);
			graphics.setPaint(Color.RED);
			graphics.setStroke(new BasicStroke(3.5f));
			graphics.drawOval(32, 32, 64, 64);
		} finally {
			graphics.dispose();
		}

		return image;
	}

	public Image getLoadedImage() throws IOException {
		return Toolkit.getDefaultToolkit().
				createImage("src/test/java/samples/ImageSample.png");
	}

	public BufferedImageOp getGaussianBlur(Map parameters) {
		float radius = parameters.containsKey("radius") ?
				parseFloat(valueOf(parameters.get("radius"))) : 5f;

		return new GaussianFilter(radius);
	}

	public Object throwHeadLessException() {
		throw new HeadlessException();
	}
}
