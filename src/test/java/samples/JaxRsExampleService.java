/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package samples;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Implements a sample JAX-RS service.
 * <p/>
 * TODO: Implement a pretty example.. this is just for proof of concept.
 *
 * @author Juergen_Kellerer, 2012-07-29
 */
@Path("example")
@Produces(MediaType.APPLICATION_XML)
public class JaxRsExampleService {

	/**
	 * Returns the sample xml type.
	 *
	 * @return the sample xml type.
	 * @dim.example.response samples.SampleXmlType.getSampleInstance()
	 */
	@GET
	public SampleXmlType getSampleXmlType() {
		return SampleXmlType.getSampleInstance();
	}

	/**
	 * Returns the YesAndNo option from the sample.
	 *
	 * @param id the id of the sample to select.
	 * @return the YesAndNo option from the sample.
	 * @dim.example.response samples.JaxRsExampleService.exampleResponseGetOptionFromSample()
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String getOptionFromSample(@PathParam("id") @DefaultValue("") String id) {
		return SampleXmlType.YesNoOption.No.toString();
	}

	static String exampleResponseGetOptionFromSample() {
		return SampleXmlType.YesNoOption.No.toString();
	}

	/**
	 * Changes the activity for the selected sample.
	 *
	 * @param id the id of the sample to select.
	 * @param activity the activity to set.
	 * @return the changed sample.
	 * @dim.example.response samples.SampleXmlType.getSampleInstance()
	 */
	@POST
	@Path("/change")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public SampleXmlType changeSample(
			@QueryParam("id") @DefaultValue("") String id,
			@FormParam("activity") @DefaultValue("") String activity) {
		return SampleXmlType.getSampleInstance();
	}

	/**
	 * Replaces the selected sample.
	 *
	 * @param id the id of the sample to replace.
	 * @param newSample the sample to set.
	 * @dim.example.request samples.SampleXmlType.getSampleInstance()
	 */
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_XML)
	public Response replaceSample(@PathParam("id") @DefaultValue("") String id, SampleXmlType newSample) {
		return Response.ok().build();
	}

	/**
	 * Deletes the selected sample.
	 *
	 * @param id the id of the sample to delete.
	 */
	@DELETE
	@Path("/{id}")
	public Response deleteSample(@PathParam("id") @DefaultValue("") String id) {
		return Response.ok().build();
	}
}
