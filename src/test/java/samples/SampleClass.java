package samples;

/**
 * Sample class used to demonstrate snippet selection.
 *
 * @author Juergen_Kellerer, 2011-10-16
 */
public class SampleClass {

	int field;

	// START-SNIPPET: ClassMethod
	@SuppressWarnings("none")
	void method() {
		//
	}
	// END-SNIPPET: ClassMethod

	@SuppressWarnings("none")
	void method(Integer value) {
	}

	// START-SNIPPET: InnerClass
	@Deprecated
	static class InnerClass {
		String innerField = "abc";

		@SuppressWarnings("none")
		void innerMethod() {
		}
	}
	// END-SNIPPET: InnerClass

	public int getField() {
		return field;
	}
}
