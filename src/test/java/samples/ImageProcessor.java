/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package samples;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Map;

import static java.lang.String.valueOf;

//
// Note: Take care on the total width, if should not be more than 80 chars
// as the methods are included inside the documentation pages.
//

/**
 * Simple sample of a image processor.
 *
 * @author Juergen_Kellerer, 2012-05-30
 */
public class ImageProcessor {
	public void drawBorder(Map parameters) {
		BufferedImage image = (BufferedImage) parameters.get("image");

		Paint borderColor = parameters.containsKey("borderColor") ?
				Color.decode(valueOf(parameters.get("borderColor"))) :
				Color.BLACK;

		Graphics2D g = image.createGraphics();
		try {
			g.setPaint(borderColor);
			g.drawRect(0, 0, image.getWidth() - 1, image.getHeight() - 1);
		} finally {
			g.dispose();
		}
	}
}
