package samples;

import javax.xml.bind.annotation.*;

/**
 * Provides a sample file for JAXB related operations.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "sample", namespace = "urn:org.tinyjee.dim")
public class SampleXmlType {

	@XmlType
	public enum YesNoOption {
		Yes,
		No
	}

	@XmlAttribute(required = true)
	private YesNoOption doIt;
	@XmlElement(required = true)
	private String activity;

	public SampleXmlType() {
	}

	public SampleXmlType(YesNoOption doIt, String activity) {
		this.doIt = doIt;
		this.activity = activity;
	}

	public static SampleXmlType getSampleInstance() {
		return new SampleXmlType(YesNoOption.Yes, "Include serialized Xml from JAXB root elements.");
	}
}
