/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.tinyjee.maven.dim.spi.RequestParameterTransformer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import static org.junit.Assert.*;

/**
 * Tests the implementations of MacroPresetParameterTransformer and MacroPreset.
 *
 * @author Juergen_Kellerer, 2012-02-21
 */
public class MacroPresetParameterTransformerTest {

	MacroPresetParameterTransformer transformer = new MacroPresetParameterTransformer();
	Map<String, Object> requestParams = new HashMap<String, Object>(), expectedParams;
	MacroPreset preset1, preset2;
	boolean alias;

	@Before
	public void setUp() throws Exception {
		requestParams.put("aKey", "aValue");
		requestParams.put("bKey", "bValue");
		expectedParams = new HashMap<String, Object>(requestParams);
	}

	@After
	public void tearDown() throws Exception {
		for (Iterator<Map.Entry<Object, Object>> iterator = System.getProperties().entrySet().iterator(); iterator.hasNext(); ) {
			final String key = String.valueOf(iterator.next().getKey());
			if (key.startsWith(MacroPreset.KEY_PREFIX) || key.startsWith(MacroPreset.ALIAS_KEY))
				iterator.remove();
		}
	}

	public void initPresets() {
		preset1 = new MacroPreset("preset1", alias);
		preset2 = new MacroPreset("preset2", alias);

		preset1.getProperties().put("p1-k1", "p1-k1-v1");
		preset1.getProperties().put("p1-k2", "p1-k2-${value}");

		preset2.getProperties().put("p2-k1", "p2-k1-v1");
		preset2.getProperties().put("p2-k2", "p2-k2-${value}");

		final Properties properties = System.getProperties();
		preset1.encodeTo(properties);
		preset2.encodeTo(properties);
	}

	@Test
	public void testTransformerIsRegistered() throws Exception {
		boolean found = alias;
		for (RequestParameterTransformer transformer : RequestParameterTransformer.TRANSFORMERS)
			found |= MacroPresetParameterTransformer.class.equals(transformer.getClass());
		assertTrue(found);
	}

	@Test
	public void testTransformNonExistingDoesNotFail() throws Exception {
		assertTransformationIsValid();
	}

	private void assertTransformationIsValid() {
		transformer.transformParameters(requestParams);
		assertEquals(expectedParams, requestParams);
	}

	@Test
	public void testTransformerWorksOnPreset() throws Exception {
		initPresets();

		requestParams.put(MacroPresetParameterTransformer.KEY_PRESET, "preset1");
		setUp();
		expectedParams.putAll(preset1.getProperties());
		assertTransformationIsValid();

		requestParams.clear();
		requestParams.put(MacroPresetParameterTransformer.KEY_PRESET, "preset2");
		setUp();
		expectedParams.putAll(preset2.getProperties());
		assertTransformationIsValid();
	}

	@Test
	public void testSupportsDefaultPreset() throws Exception {
		final Properties properties = System.getProperties();
		final MacroPreset defaultPreset = new MacroPreset(MacroPresetParameterTransformer.DEFAULT_PRESET, alias);
		defaultPreset.getProperties().put("d1", "dv1");
		defaultPreset.encodeTo(properties);

		setUp();
		expectedParams.putAll(defaultPreset.getProperties());
		assertTransformationIsValid();
	}

	@Test
	public void testSupportsAliases() throws Exception {
		testTransformerWorksOnPreset();

		requestParams.clear();
		requestParams.put("preset1", "123");
		transformer.transformParameters(requestParams);
		assertEquals(1, requestParams.size());
		assertEquals("123", requestParams.get("preset1"));

		alias = true;
		initPresets();

		requestParams.clear();
		requestParams.put("preset1", "123");
		transformer.transformParameters(requestParams);
		assertEquals("p1-k2-123", requestParams.get("p1-k2"));

		requestParams.clear();
		setUp();
		requestParams.put("preset1", "${value}");
		expectedParams.putAll(preset1.getProperties());
		assertTransformationIsValid();

		requestParams.clear();
		setUp();
		requestParams.put("preset2", "${value}");
		expectedParams.putAll(preset2.getProperties());
		assertTransformationIsValid();
	}
}
