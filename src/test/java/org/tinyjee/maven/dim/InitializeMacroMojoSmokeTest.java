/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim;

import org.apache.maven.model.Build;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.apache.maven.project.MavenProject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Arrays;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

/**
 * Primarily a smoke test to protect from build errors.
 *
 * @author Juergen_Kellerer, 2011-10-27
 */
public class InitializeMacroMojoSmokeTest {

	InitializeMacroMojo mojo = new InitializeMacroMojo();
	MavenProject mavenProject = mock(MavenProject.class);

	private void setField(String name, Object value) throws Exception {
		Field field = InitializeMacroMojo.class.getDeclaredField(name);
		field.setAccessible(true);
		field.set(mojo, value);
	}

	@Before
	public void setUp() throws Exception {
		mojo.setLog(new SystemStreamLog());
		setField("basedir", new File("."));
		setField("project", mavenProject);
		setField("exportAdditionalProjectPaths", true);
		setField("exportProjectClasspath", true);
		setField("inputEncoding", "utf-8");
		setField("siteDirectory", "123");

		Build build = mock(Build.class, withSettings().defaultAnswer(new Answer<Object>() {
			public Object answer(InvocationOnMock invocation) throws Throwable {
				Class<?> returnType = invocation.getMethod().getReturnType();
				if(String.class.isAssignableFrom(returnType))
					return "a";
				else {
					Resource mock = mock(Resource.class);
					when(mock.getDirectory()).thenReturn("b");
					return Arrays.asList(mock);
				}
			}
		}));
		when(mavenProject.getBuild()).thenReturn(build);
	}

	@After
	public void tearDown() throws Exception {
		new ResetMojo().execute();
	}

	@Test
	public void testCanExecute() throws Exception {
		mojo.execute();
	}
}
