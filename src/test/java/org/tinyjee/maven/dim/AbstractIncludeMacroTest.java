/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim;

import org.apache.maven.doxia.macro.MacroExecutionException;
import org.apache.maven.doxia.macro.MacroRequest;
import org.apache.maven.doxia.macro.manager.MacroManager;
import org.apache.maven.doxia.sink.Sink;
import org.apache.maven.doxia.sink.XhtmlBaseSink;
import org.codehaus.plexus.PlexusConstants;
import org.codehaus.plexus.PlexusContainer;
import org.codehaus.plexus.context.Context;
import org.junit.After;
import org.junit.Before;
import org.tinyjee.maven.dim.spi.GlobalsStack;
import org.tinyjee.maven.dim.spi.Source;

import java.io.File;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Base class for tests that run tests using the Macro interface.
 *
 * @author Juergen_Kellerer, 2011-10-16
 */
public abstract class AbstractIncludeMacroTest {

	protected Source capturedSource;
	protected IncludeMacro macro = new IncludeMacro() {
		@Override
		protected void processSource(Map<String, String> textParameters,
									 File basePath, Source source, Sink sink) throws MacroExecutionException {
			capturedSource = source;
			super.processSource(textParameters, basePath, source, sink);
		}
	};

	protected Map<String, String> params = new HashMap<String, String>();
	protected MacroRequest request = new MacroRequest(params, new File(".").getAbsoluteFile());
	protected StringWriter outputBuffer = new StringWriter();
	protected Sink output = new XhtmlBaseSink(outputBuffer);

	protected PlexusContainer container = mock(PlexusContainer.class);
	protected MacroManager macroManager = mock(MacroManager.class);

	@Before
	public void initializeGlobals() throws Exception {
		when(container.lookup(MacroManager.ROLE)).thenReturn(macroManager);
		when(macroManager.getMacro("include")).thenReturn(macro);

		Context context = mock(Context.class);
		when(context.get(PlexusConstants.PLEXUS_KEY)).thenReturn(container);
		macro.contextualize(context);

		GlobalsStack.initializeGlobals(container, null, null, null, null, null);
	}

	@After
	public void removeGlobals() throws Exception {
		GlobalsStack.removeGlobals();
	}

	protected void assertBufferContains(String... tokens) {
		StringBuffer buffer = outputBuffer.getBuffer();
		for (String token : tokens)
			assertTrue("Not contains: " + token, buffer.indexOf(token) != -1);
	}

	protected void assertBufferDoesNotContain(String... tokens) {
		StringBuffer buffer = outputBuffer.getBuffer();
		for (String token : tokens)
			assertEquals("Contains: " + token, -1, buffer.indexOf(token));
	}

	protected String callMacro(boolean clearOutputFirst) throws MacroExecutionException {
		if (clearOutputFirst) clearOutput();
		return callMacro();
	}

	protected String callMacro() throws MacroExecutionException {
		macro.execute(output, request);
		return outputBuffer.getBuffer().toString();
	}

	protected void clearOutput() {
		params.remove(IncludeMacroSignature.PARAM_SOURCE_CONTENT);
		outputBuffer.getBuffer().setLength(0);
	}

	protected void assertCauseIs(Throwable e, Object classOrMessage) {
		for (Throwable t = e; t != null; t = t.getCause())
			if (classOrMessage.equals(t.getClass()) || classOrMessage.equals(classOrMessage) || classOrMessage.equals(t.getMessage()))
				return;

		throw new AssertionError(e);
	}
}
