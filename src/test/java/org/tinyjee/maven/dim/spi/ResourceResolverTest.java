/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.setProperty;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.tinyjee.maven.dim.spi.ResourceResolver.*;

/**
 * Tests the resource resolver utils.
 *
 * @author Juergen_Kellerer, 2010-09-06
 * @version 1.0
 */
public class ResourceResolverTest {

	File baseDir = new File(".");

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@Test
	public void testToFilePathCanResolveFileURLs() throws Exception {
		for (String prefix : new String[]{"/", "file:/", "file:///"})
			assertEquals(prefix, "/path/to/test", toFilePath(prefix + "path/to/test"));
		assertNull(toFilePath("classpath:/path/to/resource"));
		assertEquals("äöü", toFilePath("äöü"));
	}

	@Test
	public void testFindSiteSourceDirectory() throws Exception {
		File expected = new File(baseDir, "src/site").getCanonicalFile();
		File proposedSite = folder.newFolder("custom/site").getCanonicalFile();

		assertEquals(expected, findSiteSourceDirectory(baseDir, null));
		assertEquals(expected, findSiteSourceDirectory(baseDir, proposedSite));

		assertTrue(proposedSite.mkdirs());
		assertEquals(proposedSite, findSiteSourceDirectory(baseDir, proposedSite));
	}

	@Test
	public void testFindMatchingURLsSupportsModulePath() throws Exception {
		final List<String> dependencies = Arrays.asList("my-other-group:my-other-module");
		setModulePath("my-test-group", "my-test-module", baseDir, dependencies);
		List<URL> result = new ArrayList<URL>();
		findMatchingURLs(baseDir, "[my-test-module]:/org/tinyjee/maven/dim/sh/highlight.js", result);
		assertEquals(1, result.size());

		result.clear();
		findMatchingURLs(baseDir, "[my-test-group:my-test-module]:/org/tinyjee/maven/dim/sh/highlight.js", result);
		assertEquals(1, result.size());

		assertEquals(dependencies, getModuleDependencies("my-test-group:my-test-module"));
		assertEquals(dependencies, getModuleDependencies("my-test-module"));

		try {
			result.clear();
			findMatchingURLs(baseDir, "[anything]:does-not-exist", result);
			fail("Expected IllegalArgumentException");
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().contains("[my-test-group:my-test-module]=" + baseDir.getAbsolutePath()));
		}
	}

	@Test
	public void testFindMatchingURLsSupportsClassPath() throws Exception {
		List<URL> result = new ArrayList<URL>();
		findMatchingURLs(baseDir, "classpath:/org/tinyjee/maven/dim/sh/highlight.js", result);
		assertFalse(result.isEmpty());
		assertNotNull(result.get(0).getContent());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFindMatchingURLsThrowsIAWhenNotFoundInClassPath() throws Exception {
		findMatchingURLs(baseDir, "classpath:/does/not/exist", new ArrayList<URL>());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testResolveClassFailsIfNotFound() throws Exception {
		resolveClass(baseDir, "does.not.Exist");
	}

	@Test
	public void testResolveClassFailsIfModuleIsNotFound() throws Exception {
		try {
			resolveClass(baseDir, "[non-existing-module]:org.tinyjee.maven.dim.spi.ResourceResolverTest");
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().contains("'org.tinyjee.maven.dim.spi.ResourceResolverTest'"));
			assertTrue(e.getMessage().contains("below the module base paths ('non-existing-module"));
		}
	}

	public void testResolveClass() throws Exception {
		resolveClass(baseDir, "org.tinyjee.maven.dim.spi.ResourceResolverTest");
	}

	@Test
	public void testFindSource() throws Exception {
		File expected = new File("src/site/resources/samples/table-template.apt.vm").getCanonicalFile();
		String[] sources = {
				"samples/table-template.apt.vm",
				"resources/samples/table-template.apt.vm",
				"src/site/resources/samples/table-template.apt.vm"
		};

		for (String source : sources) {
			URL url = findSource(new File("."), source);
			assertNotNull(url);
			assertEquals(expected, new File(url.toURI()).getCanonicalFile());
		}
	}

	@Test
	public void testSpecifiedLocationsMustBeBelowBase() throws Exception {
		final String fileName = "tä st";
		final File testFile = folder.newFile(fileName).getCanonicalFile();
		setProperty("org.tinyjee.maven.dim.project.siteDirectory", testFile.getParentFile().getAbsolutePath());

		try {
			findSource(new File("."), fileName);
			fail();
		} catch (IllegalArgumentException e) {
		}

		assertEquals(testFile, new File(findSource(folder.getRoot(), fileName).toURI()));
	}

	@Test
	public void testCanSpecifyLocationsOfStandardPaths() throws Exception {
		final File basePath = folder.getRoot();
		final String[] keys = {
				"org.tinyjee.maven.dim.project.siteDirectory",
				"org.tinyjee.maven.dim.project.sourceDirectory",
				"org.tinyjee.maven.dim.project.resourceDirectories",
				"org.tinyjee.maven.dim.project.testSourceDirectory",
				"org.tinyjee.maven.dim.project.testResourceDirectories",
				"org.tinyjee.maven.dim.project.targetDirectory",
		} ;

		for (String key : keys) {
			File expected = folder.newFolder(key).getCanonicalFile();
			setProperty(key, expected.getAbsolutePath());
			try {
				File expectedFile = new File(expected, "sample.txt");
				expectedFile.createNewFile();

				URL url = findSource(basePath, "sample.txt");
				assertEquals(expectedFile, new File(url.toURI()).getCanonicalFile());
			} finally {
				System.getProperties().remove(key);
			}
		}

	}
}
