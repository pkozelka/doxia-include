/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonMap;
import static org.junit.Assert.*;

/**
 * Tests some functions of the Codec.
 *
 * @author Juergen_Kellerer, 2010-09-07
 * @version 1.0
 */
public class SnippetExtractorTest {

	static final String EOL = System.getProperty("line.separator");
	static final String TEST_CONTENT = "Test Content Line 1" + EOL +
			"Line 2" + EOL +
			"Line 3 START SNIPPET: first" + EOL +
			"Line 4" + EOL +
			"Line 5 END SNIPPET: first" + EOL +
			"Line 6 START SNIPPET: multi" + EOL +
			"Line 7" + EOL +
			"Line 8 END SNIPPET: multi" + EOL +
			"Line 9 START SNIPPET: multi" + EOL +
			"Line 10" + EOL +
			"Line 11 END SNIPPET: multi" + EOL +
			"Line 12 START SNIPPET: empty" + EOL +
			"Line 13 END SNIPPET: empty" + EOL +
			"Line 14";

	SnippetExtractor extractor = SnippetExtractor.getInstance();

	@SuppressWarnings("unchecked")
	Source makeSource(Map parameters) throws Exception {
		return new FixedContentSource(TEST_CONTENT, parameters);
	}

	@Test
	public void testIndentDetectEmptyLinesAsMaxIndent() throws Exception {
		for (String line : new String[]{"", " ", " \t", "\t\t"})
			assertEquals(Integer.MAX_VALUE, SnippetExtractor.detectIndentWith(line));
	}

	@Test
	public void testIndentDetectTabsAndSpaces() throws Exception {
		assertEquals(4, SnippetExtractor.detectIndentWith("\ta"));
		assertEquals(4, SnippetExtractor.detectIndentWith("    a"));
		assertEquals(8, SnippetExtractor.detectIndentWith("\t    a"));
		assertEquals(8, SnippetExtractor.detectIndentWith("    \ta"));
	}

	@Test
	public void testUnIndentHandlesTabsAndSpaces() throws Exception {
		assertEquals("a", SnippetExtractor.unIndent("\ta", 4));
		assertEquals("a", SnippetExtractor.unIndent("    a", 4));
		assertEquals("a", SnippetExtractor.unIndent("\t    a", 8));
		assertEquals("a", SnippetExtractor.unIndent("    \ta", 8));
	}

	@Test
	public void testReadWriteWithoutSnippet() throws Exception {
		assertEquals(TEST_CONTENT, extractor.toString(extractor.selectSnippets(makeSource(null)), false));
	}

	@Test
	public void testEmptySingleSnippet() throws Exception {
		Map<Integer, List<String>> map = extractor.selectSnippets(makeSource(singletonMap("snippet", "#empty")));
		assertTrue(map.isEmpty());
	}

	@Test
	public void testNonExistingSnippet() throws Exception {
		Map<Integer, List<String>> map = extractor.selectSnippets(makeSource(singletonMap("snippet", "#does-not-exist")));
		assertTrue(map.isEmpty());
	}

	@Test
	public void testReadSingleSnippet() throws Exception {
		Map<Integer, List<String>> map = extractor.selectSnippets(makeSource(singletonMap("snippet", "#first")));
		doTestLine(4, map);
	}

	@Test
	public void testReadMultipleSnippets() throws Exception {
		Map<Integer, List<String>> map = extractor.selectSnippets(makeSource(singletonMap("snippet", "#multi")));
		for (int line : new int[]{7, 10})
			doTestLine(line, map);
	}

	@Test
	public void testCanGrowSnippets() throws Exception {
		Map<String, String> snippet = new HashMap<String, String>(singletonMap("snippet", "grep:Line 6"));
		snippet.put("snippet-grow-offset", "3");
		Map<Integer, List<String>> map = extractor.selectSnippets(makeSource(snippet));
		assertEquals(1, map.size());
		assertNotNull(map.get(3));
		assertTrue(map.get(3).get(0).startsWith("Line 3"));
		assertTrue(map.get(3).get(map.get(3).size() - 1).startsWith("Line 9"));
	}

	@Test
	public void testCanRemoveSnippetIds() throws Exception {
		SortedMap<Integer, List<String>> snippet = extractor.selectSnippets(makeSource(singletonMap("snippet", "!snippet-ids")));
		assertEquals(5, snippet.size());
		for (List<String> strings : snippet.values()) {
			for (String string : strings) assertFalse(string.contains("SNIPPET"));
		}
	}

	private void doTestLine(int line, Map<Integer, List<String>> map) {
		assertNotNull(map.get(line));
		assertEquals("Line " + line, map.get(line).get(0));
		assertEquals(1, map.get(line).size());
	}

	@Test
	public void testCanParseVariousExpressions() throws Exception {
		Object[][] tests = {
				{"#id value1,#idvalue2   ,  #id,,.-value3   , !#idvalue4",
						asList("#id value1", "#idvalue2", "#id,,.-value3", "!#idvalue4")},
				{"snippet-ids", asList("snippet-ids")},
				{"!snippet-ids", asList("!snippet-ids")},
				{"re:.+", asList("re:.+")},
		};
		for (Object[] test : tests) {
			String expression = (String) test[0];
			assertEquals(expression + "\n", test[1], asList(extractor.splitExpression(expression)));
		}
	}

	/**
	 * Verifies that the problem reported in https://sourceforge.net/p/doxia-include/tickets/11/ is fixed.
	 */
	@Test
	public void testRegressionForTicket11() throws Exception {
		final String content = "class Outer {\n" +
				"  class Test {\n" +
				"\n" +
				"  }\n" +
				"}\n" +
				"// Some trailing comment\n";
		FixedContentSource source = new FixedContentSource(content, singletonMap("snippet", (Object) "tb:Test,!tb:Outer"));
		assertEquals(0, extractor.selectSnippets(source).size());

		// Check if other cases still work correctly.
		source = new FixedContentSource(content, singletonMap("snippet", (Object) "tb:Test"));
		assertEquals("class Test {", extractor.selectSnippets(source).get(2).get(0).trim());

		source = new FixedContentSource(content, singletonMap("snippet", (Object) "!tb:Outer"));
		assertEquals("// Some trailing comment", extractor.selectSnippets(source).get(6).get(0).trim());
	}
}
