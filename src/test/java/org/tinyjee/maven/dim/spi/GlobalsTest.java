/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import org.apache.maven.doxia.index.IndexingSink;
import org.apache.maven.doxia.markup.HtmlMarkup;
import org.apache.maven.doxia.sink.Sink;
import org.apache.maven.doxia.sink.XhtmlBaseSink;
import org.codehaus.plexus.util.IOUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URLEncoder;

import static org.junit.Assert.*;

/**
 * Smoke test on Globals.
 *
 * @author Juergen_Kellerer, 2010-09-08
 */
public class GlobalsTest {

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	Globals instance;
	StringWriter output = new StringWriter();

	@Before
	public void setUp() throws Exception {
		temporaryFolder.create();
		GlobalsStack.initializeGlobals(null, null, new XhtmlBaseSink(output), null, null, temporaryFolder.getRoot());
		instance = Globals.getInstance();
	}

	@After
	public void tearDown() throws Exception {
		GlobalsStack.removeGlobals();
	}

	private File newTestFile(String fileName) throws IOException {
		File file = temporaryFolder.newFile(fileName);
		assertEquals(0, file.length());
		return file;
	}

	private void assertAttachedContentIsValid(File file, String link, String expectedContent) throws IOException {
		if (link != null)
			assertEquals(URLEncoder.encode(file.getName(), "UTF-8").replace("+", "%20"), link);
		assertEquals(expectedContent, IOUtil.toString(new FileInputStream(file)));
	}

	private void assertOutputContains(String... tokens) {
		for (String token : tokens) assertTrue(token, output.getBuffer().indexOf(token) != -1);
	}

	@Test
	public void testLoggerIsAlwaysAvailable() throws Exception {
		assertNotNull(Globals.getLog());
		assertNotNull(instance.getLogger());
	}

	@Test
	public void testCanResolveLocalPath() throws Exception {
		assertEquals("pom.xml", instance.resolveLocalPath("pom.xml").getName());
	}

	@Test
	public void testCanFetchContent() throws Exception {
		assertTrue(instance.fetchText("pom.xml").contains("tinyjee"));
		assertTrue(new String(instance.fetch("pom.xml")).contains("tinyjee"));
	}

	@Test
	public void testCanAttachContent() throws Exception {
		String fileName = "täst file.txt";
		File file = newTestFile(fileName);
		String link = instance.attachContent(fileName, "Test Entry");
		assertNotNull(link);
		assertAttachedContentIsValid(file, link, "Test Entry");
	}

	@Test
	public void testCanAttachContentWithComplexFolderHierarchy() throws Exception {
		final File attachedIncludes = temporaryFolder.newFolder("attached-includes");
		instance = GlobalsStack.reInitializeGlobals(null, null, new XhtmlBaseSink(output), null, null, attachedIncludes);

		String fileName = "test.txt";
		String link = instance.attachContent(fileName, "Test Entry");
		assertEquals("attached-includes/" + fileName, link);

		// TODO: Test the case that the target file is in a sub-folder as described in
		// TODO: https://sourceforge.net/p/doxia-include/tickets/14/
	}

	@Test
	public void testCanAttachCSS() throws Exception {
		String fileName = "täst file.css";
		File file = newTestFile(fileName);
		assertTrue(instance.attachCss(fileName, "Test Entry"));
		assertOutputContains("<style", "@import", URLEncoder.encode(file.getName(), "UTF-8").replace("+", "%20"));
		assertAttachedContentIsValid(file, null, "Test Entry");
	}

	@Test
	public void testCanAttachInlinedCSS() throws Exception {
		assertTrue(instance.attachCss("", "Test Entry"));
		assertOutputContains("<style", "Test Entry");
	}

	@Test
	public void testCanAttachJs() throws Exception {
		String fileName = "täst file.js";
		File file = newTestFile(fileName);
		assertTrue(instance.attachJs(fileName, "Test Entry"));
		assertOutputContains("<script", "language=\"javascript\"", "type=\"text/javascript\"",
				"src=\"" + URLEncoder.encode(file.getName(), "UTF-8").replace("+", "%20") + '\"',
				"</script>");
		assertAttachedContentIsValid(file, null, "Test Entry");
	}

	@Test
	public void testCanAttachInlinedJs() throws Exception {
		assertTrue(instance.attachJs("", "Test Entry"));
		assertOutputContains("<script", "Test Entry");
	}

	@Test
	public void testCanAttachContentUsingAnURLAsFileName() throws Exception {
		File targetFile, basedir = instance.getAttachedIncludesPath().getCanonicalFile();

		targetFile = Globals.createTargetFile(basedir,
				"file:/c:/Projects/OS/doxia-include/target/doxia-include-macro-1.1-SNAPSHOT.jar!/META-INF/syntax-highlighter.LICENSE");
		assertEquals(new File(basedir, "META-INF/syntax-highlighter.LICENSE").getCanonicalFile(), targetFile);

		targetFile = Globals.createTargetFile(basedir, "file:/c:/META-INF/syntax-highlighter.LICENSE");
		assertEquals(new File(basedir, "META-INF/syntax-highlighter.LICENSE").getCanonicalFile(), targetFile);

		targetFile = Globals.createTargetFile(basedir, "http://somehost/test/path/file.xy?q=134&x=1#342");
		assertEquals(new File(basedir, "test/path/file.xy").getCanonicalFile(), targetFile);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCannotWriteFilesOutsideOfIncludesPath() throws Exception {
		File basedir = instance.getAttachedIncludesPath().getCanonicalFile();
		Globals.createTargetFile(basedir, "../test.txt");
	}

	@Test
	public void testIsHTMLCapableSink() throws Exception {
		Sink[] tests = {new XhtmlBaseSink(new StringWriter()), new IndexingSink(null)};
		for (Sink sink : tests) {
			GlobalsStack.reInitializeGlobals(null, null, sink, null, null, temporaryFolder.getRoot());
			assertEquals(sink instanceof HtmlMarkup, GlobalsStack.getGlobals().isHTMLCapableSink());
		}
	}
}
