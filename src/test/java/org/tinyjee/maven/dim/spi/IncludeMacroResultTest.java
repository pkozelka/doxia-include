/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import org.apache.maven.doxia.macro.manager.MacroManager;
import org.codehaus.plexus.PlexusContainer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.tinyjee.maven.dim.AbstractIncludeMacroTest;
import org.tinyjee.maven.dim.IncludeMacro;

import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.singletonMap;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests IncludeMacroResult.
 *
 * @author Juergen_Kellerer, 2012-03-11
 */
public class IncludeMacroResultTest extends AbstractIncludeMacroTest {

	Map<String, Object> parameters = new HashMap<String, Object>(singletonMap("source", (Object) "samples.SampleClass"));

	@Test
	public void testGetText() throws Exception {
		IncludeMacroResult result = IncludeMacroResult.createInstance(container, parameters);
		assertResultIsValid(result);
	}

	@Test
	public void testGetTextFromGlobals() throws Exception {
		IncludeMacroResult result = Globals.getInstance().callIncludeMacro(parameters, true);
		assertEquals(1, parameters.size());
		assertResultIsValid(result);
	}

	private void assertResultIsValid(IncludeMacroResult result) {
		assertNotNull(result.getUrl());
		assertNotNull(result.getContent());
		assertTrue(result.getText(), result.getText().contains("// START-SNIPPET: ClassMethod"));
	}
}
