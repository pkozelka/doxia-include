/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sh;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * TODO: Create Description.
 *
 * @author Juergen_Kellerer, 2010-09-06
 * @version 1.0
 */
public class BundledBrushesResolverTest {

	BundledBrushesResolver resolver = new BundledBrushesResolver();

	@Test
	public void testResolveBrushesIsNotEmpty() throws Exception {
		assertNotNull(resolver.resolveBrushes());
		assertFalse(resolver.resolveBrushes().isEmpty());
	}

	@Test
	public void testDetectBrushAliasByExtension() throws Exception {
		assertEquals("java", resolver.detectAliasByExtension("/path/to/somefile.java"));
		assertEquals("java", resolver.detectAliasByExtension("/path/to/somefile.JaVa"));
		assertEquals("js", resolver.detectAliasByExtension("somefile.js"));
		assertEquals("html/js", resolver.detectAliasByExtension("somefile.js.html"));
		assertEquals("html/java", resolver.detectAliasByExtension("somefile.jsp"));
		assertEquals(null, resolver.detectAliasByExtension("somefile"));
		assertEquals(null, resolver.detectAliasByExtension("somefile.something"));
	}

	@Test
	public void testDetectBrushAliasByContent() {
		Map<Integer, List<String>> content = new HashMap<Integer, List<String>>();

		content.put(1, Arrays.asList("#!/bin/bash"));
		assertEquals("bash", resolver.detectAliasByContent(content));

		content.put(1, Arrays.asList("   ", "#!/bin/bash  "));
		assertEquals("bash", resolver.detectAliasByContent(content));

		content.put(1, Arrays.asList("   #!/bin/bash"));
		assertEquals(null, resolver.detectAliasByContent(content));
	}

	@Test
	public void testDetectBrushAliasByContentDoesntBreakOnEmptyContent() {
		assertEquals(null, resolver.detectAliasByContent(null));
		assertEquals(null, resolver.detectAliasByContent(new HashMap<Integer, List<String>>()));
	}
}
