/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sh;

import org.apache.maven.doxia.index.IndexingSink;
import org.apache.maven.doxia.sink.XhtmlBaseSink;
import org.junit.Test;
import org.tinyjee.maven.dim.spi.FixedContentSource;
import org.tinyjee.maven.dim.spi.SnippetExtractor;
import org.tinyjee.maven.dim.spi.Source;
import org.tinyjee.maven.dim.sources.SourceClassAdapter;

import java.io.StringWriter;
import java.net.URL;
import java.util.*;

import static java.util.Collections.singletonMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_HIGHLIGHT;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_HIGHLIGHT_LINES;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_HIGHLIGHT_THEME;
import static org.tinyjee.maven.dim.sh.CodeHighlighter.LB;

/**
 * Smoke test on the CodeHighlighter.
 *
 * @author Juergen_Kellerer, 2010-09-07
 * @version 1.0
 */
public class CodeHighlighterTest {

	CodeHighlighter highlighter = new CodeHighlighter();
	StringWriter output = new StringWriter();
	HashMap<String, String> parameters = new HashMap<String, String>();

	public static class TestContent extends HashMap<String, Object> {
		{
			try {
				put("content", getContent(true));
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	static Map<Integer, List<String>> getContent(boolean fullContent) throws Exception {
		String content = "Test Content in a single line.\n" +
				"START SNIPPET: a\n" +
				"A1 content\n" +
				"END SNIPPET: a\n" +
				"START SNIPPET: a\n" +
				"A2 content\n" +
				"END SNIPPET: a\n"+
				"START SNIPPET: a\n" +
				"A3 content\n" +
				"END SNIPPET: a\n";
		SnippetExtractor extractor = SnippetExtractor.getInstance();
		return extractor.selectSnippets(new FixedContentSource(content, fullContent ? null : singletonMap("snippet", (Object) "#a")));
	}

	Source source = new SourceClassAdapter(null, null, "org.tinyjee.maven.dim.sh.CodeHighlighterTest$TestContent");

	@Test
	public void testExecuteContentWithoutURLAndTypeMapping() throws Exception {
		SortedMap<Integer, List<String>> snippets = SnippetExtractor.getInstance().selectSnippets(source);
		highlighter.execute(source.getUrl(), parameters, snippets, new XhtmlBaseSink(output));
		assertTrue(output.toString().contains("plain plain"));
	}

	@Test
	public void testExecuteWithMultipleSections() throws Exception {
		highlighter.execute(source.getUrl(), parameters, getContent(false), new XhtmlBaseSink(output));
		String content = output.getBuffer().toString();
		assertTrue(content.contains("</style>" + LB + "<div class=\"syntaxhighlighter "));
		assertTrue(content.endsWith("</div>"));

		assertTrue(content.contains("A1 content"));
		assertTrue(content.contains("A2 content"));
		assertTrue(content.contains("number3"));
		assertFalse(content.contains("number4"));
	}

	@Test
	public void testCanHighlightLinesUsingLegacyProperty() throws Exception {
		parameters.put(PARAM_HIGHLIGHT, "3,9");
		highlighter.execute(source.getUrl(), parameters, getContent(false), new XhtmlBaseSink(output));
		String content = output.getBuffer().toString();

		assertTrue(content.contains("number3 index0 alt2 highlighted"));
		assertTrue(content.contains("number9 index6 alt2 highlighted"));
	}

	@Test
	public void testCanHighlightLines() throws Exception {
		parameters.put(PARAM_HIGHLIGHT_LINES, "grep:a1, grep:a3");
		highlighter.execute(source.getUrl(), parameters, getContent(false), new XhtmlBaseSink(output));
		String content = output.getBuffer().toString();

		assertTrue(content.contains("number3 index0 alt2 highlighted"));
		assertTrue(content.contains("number9 index6 alt2 highlighted"));
	}

	@Test
	public void testExecuteContentCanUseNonDefaultTheme() throws Exception {
		SortedMap<Integer, List<String>> snippets = SnippetExtractor.getInstance().selectSnippets(source);
		highlighter.execute(source.getUrl(), singletonMap(PARAM_HIGHLIGHT_THEME, "eclipse"), snippets, new XhtmlBaseSink(output));
		assertTrue(output.toString().contains("color: #3f7f7f !important;"));
	}

	@Test
	public void testIsCodeSurroundedByHTMLDoesntBreakWhenAllIsNull() throws Exception {
		highlighter.isCodeSurroundedByHTML(null, null, null);
	}

	@Test
	public void testIsCodeSurroundedByHTML() throws Exception {
		assertTrue(highlighter.isCodeSurroundedByHTML(new URL("http:///path/test.jsp"), null, null));
		assertFalse(highlighter.isCodeSurroundedByHTML(new URL("http:///path/test.java"), null, null));
		assertTrue(highlighter.isCodeSurroundedByHTML(new URL("http:///path/test.java"),
				null, singletonMap("highlight-type", "html/plain")));
	}

	@Test
	public void testRemoveLinesPreservesNonBreakingSpace() throws Exception {
		//Ticket: https://sourceforge.net/p/doxia-include/tickets/12/
		String expected = "<div>A&nbsp;B</div>", content = highlighter.removeLines(expected, Collections.<Integer>emptySet());
		assertEquals(expected, content);
	}
}
