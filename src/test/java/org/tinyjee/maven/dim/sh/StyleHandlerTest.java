/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sh;

import org.codehaus.plexus.util.StringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

import static org.junit.Assert.*;

/**
 * Smoke tests the StyleHandler.
 *
 * @author Juergen_Kellerer, 2010-09-03
 * @version 1.0
 */
public class StyleHandlerTest {

	StyleHandler handler = new StyleHandler();
	List<String> expectedCss = Arrays.asList("css/shCoreDefault.css");

	@Test
	public void testGetCss() throws Exception {
		String css = handler.getCSSContent(null);
		assertNotNull(css);
		assertFalse(StringUtils.isEmpty(css));
	}

	@Test
	public void testHandlerHasSources() {
		assertEquals(new TreeSet<String>(expectedCss), new TreeSet<String>(handler.sources.keySet()));
		for (byte[] bytes : handler.sources.values()) {
			assertNotNull(bytes);
			assertTrue(bytes.length > 0);
		}
	}
}
