/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sh;

import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Smoke test all included default brushes.
 *
 * @author Juergen_Kellerer, 2010-09-03
 * @version 1.0
 */
public class HighlighterScriptFacadeTest {

	static final HighlighterThreadLocal highlighters = new HighlighterThreadLocal();

	HighlighterScriptFacade facade = highlighters.getHighlighter();

	@Test
	public void testCanUseCustomParams() throws Exception {
		Map<String, String> params = new HashMap<String, String>();
		params.put("title", "'Some Title'");
		params.put("smart-tabs", "false");
		params.put("highlight", "[1, 5, 8]");

		String result = facade.highlightToHTML("java", "/* some comment*/", params);

		assertTrue(result.contains("Some Title"));
	}

	@Test
	public void testBundledBrushes() throws Exception {
		String[][] tests = {
				{"actionscript3", "public class Test { /* some comment */ };"},
				{"bash", "#!/bin/bash\nif [ $X -lt $Y ]"},
				{"cmd", "@echo off"},
				{"coldfusion", "<cfcache action=\"cache\" timespan=\"#CreateTimeSpan(0,1,0,0)#\"/>"},
				{"css", ".body {margin:0;}"},
				{"csharp", "public class Test { /* some comment */ };"},
				{"cpp", "class Test { /* some comment */ };"},

				{"delphi", "unit OmniXMLShared;"},
				{"diff", "--- ...\\index.php-rev104.svn000.tmp.php"},
				{"erlang", "update(File, In, Out) ->"},
				{"groovy", "def Test = {};"},
				{"js", "var Test = {};"},
				{"java", "public class Test { /* some comment */ };"},
				{"javafx", "Stage {};"},

				{"pl", "#!/usr/bin/perl\n\nuse strict;"},
				{"php", "<?php\nif (!defined('MEDIAWIKI'))\n  exit(1);"},
				{"plain", "Plain Text"},
				{"powershell", "$readStream = new-object System.IO.StreamReader $requestStream"},
				{"py", "class myParser(SAX_XPValParser):"},
				{"ruby", "class Post < ActiveRecord::Base"},

				{"scala", "object Branch { }"},
				{"sql", "SELECT COUNT(*) FROM TEST"},
				{"vb", "Public Shared Function AddAppSetting()"},
				{"html", "<html><body></body></html>"},
				{"xhtml", "<html><body></body></html>"},
				{"xml", "<html><body></body></html>"},
		};

		for (String[] test : tests) {
			String brushName = test[0], code = test[1], result = null;
			try {
				result = facade.highlightToHTML(brushName, code, null);
				System.out.println(brushName + "|" + code + "|" + result);
			} catch (RuntimeException e) {
				e.printStackTrace();
				fail("Failed to hightlight '" + code + "' using brush '" + brushName + "'");
			}

			assertNotNull(result);
			assertFalse(result.equals(code));
			assertTrue(result.length() > code.length());
		}
	}

	@Test
	public void testCanHandleNestedJSPInHTML() throws Exception {
		Map<String, String> params = new HashMap<String, String>();
		params.put("html-script", "true");
		params.put("highlight", "[1]");
		params.put("pad-line-numbers", "true");
		
		String code = "<%@ page contentType=\"text/html;charset=WINDOWS-1252\"%>\n" +
				"<HTML>\n" +
				"    <BODY>\n" +
				"    <% out.println(\" Hello World\"); %> !\n" +
				"     </BODY>\n" +
				"</HTML>";

		String result = facade.highlightToHTML("java", code, params);
		assertNotNull(result);
		assertTrue(result.contains("syntaxhighlighter  htmlscript"));
		System.out.println("htmlscript|" + code + "|" + result);
	}
}
