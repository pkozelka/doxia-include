/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sh;

import org.junit.Test;

import java.lang.ref.SoftReference;

import static org.junit.Assert.assertNotNull;

/**
 * Smoke tests the ThreadLocal.
 *
 * @author Juergen_Kellerer, 2010-09-08
 * @version 1.0
 */
public class HighlighterThreadLocalTest {

	static HighlighterThreadLocal threadLocal = new HighlighterThreadLocal();

	@Test
	public void testGetHighlighter() throws Exception {
		assertNotNull(threadLocal.getHighlighter());
	}

	@Test
	public void testGetHighlighterCanWorkHandleCleanedReference() throws Exception {
		assertNotNull(threadLocal.getHighlighter());
		threadLocal.set(null);
		assertNotNull(threadLocal.getHighlighter());
		threadLocal.set(new SoftReference<HighlighterScriptFacade>(null));
		assertNotNull(threadLocal.getHighlighter());
	}
}
