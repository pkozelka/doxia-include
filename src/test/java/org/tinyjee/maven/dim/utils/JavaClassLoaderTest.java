/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaMethod;
import com.thoughtworks.qdox.model.JavaSource;
import com.thoughtworks.qdox.model.Type;
import org.codehaus.plexus.util.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Tests the shared java class loader.
 *
 * @author Juergen_Kellerer, 2011-10-11
 */
public class JavaClassLoaderTest {

	JavaClassLoader classLoader = JavaClassLoader.getInstance();
	File sourceFolder = new File("src/main/java"), testSourceFolder = new File("src/test/java");

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Before
	@After
	public void initializeAndCleanup() throws Exception {
		classLoader.clear();
	}

	@Test
	public void testAddSourceTree() throws Exception {
		classLoader.addSourceTree(testSourceFolder);
		assertTrue(classLoader.getSources().length > 1);
		assertTrue(classLoader.getClasses().size() > 1);
		assertTrue(classLoader.getPackages().size() > 1);
	}

	@Test
	public void testAddSource() throws Exception {
		JavaSource javaSource = classLoader.addSource(sourceFolder, "org/tinyjee/maven/dim/utils/JavaClassLoader.java");
		assertEquals("org.tinyjee.maven.dim.utils.JavaClassLoader", javaSource.getClasses()[0].getFullyQualifiedName());
		assertTrue(classLoader.getSources().length > 1);
	}

	@Test
	public void testCanReadStarImportsViaOrdinaryClassLoader() throws Exception {
		JavaSource javaSource = classLoader.addSource(testSourceFolder, "org/tinyjee/maven/dim/utils/ImportStarLoadingTestClass.java");
		final JavaClass interfaceClass = javaSource.getClasses()[0];
		assertEquals("org.tinyjee.maven.dim.utils.ImportStarLoadingTestClass", interfaceClass.getFullyQualifiedName());
		final JavaMethod method = interfaceClass.getMethod("getGlobals", new Type[0], false);
		assertEquals("org.tinyjee.maven.dim.spi.Globals", method.getReturnType().getFullyQualifiedName());
	}

	@Test
	public void testLoaderRecognizesChangedFiles() throws Exception {
		doTestLoaderRecognizesChangedFiles(null, true);
	}

	@Test
	public void testLoaderRecognizesChangedFilesWithPackage() throws Exception {
		doTestLoaderRecognizesChangedFiles("tinyjee", true);
	}

	@Test
	public void testLoaderRecognizesChangedFile() throws Exception {
		doTestLoaderRecognizesChangedFiles(null, false);
	}

	@Test
	public void testLoaderRecognizesChangedFileWithPackage() throws Exception {
		doTestLoaderRecognizesChangedFiles("tinyjee", false);
	}

	private void doTestLoaderRecognizesChangedFiles(String packageName, boolean loadViaSourceTree) throws IOException {
		final String sourceFolder = "T ä s t";
		temporaryFolder.newFolder(sourceFolder);

		if (packageName != null) temporaryFolder.newFolder(sourceFolder + '/' + packageName);
		final File file = temporaryFolder.newFile(sourceFolder + (packageName == null ? "" : '/' + packageName) + "/Test.java");

		writeTestClass(file, packageName, "dummy1");

		// Setting LM to the past to ensure the upcoming change gets recognized.
		assertTrue(file.setLastModified(System.currentTimeMillis() - 5000));

		classLoader.addSource(file.toURI().toURL(), !loadViaSourceTree);

		assertEquals(1, classLoader.getSources().length);
		assertEquals("dummy1", classLoader.getClasses().first().getMethods()[0].getName());

		classLoader.scheduleUpToDateCheck();

		writeTestClass(file, packageName, "dummy2");

		assertEquals(1, classLoader.getSources().length);
		assertEquals("dummy2", classLoader.getClasses().first().getMethods()[0].getName());
	}

	private static void writeTestClass(File file, String packageName, String methodName) throws IOException {
		FileUtils.fileWrite(file.toString(), (packageName == null ? "" : "package " + packageName + "; ") +
				"public class Test { public void " + methodName + "() {} }");
	}
}
