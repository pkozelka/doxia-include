/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.junit.Test;

import java.io.File;
import java.util.Map;
import java.util.concurrent.Callable;

import static org.junit.Assert.assertEquals;

/**
 * Tests ClassUtils.
 *
 * @author juergen kellerer, 2012-05-28
 */
public class ClassUtilsTest {

	private File basePath = new File(".");

	public static class ClassToReturn {
		private File path;

		public ClassToReturn(File path, Map params) {
			this.path = path;
		}
	}

	private abstract static class NoInstance {
		private static String getValue() {
			return "a";
		}
	}

	public ClassToReturn getClassToReturn(File path, Map params) {
		return new ClassToReturn(new File("getClassToReturn2"), null);
	}

	ClassToReturn getClassToReturn() {
		return new ClassToReturn(new File("getClassToReturn"), null);
	}

	private static ClassToReturn getStaticClassToReturn() {
		return new ClassToReturn(new File("getStaticClassToReturn"), null);
	}

	private Callable<ClassToReturn> getWithParams(File path, Map params) {
		return new Callable<ClassToReturn>() {
			public ClassToReturn call() throws Exception {
				return new ClassToReturn(new File("getWithParams"), null);
			}
		};
	}

	private ClassToReturn doInvoke(String expression) {
		return (ClassToReturn) ClassUtils.invoke(expression, basePath, null);
	}

	@Test(expected = RuntimeException.class)
	public void testInvokeFailsOnInvalidSubclassExpression() throws Exception {
		doInvoke("org.tinyjee.maven.dim.utils.ClassUtilsTest.ClassToReturn");
	}

	@Test
	public void testInvokeWithClassReference() throws Exception {
		ClassToReturn instance = doInvoke("org.tinyjee.maven.dim.utils.ClassUtilsTest$ClassToReturn");
		assertEquals(basePath, instance.path);
	}

	@Test
	public void testInvokeWithStaticMethodReference() throws Exception {
		ClassToReturn instance = doInvoke("org.tinyjee.maven.dim.utils.ClassUtilsTest.getStaticClassToReturn()");
		assertEquals(new File("getStaticClassToReturn"), instance.path);
	}

	@Test
	public void testInvokeWithMethodReference() throws Exception {
		ClassToReturn instance = doInvoke("org.tinyjee.maven.dim.utils.ClassUtilsTest.getClassToReturn()");
		assertEquals(new File("getClassToReturn"), instance.path);
	}

	@Test
	public void testInvokeWithMethodReferenceAndParams() throws Exception {
		ClassToReturn instance = doInvoke("org.tinyjee.maven.dim.utils.ClassUtilsTest.getWithParams().call()");
		assertEquals(new File("getWithParams"), instance.path);
	}

	@Test
	public void testCanCallStaticMethodInAbstractClass() throws Exception {
		assertEquals("a", ClassUtils.invoke("org.tinyjee.maven.dim.utils.ClassUtilsTest$NoInstance.getValue()", basePath, null));
	}
}
