/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Tests sorting of various strings.
 *
 * @author Juergen_Kellerer, 2012-06-03
 */
public class NumberAwareStringComparatorTest {

	final NumberAwareStringComparator comparator = new NumberAwareStringComparator();

	String[] strings = {
			null,
			"  1  ",
			" 1.0 ",
			"1.0.0",
			"1.0.0.1000",
			"1.0.0.1010",
			"2.0.0.1000",
			"4.1000",
			"6.1-1311",
			"6(DE)",
			"7?134234234",
			"14-SNAPSHOT",
			"2000",
			"2010",
			"path/file",
			"path/to/File1",
			"path/to/File  2.ext",
			"path/to/filE 10",
	};

	@Test
	public void testCanSortVersionsAsExpected() throws Exception {
		for (int i = 0; i < 100; i++) {
			List<String> testStrings = new ArrayList<String>(Arrays.asList(strings));
			Collections.shuffle(testStrings);
			Collections.sort(testStrings, comparator);
			assertArrayEquals(strings, testStrings.toArray(new String[testStrings.size()]));
		}
	}

	@Test
	public void testComparesSameVersionEqual() throws Exception {
		for (String string : strings)
			assertEquals(0, comparator.compare(string, string));
	}
}
