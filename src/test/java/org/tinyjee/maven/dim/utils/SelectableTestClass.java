/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import java.util.ArrayList;

/**
 * Is used to test class matching.
 *
 * @author Juergen_Kellerer, 2011-10-11
 */
@SuppressWarnings("none")
public class SelectableTestClass<E> extends ArrayList<E> implements Comparable<SelectableTestClass<E>> {

	private static final long serialVersionUID = -1107829194242970196L;

	public SelectableTestClass() {
	}

	private void dummyMethod() {

	}

	public int compareTo(SelectableTestClass<E> o) {
		return 0;
	}

	@Override
	public Object clone() {
		return super.clone();
	}

	private static class SelectableInner implements Cloneable {
		public void innerMethod() {
		}

		@Override
		protected Object clone() throws CloneNotSupportedException {
			return super.clone();
		}
	}
}
