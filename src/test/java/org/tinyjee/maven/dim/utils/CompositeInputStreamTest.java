/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.junit.Test;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertEquals;

/**
 * Test for CompositeInputStream.
 *
 * @author Juergen_Kellerer, 2011-10-27
 */
public class CompositeInputStreamTest {

	final CompositeInputStream inputStream = new CompositeInputStream(
			new ByteArrayInputStream("".getBytes()),
			new ByteArrayInputStream("a".getBytes()),
			new ByteArrayInputStream("bb".getBytes()),
			new ByteArrayInputStream("ccc".getBytes()),
			new ByteArrayInputStream("".getBytes()));

	@Test
	public void testRead() throws Exception {
		for (byte expected : "abbccc".getBytes()) {
			byte actual = (byte) (inputStream.read() & 0xff);
			assertEquals((char) expected + " != " + (char) actual, expected, actual);
		}
		inputStream.close();
	}

	@Test
	public void testReadWithBuffer() throws Exception {
		int r = 0, count = 0;
		byte[] buffer = new byte[128];
		while ((r = inputStream.read(buffer, count, buffer.length - count)) != -1) count += r;
		inputStream.close();

		assertEquals("abbccc", new String(buffer, 0, count));
	}

	@Test
	public void testClose() throws Exception {
		inputStream.close();
		assertEquals(-1, inputStream.read());
	}
}
