/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.*;

/**
 * Tests PrintableMap.
 *
 * @author Juergen_Kellerer, 2012-05-12
 */
public class PrintableMapTest {

	private Object lastLoggedKey;
	private PrintableMap<String, String> map = new PrintableMap<String, String>("Test") {
		@Override
		void logMissingKeyAccess(Object key) {
			lastLoggedKey = key;
			super.logMissingKeyAccess(key);
		}
	};

	@Before
	public void setUp() throws Exception {
		map.put("1", "v1");
		map.put("2", "v2");
		assertNull(lastLoggedKey);
	}

	@Test
	public void testGet() throws Exception {
		assertEquals("v1", map.get("1"));
		assertEquals("v2", map.get("2"));
		assertNull(map.get("something-else"));
		assertEquals("something-else", lastLoggedKey);
	}

	@Test
	public void testGetWithDefaultValue() throws Exception {
		assertEquals("v2", map.get("2", "x"));
		assertEquals("x", map.get("something-else", "x"));
		assertNull(lastLoggedKey);
	}

	@Test
	public void testGetContentAsString() throws Exception {
		final String content = map.getContentAsString();
		assertTrue(content.contains("1=v1") && content.contains("2=v2"));
	}

	@Test
	public void testIterator() throws Exception {
		Iterator i1 = map.values().iterator(), i2 = map.iterator();
		for (; i1.hasNext() && i2.hasNext(); )
			assertEquals(i1.next(), i2.next());
		assertEquals(i1.hasNext(), i2.hasNext());
	}
}
