/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.tinyjee.maven.dim.utils.AbstractPositioningDocumentBuilder.KEY_COLUMN_NUMBER;
import static org.tinyjee.maven.dim.utils.AbstractPositioningDocumentBuilder.KEY_LINE_NUMBER;

/**
 * Tests that the parsed documents have line numbers set and the general location is increasing when iterating elements.
 *
 * @author Juergen_Kellerer, 2011-10-12
 */
public class PositioningDocumentBuilderTest extends AbstractPositioningDocumentBuilderTest {

	boolean namespaceAware;

	@Override
	protected AbstractPositioningDocumentBuilder createBuilder() {
		return new PositioningDocumentBuilder(namespaceAware);
	}

	@Test
	public void testParseDocument() throws Exception {
		namespaceAware = false;
		doSetUp("pom.xml");
		assertNodesHaveIncreasingLineAndColumnNumbers();
	}

	@Test
	public void testParseDocumentNamespaceAware() throws Exception {
		namespaceAware = true;
		doSetUp("pom.xml");
		assertNodesHaveIncreasingLineAndColumnNumbers();
	}
}
