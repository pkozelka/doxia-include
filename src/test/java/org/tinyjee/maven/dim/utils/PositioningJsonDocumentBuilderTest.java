/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests JSON to XML transcoding using XPath queries.
 *
 * @author Juergen_Kellerer, 2011-10-13
 */
public class PositioningJsonDocumentBuilderTest extends AbstractPositioningDocumentBuilderTest {

	@Override
	protected AbstractPositioningDocumentBuilder createBuilder() {
		return new PositioningJsonDocumentBuilder();
	}

	@Before
	public void setUp() throws Exception {
		doSetUp("src/test/java/samples/Sample.json");
	}

	@Test
	public void testDocumentIsParsed() throws Exception {
		System.out.println(evaluator.serialize(document));
		assertNotNull(document);

		assertNodesHaveIncreasingLineAndColumnNumbers();
	}

	@Test
	public void testCanMatchInnerObject() throws Exception {
		List<Node> nodes = evaluator.findNodes("/json/name/*");
		assertEquals(2, nodes.size());
		assertEquals("Joe", nodes.get(0).getTextContent());
		assertEquals("Sixpack", nodes.get(1).getTextContent());
	}

	@Test
	public void testCanMatchArray() throws Exception {
		List<Node> nodes = evaluator.findNodes("//flags");
		assertEquals(4, nodes.size());
	}

	@Test
	public void testCanMatchObjectInArray() throws Exception {
		List<Node> nodes = evaluator.findNodes("//flags/composite");
		assertEquals(1, nodes.size());
		assertEquals("987", nodes.get(0).getTextContent());
	}

	@Test
	public void testCanMatchArrayInArray() throws Exception {
		List<Node> nodes = evaluator.findNodes("//flags/inner_flags");
		assertEquals(3, nodes.size());
	}
}
