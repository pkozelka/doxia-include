/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.io.File;

import static org.junit.Assert.*;

/**
 * Tests the XPathEvaluator implementation.
 *
 * @author Juergen_Kellerer, 2011-10-12
 */
public class XPathEvaluatorImplementationTest {

	Document document;
	boolean namespaceAware;
	XPathEvaluator evaluator;

	@Before
	public void setUp() throws Exception {
		AbstractPositioningDocumentBuilder builder = new PositioningDocumentBuilder(namespaceAware);
		document = builder.parse(new File("pom.xml").toURI().toURL());
		evaluator = new XPathEvaluatorImplementation(document);
	}

	@Test
	public void testSerialize() throws Exception {
		String content = evaluator.serialize(document);
		assertTrue(content, content.contains("<groupId>org.tinyjee.dim"));
	}

	@Test
	public void testSerializeWithLineWidth() throws Exception {
		String input = "" +
				"<clients page=\"1\" pageSize=\"100\" resultSize=\"1\" resultType=\"Ok\"> \n" +
				"    <client> \n" +
				"        <blockedCount>15</blockedCount> \n" +
				"        <id machineId=\"368389db-3dbd-42f4-9ec3-296585381a13\" userId=\"ea380afb-b0f0-4146-9d52-168411dac5ab\"/> \n" +
				"        <details agentVersion=\"1.0.0\" clientState=\"Connected\" machineArchitecture=\"native\" machineName=\"jd-host-01\" osArchitecture=\"amd64\" osLocale=\"en_US\" osName=\"Windows NT\" osPatchVersion=\"13.48.1\" osVersion=\"6.1\" pushNotificationListenerPort=\"0\" timeZoneDSTOffsetMinutes=\"0\" timeZoneOffsetMinutes=\"0\" userName=\"John Doe\"> \n" +
				"            <ipAddress>192.168.2.3</ipAddress> \n" +
				"            <macAddress>AAAAAAAAAAAAAAAAAAAAAA==</macAddress> \n" +
				"        </details> \n" +
				"        <enabled>false</enabled> \n" +
				"        <group>product:/</group> \n" +
				"        <lastModificationAge timeUnit=\"MILLISECONDS\">-1.0</lastModificationAge> \n" +
				"        <lastRequestAge actualTime=\"2012-08-19T19:27:29.090+02:00\" referenceTime=\"2012-08-19T19:27:29.114+02:00\" timeUnit=\"SECONDS\">0.02</lastRequestAge> \n" +
				"        <upToDate>false</upToDate> \n" +
				"    </client> \n" +
				"</clients>";
		String expected = "<clients page=\"1\"\n" +
				" pageSize=\"100\"\n" +
				" resultSize=\"1\"\n" +
				" resultType=\"Ok\"> \n" +
				"    <client> \n" +
				"        <blockedCount>15</blockedCount> \n" +
				"        <id\n" +
				"         machineId=\"368389db-3dbd-42f4-9ec3-296585381a13\"\n" +
				"         userId=\"ea380afb-b0f0-4146-9d52-168411dac5ab\"/> \n" +
				"        <details\n" +
				"         agentVersion=\"1.0.0\"\n" +
				"         clientState=\"Connected\"\n" +
				"         machineArchitecture=\"native\"\n" +
				"         machineName=\"jd-host-01\"\n" +
				"         osArchitecture=\"amd64\"\n" +
				"         osLocale=\"en_US\"\n" +
				"         osName=\"Windows NT\"\n" +
				"         osPatchVersion=\"13.48.1\"\n" +
				"         osVersion=\"6.1\"\n" +
				"         pushNotificationListenerPort=\"0\"\n" +
				"         timeZoneDSTOffsetMinutes=\"0\"\n" +
				"         timeZoneOffsetMinutes=\"0\"\n" +
				"         userName=\"John Doe\"> \n" +
				"            <ipAddress>192.168.2.3</ipAddress> \n" +
				"            <macAddress>AAAAAAAAAAAAAAAAAAAAAA==</macAddress> \n" +
				"        </details> \n" +
				"        <enabled>false</enabled> \n" +
				"        <group>product:/</group> \n" +
				"        <lastModificationAge\n" +
				"         timeUnit=\"MILLISECONDS\">-1.0</lastModificationAge> \n" +
				"        <lastRequestAge\n" +
				"         actualTime=\"2012-08-19T19:27:29.090+02:00\"\n" +
				"         referenceTime=\"2012-08-19T19:27:29.114+02:00\"\n" +
				"         timeUnit=\"SECONDS\">0.02</lastRequestAge> \n" +
				"        <upToDate>false</upToDate> \n" +
				"    </client> \n" +
				"</clients>";
		assertEquals(expected, XPathEvaluatorImplementation.applyMaxLineWith(input, 25, "\n").toString());
	}

	@Test
	public void testSerializeWithNS() throws Exception {
		namespaceAware = true;
		setUp();
		testSerialize();
	}

	@Test
	public void testCanEvaluateXPathText() throws Exception {
		assertEquals("org.tinyjee.dim", evaluator.findText("/project/groupId"));
	}

	@Test
	public void testCanEvaluateXPathTextWithNS() throws Exception {
		namespaceAware = true;
		setUp();
		assertEquals("org.tinyjee.dim", evaluator.findText("/default:project/default:groupId"));
	}

	@Test
	public void testCanEvaluateXPathNodes() throws Exception {
		assertFalse(evaluator.findNodes("/project/developers").isEmpty());
	}

	@Test
	public void testXPathNodesCanContainLineNumbers() throws Exception {
		for (Node node : evaluator.findNodes("/project/developers"))
			assertTrue(AbstractPositioningDocumentBuilder.getLineNumber(node) > -1);
	}
}
