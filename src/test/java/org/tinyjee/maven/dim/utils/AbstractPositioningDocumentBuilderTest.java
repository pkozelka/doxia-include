/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;

import static org.junit.Assert.assertTrue;

/**
 * Is a base class for tests around the positioning document builders.
 *
 * @author Juergen_Kellerer, 2011-10-17
 */
public abstract class AbstractPositioningDocumentBuilderTest {

	protected Document document;
	protected XPathEvaluator evaluator;
	protected AbstractPositioningDocumentBuilder builder;

	protected abstract AbstractPositioningDocumentBuilder createBuilder();

	protected void doSetUp(String source) throws Exception {
		builder = createBuilder();
		document = builder.parse(new File(source).toURI().toURL());
		evaluator = new XPathEvaluatorImplementation(document);
	}

	protected void assertNodesHaveIncreasingLineAndColumnNumbers() {
		NodeList allElements = document.getElementsByTagName("*");
		assertTrue(allElements.getLength() > 1);
		for (int previousLocation = -1, i = 0, length = allElements.getLength(); i < length; i++) {
			Node element = allElements.item(i);
			int lineNumber = AbstractPositioningDocumentBuilder.getLineNumber(element),
					columnNumber = AbstractPositioningDocumentBuilder.getColumnNumber(element);

			int location = (lineNumber * 1000) + columnNumber;
			assertTrue(location > previousLocation);
			previousLocation = location;
		}
	}
}
