/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Tests ClassPathUtils.
 *
 * @author juergen_kellerer, 2012-06-06
 */
public class ClassPathUtilsTest {
	@Test
	public void testListUrls() throws Exception {
		List<String> urls = ClassPathUtils.listPaths(Thread.currentThread().getContextClassLoader());
		assertFalse(urls.isEmpty());

		assertContains("doxia-module-apt", urls);
		assertContains("commons-collections", urls);
	}

	private static void assertContains(String needle, List<String> urls) {
		boolean found = false;
		for (String url : urls) {
			if (url.contains(needle)) found = true;
		}
		assertTrue(needle + " not found in " + urls, found);
	}
}
