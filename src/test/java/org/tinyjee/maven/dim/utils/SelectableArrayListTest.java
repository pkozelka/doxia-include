/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Simple smoke test on the functionality of SelectableArrayList.
 *
 * @author Juergen_Kellerer, 2011-10-09
 */
public class SelectableArrayListTest {

	SelectableArrayList<Integer> numbers = new SelectableArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5));

	@Test
	public void testAppend() throws Exception {
		assertEquals(Arrays.asList(2, 4, 1, 3, 4), numbers.selectMatching("[24]").append(1).append(3, 4));
	}

	@Test
	public void testReverse() throws Exception {
		assertEquals(Arrays.asList(5, 4, 3, 2, 1), numbers.reverse());
	}

	@Test
	public void testSelectMatching() throws Exception {
		assertEquals(Arrays.asList(1, 3, 4), numbers.selectMatching("[134]"));
	}

	@Test
	public void testSelectNonMatching() throws Exception {
		assertEquals(Arrays.asList(2, 5), numbers.selectNonMatching("[134]"));
	}
}
