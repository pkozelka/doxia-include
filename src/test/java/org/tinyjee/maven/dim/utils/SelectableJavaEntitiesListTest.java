/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import com.thoughtworks.qdox.model.JavaClass;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * TODO: Describe Class
 *
 * @author Juergen_Kellerer, 2011-10-11
 */
public class SelectableJavaEntitiesListTest {

	File sourceFolder = new File("src/main/java"), testSourceFolder = new File("src/test/java");
	JavaClassLoader classLoader = JavaClassLoader.getInstance();
	SelectableJavaEntitiesList entitiesList;

	@Before
	public void setUp() throws Exception {
		classLoader.addSourceTree(sourceFolder);
		classLoader.addSourceTree(testSourceFolder);
		classLoader.addSource(testSourceFolder, "org/tinyjee/maven/dim/utils/AnnotatedTestClass.nocompile");
	}

	@Test
	public void testCanSelectClasses() throws Exception {
		SelectableJavaEntitiesList<JavaClass> classes = new SelectableJavaEntitiesList<JavaClass>(classLoader.getClasses());
		String[][] tests = {
				{"abstract org.tinyjee.maven.dim.utils.*PositioningDocumentBuilder",
						"org.tinyjee.maven.dim.utils.AbstractPositioningDocumentBuilder"},
				{"org..SelectableTestClass", "org.tinyjee.maven.dim.utils.SelectableTestClass"},
				{"org.tinyjee.maven.dim.utils.SelectableTestClass", "org.tinyjee.maven.dim.utils.SelectableTestClass"},
				{"..SelectableTestCl*", "org.tinyjee.maven.dim.utils.SelectableTestClass"},
				{"SelectableTestClass", "org.tinyjee.maven.dim.utils.SelectableTestClass"},

				{"SelectableTestClass$SelectableInner", "org.tinyjee.maven.dim.utils.SelectableTestClass$SelectableInner"},
				{"..SelectableInner", "org.tinyjee.maven.dim.utils.SelectableTestClass$SelectableInner"},
				{"SelectableInner", "org.tinyjee.maven.dim.utils.SelectableTestClass$SelectableInner"},
		};
		for (String[] test : tests) {
			SelectableJavaEntitiesList<JavaClass> list = (SelectableJavaEntitiesList<JavaClass>) classes.selectMatching(test[0]);
			assertEquals(test[0] + '|' + list, 1, list.size());
			assertEquals(test[1], list.get(0).getFullyQualifiedName());
			assertEquals(test[1], list.asMap().get(list.get(0).getName()).getFullyQualifiedName());
		}
	}

	@Test
	public void testCanSelectDerivedClasses() throws Exception {
		SelectableJavaEntitiesList<JavaClass> classes = new SelectableJavaEntitiesList<JavaClass>(classLoader.getClasses());
		String[][] tests = {
				{"Iterable", "org.tinyjee.maven.dim.utils.SelectableTestClass"},
				{"Comparable", "org.tinyjee.maven.dim.utils.SelectableTestClass"},
				{"Cloneable", "org.tinyjee.maven.dim.utils.SelectableTestClass"},
				{"Cloneable", "org.tinyjee.maven.dim.utils.SelectableTestClass$SelectableInner"},
		};
		for (String[] test : tests) {
			List<JavaClass> list = classes.selectDerived(test[0]);
			assertTrue(test[0] + '|' + list.toString(), isClassContained(list, test[1]));
		}
	}

	@Test
	public void testCanSelectAnnotatedClasses() throws Exception {
		SelectableJavaEntitiesList<JavaClass> classes = new SelectableJavaEntitiesList<JavaClass>(classLoader.getClasses());
		String[][] tests = {
				{"@..SuppressWarnings(value=\"none\")", "org.tinyjee.maven.dim.utils.SelectableTestClass"},
				{"@..SuppressWarnings(..)", "org.tinyjee.maven.dim.utils.SelectableTestClass"},
				{"@..SuppressWarnings", "org.tinyjee.maven.dim.utils.SelectableTestClass"},
				{"@java.lang.SuppressWarnings", "org.tinyjee.maven.dim.utils.SelectableTestClass"},
				{"@java..SuppressWarnings(..)", "org.tinyjee.maven.dim.utils.SelectableTestClass"},
				{"@SuppressWarnings(..)", "org.tinyjee.maven.dim.utils.SelectableTestClass"},
				{"@SuppressWarnings()", null},
				{"@SuppressWarnings", "org.tinyjee.maven.dim.utils.SelectableTestClass"},
				{"SuppressWarnings", "org.tinyjee.maven.dim.utils.SelectableTestClass"},

				{"@NonReferencedAnnotation2", "org.tinyjee.maven.dim.utils.AnnotatedTestClass"},
				{"NonReferencedAnnotation2", "org.tinyjee.maven.dim.utils.AnnotatedTestClass"},
				{"@my.pack.NonReferencedAnnotation2", "org.tinyjee.maven.dim.utils.AnnotatedTestClass"},
				{"@NonReferencedAnnotation1", "org.tinyjee.maven.dim.utils.AnnotatedTestClass"},
				{"NonReferencedAnnotation1", "org.tinyjee.maven.dim.utils.AnnotatedTestClass"},
		};
		for (String[] test : tests) {
			List<JavaClass> list = classes.selectAnnotated(test[0]);
			assertTrue(test[0] + '|' + list.toString(), test[1] == null ? list.isEmpty() : isClassContained(list, test[1]));
		}
	}

	private static boolean isClassContained(List<JavaClass> list, String className) {
		for (JavaClass javaClass : list) {
			if (javaClass.getFullyQualifiedName().equals(className))
				return true;
		}
		return false;
	}
}
