/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the equally named snippet selector implementation.
 *
 * @author Juergen_Kellerer, 2011-10-16
 */
public class GrepLikeSnippetSelectorTest extends AbstractSnippetSelectorTest {

	GrepLikeSnippetSelector selector = new GrepLikeSnippetSelector();

	@Test
	public void testCanSelectUsingTokens() throws Exception {
		load("Sample.log");
		Iterator<Integer> iterator = selector.selectSnippets("grep:WaRnInG", contentUrl, contentAsReader(), parameters);
		assertLinesAreSelected(1, 3, iterator, false);
		assertLinesAreSelected(12, 12, iterator);
	}

	@Test
	public void testCanMatchCaseSensitive() throws Exception {
		load("Sample.log");
		parameters.put(GrepLikeSnippetSelector.CASE_SENSITIVE, true);
		assertFalse(selector.selectSnippets("grep:WaRnInG", contentUrl, contentAsReader(), parameters).hasNext());
		assertTrue(selector.selectSnippets("grep:WARNING", contentUrl, contentAsReader(), parameters).hasNext());
	}

	@Test
	public void testCanAddEndOffset() throws Exception {
		load("Sample.log");
		assertLinesAreSelected(17,18, selector.selectSnippets("grep:Mailing Lists", contentUrl, contentAsReader(), parameters));
		assertLinesAreSelected(18,18, selector.selectSnippets("grep:Mailing Lists\\+2", contentUrl, contentAsReader(), parameters));
		assertLinesAreSelected(17,20, selector.selectSnippets("grep:Mailing Lists+2", contentUrl, contentAsReader(), parameters));
	}
}
