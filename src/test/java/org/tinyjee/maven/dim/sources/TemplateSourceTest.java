/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.apache.velocity.texen.util.FileUtil;
import org.codehaus.plexus.util.FileUtils;
import org.codehaus.plexus.util.IOUtil;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.tinyjee.maven.dim.spi.FixedContentSource;

import java.io.File;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests some aspects of TemplateSource.
 *
 * @author Juergen_Kellerer, 2011-03-09
 * @version 1.0
 */
public class TemplateSourceTest {

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void testToolManagerIncludesLists() throws Exception {
		assertNotNull(TemplateSource.VELOCITY_TOOL_MANAGER.createContext().get("lists"));
	}

	@Test
	public void testCanUseImportCommentSyntax() throws Exception {
		final File aFile = temporaryFolder.newFile("a.txt");
		final File bFile = temporaryFolder.newFile("b.txt");

		FileUtils.fileWrite(aFile.toString(), "a-content");
		FileUtils.fileWrite(bFile.toString(), "b-content");

		String template = "A template with some ##import " + aFile + '\r' +
				"----\n" +
				"##import" + aFile + '\n' +
				"----\n" +
				"##import\t\t" + bFile;

		TemplateSource source = new TemplateSource(null, new FixedContentSource(template, Collections.<String, Object>emptyMap()));
		String result = IOUtil.toString(source.getContent().openReader());

		assertEquals("A template with some a-content\r" +
				"----\n" +
				"a-content\n" +
				"----\n" +
				"b-content", result);
	}
}
