/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.junit.Test;

import java.util.Iterator;

import static java.util.Arrays.asList;

/**
 * Tests the equally named snippet selector implementation.
 *
 * @author Juergen_Kellerer, 2011-10-21
 */
public class LineRangeSnippetSelectorTest extends AbstractSnippetSelectorTest {

	LineRangeSnippetSelector selector = new LineRangeSnippetSelector();

	@Test
	public void testCanSelectRange() throws Exception {
		load("SampleClass.java");

		assertLinesAreSelected(10, 20, selector.selectSnippets("lines:10-20", contentUrl, contentAsReader(), parameters));
		assertLinesAreSelected(1, 7, selector.selectSnippets("lines:1-7", contentUrl, contentAsReader(), parameters));
	}

	@Test
	public void testCanSelectOpenRange() throws Exception {
		load("SampleClass.java");

		assertLinesAreSelected(1, 1, selector.selectSnippets("lines:1-", contentUrl, contentAsReader(), parameters));
		assertLinesAreSelected(1, 10, selector.selectSnippets("lines:10-", contentUrl, contentAsReader(), parameters));
		assertLinesAreSelected(1, 37, selector.selectSnippets("lines:1+", contentUrl, contentAsReader(), parameters));
		assertLinesAreSelected(10, 37, selector.selectSnippets("lines:10+", contentUrl, contentAsReader(), parameters));
	}

	@Test
	public void testCanSelectMultipleRangesInOneExpression() throws Exception {
		load("SampleClass.java");

		Iterator<Integer> iterator = selector.selectSnippets("lines:3,5, 10-11, 2-, 20+", contentUrl, contentAsReader(), parameters);
		assertLinesAreSelected(1, 3, iterator, false);
		assertLinesAreSelected(asList(5, 10, 11), iterator, false);
		assertLinesAreSelected(20, 37, iterator);
	}
}
