/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertFalse;

/**
 * Tests the equally named snippet selector implementation.
 *
 * @author Juergen_Kellerer, 2011-10-16
 */
public class IdSnippetSelectorTest extends AbstractSnippetSelectorTest {

	IdSnippetSelector selector = new IdSnippetSelector();

	@Test
	public void testCanSelectSingleSnippets() throws Exception {
		load("SampleClass.java");
		assertLinesAreSelected(13, 16, selector.selectSnippets("#ClassMethod", contentUrl, contentAsReader(), parameters));
		assertLinesAreSelected(24, 31, selector.selectSnippets("#InnerClass", contentUrl, contentAsReader(), parameters));
	}

	@Test
	public void testDoesNotSelectPartialIds() throws Exception {
		load("SampleClass.java");
		Iterator<Integer> iterator = selector.selectSnippets("#Class", contentUrl, contentAsReader(), parameters);
		assertFalse(iterator.hasNext());
	}

	@Test
	public void testCanSelectMultipleSnippets() throws Exception {
		load("Sample.xml");
		Iterator<Integer> iterator = selector.selectSnippets("#Signature", contentUrl, contentAsReader(), parameters);
		assertLinesAreSelected(7, 9, iterator, false);
		assertLinesAreSelected(16, 21, iterator);
		assertLinesAreSelected(25, 33, selector.selectSnippets("#Remote Resources", contentUrl, contentAsReader(), parameters));
	}
}
