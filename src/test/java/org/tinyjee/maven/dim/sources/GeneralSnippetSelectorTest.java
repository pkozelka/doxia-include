/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.junit.Test;
import org.tinyjee.maven.dim.spi.SnippetSelector;

import java.io.LineNumberReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Basic test to ensure the all 'expected' selectors are bundled and operational.
 *
 * @author Juergen_Kellerer, 2011-10-15
 */
public class GeneralSnippetSelectorTest extends AbstractSnippetSelectorTest {

	private final URL contentUrl;
	private final HashMap<String, Object> macroParameters = new HashMap<String, Object>();

	public GeneralSnippetSelectorTest() throws MalformedURLException {
		contentUrl = new URL("file:///something");
	}

	@Test
	public void testCanHandleExpectedPrefixes() throws Exception {
		String[] prefixes = {"#", "re:", "grep:", "aj:", "xp:"};
		for (String prefix : prefixes) {
			int matching = 0;
			for (SnippetSelector selector : SnippetSelector.SELECTORS) {
				for (String p : new String[]{prefix, prefix.toUpperCase()}) {
					if (selector.canSelectSnippetsWith(p + "some expression", contentUrl, macroParameters))
						matching++;
				}
			}
			assertEquals(prefix, 2, matching);
		}
	}

	@Test
	public void testAllRegisteredSelectorsRefuseInvalidExpressions() throws Exception {
		for (SnippetSelector selector : SnippetSelector.SELECTORS)
			assertFalse(selector.canSelectSnippetsWith("some non-supported expression", contentUrl, macroParameters));

		for (SnippetSelector selector : SnippetSelector.SELECTORS) {
			try {
				selector.selectSnippets("some non-supported expression",
						contentUrl, new LineNumberReader(new StringReader("")), macroParameters);
				fail(selector.toString());
			} catch (RuntimeException expected) {
			}
		}
	}
}
