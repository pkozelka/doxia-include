/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

/**
 * TODO: Describe Class
 *
 * @author Juergen_Kellerer, 2012-07-09
 */
public class VelocityVariablesStackToolTest {

	List<String> expected = Arrays.asList("a", "b", "c");
	VelocityVariablesStackTool tool = new VelocityVariablesStackTool();

	private void pustExpected() {
		for (String s : expected) tool.push(s);
	}

	@Test
	public void testStackFunctions() throws Exception {
		tool.push("a");
		assertEquals("a", tool.get());
		tool.push("b");
		tool.push("c");
		assertEquals("c", tool.get());
		assertEquals(3, tool.size());
		tool.pop();
		assertEquals("b", tool.get());
		tool.pop();
		assertEquals("a", tool.get());
		assertEquals(1, tool.size());
		tool.pop();
		assertNull(tool.get());
		assertEquals(0, tool.size());
	}

	@Test
	public void testIterate() throws Exception {
		pustExpected();

		final Iterator<String> iterator = expected.iterator();

		for (Object o : tool.iterate())
			assertEquals(iterator.next(), o);

		assertFalse(iterator.hasNext());
	}

	@Test
	public void testIterateParents() throws Exception {
		pustExpected();

		Collections.reverse(expected);
		final Iterator<String> iterator = expected.iterator();

		for (Object o : tool.iterateParents())
			assertEquals(iterator.next(), o);

		assertFalse(iterator.hasNext());
	}
}
