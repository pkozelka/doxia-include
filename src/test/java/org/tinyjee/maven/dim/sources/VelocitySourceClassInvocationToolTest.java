/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.junit.Test;
import org.tinyjee.maven.dim.AbstractIncludeMacroTest;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests that the velocity tool is registered and operational.
 *
 * @author Juergen_Kellerer, 2011-10-16
 */
public class VelocitySourceClassInvocationToolTest extends AbstractIncludeMacroTest {

	static Map<String, Object> sourceProperties;

	public static class SourceClass extends HashMap<String, Object> {
		public SourceClass(Map<String, Object> properties) {
			super(properties);
			put("p-4", "321");
			sourceProperties = this;
		}
	}

	@Test
	public void testCanImplicitlyInvokeSourceClass() throws Exception {
		params.put("source-content", "$invoker.clear().put('p-1', 'xyz').put('p-3', '123')." +
				"invokeClass('org.tinyjee.maven.dim.sources.VelocitySourceClassInvocationToolTest$SourceClass')");
		params.put("p-1", "abc");
		params.put("p-2", "abc");

		macro.execute(output, request);

		assertNotNull("source-class was not called.", sourceProperties);
		assertEquals("xyz", sourceProperties.get("p-1"));
		assertEquals("abc", sourceProperties.get("p-2"));
		assertEquals("123", sourceProperties.get("p-3"));
		assertEquals("321", sourceProperties.get("p-4"));
	}
}
