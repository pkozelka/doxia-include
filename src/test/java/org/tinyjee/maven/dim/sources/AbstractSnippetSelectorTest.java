/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.codehaus.plexus.util.IOUtil;

import java.io.File;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.net.URL;
import java.util.*;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Base class to tests around snippet selector implementations.
 *
 * @author Juergen_Kellerer, 2011-10-16
 */
public abstract class AbstractSnippetSelectorTest {

	protected URL contentUrl;
	protected String content;
	protected Map<String, Object> parameters = new HashMap<String, Object>();

	public String load(String name) throws IOException {
		contentUrl = new File("src/test/java/samples/" + name).toURI().toURL();
		content = IOUtil.toString(contentUrl.openStream(), "UTF-8");
		return content;
	}

	public LineNumberReader contentAsReader() {
		return new LineNumberReader(new StringReader(content));
	}

	protected void assertLinesAreSelected(int from, int to, Iterator<Integer> iterator) {
		assertLinesAreSelected(from, to, iterator, true);
	}

	protected void assertLinesAreSelected(int from, int to, Iterator<Integer> iterator, boolean isEntireRange) {
		List<Integer> selectedLines = new ArrayList<Integer>(to - from);
		for (; from <= to; from++)
			selectedLines.add(from);
		assertLinesAreSelected(selectedLines, iterator, isEntireRange);
	}

	protected void assertLinesAreSelected(List<Integer> selectedLines, Iterator<Integer> iterator) {
		assertLinesAreSelected(selectedLines, iterator, true);
	}

	protected void assertLinesAreSelected(List<Integer> selectedLines, Iterator<Integer> iterator, boolean isEntireRange) {
		for (Integer integer : selectedLines)
			assertEquals("Line " + integer + " is not selected.", integer, iterator.hasNext() ? iterator.next() : Integer.valueOf(-1));

		if (isEntireRange) {
			boolean hasMore = iterator.hasNext();
			assertFalse("More lines are selected than expected next is " + (hasMore ? iterator.next() : "") +
					" [" + selectedLines + "].", hasMore);
		}
	}
}
