/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.junit.Test;

import static java.util.Arrays.asList;

/**
 * Tests the equally named snippet selector implementation.
 *
 * @author Juergen_Kellerer, 2011-10-21
 */
public class TokenAndBraceSnippetSelectorTest extends AbstractSnippetSelectorTest {

	TokenAndBraceSnippetSelector selector = new TokenAndBraceSnippetSelector();

	@Test
	public void testCanSelectInJSON() throws Exception {
		load("Sample.json");

		assertLinesAreSelected(2, 5, selector.selectSnippets("tb:name", contentUrl, contentAsReader(), parameters));
		assertLinesAreSelected(8, 16, selector.selectSnippets("tb:flags[", contentUrl, contentAsReader(), parameters));
	}

	@Test
	public void testCanCommentsInXML() throws Exception {
		load("Sample.xml");

		assertLinesAreSelected(asList(6, 10, 15, 22, 24, 34),
				selector.selectSnippets("tb:<!--", contentUrl, contentAsReader(), parameters));
	}
}
