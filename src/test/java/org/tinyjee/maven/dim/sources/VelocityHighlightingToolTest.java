/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.junit.Test;
import org.tinyjee.maven.dim.AbstractIncludeMacroTest;

/**
 * TODO: Describe Class
 *
 * @author Juergen_Kellerer, 2011-10-18
 */
public class VelocityHighlightingToolTest extends AbstractIncludeMacroTest {

	@Test
	public void testCanHighlightCodeBlocks() throws Exception {
		params.put("source-content", "$highlighter.highlightCodeBlocks($content)");
		params.put("content", "<p/>\n" +
				"<b>Implementation</b>:<code><pre>\n" +
				"package my.package;\n" +
				"public class MyRequestParameterTransformer implements RequestParameterTransformer {\n" +
				"     public void transformParameters(Map&lt;String, Object&gt; requestParams) {\n" +
				"         Object value = requestParams.get(\"my-input-param\");\n" +
				"         if (value != null) {\n" +
				"             requestParams.put(\"myText\", value);\n" +
				"             requestParams.put(\"source\", \"classpath:my-template.vm\");\n" +
				"         }\n" +
				"     }\n" +
				"}\n" +
				"</pre></code>\n" +
				"<p/>\n" +
				"Create the following file:<code><pre>\n" +
				"META-INF/services/\n" +
				"   org.tinyjee.maven.dim.spi.RequestParameterTransformer\n" +
				"</pre></code>");

		macro.execute(output, request);

		assertBufferContains(
				"<b>Implementation</b>:<div ",
				"syntaxhighlighter  java",
				"Create the following file:<div ",
				"syntaxhighlighter  plain");
	}
}
