/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.junit.Ignore;
import org.junit.Test;
import org.tinyjee.maven.dim.spi.SnippetSelector;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Tests the equally named snippet selector implementation.
 *
 * @author Juergen_Kellerer, 2011-10-16
 */
public class AOPLikeSnippetSelectorTest extends AbstractSnippetSelectorTest {

	AOPLikeSnippetSelector selector = new AOPLikeSnippetSelector();

	@Test
	public void testCanSelectUsingExpressions() throws Exception {
		load("SampleClass.java");
		assertLinesAreSelected(13, 16, selector.selectSnippets("aj:..method()", contentUrl, contentAsReader(), parameters));
		assertLinesAreSelected(19, 21, selector.selectSnippets("aj:..method(java.lang.Integer)", contentUrl, contentAsReader(), parameters));
		assertLinesAreSelected(19, 21, selector.selectSnippets("aj:..method(..Integer..)", contentUrl, contentAsReader(), parameters));
		assertLinesAreSelected(19, 21, selector.selectSnippets("aj:..method(..Integer)", contentUrl, contentAsReader(), parameters));
		assertLinesAreSelected(8, 37, selector.selectSnippets("aj:..SampleClass", contentUrl, contentAsReader(), parameters));
		assertLinesAreSelected(24, 31, selector.selectSnippets("aj:..InnerClass", contentUrl, contentAsReader(), parameters));
		assertLinesAreSelected(24, 31, selector.selectSnippets("aj:..SampleClass$InnerClass", contentUrl, contentAsReader(), parameters));
	}

	@Test
	public void testCanMatchMultipleSnippets() throws Exception {
		load("SampleClass.java");
		for (String expression : Arrays.asList(
				"aj:@SuppressWarnings(value=\"none\")",
				"aj:@SuppressWarnings(..\"none\")",
				"aj:@SuppressWarnings(..)",
				"aj:@SuppressWarnings",
				"aj:..*ethod(..)")) {
			Iterator<Integer> iterator = selector.selectSnippets(expression, contentUrl, contentAsReader(), parameters);
			assertLinesAreSelected(13, 16, iterator, false);
			assertLinesAreSelected(19, 21, iterator, false);
			assertLinesAreSelected(28, 30, iterator);
		}
	}

	@Test
	public void testCanExpandBeanPropertySnippet() throws Exception {
		load("SampleClass.java");
		parameters.put(SnippetSelector.EXPAND_SNIPPETS, true);
		Iterator<Integer> iterator;
		iterator = selector.selectSnippets("aj:..field", contentUrl, contentAsReader(), parameters);
		assertLinesAreSelected(10, 10, iterator, false);
		assertLinesAreSelected(34, 36, iterator);

		iterator = selector.selectSnippets("aj:..getField()", contentUrl, contentAsReader(), parameters);
		assertLinesAreSelected(34, 36, iterator);
	}

	@Test
	public void testCanExpandComments() throws Exception {
		load("SampleClass.java");
		parameters.put(SnippetSelector.EXPAND_SNIPPETS, true);
		assertLinesAreSelected(3, 37, selector.selectSnippets("aj:..SampleClass", contentUrl, contentAsReader(), parameters));
	}

	@Test
	public void testCanFoldCodeBlocks() throws Exception {
		load("SampleClass.java");
		parameters.put(AOPLikeSnippetSelector.FOLD_CODE_BLOCKS, true);
		assertLinesAreSelected(Arrays.asList(8, 34), selector.selectSnippets("aj:public ..*", contentUrl, contentAsReader(), parameters));
	}
}
