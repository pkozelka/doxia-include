/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaMethod;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.tinyjee.maven.dim.extensions.utils.JavaEntitiesList;
import org.tinyjee.maven.dim.utils.AbstractSelectableJavaEntitiesList;
import org.tinyjee.maven.dim.utils.JavaClassLoader;
import org.tinyjee.maven.dim.utils.PrintableMap;
import org.tinyjee.maven.dim.utils.SelectableJavaEntitiesList;

import java.io.File;
import java.util.*;

import static org.junit.Assert.*;
import static org.tinyjee.maven.dim.extensions.JavaSourceLoader.*;

/**
 * Smoke-Tests the interface scanner.
 *
 * @author Juergen_Kellerer, 2010-09-08
 * @version 1.0
 */
public class JavaSourceLoaderTest {

	File baseDir = new File(".");
	JavaSourceLoader loader;
	Map<String, Object> params = new HashMap<String, Object>();

	@After
	public void initializeAndCleanup() throws Exception {
		JavaClassLoader.getInstance().clear();

		params.put(PARAM_JAVA_SOURCE, "org.tinyjee.maven.dim.extensions.InterfaceScannerTester");
		params.put(PARAM_LEGACY_MODE, true);
	}

	@Before
	public void createLoader() throws Exception {
		initializeAndCleanup();
		loader = new JavaSourceLoader(baseDir, params);
	}

	@SuppressWarnings("unchecked")
	private List<Map> asMapList(String key) {
		return (List<Map>) loader.get(key);
	}

	private JavaEntitiesList asJavaEntitiesList(String key) {
		return (JavaEntitiesList) loader.get(key);
	}

	private Map getAnnotation(Map row, String annotationName) {
		return ((Map) ((Map) row.get("annotations")).get(annotationName));
	}

	private JavaSourceLoader newLoaderFor(String className) {
		className = resolveTestClassName(className);
		return new JavaSourceLoader(baseDir, className);
	}

	private String resolveTestClassName(String className) {
		if (className.indexOf('.') == -1) className = "org.tinyjee.maven.dim.extensions." + className;
		return className;
	}

	@Test
	public void testConvertToJavaEntitiesList() throws Exception {
		String[] names = {"java.lang.Object", "java.lang.Iterable", "InterfaceScannerTesterAnnotation",
				"InterfaceScannerTesterImpl", "InterfaceScannerTesterImpl$MyEnum"};
		List<Map<?, ?>> tests = loader.convert((String[]) names);
		assertEquals(5, tests.size());
		final Iterator<Map<?, ?>> iterator = tests.iterator();
		for (String name : names) {
			assertEquals(resolveTestClassName(name), iterator.next().get("name"));
		}
	}

	@Test
	public void testConvertDoesNotFailOnNonExisting() throws Exception {
		assertEquals(0, loader.convert("some-fantasy-name", "AnotherNonExistingClass").size());
	}

	@Test
	public void testCanParseGenericTypeString() throws Exception {
		Map<String, Object> map = loader.genericTypeToMap(" java.util.Map < List<Map<String, ? extends Object>>, SomeOther>");
		assertEquals(2, map.size());
		assertEquals("java.util.Map", map.get("type"));
		assertEquals("java.util.Map", ((Map) map.get("typeClass")).get("name"));

		List<Map> parameters = (List) map.get("parameters");

		assertEquals("List", parameters.get(0).get("type"));
		assertEquals("SomeOther", parameters.get(1).get("type"));
		assertEquals(0, ((List) parameters.get(1).get("parameters")).size());

		parameters = (List<Map>) parameters.get(0).get("parameters");
		assertEquals(1, parameters.size());
		assertEquals("Map", parameters.get(0).get("type"));

		parameters = (List<Map>) parameters.get(0).get("parameters");
		assertEquals(2, parameters.size());
		assertEquals("String", parameters.get(0).get("type"));
		assertEquals("Object", parameters.get(1).get("type"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCannotLoadNonExistingClass() throws Exception {
		newLoaderFor("com.AnyThingNonExisting");
	}

	@Test
	public void testCanLoadJavaClassWithoutSource() throws Exception {
		assertEquals("java.lang.Object", newLoaderFor("java.lang.Object").get("name"));
	}

	@Test
	public void testCanLoadClassByExpression() throws Exception {
		String[] expressions = {"..InterfaceScannerTesterImpl", "org..InterfaceScannerTesterImpl", "InterfaceScannerTesterImpl"};
		for (String expression : expressions) {
			assertEquals(resolveTestClassName("InterfaceScannerTesterImpl"), newLoaderFor(expression).get("name"));
		}
	}

	@Test
	public void testFastGetSuperClass() throws Exception {
		List<Map<?, ?>> tests = loader.convert("InterfaceScannerTesterAnnotation",
				"InterfaceScannerTesterImpl", "InterfaceScannerTesterImpl$MyEnum");
		tests.add(loader);

		assertEquals(4, tests.size());

		for (Map loader : tests) {
			JavaClass javaClass = (JavaClass) loader.get("javaClass");
			final JavaClass superJavaClass = fastGetSuperClass(javaClass);
			assertEquals(javaClass.getSuperClass(), superJavaClass == null ? null : superJavaClass.asType());
		}
	}

	@Test
	public void testFindSuperMethods() throws Exception {
		loader = newLoaderFor("InterfaceScannerTesterImpl");
		final Map<?, ?> methodInfo = asJavaEntitiesList(OUT_PARAM_METHODS).selectMatching("..ifMethod(..)").get(0);
		JavaMethod method = (JavaMethod) methodInfo.get("method");
		SelectableJavaEntitiesList<JavaMethod> methods = loader.findSuperMethods(method, true);

		assertTrue(methods.contains(method));
		assertEquals(method, methods.get(0));
		assertEquals(2, methods.size());

		assertEquals("ifMethod Comment.", methodInfo.get("comment"));
	}

	@Test
	public void testVisibilityCanBeChanged() throws Exception {
		assertEquals(2, asJavaEntitiesList(OUT_PARAM_CONSTANTS).size());
		assertEquals(5, asJavaEntitiesList(OUT_PARAM_PROPERTIES).size());
		assertEquals(1, asJavaEntitiesList(OUT_PARAM_FIELDS).size());
		assertEquals(2, asJavaEntitiesList(OUT_PARAM_INTERFACE_METHODS).size());
		assertEquals(0, asJavaEntitiesList(OUT_PARAM_METHODS).size());

		params.put(PARAM_VISIBILITY, "public");
		createLoader();

		assertEquals(2, asJavaEntitiesList(OUT_PARAM_CONSTANTS).size());
		assertEquals(5, asJavaEntitiesList(OUT_PARAM_PROPERTIES).size());
		assertEquals(1, asJavaEntitiesList(OUT_PARAM_FIELDS).size());
		assertEquals(1, asJavaEntitiesList(OUT_PARAM_INTERFACE_METHODS).size());

		params.put(PARAM_VISIBILITY, "private");
		createLoader();

		assertEquals(1, asJavaEntitiesList(OUT_PARAM_METHODS).size());
	}

	@Test
	public void testInterfaceScanner() {
		List<Map> constants = asMapList(OUT_PARAM_CONSTANTS);
		assertTupleHasCorrectComment(constants);
		for (Map constant : constants) {
			assertNotNull(constant.get(6));
			assertTrue(constant.get(5).toString().contains("CONSTANT"));
		}

		List<Map> methods = asMapList(OUT_PARAM_INTERFACE_METHODS);
		assertTupleHasCorrectComment(methods);
		for (Map method : methods) {
			assertNotNull(method.get(6));
			assertTrue(method.get(5).toString().contains("Method"));
		}
	}

	void assertTupleHasCorrectComment(List<Map> tuple) {
		assertEquals(2, tuple.size());
		assertNull(tuple.get(0).get(7));
		assertNull(tuple.get(0).get("comment"));
		assertEquals(tuple.get(1).get(5) + " Comment.", tuple.get(1).get(7));
		assertEquals(tuple.get(1).get("name") + " Comment.", tuple.get(1).get("comment"));
	}

	@Test
	public void testMethodAnnotations() throws Exception {
		List<Map> ifMethods = asMapList(OUT_PARAM_INTERFACE_METHODS);
		assertEquals(2, ifMethods.size());

		assertEquals("\"ifMethod\"", getAnnotation(ifMethods.get(1), "@WebMethod").get("operationName"));
		Map parameterAnnotations = (Map) ifMethods.get(1).get("parameterAnnotations");
		assertEquals("\"paramName\"", ((Map) ((Map) parameterAnnotations.get("param")).get("@WebParam")).get("name"));
	}

	@Test
	public void testExtractsCommentsFromSuperMethod() throws Exception {
		loader = newLoaderFor("InterfaceScannerTesterImpl");
		List<Map> methods = asMapList(OUT_PARAM_METHODS);
		assertEquals(3, methods.size());

		assertEquals("Comment in overriden method.", methods.get(0).get("comment"));
		assertEquals("ifMethod Comment.", methods.get(1).get("comment"));
		assertEquals("param Comment.", ((Map) ((Map) methods.get(1).get("tags")).get("param")).get("param"));
		assertEquals("param Comment.", ((Map) ((Map) methods.get(1).get("parameters")).get("param")).get("comment"));
		assertEquals("return Comment.", ((Map) methods.get(1).get("tags")).get("return"));
	}

	@Test
	public void testSupportsInheritDoc() throws Exception {
		loader = newLoaderFor("InterfaceScannerTesterImpl$Inner");
		List<Map> methods = asMapList(OUT_PARAM_METHODS);
		assertEquals(1, methods.size());

		assertEquals("ifMethod Comment. <p/> Additional content in \"InterfaceScannerTesterImpl.Inner\".",
				String.valueOf(methods.get(0).get("comment")).replaceAll("[\r\n]+", " "));
	}

	@Test
	public void testInvalidXmlInCommentsIsCorrected() throws Exception {
		loader = newLoaderFor("InterfaceScannerTesterImpl$InvalidComment");
		String comment = (String) loader.get(OUT_PARAM_COMMENT);

		assertEquals("Special chars &amp; non closed tags like  <b> are fixed using HTML tidy.</b>",
				comment.replaceAll("[\r\n]+", " "));
	}

	@Test
	public void testInterfaceScannerHandlesBeanProperties() throws Exception {
		List<Map> props = asMapList(OUT_PARAM_PROPERTIES);
		assertEquals(5, props.size());

		assertEquals("\"propertyAttributeB\"", getAnnotation(props.get(0), "@XmlAttribute").get("name"));
		assertEquals("\"propertyAttributeD\"", getAnnotation(props.get(2), "@XmlAttribute").get("name"));
		assertEquals("\"initialValue\"", props.get(2).get("value"));
		assertEquals("propertyD Getter Comment.", props.get(2).get("comment"));
		assertEquals("propertyD Setter Comment.", props.get(2).get("setterComment"));
	}

	@Test
	@SuppressWarnings("unchecked")
	public void testInterfaceScannerHandlesBeanPropertiesAsMap() throws Exception {
		Map<String, Map> props = (Map<String, Map>) loader.get(OUT_PARAM_PROPERTIES_MAP);
		assertEquals(5, props.size());

		assertEquals("\"initialValue\"", props.get("propertyD").get("value"));
	}

	@Test
	@SuppressWarnings("unchecked")
	public void testCanPrintMappedParameters() throws Exception {
		Map<String, Map> props = (Map<String, Map>) loader.get(OUT_PARAM_PROPERTIES_MAP);
		assertTrue(props instanceof PrintableMap);
		String mapContent = ((PrintableMap) props).getContentAsString();
		assertTrue(mapContent.contains("propertyD"));

		props.get("non-existing-key-does-not-throw-an-exception");
	}

	@Test
	public void testCanLoadParentClasses() throws Exception {
		loader = newLoaderFor("InterfaceScannerTesterImpl$Inner");
		JavaEntitiesList parents = loader.getSelector().getSelfAndParentClasses();

		LinkedList<String> expected = new LinkedList<String>(Arrays.asList(
				"org.tinyjee.maven.dim.extensions.InterfaceScannerTesterImpl$Inner",
				"org.tinyjee.maven.dim.extensions.InterfaceScannerTesterImpl",
				"java.lang.Iterable",
				"org.tinyjee.maven.dim.extensions.InterfaceScannerTester",
				"java.lang.Object"));

		for (Map<?, ?> parent : parents) {
			String name = (String) parent.get(OUT_PARAM_NAME);
			assertEquals(expected.removeFirst(), name);
		}

		assertTrue("not all expected classes matched: " + expected, expected.isEmpty());
	}

	@Test
	public void testCanLoadAnnotatedClasses() throws Exception {
		loader = newLoaderFor("InterfaceScannerTesterAnnotation");
		JavaEntitiesList annotatedClasses = loader.getSelector().getAnnotatedClasses();
		assertEquals(1, annotatedClasses.selectMatching("..InterfaceScannerTester").size());
	}

	@Test
	public void testCanLoadCreatedClasses() throws Exception {
		loader = newLoaderFor("InterfaceScannerTesterImpl");
		JavaEntitiesList createdClasses = loader.getSelector().getCreatedClasses();

		@SuppressWarnings("unchecked")
		Set expected = new HashSet(Arrays.asList("java.lang.Integer", "java.util.HashMap", "java.util.Date"));
		for (Map<?, ?> createdClass : createdClasses) {
			expected.remove(createdClass.get(OUT_PARAM_NAME));
		}

		assertEquals("Not matched: " + expected, 0, expected.size());
	}

	@Test
	public void testCanLoadPackages() throws Exception {
		JavaEntitiesList classesInDim = new JavaSourceLoader(baseDir, "org.tinyjee.maven.dim").getSelector().getPackageClasses(),
				classesBelowDim = new JavaSourceLoader(baseDir, "org.tinyjee").getSelector().getClassesBelowPackage(),
				twoPackages = new JavaSourceLoader(baseDir, "org.tinyjee.maven.dim.extensions, " +
						"org.tinyjee.maven.dim.spi").getSelector().getClassesBelowPackage();

		assertEquals(0, classesInDim.selectMatching("..InterfaceScannerTester").size());
		assertEquals(1, classesInDim.selectMatching("..AbstractIncludeMacroTest").size());
		assertEquals(1, classesBelowDim.selectMatching("..InterfaceScannerTester").size());
		assertEquals(1, classesBelowDim.selectMatching("org.tinyjee.maven.dim.i18n.I18NTest").size());


		assertFalse(0 == twoPackages.size());
		int packageA = 0, packageB = 0;
		for (Map<?, ?> map : twoPackages) {
			String name = (String) map.get("name");
			boolean isExtension = name.startsWith("org.tinyjee.maven.dim.extensions");
			if (isExtension) packageA++;
			else packageB++;

			assertTrue(isExtension || name.startsWith("org.tinyjee.maven.dim.spi"));
		}
		assertTrue(packageA > 0 && packageB > 0);
	}

	@Test
	public void testMultiplePackageLoadingDeduplicates() throws Exception {
		// Init package first (this loads some more classes thus we need it before doing the actual measurement)
		new JavaSourceLoader(baseDir, "org.tinyjee.maven.dim").getSelector().getPackageClasses();
		// Init the tst classes
		JavaEntitiesList classesInDim = new JavaSourceLoader(baseDir, "org.tinyjee.maven.dim").getSelector().getPackageClasses(),
				classesInMultipleDim = new JavaSourceLoader(baseDir, "org.tinyjee.maven.dim, " +
						"org.tinyjee.maven.dim").getSelector().getPackageClasses();

		assertEquals(classesInDim.size(), classesInMultipleDim.size());
		for (Iterator<Map<?, ?>> i1 = classesInDim.iterator(), i2 = classesInMultipleDim.iterator(); i1.hasNext(); )
			assertEquals(i1.next().get(OUT_PARAM_NAME), i2.next().get(OUT_PARAM_NAME));
	}

	@Test
	public void testCanMatchOutAndInParams() throws Exception {
		loader = newLoaderFor("JavaSourceLoader");
		JavaEntitiesList list = asJavaEntitiesList("constants");
		assertFalse(list.selectMatching("..OUT_*").isEmpty());
		assertFalse(list.selectNonMatching("..OUT_*").isEmpty());
		for (Map<?, ?> stringMap : list.selectMatching("..OUT_*"))
			assertFalse(list.selectNonMatching("..OUT_*").contains(stringMap));
	}

	@Test
	public void testCanMatchProperties() throws Exception {
		loader = newLoaderFor("InterfaceScannerTester");
		JavaEntitiesList list = asJavaEntitiesList("properties");
		for (String propertyName : Arrays.asList("propertyB", "propertyC", "propertyD", "propertyE", "propertyF")) {
			final AbstractSelectableJavaEntitiesList<Map<?, ?>> maps = list.selectMatching(".." + propertyName);
			assertEquals(propertyName, 1, maps.size());
			assertEquals(propertyName, maps.get(0).get("name"));
		}
	}
}
