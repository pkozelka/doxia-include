/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.bind.annotation.XmlAttribute;
import java.lang.annotation.Documented;

/**
 * Defines a test bean to use with the interface scanner.
 *
 * @author Juergen_Kellerer, 2010-09-08
 * @version 1.0
 */
@InterfaceScannerTesterAnnotation({@SuppressWarnings("abc"), @SuppressWarnings("xyz")})
public abstract class InterfaceScannerTester {

	public static final String UNDOCUMENTED_CONSTANT = "";

	/**
	 * DOCUMENTED_CONSTANT Comment.
	 */
	public static final String DOCUMENTED_CONSTANT = System.getProperty("const");

	static final String PRIVATE_CONSTANT = "";

	/**
	 * changeableField Comment.
	 */
	public String changeableField = "initialValue";

	protected abstract void emptyIfMethod();

	/**
	 * ifMethod Comment.
	 *
	 * @param param param Comment.
	 * @return return Comment.
	 */
	@WebMethod(operationName = "ifMethod")
	public abstract String ifMethod(@WebParam(name = "paramName") String param);

	private String propertyA, propertyB, propertyC;

	@XmlAttribute(name = "propertyAttributeD")
	private String propertyD = "initialValue";

	@XmlAttribute(name = "propertyAttributeB")
	public String getPropertyB() {
		return propertyB;
	}

	public void setPropertyC(String propertyC) {
		this.propertyC = propertyC;
	}

	/**
	 * propertyD Getter Comment.
	 *
	 * @return return Comment.
	 */
	public String getPropertyD() {
		return propertyD;
	}

	/**
	 * propertyD Setter Comment.
	 *
	 * @param propertyD param Comment.
	 */
	public void setPropertyD(String propertyD) {
		this.propertyD = propertyD;
	}

	public int getPropertyE() {
		return 0;
	}

	public void setPropertyF(long fValue) {
	}

	private void privateMethod() {
	}

	public InterfaceScannerTester() {
	}
}
