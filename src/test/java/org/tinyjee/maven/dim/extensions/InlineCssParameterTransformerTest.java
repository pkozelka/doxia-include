/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.junit.Test;
import org.tinyjee.maven.dim.AbstractIncludeMacroTest;

/**
 * Simple test that verifies the extension.
 *
 * @author Juergen_Kellerer, 2011-11-08
 */
public class InlineCssParameterTransformerTest extends AbstractIncludeMacroTest {
	@Test
	public void testCanInlineCss() throws Exception {
		final String css = ".myclass { background-color:red }";
		params.put(InlineCssParameterTransformer.PARAM_INLINE_CSS, css);
		macro.execute(output, request);
		assertBufferContains("<style ", css, "</style>");
	}
}
