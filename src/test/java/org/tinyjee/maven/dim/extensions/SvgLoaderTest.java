/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.junit.Before;
import org.junit.Test;
import org.tinyjee.maven.dim.AbstractIncludeMacroTest;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_SOURCE_CONTENT;

/**
 * Simple test for SvgLoader.
 *
 * @author juergen kellerer, 2012-05-27
 */
public class SvgLoaderTest extends AbstractIncludeMacroTest {

	@Before
	public void setUp() throws Exception {
		params.put(SvgLoader.PARAM_ALIAS, "test/java/samples/Sample.svg");
	}

	@Test
	public void testDocumentContentIsSet() throws Exception {
		callMacro();
		String content = String.valueOf(capturedSource.getContent().getParameters().get(PARAM_SOURCE_CONTENT));
		assertTrue(content, content.contains("<linearGradient"));
	}

	@Test
	public void testCanLoadGZIPSource() throws Exception {
		String uncompressed = callMacro().trim();
		params.put(SvgLoader.PARAM_ALIAS, "test/java/samples/Sample.svg.gz");
		assertEquals(uncompressed, callMacro(true).trim());
	}

	@Test
	public void testSourceIsNotOverridden() throws Exception {
		params.put(PARAM_SOURCE_CONTENT, "abc");
		assertEquals("abc", callMacro().trim());
	}

	@Test
	public void testRenderedSvgCanBeAccessedInTemplate() throws Exception {
		params.put(PARAM_SOURCE_CONTENT, "$" + SvgLoader.OUT_PARAM_RENDER_RESULT);
		params.put(SvgLoader.PARAM_RENDER, "true");
		assertTrue(callMacro().trim().startsWith("[B@"));
	}

	@Test
	public void testRenderedCanBeRenderedInTemplate() throws Exception {
		params.put(PARAM_SOURCE_CONTENT, "$loader.renderAndAttach('png', 'my.png')");
		assertEquals("attached-includes/my.png", callMacro().trim());
	}

	@Test
	public void testSvgCanBeRenderedAndAttachedWithCustomName() throws Exception {
		params.put(SvgLoader.PARAM_RENDER, "true");
		params.put(SvgLoader.PARAM_ATTACH, "m&y.png");
		assertTrue(callMacro().trim().contains("'attached-includes/m&amp;y.png'"));
	}

	@Test
	public void testCanAccessRenderedAttachedContentInVM() throws Exception {
		params.put(SvgLoader.PARAM_RENDER, "true");
		params.put(SvgLoader.PARAM_ATTACH, "m&y.png");
		params.put(PARAM_SOURCE_CONTENT, "$" + SvgLoader.OUT_PARAM_RENDER_HREF);
		assertEquals("attached-includes/m&y.png", callMacro().trim());
	}

	@Test
	public void testSvgCanBeRenderedToTiff() throws Exception {
		params.put(SvgLoader.PARAM_RENDER, "tiff");
		params.put(SvgLoader.PARAM_ATTACH, "m&y.tiff");
		assertEquals("<a href='attached-includes/m&amp;y.tiff'>m&amp;y.tiff</a>", callMacro().trim());
	}

	@Test
	public void testSvgCanBeRenderedToWebImages() throws Exception {
		params.put(SvgLoader.PARAM_RENDER_DPI, "100");
		params.put(SvgLoader.PARAM_RENDER_MEDIA, "screen");
		params.put(SvgLoader.PARAM_RENDER_BACKGROUND, "#ffffff");

		String[] values = {"true", "png", "jpg", "jpeg"};
		String[] extensions = {"png", "png", "jpg", "jpeg"};
		for (int i = 0; i < values.length; i++) {
			params.remove("title");
			params.put(SvgLoader.PARAM_RENDER, values[i]);
			String name = params.get(SvgLoader.PARAM_ALIAS) + '.' + extensions[i];
			String expected = String.format("<img src='attached-includes/%s' alt='%s'/>", name, new File(name).getName());
			assertEquals(values[i], expected, callMacro(true).trim());
		}
	}
}
