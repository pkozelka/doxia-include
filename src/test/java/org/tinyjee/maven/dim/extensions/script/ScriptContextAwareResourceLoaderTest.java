/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.script;

import org.junit.Test;
import org.tinyjee.maven.dim.extensions.script.ScriptContextAwareResourceLoader;

import java.io.File;

import static org.junit.Assert.*;

/**
 * Quick class-loading test.
 *
 * @author juergen_kellerer, 2012-06-06
 */
public class ScriptContextAwareResourceLoaderTest {

	ScriptContextAwareResourceLoader resourceLoader = new ScriptContextAwareResourceLoader(ClassLoader.getSystemClassLoader());

	@Test
	public void testCanLoadResourceOnCustomSearchPath() throws Exception {
		assertNull(resourceLoader.getResource("/samples/Sample.json"));
		resourceLoader.setScriptContextPath(new File("src/test/java"));
		assertNotNull(resourceLoader.getResource("/samples/Sample.json"));
		resourceLoader.setScriptContextPath();
		assertNull(resourceLoader.getResource("/samples/Sample.json"));
	}
}
