/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.graph;

import org.junit.Test;

import java.util.Arrays;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests TrivialGraphFormatParser.
 *
 * @author Juergen_Kellerer, 2012-07-22
 */
public class TrivialGraphFormatParserTest {

	GraphBuilder builder = new GraphBuilder();
	TrivialGraphFormatParser parser = new TrivialGraphFormatParser(false);

	@Test
	public void testParseGraph() throws Exception {
		parser.parseGraph("n1 node \\n 1\n" +
				"n2\n" +
				"#\n" +
				"n1 n2 the edge {@style abc}", builder);

		assertEquals("node \\n 1", builder.getVertexProperties().get("n1").get(GraphBuilder.GRAPH_PROPERTY_LABEL));
		assertNotNull(null, builder.getVertexProperties().get("n2"));
		assertNull(builder.getVertexProperties().get("n2").get(GraphBuilder.GRAPH_PROPERTY_LABEL));

		assertEquals("the edge {@style abc}", builder.getEdgeProperties().get(Arrays.asList("n1", "n2")).get(GraphBuilder.GRAPH_PROPERTY_LABEL));
	}

	@Test
	public void testParseWithStylesGraph() throws Exception {
		parser = new TrivialGraphFormatParser(true);

		parser.parseGraph("n1 node \\n 1 {@style s1}\n" +
				"n2 {@style s2}{@width 1}{@height 2}\n" +
				"#\n" +
				"n1 n2 the edge {@style abc}", builder);

		final Map<String,Object> n1 = builder.getVertexProperties().get("n1");
		assertEquals("node \n 1", n1.get(GraphBuilder.GRAPH_PROPERTY_LABEL));
		assertEquals("s1", n1.get(GraphBuilder.GRAPH_PROPERTY_STYLE));

		final Map<String, Object> n2 = builder.getVertexProperties().get("n2");
		assertNotNull(null, n2);
		assertNull(n2.get(GraphBuilder.GRAPH_PROPERTY_LABEL));
		assertEquals("s2", n2.get(GraphBuilder.GRAPH_PROPERTY_STYLE));
		assertEquals("1", n2.get(GraphBuilder.GRAPH_PROPERTY_WIDTH));
		assertEquals("2", n2.get(GraphBuilder.GRAPH_PROPERTY_HEIGHT));

		final Map<String, Object> edge = builder.getEdgeProperties().get(Arrays.asList("n1", "n2"));
		assertEquals("the edge", edge.get(GraphBuilder.GRAPH_PROPERTY_LABEL));
		assertEquals("abc", edge.get(GraphBuilder.GRAPH_PROPERTY_STYLE));
	}
}
