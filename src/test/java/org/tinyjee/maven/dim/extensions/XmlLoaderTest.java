/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.junit.Before;
import org.junit.Test;
import org.tinyjee.maven.dim.IncludeMacroSignature;
import org.tinyjee.maven.dim.utils.JaxbXmlSerializer;
import org.tinyjee.maven.dim.utils.XPathEvaluator;
import org.w3c.dom.Document;
import samples.SampleXmlType;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonMap;
import static org.junit.Assert.*;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_SOURCE_CONTENT;
import static org.tinyjee.maven.dim.extensions.XmlLoader.PARAM_XML_CLASS;

/**
 * Tests the most important parts of XmlLoader.
 *
 * @author Juergen_Kellerer, 2011-10-06
 */
public class XmlLoaderTest {

	XmlLoader xmlLoader;
	XPathEvaluator evaluator;
	Map<String, Object> requestParams = singletonMap("xml", (Object) "pom.xml");

	@Before
	public void setUp() throws Exception {
		xmlLoader = new XmlLoader(new File("."), requestParams);
		evaluator = (XPathEvaluator) xmlLoader.get(XmlLoader.OUT_PARAM_XPATH);
	}

	@Test
	public void testDocumentContentIsSet() throws Exception {
		String content = String.valueOf(xmlLoader.get(PARAM_SOURCE_CONTENT));
		assertTrue(content, content.contains("<groupId>org.tinyjee.dim"));
	}

	@Test
	public void testCanSerializeAsDomToJson() throws Exception {
		requestParams = new HashMap<String, Object>(requestParams);
		requestParams.put(XmlLoader.PARAM_SERIALIZE_AS, "json");
		setUp();
		String content = String.valueOf(xmlLoader.get(PARAM_SOURCE_CONTENT));
		assertTrue(content, content.contains("\"name\" : \"groupId\"") && content.contains("\"$\" : \"org.tinyjee.dim\""));

	}

	@Test
	public void testCanAccessDocument() throws Exception {
		assertNotNull(xmlLoader.get(XmlLoader.OUT_PARAM_DOCUMENT));
		assertTrue(xmlLoader.get(XmlLoader.OUT_PARAM_DOCUMENT) instanceof Document);
	}

	@Test
	public void testCanEvaluateXPathText() throws Exception {
		assertEquals("org.tinyjee.dim", evaluator.findText("/project/groupId"));
	}

	@Test
	public void testCanEvaluateXPathTextWithNS() throws Exception {
		requestParams = new HashMap<String, Object>(requestParams);
		requestParams.put(XmlLoader.PARAM_NAMESPACE_AWARE, true);
		setUp();
		assertEquals("org.tinyjee.dim", evaluator.findText("/default:project/default:groupId"));
	}

	@Test
	public void testCanEvaluateXPathNodes() throws Exception {
		assertFalse(evaluator.findNodes("/project/developers").isEmpty());
	}

	@Test
	public void testCanTransformUsingXSL() throws Exception {
		requestParams = new HashMap<String, Object>(requestParams);
		requestParams.put(XmlLoader.PARAM_XSL, "src/test/java/org/tinyjee/maven/dim/extensions/pom-html-summary.xsl");
		setUp();
		String content = String.valueOf(xmlLoader.get(PARAM_SOURCE_CONTENT));
		assertTrue(content, content.contains("Group \"org.tinyjee.dim\""));
	}

	@Test
	public void testCanLoadJaxbClasses() throws Exception {
		requestParams = new HashMap<String, Object>(singletonMap(PARAM_XML_CLASS, SampleXmlType.class.getName() + ".getSampleInstance()"));
		doTestLoadingJaxbInstances();
	}

	public static JAXBElement<SampleXmlType> getSampleXmlTypeJAXBElement() {
		return new JAXBElement<SampleXmlType>(new QName("urn:org.tinyjee.dim", "sample"), SampleXmlType.class, SampleXmlType.getSampleInstance());
	}

	@Test
	public void testCanLoadJaxbElementClasses() throws Exception {
		requestParams = new HashMap<String, Object>(singletonMap(PARAM_XML_CLASS, getClass().getName() + ".getSampleXmlTypeJAXBElement()"));
		doTestLoadingJaxbInstances();
	}

	private void doTestLoadingJaxbInstances() throws Exception {
		requestParams.put(XmlLoader.PARAM_LOAD_SCHEMA, "local-only");
		setUp();

		String content = String.valueOf(xmlLoader.get(PARAM_SOURCE_CONTENT));
		JaxbXmlSerializer<SampleXmlType> serializer = new JaxbXmlSerializer<SampleXmlType>(SampleXmlType.class);
		Document expected = xmlLoader.beautifyDocument(serializer.serialize(SampleXmlType.getSampleInstance(), true), true);
		assertEquals(evaluator.serialize(expected), content);

		Set<String> expectedKeys = new HashSet<String>(asList("sample-xml-type.xsd", "sample-xml-type-2.xsd"));
		assertEquals(expectedKeys, ((Map) xmlLoader.get("schemaMap")).keySet());
	}

	@Test
	public void testCanSerializeAsJaxbToJson() throws Exception {
		requestParams = new HashMap<String, Object>(singletonMap(PARAM_XML_CLASS, SampleXmlType.class.getName() + ".getSampleInstance()"));
		requestParams.put(XmlLoader.PARAM_SERIALIZE_AS, "json");
		setUp();
		String content = String.valueOf(xmlLoader.get(PARAM_SOURCE_CONTENT));
		assertTrue(content, content.contains("\"@doIt\" : \"Yes\"") &&
				content.contains("\"activity\" : \"Include serialized Xml from JAXB root elements.\""));
	}

	@Test
	public void testCanRestrictLoadingToLocalSchemaOnly() throws Exception {
		requestParams = new HashMap<String, Object>(requestParams);
		requestParams.put(XmlLoader.PARAM_LOAD_SCHEMA, "local-only");
		setUp();

		Map schemaMap = (Map) xmlLoader.get("schemaMap");
		assertEquals(0, schemaMap.size());
	}

	@Test
	public void testCanLoadPublicSchema() throws Exception {
		requestParams = new HashMap<String, Object>(requestParams);
		requestParams.put(XmlLoader.PARAM_LOAD_SCHEMA, "true");
		setUp();

		Map schemaMap = (Map) xmlLoader.get("schemaMap");
		assertTrue(schemaMap.toString(), schemaMap.containsKey("http://maven.apache.org/maven-v4_0_0.xsd"));
	}
}
