<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:pom="http://maven.apache.org/POM/4.0.0">
	<xsl:template match="pom:project">
		<ul>
			<li>
				Project "<xsl:value-of select="pom:name"/>"
			</li>
			<li>
				Group "<xsl:value-of select="pom:groupId"/>"
			</li>
			<li>
				Version "<xsl:value-of select="pom:version"/>"
			</li>
		</ul>
	</xsl:template>
</xsl:stylesheet>
