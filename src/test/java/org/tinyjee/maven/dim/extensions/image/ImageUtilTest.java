/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.image;

import org.codehaus.classworlds.BytesURLStreamHandler;
import org.junit.Test;
import org.tinyjee.maven.dim.extensions.image.ImageUtil;

import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.Arrays;

import static org.junit.Assert.*;
import static org.tinyjee.maven.dim.extensions.image.ImageUtil.createImageFileContent;
import static org.tinyjee.maven.dim.extensions.image.ImageUtil.loadImage;
import static org.tinyjee.maven.dim.extensions.image.ImageUtil.scale;

/**
 * Tests ImageUtil.
 *
 * @author Juergen_Kellerer, 2012-05-25
 */
public class ImageUtilTest {

	BufferedImage image = new BufferedImage(124, 432, BufferedImage.TYPE_3BYTE_BGR);

	@Test
	public void testShrinkByBackgroundColor() throws Exception {
		image.getGraphics().drawOval(41, 32, 20, 30);

		final BufferedImage shrinkedImage = ImageUtil.shrinkByBackgroundColor(image, null);
		assertEquals(21, shrinkedImage.getWidth());
		assertEquals(31, shrinkedImage.getHeight());
	}

	@Test
	public void testCreateImageFileContent() throws Exception {
		for (String format : Arrays.asList("png", "jpg", "jpeg"))
			assertNotNull(createImageFileContent(image, format));
	}

	@Test
	public void testLoadImage() throws Exception {
		for (String format : Arrays.asList("png", "jpg", "jpeg")) {
			byte[] fileContent = createImageFileContent(image, format);
			URL imageUrl = new URL(null, "about:image", new BytesURLStreamHandler(fileContent));
			assertEquals(image.getWidth(), loadImage(imageUrl).getWidth());
			assertEquals(image.getWidth(), loadImage(fileContent).getWidth());
		}
	}

	@Test
	public void testScaleImage() throws Exception {
		// downscale
		assertSizeIs(12, 12, scale(image, 12, 12, false));
		assertSizeIs(3, 12, scale(image, 12, 12, true));
		assertSizeIs(12, 42, scale(image, 12, null, false));
		assertSizeIs(12, 42, scale(image, null, 42, false));

		// upscale
		assertSizeIs(1200, 1200, scale(image, 1200, 1200, false));
		assertSizeIs(344, 1200, scale(image, 1200, 1200, true));
		assertSizeIs(1240, 4320, scale(image, 1240, null, false));
		assertSizeIs(1240, 4320, scale(image, null, 4320, false));

		// equality
		assertSizeIs(124, 432, scale(image, null, null, false));
		assertSame(image, scale(image, null, null, false));
	}

	private void assertSizeIs(int width, int height, BufferedImage image) {
		assertEquals("width", width, image.getWidth());
		assertEquals("height", height, image.getHeight());
	}
}
