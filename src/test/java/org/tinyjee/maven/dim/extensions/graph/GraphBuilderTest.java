/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.graph;

import com.mxgraph.view.mxGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

/**
 * TODO: Describe Class
 *
 * @author Juergen_Kellerer, 2012-07-20
 */
public class GraphBuilderTest {

	GraphBuilder builder = new GraphBuilder();

	@Before
	public void setUp() throws Exception {
		builder.addVertex("v-a");
		builder.addVertex("v-b");
		builder.addVertex("v-c");
		builder.addVertex("v-d");

		builder.addEdge("v-a", "v-b");
		builder.addEdge("v-c", "v-b");
		builder.addEdge("v-b", "v-a");
		builder.addEdge("unrelated", "v-a");
		builder.addEdge("unrelated", "unrelated");
		builder.addEdge("v-b", "unrelated");

		builder.addToGroup("g-1", "v-a");
		builder.addToGroup("g-1", "v-b");
		builder.addToGroup("g-2", "v-c");
		builder.addToGroup("g-3", "unrelated");
	}

	@Test
	public void testCanGetVertexes() throws Exception {
		assertEquals(asList("v-a", "v-b", "v-c", "v-d"), builder.getVertexes());
		assertEquals(asList("v-a", "v-b", "v-c", "v-d", "g-1", "g-2", "g-3"), builder.getVertexes(true));
		assertEquals(asList("v-d", "g-1", "g-2", "g-3"), builder.getUnrelatedVertexes(true));
		assertEquals(asList("v-d"), builder.getUnrelatedVertexes());
	}

	@Test
	public void testCanRemoveVertex() throws Exception {
		builder.removeVertexes(builder.getUnrelatedVertexes());
		assertEquals(Collections.emptyList(), builder.getUnrelatedVertexes());

		builder.removeVertexes(builder.getEdges().selectNotMatchingFrom(".*b").toVertexes());
		assertEquals(asList("v-a", "v-c"), builder.getVertexes());
	}

	@Test
	public void testCanGetEdges() {
		assertEquals(asList("v-b"), builder.getEdges().selectFrom("v-a").toVertexes());
		assertEquals(asList("v-b"), builder.getEdges().selectFrom("v-c").toVertexes());
		assertEquals(asList("v-a", "v-c"), builder.getEdges().selectTo("v-b").fromVertexes());

		assertEquals(asList("v-a", "unrelated"), builder.getEdges().selectFrom("v-b").toVertexes());
		assertEquals(asList("v-a", "unrelated"), builder.getEdges().selectFrom("unrelated").toVertexes());
	}

	@Test
	public void testCanGetGroups() {
		assertEquals(asList("g-1", "g-2", "g-3"), builder.getGroups());
		assertEquals(asList("v-a", "v-b"), builder.getVertexesInGroup("g-1"));
		assertEquals(asList("v-c"), builder.getVertexesInGroup("g-2"));
		assertEquals(asList("unrelated"), builder.getVertexesInGroup("g-3"));
	}

	@Test
	public void testCanRemoveOrphaned() throws Exception {
		builder.removeOrphanedEdges();
		assertEquals(asList("v-a"), builder.getEdges().selectFrom("v-b").toVertexes());
		assertEquals(Collections.emptyList(), builder.getEdges().selectFrom("unrelated").toVertexes());

		builder.removeOrphanedGroups();
		assertTrue(builder.containsGroup("g-1"));
		assertTrue(builder.containsGroup("g-2"));
		assertFalse(builder.containsGroup("g-3"));
	}

	@Test
	public void testCanBuildTheGraph() throws Exception {
		builder.buildGraph(new mxGraph());
	}
}
