/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.image;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

import static org.junit.Assert.*;

/**
 * TODO: Describe Class
 *
 * @author Juergen_Kellerer, 2012-06-02
 */
public class ImageFilterSmokeTest {

	BufferedImage image = new BufferedImage(128, 128, BufferedImage.TYPE_INT_ARGB);

	@Before
	public void setUp() throws Exception {
		final Graphics2D graphics = image.createGraphics();
		try {
			graphics.setPaint(Color.BLUE);
			graphics.fillRect(0, 0, 128, 128);
			graphics.setPaint(Color.RED);
			graphics.fillRect(32, 32, 64, 64);
		} finally {
			graphics.dispose();
		}
	}

	@After
	public void tearDown() throws Exception {
		final File testFile = new File("target/filter-test.png");
		FileUtils.writeByteArrayToFile(testFile, ImageUtil.createImageFileContent(image, "png"));
	}

	@Test
	public void testClear() throws Exception {
		ClearFilter clearFilter = new ClearFilter();
		image = clearFilter.filter(image, null);
		clearFilter.setColor("green");
		image = clearFilter.filter(image, null);
	}

	@Test
	public void testInnerGlow() throws Exception {
		InnerGlowFilter glowFilter = new InnerGlowFilter();
		image = glowFilter.filter(image, null);
		//glowFilter.setColor("green");
		//image = glowFilter.filter(image, null);
	}

	@Test
	public void testTextOverlay() throws Exception {
		TextFilter textFilter = new TextFilter();
		assertNotNull(textFilter.filter(image, null));

		textFilter.setText("Ab");
		textFilter.setFont("serif-italic-60");
		textFilter.setColor("yellow");

		textFilter.setSmooth(true);
		textFilter.setAlign("right");
		textFilter.setVerticalAlign("bottom");
		textFilter.setAlpha(0.5f);
		textFilter.setRule("Hard Light");

		image = textFilter.filter(image, null);
	}


}
