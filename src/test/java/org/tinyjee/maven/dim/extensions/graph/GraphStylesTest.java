/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.graph;

import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;
import org.junit.Test;

import static java.lang.String.valueOf;
import static org.junit.Assert.*;
import static org.tinyjee.maven.dim.extensions.graph.GraphStyles.*;
import static org.tinyjee.maven.dim.extensions.graph.GraphStyles.getMappedStyles;

/**
 * Simple smoke test testing the basic functionality of the style handler GraphStyles.
 *
 * @author juergen kellerer, 2012-07-07
 */
public class GraphStylesTest {

	mxGraph graph = new mxGraph();

	@Test
	public void testDefineStyleClass() throws Exception {
		defineStyleClass(graph, "myStyle", "fontFamily=arial");
		defineStyleClass(graph, "myStyle1", "fontUnknown=y;myStyle");
		assertEquals(getMappedStyles("fontUnknown=y;fontFamily=arial"), graph.getStylesheet().getStyles().get("myStyle1"));
	}

	@Test
	public void testCanReDefineStyle() throws Exception {
		testDefineStyleClass();
		changeStyleClass(graph, "myStyle1", "fontUnknown=x;myStyle");
		assertEquals(getMappedStyles("fontUnknown=x;fontFamily=arial"), graph.getStylesheet().getStyles().get("myStyle1"));
	}

	@Test
	public void testPreProcessStyle() throws Exception {
		String[][] tests = {
				{"bold", valueOf(mxConstants.FONT_BOLD)},
				{"italic", valueOf(mxConstants.FONT_ITALIC)},
				{"underline", valueOf(mxConstants.FONT_UNDERLINE)},
				{"shadow", valueOf(mxConstants.FONT_SHADOW)},
		};

		for (String[] test : tests) {
			assertEquals(test[0], test[1], preProcessStyle(mxConstants.STYLE_FONTSTYLE, test[0]));
		}
	}

	@Test
	public void testCanLoadStylesheet() throws Exception {
		final mxStylesheet stylesheet = loadStyles("classpath:/org/tinyjee/maven/dim/extensions/graph/default-style.xml");
		assertEquals("white", stylesheet.getDefaultEdgeStyle().get("labelBackgroundColor"));
	}
}
