/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.junit.Test;
import org.tinyjee.maven.dim.AbstractIncludeMacroTest;
import org.tinyjee.maven.dim.IncludeMacroSignature;
import org.tinyjee.maven.dim.spi.Globals;

import java.io.File;
import java.lang.reflect.Field;

/**
 * Smoke tests the bundles extensions using the real macro call environment.
 *
 * @author Juergen_Kellerer, 2011-10-27
 */
public class ExtensionsSmokeTest extends AbstractIncludeMacroTest {
	@Test
	public void testExecuteWithTemplateAndSourceClass() throws Exception {
		params.put("source", "samples/hello-world.jsp.vm");
		params.put("source-class", "org.tinyjee.maven.dim.extensions.PropertiesLoader");
		params.put("properties", "samples/props.properties");
		macro.execute(output, request);

		assertBufferDoesNotContain("${title}");
		assertBufferContains("Hello World (from Properties file)");
	}

	@Test
	public void testExecuteWithTemplateAndSourceClassWithoutTrivialProperties() throws Exception {
		params.put("source", "interface-table.xdoc.vm");
		params.put("source-class", "org.tinyjee.maven.dim.extensions.JavaSourceLoader");
		params.put("interface", "org.tinyjee.maven.dim.IncludeMacroSignature");
		macro.execute(output, request);

		for (Field field : IncludeMacroSignature.class.getFields())
			assertBufferContains((String) field.get(null));
	}

	@Test
	public void testExecuteWithScriptInvoker() throws Exception {
		String text = "Hello World in UnitTest";
		params.put("source-script", "eval[js]:println('"+text+"'); print(scriptName);");
		if (!System.getProperty("java.version", "1.5").startsWith("1.5")) {
			macro.execute(output, request);
			assertBufferContains(
					text,
					Globals.getInstance().getBasedir() + File.separator + "local.js");
		}
	}

	@Test
	public void testBuildJaxRsReference() throws Exception {
		params.put("jaxrs-reference", "samples.JaxRsExampleService");
		macro.execute(output, request);
		assertBufferContains(
				"JaxRsExampleService", "Implements a sample JAX-RS service",
				"/example/..", "example/___id___");
	}
}
