/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Implements the test class for inheritance checks.
 *
 * @author Juergen_Kellerer, 2012-05-13
 */
public class InterfaceScannerTesterImpl extends InterfaceScannerTester implements Iterable {

	/**
	 * Comment in overriden method.
	 */
	@Override
	public void emptyIfMethod() {
	}

	@Override
	public String ifMethod(String param) {
		return null;
	}

	public enum MyEnum {
		a, b
	}

	public Iterator iterator() {
		return null;
	}

	public static class Inner extends InterfaceScannerTesterImpl {
		/**
		 * {@inheritDoc}
		 * <p/>
		 * Additional content in "InterfaceScannerTesterImpl.Inner".
		 */
		@Override
		public String ifMethod(String param) {
			return super.ifMethod(param);
		}
	}

	/**
	 * Special chars & non closed tags like <b> are fixed using HTML tidy.
	 */
	public static class InvalidComment extends InterfaceScannerTesterImpl {
	}

	private static void createElements() {
		// Do not modifiy, this tests $select.createdClasses!
		Map<String, Date> dates = new HashMap<String, Date>(new Integer(10));
		dates.put("a", new Date());
	}
}
