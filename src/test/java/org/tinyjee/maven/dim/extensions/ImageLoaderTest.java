/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.apache.maven.doxia.macro.MacroExecutionException;
import org.junit.Test;
import org.tinyjee.maven.dim.AbstractIncludeMacroTest;
import samples.ImageProcessor;

import java.awt.*;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * TODO
 *
 * @author juergen kellerer, 2012-05-28
 */
public class ImageLoaderTest extends AbstractIncludeMacroTest {

	static boolean processorWasCalled;

	public static class Processor extends ImageProcessor {
		@Override
		public void drawBorder(Map parameters) {
			super.drawBorder(parameters);
			processorWasCalled = true;
		}
	}

	public void setUp(String image) throws Exception {
		processorWasCalled =  false;
		params.put(ImageLoader.PARAM_ALIAS, image);
	}

	@Test
	public void testCanProcessImage() throws Exception {
		setUp("test/java/samples/ImageSample.png");
		params.put(ImageLoader.PARAM_PROCESSOR, "class:org.tinyjee.maven.dim.extensions.ImageLoaderTest$Processor.drawBorder()");
		assertEquals("<img src='attached-includes/test/java/samples/ImageSample.png' alt=''/>", callMacro().trim());
		assertTrue("processorWasNotCalled", processorWasCalled);
	}

	@Test
	public void testCanLoadFile() throws Exception {
		setUp("test/java/samples/ImageSample.png");
		assertEquals("<img src='attached-includes/test/java/samples/ImageSample.png' alt=''/>", callMacro().trim());
	}

	@Test
	public void testCanLoadBufferedImage() throws Exception {
		setUp("class:samples.ImageSample.getImage()");
		assertEquals("<img src='attached-includes/class_samples_ImageSample_getImage_.png' alt=''/>", callMacro().trim());
	}

	@Test
	public void testCanLoadScreenCapture() throws Exception {
		if (!GraphicsEnvironment.isHeadless()) {
			setUp("screen:[25,10,100,60]");
			try {
				callMacro();
				fail();
			} catch (MacroExecutionException e) {
				assertCauseIs(e, "The parameter 'attach' must be set with screen captures like screen:[25,10,100,60]");
			}

			params.put("attach", "my.png");
			assertEquals("<img src='attached-includes/my.png' alt=''/>", callMacro().trim());
		}
	}

	@Test
	public void testCanLoadImage() throws Exception {
		setUp("class:samples.ImageSample.getLoadedImage()");
		assertEquals("<img src='attached-includes/class_samples_ImageSample_getLoadedImage_.png' alt=''/>", callMacro().trim());
	}

	@Test
	public void testCanRenderJComponent() throws Exception {
		params.put(ImageLoader.PARAM_LNF, "native");
		setUp("class:samples.ImageSample.getPanel()");
		try {
			assertEquals("<img src='attached-includes/class_samples_ImageSample_getPanel_.png' alt=''/>", callMacro().trim());
		} catch (MacroExecutionException e) {
			assertCauseIs(e, HeadlessException.class); // ignoring HeadlessException, is thrown in Java5 when it is headless.
		}
	}

	@Test
	public void testCanRenderComponent() throws Exception {
		if (!GraphicsEnvironment.isHeadless()) {
			params.put(ImageLoader.PARAM_LNF, "native");
			params.put("text", "Hello World Frame!");
			setUp("class:samples.ImageSample.getFrame()");
			assertEquals("<img src='attached-includes/class_samples_ImageSample_getFrame_.png' alt=''/>", callMacro().trim());
		}
	}

	@Test
	public void testCanSwallowHeadlessException() throws Exception {
		params.put(ImageLoader.PARAM_NO_HEADLESS_EXCEPTIONS, "true");
		setUp("class:samples.ImageSample.throwHeadLessException()");
		assertEquals("<img src='attached-includes/class_samples_ImageSample_throwHeadLessException_.png' alt=''/>", callMacro().trim());
	}
}
