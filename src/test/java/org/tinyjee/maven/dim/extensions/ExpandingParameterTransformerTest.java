/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.codehaus.plexus.util.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.tinyjee.maven.dim.AbstractIncludeMacroTest;

import java.io.File;
import java.util.Map;

/**
 * TODO
 *
 * @author juergen kellerer, 2012-05-26
 */
public class ExpandingParameterTransformerTest extends AbstractIncludeMacroTest {

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@Before
	public void setUp() throws Exception {
		params.put("source-content", "$myParam");
	}

	@Test
	public void testSystemPropertyExpansion() throws Exception {
		System.setProperty("recursiveParam", "#system:[java.version]");
		params.put("myParam", "#system:[recursiveParam]");
		macro.execute(output, request);
		assertBufferContains(System.getProperty("java.version"));
	}

	@Test
	public void testEnvironmentExpansion() throws Exception {
		Map.Entry<String, String> entry = System.getenv().entrySet().iterator().next();
		params.put("myParam", "#env:[" + entry.getKey() + "]");
		macro.execute(output, request);
		assertBufferContains(entry.getValue());
	}

	@Test
	public void testFileLoading() throws Exception {
		File file = folder.newFile("content.txt");
		FileUtils.fileWrite(file.getAbsolutePath(), "MyContent");

		params.put("myParam", "#load:[" + file.getAbsolutePath() + "]");
		macro.execute(output, request);
		assertBufferContains("MyContent");
	}

	@Test
	public void testTestPropertiesLoading() throws Exception {
		File file = folder.newFile("content.properties");
		FileUtils.fileWrite(file.getAbsolutePath(), "a=b");

		params.put("myParam", "#load:[" + file.getAbsolutePath() + ":a]");
		macro.execute(output, request);
		assertBufferContains("b");
		assertBufferDoesNotContain("a");
	}
}
