/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.script;

import org.junit.Test;
import org.tinyjee.maven.dim.AbstractIncludeMacroTest;

import static org.junit.Assert.*;

/**
 * Tests the implementation of the CommonJS environment.
 *
 * @author Juergen_Kellerer, 2012-06-19
 */
public class ScriptEnvironmentCommonJSTest extends AbstractIncludeMacroTest {
	@Test
	public void testCommonJSRequire() throws Exception {
		String text = "|Hello World|";
		params.put("source-script", "eval[js]:println('|' + " +
				"require('src/test/java/org/tinyjee/maven/dim/extensions/script/helloWorld').helloWorld() + '|'); d = '123';");

		if (!System.getProperty("java.version", "1.5").startsWith("1.5")) {
			macro.execute(output, request);
			assertBufferContains(text);
			assertEquals("123", params.get("d"));

			assertFalse("environment is not clean", params.containsKey("module") ||
					params.containsKey("exports") || params.containsKey("dim_allRequiredExports"));
		}
	}
}
