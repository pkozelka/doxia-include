/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.i18n;

import org.apache.maven.doxia.macro.MacroExecutionException;
import org.codehaus.plexus.util.IOUtil;
import org.junit.Ignore;
import org.junit.Test;
import org.tinyjee.maven.dim.AbstractIncludeMacroTest;

import java.io.FileInputStream;
import java.io.IOException;

import static junit.framework.Assert.assertEquals;

/**
 * Verifies charset handling capabilities using test files like 'text.[charset].txt' as sources.
 *
 * @author Juergen_Kellerer, 2011-10-24
 */
public class I18NTest extends AbstractIncludeMacroTest {

	String[] encodings = {"UTF-8", "UTF-16LE", "UTF-16BE", "euc-jp"};

	private void doTest(boolean specifyCharset) throws MacroExecutionException, IOException {
		for (String encoding : encodings) {
			String path = "src/test/java/org/tinyjee/maven/dim/i18n/japanese." + encoding + ".txt";
			params.put("verbatim", "false");
			params.put("source", path);
			if (specifyCharset)
				params.put("charset", encoding);
			else
				params.remove("charset");

			callMacro(true);

			assertEquals("Encoding: " + encoding,
					stripAnyWhitespace(IOUtil.toString(new FileInputStream(path), encoding)),
					stripAnyWhitespace(outputBuffer.getBuffer().toString()));
		}
	}

	private static String stripAnyWhitespace(String content) {
		return content.replaceAll("[\\s]+", "");
	}

	@Test
	public void testInclusionWithCharsetExternallySet() throws Exception {
		doTest(true);
	}

	@Test
	public void testInclusionWithCharsetGuessed() throws Exception {
		params.put("charset-autodetect", "force");
		doTest(false);
	}

	@Test
	public void testTemplatesWithCharsetExternallySet() throws Exception {
		params.put("source-is-template", "true");
		doTest(true);
	}

	@Test
	public void testTemplatesWithCharsetGuessed() throws Exception {
		params.put("charset-autodetect", "force");
		params.put("source-is-template", "true");
		doTest(false);
	}
}
