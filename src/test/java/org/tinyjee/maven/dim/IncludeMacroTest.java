/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim;

import org.apache.maven.doxia.macro.MacroExecutionException;
import org.apache.maven.doxia.macro.manager.MacroManager;
import org.codehaus.plexus.PlexusConstants;
import org.codehaus.plexus.PlexusContainer;
import org.codehaus.plexus.context.Context;
import org.junit.Test;

import java.awt.*;
import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_SOURCE_CONTENT;

/**
 * Is the base test of the general macro features.
 *
 * @author Juergen_Kellerer, 2010-09-07
 */
public class IncludeMacroTest extends AbstractIncludeMacroTest {

	// START SNIPPET: version
	public class Version {
		final String version = "${projectVersion}";
	}
	// END SNIPPET: version

	// START SNIPPET: ColorSource
	public static class ColorSource extends HashMap<String, Object> {
		public ColorSource(File basePath, Map requestParameters) throws Exception {
			final String[] colors = requestParameters.get("colors").toString().toUpperCase().split(",");

			final List<List<String>> rows = new ArrayList<List<String>>();
			put("colors", rows);

			for (String colorName : colors) {
				Color color = (Color) Color.class.getField(colorName.trim()).get(null);
				rows.add(Arrays.asList(colorName, '#' + Integer.toHexString(color.getRGB()).substring(2)));
			}
		}
	}
	// END SNIPPET: ColorSource

	public static class ContentSource extends HashMap<String, Object> {
		public static final String VALUE = "Hello World! (from source-class)";

		public ContentSource() {
			put(PARAM_SOURCE_CONTENT, VALUE);
		}
	}

	public static class LegacyContentSource extends HashMap<String, Object> {
		public static final String VALUE = "Hello World! (from source-class, legacy)";

		public LegacyContentSource() {
			put("content", VALUE);
		}
	}

	@Test(expected = MacroExecutionException.class)
	public void testExecuteWithoutSource() throws Exception {
		macro.execute(output, request);
	}

	@Test
	public void testExecuteWithStandardInclude() throws Exception {
		params.put("source", "org/tinyjee/maven/dim/IncludeMacroTest.java");
		macro.execute(output, request);

		assertBufferContains("java ", "testExecuteWithStandardInclude", "highlighter");
	}

	@Test
	public void testCanUseClassNameAsSource() throws Exception {
		params.put("source", IncludeMacroTest.class.getName());
		macro.execute(output, request);

		assertBufferContains("java ", "testExecuteWithStandardInclude", "highlighter");
	}

	@Test
	public void testBasePathIsStrippedToFindSourceInSecondTurn() throws Exception {
		// This is a false path, but the resolution algorithm should strip the base path and retry.
		params.put("source", new File(".").getCanonicalPath() + "/org/tinyjee/maven/dim/IncludeMacroTest.java");
		macro.execute(output, request);

		assertBufferContains("java ", "testExecuteWithStandardInclude", "highlighter");
	}

	@Test
	public void testExecuteWithInlineSource() throws Exception {
		params.put("source-content", "Inline Test Content");
		macro.execute(output, request);

		assertBufferContains("Inline Test Content");
	}

	@Test
	public void testExecuteWithTemplateAndSourceClass() throws Exception {
		params.put("source", "samples/hello-world.jsp.vm");
		params.put("source-class", "org.tinyjee.maven.dim.extensions.PropertiesLoader");
		params.put("properties", "samples/props.properties");
		macro.execute(output, request);

		assertBufferDoesNotContain("${title}");
		assertBufferContains("Hello World (from Properties file)");
	}

	@Test
	public void testSourceClassThatProducesContent() throws Exception {
		params.put("source-class", "org.tinyjee.maven.dim.IncludeMacroTest$ContentSource");
		macro.execute(output, request);
		assertBufferContains(ContentSource.VALUE);
	}

	@Test
	public void testSourceClassThatProducesLegacyContent() throws Exception {
		params.put("source-class", "org.tinyjee.maven.dim.IncludeMacroTest$LegacyContentSource");
		macro.execute(output, request);
		assertBufferContains(LegacyContentSource.VALUE);
	}

	@Test
	public void testExecuteWithTemplateFile() throws Exception {
		params.put("file", "src/site/pom-usage-sample.xml.vm");
		params.put("moduleVersion", "##version-value##");
		macro.execute(output, request);

		assertBufferDoesNotContain("${moduleVersion}");
		assertBufferContains("##version-value##");
	}

	@Test
	public void testExecuteWithDisabledTemplate() throws Exception {
		params.put("source", "pom-usage-sample.xml.vm");
		params.put("source-is-template", "false");
		params.put("moduleVersion", "##version-value##");
		macro.execute(output, request);

		assertBufferContains("${moduleVersion}");
		assertBufferDoesNotContain("##version-value##");
	}

	@Test
	public void testExecuteWithDisabledHighlighting() throws Exception {
		params.put("source", "pom-usage-sample.xml.vm");
		params.put("highlight-type", "none");
		macro.execute(output, request);

		assertBufferDoesNotContain("highlighter");
	}

	@Test
	public void testExecuteNonVerbatim() throws Exception {
		params.put("source", "pom-usage-sample.xml.vm");
		params.put("verbatim", "false");
		macro.execute(output, request);

		assertBufferDoesNotContain("highlighter");
	}

	@Test
	public void testExecuteNonVerbatimParseableAptContent() throws Exception {
		params.put("source", "samples/table-template.apt.vm");
		params.put("rows", "n1,d1;n2,d2");
		macro.execute(output, request);

		assertBufferContains("<table", ">n1", ">d1", ">n2", ">d2", "</table>");
		assertBufferDoesNotContain("highlighter");
	}

	@Test
	public void testExtractTextParametersContainsAllTextParams() throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("text-key", "text");
		params.put("object-key", new Object());

		Map p = macro.extractTextParameters(params);
		assertEquals("text", p.get("text-key"));
		assertTrue(p.get("object-key") instanceof String);
	}

	@Test
	public void testFindsContentParsersAndConfiguresThem() throws Exception {
		Context context = mock(Context.class);
		PlexusContainer container = mock(PlexusContainer.class);
		when(context.get(PlexusConstants.PLEXUS_KEY)).thenReturn(container);
		macro.contextualize(context);

		String[] types = {"apt", "fml", "xdoc"};
		for (String type : types) {
			reset(container);

			params.put("source-content-type", type);
			assertTrue(macro.getContentParser(null, params).getClass().getName().toLowerCase().contains(type));
			params.remove("source-content-type");
			assertNull(macro.getContentParser(null, params));
			assertTrue(macro.getContentParser(new URL("http:///path/to/something." + type), params).
					getClass().getName().toLowerCase().contains(type));

			verify(container, times(2)).lookup(MacroManager.ROLE);
			verify(container, times(2)).release(null);
		}
	}
}
