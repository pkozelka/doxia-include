/*
 * Copyright 2011 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software distributed under the License
 *    is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and limitations under the License.
 */

/**
 * Creates the method "window.jsDim.newTabbedPanel(panelId, selectTitleCallback, configuration)"
 */
(function (window) {
	// Max age of a stored tab index key
	var maxStoredTabIndexAge = 60 * 60 * 1000;

	/**
	 * Creates a new tabbed panel.
	 *
	 * @param panelContainer The panel div to create the tabbed panel in.
	 * @param selectTitleCallback A callback of the format <code>function(title) { return true/false; }</code>
	 * @param config A object of the format: <code>{leftCornerImage: "",
	 *                      rightCornetImage: "", minHeight: "", hideTitles: true/false}</code>
	 */
	function newTabbedPanel(panelContainer, selectTitleCallback, config) {
		var tabbedPanel = $(panelContainer);
		var tableRow = tabbedPanel.find("tr").first();
		var tabIndexStorageKey = createTabIndexStorageKey(tabbedPanel.attr("id"));

		var indexedTabs = [], indexedTabContents = jsDim.protectSections(jsDim.findSections(tabbedPanel, selectTitleCallback));
		$.each(indexedTabContents, function (index, section) {
			var tabTitleElement = $(section).find("h1,h2,h3,h4,h5,h6").first();
			var tabTitle = tabTitleElement.text();
			if (config.hideTitles) tabTitleElement.addClass("inactive");
			if (config.minHeight != "")
				$(section).css("minHeight", /^[0-9]+$/.test(config.minHeight) ? config.minHeight + "px" : config.minHeight);

			indexedTabs[index] = $("<td class='inactive'></td>").text(tabTitle).data("index", index).click(function () {
				$("td.active", tableRow).addClass("inactive").removeClass("active");
				$("td.corner", tableRow).hide();
				$(this).addClass("active").removeClass("inactive");
				$(this).prev("td").show();
				$(this).next("td").show();

				var selectedTabIndex = $(this).data("index");
				tabbedPanel.data("selectedTabIndex", selectedTabIndex);
				$.jStorage.set(tabIndexStorageKey, {index:selectedTabIndex});
				$.jStorage.setTTL(tabIndexStorageKey, maxStoredTabIndexAge);

				for (var i = 0; i < indexedTabContents.length; i++) {
					var active = i == selectedTabIndex;
					indexedTabContents[i].addClass(active ? "active" : "inactive").removeClass(active ? "inactive" : "active");
				}
			});

			$(section).addClass("inactive");

			function cornerImageBuilder(src, cornerType) {
				return "<td class='corner " + cornerType + "' style='display:none;'><img src='" + src + "'/></td>";
			}

			tableRow.append(cornerImageBuilder(config.leftCornerImage, "left"));
			tableRow.append(indexedTabs[index]);
			tableRow.append(cornerImageBuilder(config.rightCornerImage, "right"));
		});

		// Add data to the tabbed-panel (div) to allow interaction via javascript.
		tabbedPanel.data("selectedTabIndex", -1);
		tabbedPanel.data("indexedTabs", indexedTabs);
		tabbedPanel.data("indexedTabContents", indexedTabContents);

		// Select one tab
		if (indexedTabs.length > 0) {
			// Select by stored selected index
			var selectedTabIndex = Math.min($.jStorage.get(tabIndexStorageKey, {index:0}).index, indexedTabs.length);
			indexedTabs[isNaN(selectedTabIndex) ? 0 : selectedTabIndex].click();

			// Select by anchor
			if(window.location.hash && window.location.hash.length > 1) {
				var name = window.location.hash.substring(1);
				$.each(indexedTabContents, function(index) {
					if ($('a[name="' + name + '"]', this).length > 0) indexedTabs[index].click();
				});
			}
		}
	}

	function createTabIndexStorageKey(panelId) {
		return "tab-index-" + document.location.href + "#" + panelId;
	}

	// Register in "window.jsDim"
	if (!window.jsDim) window.jsDim = {};
	window.jsDim.newTabbedPanel = newTabbedPanel;
})(window);
