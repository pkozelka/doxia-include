/*
 * Copyright 2011 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software distributed under the License
 *    is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and limitations under the License.
 */

RegExp.escape = function (text) {
	return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};

/**
 * Creates the methods "jsDim.jaxRs.toggleMethodDetails(button)" & "jsDim.jaxRs.prepareFormForSending(form)"
 */
(function (window) {

	var dirtyForms = [];
	var enc = encodeURIComponent;

	function toggleMethodDetails(button) {
		var label = $(button).text(), formRow = $(button).parents("tr").next("tr");
		if (label == "+") {
			$(button).text("-");
			formRow.show();
		} else {
			$(button).text("+");
			formRow.hide();
		}
	}

	function prepareFormForSending(form) {
		form = $(form);

		var originalAction = form.data("originalAction");
		if (!originalAction) form.data("originalAction", originalAction = form.attr("action"));

		form.attr("action", originalAction);
		storeFormParameters(form);

		$(".pathParam", form).each(function () {
			var action = form.attr("action");
			form.attr("action", action.replace(enc("___" + $(this).attr("name") + "___"), enc($(this).val())));
		});

		$(".matrixParam", form).each(function () {
			var action = form.attr("action"), expression = new RegExp("(;" + RegExp.escape(enc($(this).attr("name"))) + "=).+?($|;|?|/)");
			form.attr("action", action.replace(expression, "$1" + enc($(this).val()) + "$2"));
		});

		if (form.attr("method").toLowerCase() == "get") {
			$(".pathParam, .formParam, .matrixParam, .hostParam", form).attr("disabled", "true");
		} else {
			$(".pathParam, .queryParam, .matrixParam, .hostParam", form).attr("disabled", "true");

			$(".queryParam", form).each(function () {
				var action = form.attr("action"), delimiter = action.indexOf('?') == -1 ? '?' : '&';
				form.attr("action", action + delimiter + enc($(this).attr("name")) + "=" + enc($(this).val()));
			});
		}

		var host = $('.hostParam[name="__hostname"]', form).val();
		if (host) {
			host = host.replace(/[a-z]+::\/\//i, "");

			var isSSL = $('.hostParam[name="__ssl"]', form).is(':checked');
			var action = form.attr("action").replace("http://host:port/", "__baseUrl__"), baseUrl = host;

			var contextPathIndex = baseUrl.indexOf('/');
			if (contextPathIndex != -1) {
				action = action.replace(/(__baseUrl__)[^\/]+/, "$1" + baseUrl.substring(contextPathIndex + 1));
				baseUrl = baseUrl.substring(0, contextPathIndex);
			}

			baseUrl = (isSSL ? "https" : "http") + "://" + baseUrl + "/";
			form.attr("action", action.replace("__baseUrl__", baseUrl));

			// set the same host in all fields
			$('.hostParam[name="__hostname"]').val(host);
			$('.hostParam[name="__ssl"]').attr('checked', isSSL);
		}

		form.attr("accept", $('.hostParam[name="__accept"]', form).val());
		if ($('.hostParam[name="__encoding"]', form)) form.attr("enctype", $('.hostParam[name="__encoding"]', form).val());

		dirtyForms.push(form);
		setTimeout(cleanupDirtyForms, 500);
	}

	function cleanupDirtyForms() {
		while (dirtyForms.length != 0) {
			var form = dirtyForms.shift();
			$(".pathParam, .formParam, .queryParam, .matrixParam, .hostParam", form).removeAttr("disabled");
			if (form.data("originalAction")) form.attr("action", form.data("originalAction"));
		}
	}

	$(document).ready(function () {
		$("div.hidden-sample").each(function () {
			var div = $(this), id = div.attr("id");
			if (id.indexOf('sample-') == 0) {
				var frameBody = $('#' + id.substring(7)).contents().find('body');
				frameBody.html("<div style='padding:2px; border-bottom:1px solid gray;'>Response Example:</div>\n" + div.html());
				frameBody.append("<style>\n" +
						"  @import url('attached-includes/css/shCoreDefault.css'); \n" +
						"  body { font-size:9pt; background: white; font-family: sans-serif; } \n" +
						"  div.syntaxhighlighter { border: 0; } \n" +
						"  .syntaxhighlighter { overflow: visible !important; }\n" +
						"</style>");
			}
		});
	});

	$(document).ready(function () {
		$("form").each(function () {
			loadFormParameters(this);
		});
	});

	function getStorageKey(form) {
		form = $(form);
		return form.data("originalAction") ? form.data("originalAction") : form.attr("action");
	}

	function storeFormParameters(form) {
		var formData = {};
		$("input, select", form).each(function () {
			var name = $(this).attr("name");
			if (name) formData[name] = $(this).val();
		});
		$.jStorage.set(getStorageKey(form), formData);
	}

	function loadFormParameters(form) {
		var formData = $.jStorage.get(getStorageKey(form));
		if (formData) {
			$("input, select", form).each(function () {
				var name = $(this).attr("name");
				if (formData.hasOwnProperty(name)) $(this).val(formData[name]);
			});
		}
	}

	// Register in "window.jsDim"
	if (!window.jsDim) window.jsDim = {};
	if (!window.jsDim.jaxRs) window.jsDim.jaxRs = {};

	window.jsDim.jaxRs.toggleMethodDetails = toggleMethodDetails;
	window.jsDim.jaxRs.prepareFormForSending = prepareFormForSending;

})(window);
