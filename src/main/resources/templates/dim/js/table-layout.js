/*
 * Copyright 2011 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software distributed under the License
 *    is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and limitations under the License.
 */

/**
 * Creates the method "window.jsDim.createTableLayout(layoutContainerId, selectSectionTitleCallback, configuration)"
 */
(function(window) {
	/**
	 * Creates a new table layout for the given container.
	 */
	function createTableLayout(layoutElement, selectTitleCallback, configuration) {
		layoutElement = $(layoutElement);
		var sectionContents = jsDim.protectSections(jsDim.findSections(layoutElement, selectTitleCallback));

		var columns = Math.ceil(Math.max(1, configuration.columns == "*" ?
				sectionContents.length / (configuration.rows == "*" ? 1 : parseInt(configuration.rows)) :
				configuration.columns));
		var rows = Math.ceil(configuration.rows == "*" ? sectionContents.length / columns : configuration.rows);

		var verticalGap = calculateGap(configuration.gapVertical, calculateGap(configuration.gap, "5pt")),
				horizontalGap = calculateGap(configuration.gapHorizontal, calculateGap(configuration.gap, "5pt"));

		var layout = [];
		if (configuration.direction == "rtl" || configuration.direction == "ltr") {
			for (var y = 0, i = 0; i < sectionContents.length; i += columns) {
				layout[y] = [];
				for (var index, x = 0; x < columns; x++) {
					index = i + (configuration.direction == "ltr" ? x : columns - x - 1);
					layout[y][x] = index < sectionContents.length ? sectionContents[index] : null;
				}
				y++;
			}
		} else {
			for (var x = 0, i = 0; i < sectionContents.length; i += rows) {
				for (var y = 0; y < rows; y++) {
					if (!layout[y]) layout[y] = [];
					layout[y][x] = i + y < sectionContents.length ? sectionContents[i + y] : null;
				}
				x++;
			}
		}

		var columnWidths = interpolate(configuration.columnWidths ? configuration.columnWidths : [(100 / columns) + "%"], columns);
		var columnClasses = interpolate(configuration.columnClasses, columns);
		var rowHeights = interpolate(configuration.rowHeights, rows);
		var rowClasses = interpolate(configuration.rowClasses, rows);

		var table = layoutElement.find("table").first();
		for (var y = 0; y < layout.length; y++) {
			var tr = $("<tr class='table-layout-table'></tr>").appendTo(table);
			if (rowHeights[y]) tr.css("height", rowHeights[y]);
			if (rowClasses[y]) tr.addClass(rowClasses[y]);

			for (var content, x = 0, len = layout[y].length; x < len; x++) {
				if (!(content = layout[y][x])) continue;

				var width = (100 / columns) + "%";
				var td = $("<td class='table-layout-table'></td>").appendTo(tr).css("width", columnWidths[x]);
				td.css("paddingTop", y == 0 ? 0 : horizontalGap).css("paddingLeft", x == 0 ? 0 : verticalGap);
				td.css("paddingBottom", y == layout.length - 1 ? 0 : horizontalGap).css("paddingRight", x == len - 1 ? 0 : verticalGap);

				if (columnClasses[x]) td.addClass(columnClasses[x]);
				td.append(content.detach());

				// The last cell in a column can span over all remaining rows.
				var emptyCellsIndex = y + 1;
				for (; emptyCellsIndex < layout.length && !layout[emptyCellsIndex][x]; emptyCellsIndex++);
				if (emptyCellsIndex - y > 1) td.attr("rowSpan", emptyCellsIndex - y);
			}
		}
	}

	function calculateGap(inputGap, defaultGap) {
		if (/^([0-9]+)(.*)(;|$)/.test(inputGap)) return (parseInt(RegExp.$1) / 2) + RegExp.$2;
		return defaultGap;
	}

	function interpolate(input, newLength) {
		if (!(input instanceof Array)) input = ("" + input).split(/\s*,\s*/);
		for (var result = input; result.length < newLength; result = result.concat(input));
		return result;
	}

	// Register in "window.jsDim"
	if (!window.jsDim) window.jsDim = {};
	window.jsDim.createTableLayout = createTableLayout;
})(window);
