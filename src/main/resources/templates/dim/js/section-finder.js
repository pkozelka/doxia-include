/*
 * Copyright 2011 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software distributed under the License
 *    is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and limitations under the License.
 */

/**
 * Creates the method "window.jsDim.findSections(jQuery, selectSectionTitleCallback)"
 */
(function(window) {
	/**
	 * Creates a new tabbed panel.
	 *
	 * @param startElement An arbitrary element to start with the search for sections.
	 * @param selectSectionTitleCallback A callback of the format <code>function(title) { return true/false; }</code>
	 * @return An array of jQuery objects containing the matching sections.
	 */
	function findSections(startElement, selectSectionTitleCallback) {
		var theSectionWeAreIn = $(startElement).closest("div.section");

		var indexedSections = [], navigationType = 0, sections;
		while (indexedSections.length == 0) {
			switch (navigationType++) {
				case 0:
					// Find the first section below this and select siblings.
					sections = theSectionWeAreIn.find("div.section").not(".protected").first().nextAll("div.section").andSelf();
					break;
				case 1:
					// Looking for the section we're currently in and select siblings.
					sections = theSectionWeAreIn.nextAll("div.section").not(".protected");
					break;
				default:
					return [];
			}

			var index = 0;
			sections.each(function() {
				var tabTitle = $(this).find("h1,h2,h3,h4,h5,h6").first().text();
				if (selectSectionTitleCallback(tabTitle))
					indexedSections[index++] = $(this);
			});
		}

		return indexedSections;
	}

	/**
	 * Protects the given sections from being selected again.
	 * @param sections the sections to protect.
	 * @return the given list of sections after protecting them.
	 */
	function protectSections(sections) {
		$.each(sections, function(index, section) {
			$(section).addClass("protected");
		});
		return sections;
	}

	// Register in "window.jsDim"
	if (!window.jsDim) window.jsDim = {};
	window.jsDim.findSections = findSections;
	window.jsDim.protectSections = protectSections;
})(window);
