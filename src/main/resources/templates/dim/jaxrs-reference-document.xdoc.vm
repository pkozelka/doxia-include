<?xml version="1.0"?>
<!--
  ~ Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
  ~
  ~    Licensed under the Apache License, Version 2.0 (the "License");
  ~    you may not use this file except in compliance with the License.
  ~    You may obtain a copy of the License at
  ~
  ~        http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~    Unless required by applicable law or agreed to in writing, software distributed under the License
  ~    is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~    See the License for the specific language governing permissions and limitations under the License.
  -->
## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
## This template renders a method reference of multiple JAX-RS rest services that are selected via package
## or package class and the presence of an @Path annotation.
##
## Usage (in APT):
## %{include|source=classpath:/templates/dim/jaxrs-reference-document.xdoc.vm|classes=package.name|application-path=/path/to/jaxrs-app)}
##
## References:
##  - http://doxia-include.sourceforge.net/extensionsJavaSourceLoader.html
##  - http://velocity.apache.org/engine/releases/velocity-1.5/user-guide.html
##

## Imports - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##import classpath:/templates/dim/jaxrs-reference-commons.vm

#**
 * Creates a table containing all method parameters.
 *#
#macro(printMethodParamters $method)
	#extractMethodIOTypes($method $methodConsumes $methodProduces)

	#if($method.parameters.size() > 0)
	<table>
		<tr>
			<th>Parameter Type</th>
			<th>Name</th>
			<th>Required</th>
			<th>Java Type</th>
			<th style="white-space:nowrap;">Default / <br/> Fixed Values</th>
			<th width="66%">Description</th>
		</tr>
		#foreach($parameter in $method.parameters)
			#printMethodParameter($method $parameter $method.parameterAnnotations.get($parameter.name))
		#end
	</table>
	#else
	N/A
	#end
#end

#**
 * Prints a form row for the specified method parameter.
 *#
#macro(printMethodParameter $method $parameter $annotations)
	#set($typeName = "noParam")
	#if ($annotations.containsKey("@PathParam"))
		#set($typeName = "Path")   #set($paramName = $annotations.get("@PathParam").value)
	#elseif($annotations.containsKey("@QueryParam"))
		#set($typeName = "Query")  #set($paramName = $annotations.get("@QueryParam").value)
	#elseif($annotations.containsKey("@FormParam"))
		#set($typeName = "Form")   #set($paramName = $annotations.get("@FormParam").value)
	#elseif($annotations.containsKey("@MatrixParam"))
		#set($typeName = "Matrix") #set($paramName = $annotations.get("@MatrixParam").value)
	#end

	#if($typeName != "noParam")
		#set($isOptional = $annotations.containsKey("@DefaultValue"))
		#if($isOptional)
			#set($defaultValue = $annotations.get("@DefaultValue").value.replace('"', ''))
		#else
			#set($defaultValue = "")
		#end

		<tr>
			<td>$typeName</td>
			<td>$esc.xml($paramName)</td>
			<td>#if($isOptional) No #else Yes #end</td>
			<td>#dim_prettifyType($method.declaringClass, $parameter.type, $displayType) $esc.xml($displayType)</td>

			#if($parameter.typeClass.classType == "enum" && !$parameter.typeClass.fields.empty)
				<td>[
					#foreach($enumField in $parameter.typeClass.fields)
						#set($paramValue = $enumField.name)
						<span #if($defaultValue == $paramValue) style="font-weight:bold" #end>$esc.xml($paramValue)</span>,
					#end
				]</td>
			#else
				<td>$defaultValue</td>
			#end

			<td>$esc.xml($parameter.comment)</td>
		</tr>
	#end
#end

#**
 * Prints the rest methods from the specified list and method type (GET|POST|PUT|DELETE).
 *#
#macro(printRestMethods $methodList $methodsType)
	#foreach($method in $methodList)
		#set($methodPath = "")
		#createFormattedMethodPath($method $methodPath)
		#extractMethodIOTypes($method $methodConsumes $methodProduces)

		<h4>#printHeadline($method)</h4>
		<p>
			$method.comment
		</p>

		<p>
		<table>
			<tr>
				<td width="5%"><b>URI</b></td>
				<td width="95%">
					/$servicePath$methodPath
				</td>
			</tr>
			<tr>
				<td><b>METHOD</b></td>
				<td><b>$methodsType</b></td>
			</tr>
			<tr>
				<td><b>FORMAT</b></td>
				<td>
					Produces: $methodProduces<br/>
					Accepts: $methodConsumes
				</td>
			</tr>
			<tr>
				<td><b>PARAMETERS</b></td>
				<td>
					#printMethodParamters($method)
				</td>
			</tr>
			<tr>
				<td><b>RETURNS</b></td>
				<td>
					#if($method.tags.containsKey("return"))
						$method.tags.return
						<br/>
					#end
					#dim_prettifyType($method.declaringClass, $method.type, $displayType)
					(Java Type: $esc.xml($displayType))
				</td>
			</tr>
			#if($method.tags.containsKey("dim.example.request") || $method.tags.containsKey("dim.example.response"))
				<tr>
					<td><b>EXAMPLE</b></td>
					<td>
						#if($method.tags.containsKey("dim.example.request"))
							<b>Request $methodConsumes:</b>
							<br/>
							<br/>
							#printDataExample("" $method.tags.get("dim.example.request") $methodConsumes)
						#end
						#if($method.tags.containsKey("dim.example.response"))
							<b>Response $methodProduces:</b>
							<br/>
							<br/>
							#printDataExample("" $method.tags.get("dim.example.response") $methodProduces)
						#end
					</td>
				</tr>
			#end
		</table>
		</p>
	#end
#end

#macro(printHeadline $javaElement)
	#if($javaElement.tags.containsKey("dim.headline"))
		$esc.xml($javaElement.tags.get("dim.headline"))
	#elseif($javaElement.containsKey("simpleName"))
		$esc.xml("Service $javaElement.simpleName")
	#else
		$esc.xml("Service Method ${javaElement.name}(..)")
	#end
#end

<document>
	<body>
		## Select classes
		#includeClasses($includedClasses)

		#dim_attachCss(["dim/css/jaxrs-reference-document"], $cssSuccess)
		<div class="jaxrs-reference-document">
		#foreach($class in $includedClasses)

			#set($servicePath = $class.annotations.get("@Path").value.replace('"', ""))

			#collectMethodsAndProperties($class "javax.ws.rs.GET" $httpGetMethods)
			#collectMethodsAndProperties($class "javax.ws.rs.POST" $httpPostMethods)
			#collectMethodsAndProperties($class "javax.ws.rs.PUT" $httpPutMethods)
			#collectMethodsAndProperties($class "javax.ws.rs.DELETE" $httpDeleteMethods)

			<subsection name="#printHeadline($class)">
				<p>
					$class.comment
				</p>
				<p>
					<b>Service Base-URI:</b><br/>
<pre><code><span class="jaxrs-endpoint">http://host:port</span><span
class="jaxrs-service-path"><b>$application-path/$servicePath/..</b></span></code></pre></p>

				#if(!$httpGetMethods.empty)
					#printRestMethods($httpGetMethods "GET")
				#end
				#if(!$httpPostMethods.empty)
					#printRestMethods($httpPostMethods "POST")
				#end
				#if(!$httpPutMethods.empty)
					#printRestMethods($httpPutMethods "PUT")
				#end
				#if(!$httpDeleteMethods.empty)
					#printRestMethods($httpDeleteMethods "DELETE")
				#end
			</subsection>
		#end
		</div>
	</body>
</document>


