<!--
  ~ Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
  ~
  ~    Licensed under the Apache License, Version 2.0 (the "License");
  ~    you may not use this file except in compliance with the License.
  ~    You may obtain a copy of the License at
  ~
  ~        http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~    Unless required by applicable law or agreed to in writing, software distributed under the License
  ~    is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~    See the License for the specific language governing permissions and limitations under the License.
  -->
## - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
## Is a common macro library that hosts the shared functionality between jaxrs-reference-table.xdoc.vm and
## jaxrs-reference-document.xdoc.vm.
##
##

## Imports - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
##import classpath:/templates/dim/commons-class-selection.vm


## Set defaults
#if (!$apidocs) #set($apidocs = "./apidocs") #end
#if (!$application-path) #set($application-path = "") #end
#if (!$application-path.startsWith("/")) #set($application-path = "/$application-path") #end

## Select classes
#macro(includeClasses $outIncludedClasses)
	#set($outIncludedClasses = [])
	#selectClasses($classes $outIncludedClasses $modulePathPrefix)

	#if(!$strict) #set($outIncludedClasses = $outIncludedClasses.selectAnnotated("javax.ws.rs.Path")) #end
#end


#**
 * Creates the hyperlink to the 'apidocs' of the given java element that may be used within the
 * HREF or SRC attribute.
 *#
#macro(printApiDocsHref $javaElement $apiDocsPath)
	#if (!$apiDocsPath) #set ($apiDocsPath = "./apidocs") #end
	#if (!$javaElement.containsKey("type")) #set($__type = $javaElement.name) #else #set($__type = $javaElement.type) #end
		./$apiDocsPath/${__type.replace('.', '/')}.html
#end

#**
 * Creates the URL path that may be used to invoke the specified method.
 * The generated path contains HTML to highlight optional elements, param comments etc.
 * Use $display.stripTags to use the output as plain URL.
 *#
#macro(createFormattedMethodPath $method $outMethodPath)
	#set($outMethodPath = "")
	#if ($method.annotations.containsKey("@Path"))
		#set($outMethodPath = ${method.annotations.get('@Path').value.replace('"', '')})
		#if (!$outMethodPath.startsWith("/")) #set($outMethodPath = "/$outMethodPath") #end
		#set($outMethodPath = "<b>$outMethodPath</b>")
	#end

## Cycle through all parameters that are bound to this method.
	#set($query = "")
	#foreach($parameter in $method.parameters)

		#set($annotation = $method.parameterAnnotations.get($parameter.name))
		#set($paramComment = $display.stripTags($parameter.comment).replaceAll('\s+', ' '))

	## Add param comment from linked @PathParams into the method path.
		#if ($annotation.containsKey("@PathParam") && $paramComment != "")
			#set($pathParamName = $annotation.get('@PathParam').value.replace('"', ''))
			#set($outMethodPath = $outMethodPath.replace("{$pathParamName}", "<span class='has-title' title='$esc.xml($paramComment)'>{$pathParamName}</span>"))
		#end

	## Append all @MatrixParam annotated parameters.
		#if ($annotation.containsKey("@MatrixParam"))
			#addParamToUrl($annotation.get('@MatrixParam').value $paramComment $annotation ";" $outMethodPath)
		#end

	## Append all @QueryParam annotated parameters.
		#if ($annotation.containsKey("@QueryParam"))
			#if ($query.length() > 0) #set($queryDelimiter = "&#8203;&amp;") #else #set($queryDelimiter = "") #end
			#addParamToUrl($annotation.get('@QueryParam').value $paramComment $annotation $queryDelimiter $query)
		#end
	#end

	#if($query.length() > 0) #set($outMethodPath = "$outMethodPath?$query") #end
#end

#**
 * Adds a single matrix or query parameter to the specified URL.
 *#
#macro(addParamToUrl $name $comment $annotation $delimiter $outURL)
	#set($defaultValue=false)
	#if($annotation.containsKey("@DefaultValue"))
		#set($defaultValue=$esc.xml(${annotation.get("@DefaultValue").value.replace('"', '')}))
	#end

	#set($paramString=$esc.xml($name.replace('"', '')))
	#set($paramString="<i>$paramString</i>")
	#if($comment != "")
		#set($paramString="<span class='has-title' title='$esc.xml($comment)'>$paramString</span>")
	#end

	#if($defaultValue)
		#set($outURL = "${outURL}<span class='optional'>${delimiter}${paramString}=$defaultValue</span>")
	#else
		#set($outURL = "${outURL}${delimiter}${paramString}=...")
	#end
#end

#**
 * Extracts 2 lists of mime types describing what kind of data a method consumes and produces.
 *#
#macro(extractMethodIOTypes $method $outMethodConsumes $outMethodProduces)
	#set($outMethodProduces = ["*/*"])
	#if($method.annotations.containsKey("@Produces"))
		#dim_split($method.annotations.get("@Produces").value '[\[\],\s]+' $outMethodProduces)
	#elseif ($method.declaringClass.annotations.containsKey("@Produces"))
		#dim_split($method.declaringClass.annotations.get("@Produces").value '[\[\],\s]+' $outMethodProduces)
	#end
	#resolveMimeTypes($outMethodProduces)

	#set($outMethodConsumes = ["application/x-www-form-urlencoded"])
	#if($method.annotations.containsKey("@Consumes"))
		#dim_split($method.annotations.get("@Consumes").value '[\[\],\s]+' $outMethodConsumes)
	#elseif ($method.declaringClass.annotations.containsKey("@Consumes"))
		#dim_split($method.declaringClass.annotations.get("@Consumes").value '[\[\],\s]+' $outMethodConsumes)
	#end
	#resolveMimeTypes($outMethodConsumes)
#end

#set($mimeTypesTranslationTable = {
"WILDCARD": "*/*",
"APPLICATION_XML": "application/xml",
"APPLICATION_ATOM_XML": "application/atom+xml",
"APPLICATION_XHTML_XML": "application/xhtml+xml",
"APPLICATION_SVG_XML": "application/svg+xml",
"APPLICATION_JSON": "application/json",
"APPLICATION_FORM_URLENCODED": "application/x-www-form-urlencoded",
"MULTIPART_FORM_DATA": "multipart/form-data",
"APPLICATION_OCTET_STREAM": "application/octet-stream",
"TEXT_PLAIN": "text/plain",
"TEXT_XML": "text/xml",
"TEXT_HTML": "text/html"})

#macro(resolveMimeTypes $mimeTypesList)
	#set($__tempList = [])
	#foreach($mime in $mimeTypesList)
		#set($mime = $mime.replace('"', "").trim())
		#if($mime.length() > 0)
			#set($capturedSize = $__tempList.size())
			#foreach($mapping in $mimeTypesTranslationTable.entrySet())
				#if($mime.equals($mapping.key) || $mime.endsWith(".$mapping.key"))
					#dim_addToSet($mapping.value $__tempList)
				#end
			#end
			#if($capturedSize == $__tempList.size())
				#dim_addToSet($mime $__tempList)
			#end
		#end
	#end
	#set($mimeTypesList = $__tempList)
#end

#**
 * Prints a syntax highlighted box
 *#
#macro(printDataExample $divAttributes $exampleClass $exampleTypes)
	<div $divAttributes>
		#if($exampleTypes.contains('application/xml'))
			<div><i>application/xml</i></div>
			<macro name="include">
				<param name="source-xml" value="class:$esc.xml($exampleClass)"/>
				<param name="indent" value="true"/>
				<param name="show-gutter" value="false"/>
				<param name="line-width" value="100"/>
			</macro>
		#end
		#if($exampleTypes.contains('application/json'))
			<div><i>application/json</i></div>
			<macro name="include">
				<param name="source-xml" value="class:$esc.xml($exampleClass)"/>
				<param name="indent" value="true"/>
				<param name="show-gutter" value="false"/>
				<param name="serialize-as" value="json"/>
				<param name="line-width" value="100"/>
			</macro>
		#end
		#if(!$exampleTypes.contains('application/json') && !$exampleTypes.contains('application/xml'))
			<div><i>$exampleTypes</i></div>
			<macro name="include">
				<param name="source-class" value="$esc.xml($exampleClass)"/>
				<param name="show-gutter" value="false"/>
			</macro>
		#end
	</div>
#end

#**
 * Collects all methods and properties that are annotated with the specified annotation.
 *
 * Usage: #collectMethodsAndProperties($class "javax.ws.rs.GET" $httpGetMethods)
 *#
#macro(collectMethodsAndProperties $class $requiredAnnotation $outList)
	#set($__methods = $class.methods.selectAnnotated($requiredAnnotation))
	#foreach($propery in $class.properties.selectAnnotated($requiredAnnotation))
		#if($class.containsKey("javaLoader"))
			#if($propery.getter) #dim_addToList($class.javaLoader.asMap($propery.getter) $__methods) #end
			#if($propery.setter) #dim_addToList($class.javaLoader.asMap($propery.setter) $__methods) #end
		#end
	#end
	#set($outList = $__methods)
#end
