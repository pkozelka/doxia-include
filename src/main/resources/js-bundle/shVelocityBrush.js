/**
 * SyntaxHighlighter
 * http://alexgorbatchev.com/SyntaxHighlighter
 *
 * SyntaxHighlighter is donationware. If you are using it, please donate.
 * http://alexgorbatchev.com/SyntaxHighlighter/donate.html
 *
 * @version
 * 3.0.83 (July 02 2010)
 *
 * @copyright
 * Copyright (C) 2004-2010 Alex Gorbatchev.
 *
 * @license
 * Dual licensed under the MIT and GPL licenses.
 */
;
(function () {
	// CommonJS
	typeof(require) != 'undefined' ? SyntaxHighlighter = require('shCore').SyntaxHighlighter : null;

	function Brush() {
		this.regexList = [
			// one line comments => starting with ##
			{ regex:/##(.*)$/g, css:'comments' },
			// multiline comments
			{ regex:/#\*[\s\S]*?\*#/gm, css:'comments' },
			// strings
			{ regex:SyntaxHighlighter.regexLib.doubleQuotedString, css:'string' },
			// strings
			{ regex:SyntaxHighlighter.regexLib.singleQuotedString, css:'string' },
			// velocity tools 2.0
			{ regex:/\$(alternator|class|date|convert|display|esc|field|link|math|number|render|text|sorter|xml|loop|mark)/g, css:'color2' },
			// DIM specific velocity directives
			{ regex:/\$(globals|context|macroParameters)/g, css:'color1' },
			//// Methods
			//{ regex:/(?!\)|\w)\.\w+/g, css:'color1' },
			// velocity variables with $var syntax
			{ regex:/\$.+?(?=\W|$)/g, css:'variable' },
			// velocity variables with ${var} syntax
			{ regex:/\$\{.*?\}/gm, css:'variable' },
			// numbers & booleans
			{ regex:/\b([\d]+(\.[\d]+)?|0x[a-f0-9]+)\b/gi, css:'value' },
			{ regex:/\b(true|false)\b/gi, css:'value' },
			// keywords
			{ regex:/(#set|#if|#elseif|#else|#end|#foreach|#macro|#include|#parse|#stop|#break|#evaluate|#define)/g, css:'keyword' }
		];

		/*this.forHtmlScript({
			left:/(?=#|\$)/g,
			right:/(\)[^\.]|}|<|$)/g
		});*/

		// Experimental Warning: TODO: This still requires tweaking!
		this.htmlScript = {
			code : new XRegExp("" +
					"(?<left>(?=((^|[^\\*])#|##|\\$)))" +
					"(?<code>.*?(\\)(?=[^\\.\\)\\}])|}|(#|)end|(#|)else))" +
					"(?<right>.*?(\"|<|$))", "gm")
		};
	}

	Brush.prototype = new SyntaxHighlighter.Highlighter();
	Brush.aliases = ['velocity', 'vm'];

	SyntaxHighlighter.brushes.Velocity = Brush;

	// CommonJS
	typeof(exports) != 'undefined' ? exports.Brush = Brush : null;
})();
