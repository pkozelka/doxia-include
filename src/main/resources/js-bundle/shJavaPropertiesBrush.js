/**
 * Custom brush for SyntaxHighlighter (http://alexgorbatchev.com/)
 *
 * Use this brush to highlight Java .properties file syntax.
 *
 * Originally developed by "Vojtech.Szocs on Apr 20, 2009" taken from:
 * http://code.google.com/p/m2-site-tools/source/browse/trunk/stylus-ext-skin/src/main/syntaxhighlighter/scripts/shBrushJavaProperties.js
 */
;(function() {
	// CommonJS
	typeof(require) != 'undefined' ? SyntaxHighlighter = require('shCore').SyntaxHighlighter : null;

	function Brush() {
		this.regexList = [
			{ regex: /[#!].*$/gm, css: 'comments' },
			{ regex: /[:=].*$/gm, css: 'string' }
		];
	};
	Brush.prototype = new SyntaxHighlighter.Highlighter();
	Brush.aliases = ['properties'];

	SyntaxHighlighter.brushes.JavaProperties = Brush;

	// CommonJS
	typeof(exports) != 'undefined' ? exports.Brush = Brush : null;
})();