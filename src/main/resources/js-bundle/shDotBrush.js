/**
 * SyntaxHighlighter
 * http://alexgorbatchev.com/
 *
 * Very simple Graphviz brush
 * serge
 * { regex: new RegExp('\\\B\\\W[a-zA-Z0-9:\\\-]*\\\n\\\r','g'),           css: 'relation'},       // symbols
 * brush source: http://pastebin.com/s0xRrs8c
 */
;
(function () {
	// CommonJS
	typeof(require) != 'undefined' ? SyntaxHighlighter = require('shCore').SyntaxHighlighter : null;

	function Brush() {

		var keywords = 'node graph digraph strict edge subgraph ';
		var variables = 'URL arrowhead arrowsize arrowtail bb bgcolor bottomlabel center clusterrank color comment constraint decorate dir distortion ' +
				'fillcolor fixedsize fontcolor fontname fontsize group headclip headlabel headport height id label labelangle labeldistance labelfontcolor ' +
				'labelfontname labelfontsize layer layers margin mclimit minlen nodesep nslimit ordering orientation page agedir peripheries ' +
				'port_label_distance quantum rank rankdir ranksep ratio regular rotate samehead sametail searchsize shape shapefile showboxes ' +
				'sides size skew style tailclip taillabel tailport toplabel weight width';
		var values = 'Mcircle Mdiamond Mrecord Msquare auto back bold both box circle compress dashed diamond dot ' +
				'dotted doublecircle doubleoctagon egg ellipse epsf false fill filled forward global hexagon house ' +
				'inv invdot invhouse invis invodot invtrapezium invtriangle local max min none normal octagon ' +
				'odot out parallelogram plaintext polygon record same solid trapezium triangle tripleoctagon true';
		var colors = 'aliceblue antiquewhite aquamarine azure beige bisque black blanchedalmond blue blueviolet brown burlywood cadetblue ' +
				'chartreuse chocolate coral cornflowerblue cornsilk crimson cyan darkgoldenrod darkgreen darkkhaki darkolivegreen ' +
				'darkorange darkorchid darksalmon darkseagreen darkslateblue darkslategray darkturquoise darkviolet deeppink deepskyblue ' +
				'dimgray dodgerblue firebrick forestgreen gainsboro ghostwhite gold goldenrod gray green greenyellow honeydew hotpink ' +
				'indianred indigo ivory khaki lavender lavenderblush lawngreen lemonchiffon lightblue lightcyan lightgoldenrod ' +
				'lightgoldenrodyellow lightgray lightpink lightsalmon lightseagreen lightskyblue lightslateblue lightslategray ' +
				'lightyellow limegreen linen magenta maroon mediumaquamarine mediumblue mediumorchid mediumpurple mediumseagreen ' +
				'mediumslateblue mediumspringgreen mediumturquoise mediumvioletred midnightblue mintcream mistyrose moccasin navajowhite navy ' +
				'navyblue oldlace olivedrab oralwhite orange orangered orchid palegoldenrod palegreen paleturquoise palevioletred ' +
				'papayawhip peachpuff peru pink plum powderblue purple red rosybrown royalblue saddlebrown salmon salmon2 sandybrown ' +
				'seagreen seashell sienna skyblue slateblue slategray snow springgreen steelblue tan thistle tomato turquoise violet ' +
				'violetred wheat white whitesmoke yellow yellowgreen';
		var symbols = '[ ] { } - + * / < > ! ~ % & | = ';


		this.regexList = [
			{ regex:new RegExp(this.getKeywords(keywords), 'g'), css:'keyword'},
			{ regex:new RegExp(this.getKeywords(variables), 'g'), css:'variable'},
			{ regex:new RegExp(this.getKeywords(values), 'g'), css:'string'},
			{ regex:new RegExp(this.getKeywords(colors), 'g'), css:'string'},
			//{ regex:new RegExp(this.getKeywords(symbols), 'g'), css:'plain'},
			{ regex:SyntaxHighlighter.regexLib.multiLineCComments, css:'comments' },
			// multiline comments
			{ regex:SyntaxHighlighter.regexLib.singleLineCComments, css:'comments' },
			// one line comments
			{ regex:/[0-9]/gm, css:'constants'},
			{ regex:SyntaxHighlighter.regexLib.doubleQuotedString, css:'string'},
			{ regex:SyntaxHighlighter.regexLib.singleQuotedString, css:'string'}
		];
	}

	Brush.prototype = new SyntaxHighlighter.Highlighter();
	Brush.aliases = ['dot', 'graphviz'];

	SyntaxHighlighter.brushes.Dot = Brush;

	// CommonJS
	typeof(exports) != 'undefined' ? exports.Brush = Brush : null;
})();