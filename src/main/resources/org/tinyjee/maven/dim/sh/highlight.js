/*
 * Copyright 2010 - org.tinyjee.maven
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
 
/**
* Selects and instantiates an implementation of SyntaxHighlighter and uses this to highlight the given code.
*
* @param {String} brushName The name or alias name of the brush to use.
* @param {String} code The code to highlight.
* @param {Object} params A hash map containing config params that customize the behaviour of the highlighter.
* @return {String} The highlighted code using HTML markup.
*/
function highlightToHTML(brushName, code, params) {
	if (!code)
		throw "Input (code) is not defined";
	if (!brushName)
		throw "Brush name (brushName) is not defined.";
	if (!params)
		params = {};

	var highlighter;
	if (params['html-script'] == 'true' || params['html-script'] === true || SyntaxHighlighter.defaults['html-script']) {
		highlighter = new SyntaxHighlighter.HtmlScript(brushName);
		brushName = 'htmlscript';
	} else
		highlighter = newBrush(brushName);	

	params['brush'] = brushName;
	if (!params['toolbar'])
		params['toolbar'] = false; // disabling the toolbar, makes not much sense when rendered on server side.

	highlighter.init(params);

	return highlighter.getHtml(code);
};

/**
* Is a fork of the private function findBrush() below SyntaxHighlighter,
* that returns a new instance instead of the prototype.
*
* @param {String} brushName The name or alias name of the brush to use.
* @return {SyntaxHighlighter} A new instance of a highlighter.
*/
function newBrush(alias) {
	var brushes = SyntaxHighlighter.vars.discoveredBrushes, result = null;

	if (brushes == null) {
		brushes = {};

		// Find all brushes
		for (var brush in SyntaxHighlighter.brushes) {
			var info = SyntaxHighlighter.brushes[brush], aliases = info.aliases;
			if (aliases == null)
				continue;

			// keep the brush name
			info.brushName = brush.toLowerCase();

			for (var i = 0; i < aliases.length; i++)
				brushes[aliases[i]] = brush;
		}

		SyntaxHighlighter.vars.discoveredBrushes = brushes;
	}

	result = SyntaxHighlighter.brushes[brushes[alias]];
	if (result == null)
		throw "No brush found for '" + alias + "'";

	return new result();
};
