@echo off

set CP=${jython.jar}
if not "%CLASSPATH%" == "" (
  set CP=%CP%;%CLASSPATH%
)

"${java.home}\bin\java.exe" "-Dpython.executable=%0" "-Dpython.home=${python.home}" -classpath "%CP%" org.python.util.jython %*
