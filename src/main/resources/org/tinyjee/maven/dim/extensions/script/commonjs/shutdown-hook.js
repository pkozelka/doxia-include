/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

/**
 * Defines a small module that allows to register shutdown hooks
 * (= small functions that are executed after the normal script execution)
 */
(function () {
	var shutdownHooks = [];

	/**
	 * Is a list of functions that are called after the normal script operation terminated.
	 * @type {Array}
	 */
	exports.hooks = shutdownHooks;

	/**
	 * Runs all registered shutdown-hooks.
	 *
	 * This method is called by the script environment, do not call it by yourself.
	 */
	exports.run = function () {
		for (var i = 0; i < shutdownHooks.length; i++) {
			var hook = shutdownHooks[i];
			try {
				hook();
			} catch (e) {
				globals.log.error("Failed calling JS shutdown hook '" + hook + "' caused by ; " + e);
			}
		}
	};
})();

