/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

/**
 * Creates java to script and script to java converters for collections and maps.
 */
(function () {
	/**
	 * Converts the given java collection or map recursively to a JS array or object.
	 *
	 * @param javaObject A java collection or map.
	 * @param valueCallback An optional value filter using the signature <code>function(context, value) { return value; }</code>.
	 * @return {*} A JS array or object or the given object if it hasn't been a collection or map.
	 */
	function convertToJS(javaObject, valueCallback) {
		var result = javaObject;

		if (javaObject != null) {
			if (javaObject instanceof java.util.Collection) {
				result = [];
				var javaArray = javaObject.toArray();
				for (var i = 0; i < javaArray.length; i++)
					result.push(convertToJS(javaArray[i], valueCallback));

			} else if (javaObject instanceof java.util.Map) {
				result = {};
				var entry, iterator = javaObject.entrySet().iterator();
				while (iterator.hasNext()) {
					entry = iterator.next();
					result[entry.key] = convertToJS(entry.value, valueCallback);
				}
			}
		}

		return valueCallback ? valueCallback(javaObject, result) : result;
	}

	/**
	 * Converts the given javascript object or array recursively to a java map or list.
	 *
	 * @param object the javascript object to convert.
	 * @param valueCallback An optional value filter using the signature <code>function(context, value) { return value; }</code>.
	 * @return {*} a java list if the given object was an array, a java map otherwise.
	 */
	function convertToJava(object, valueCallback) {
		var result = object;

		if (object instanceof Array) {
			result = new java.util.ArrayList(object.length);
			for (var i = 0; i < object.length; i++)
				result.add(convertToJava(object[i], valueCallback));

		} else if (typeof object == 'object') {
			result = new java.util.LinkedHashMap();
			for (var key in object) {
				if (object.hasOwnProperty(key)) result.put(key, convertToJava(object[key], valueCallback));
			}
		}

		return valueCallback ? valueCallback(object, result) : result;
	}

	if (exports) {
		exports.convertToJS = convertToJS;
		exports.convertToJava = convertToJava;
	}
})();
