#!/bin/sh

CP="${jython.jar}"
if [ ! -z $CLASSPATH ] ; then
  CP=$CP:$CLASSPATH
fi

"${java.home}/bin/java" -Dpython.executable="$0" -Dpython.home="${python.home}" -classpath "$CP" org.python.util.jython "$@"
