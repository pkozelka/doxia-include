/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//
// This file is a JavaScript wrapper around the Java Image filter library provided by JHLabs.
//
// See: http://www.jhlabs.com/ip/filters/index.html for Reference, License and Copyright.
//
// The purpose of this script is to act as a general purpose processor that can be used in conjunction with ImageLoader
// to run image filters that derive from 'java.awt.image.BufferedImageOp'.
//
// Usage:
// %{include|image-source=image.png|attach=output.png|processor=/path/to/JHLabsFilterWrapper.js|filter=gaussian(radius:10)}
// %{include|image-source=image.png|attach=output.png|processor=/path/to/JHLabsFilterWrapper.js|filter=diffuse(scale:5), rotate(angle:10)}
//
// Available Filters:
// All filters provided within the library are available and accessible by name. E.g. if a filter is
// named like "DiffuseFilter" it may be accessed via "diffuse", "DiffuseFilter" and "com.jhlabs.image.DiffuseFilter".
//
// Filter properties:
// - Use "filterName(javaBeanProperty:value, anotherProperty:value)" to set any property that a filter exposes as bean property.
// Sharing / reusing properties:
// - Filter properties can be shared using "filter(property:prev.propertyName)" and "filter(property:filters[index].propertyName)".
// - Macro call properties can be used via "filter(property:req.propertyName)".
// - Configuration Holder: "c(r:10), gaussian(radius:c.r), unsharp(radius:c.r)"
// Swapping image buffers:
//   "shape(), swap(), light(environmentMap:prevImage)"
//   "c(), shape(), gaussian, swap(), light(environmentMap:c.image)"
//

importPackage(java.awt.image);

if (typeof packagePrefix === "undefined") packagePrefix = "com.jhlabs.image.,org.tinyjee.maven.dim.extensions.image.";
if (!(packagePrefix instanceof Array)) packagePrefix = packagePrefix.split(/[\s,;]/g);

// Functions ------------------------------------------------------------------

function newFilterInstance(name) {
	if (name.charAt(0).toLowerCase() == name.charAt(0)) name = name.substring(0, 1).toUpperCase() + name.substring(1) + "Filter";

	var classNames = [];
	if (name.indexOf('.') > -1) classNames.push(name);
	else {
		for (var i = 0; i < packagePrefix.length; i++) classNames.push(packagePrefix[i] + name);
	}

	var capturedException;
	for (var i = 0; i < classNames.length; i++) {
		globals.logger.debug("Looking up image filter: '" + classNames[i] + "'");
		try { return globals.classLoader.loadClass(classNames[i]).newInstance(); } catch (e) { capturedException = e; }
	}

	throw "Didn't find the requested filter '" + name + "', caused by: " + capturedException;
}

function configureFilter(filterInstance, configurationExpression) {
	globals.logger.debug("Configuring filter " + filterInstance + " using config = { " + configurationExpression + " };");

	eval("var config = { " + configurationExpression + " };");
	for (key in config) {
		if (config.hasOwnProperty(key)) filterInstance[key] = config[key];
	}
}

function copyImage(image) {
	var g, newImage = new BufferedImage(image.width, image.height, BufferedImage.TYPE_INT_ARGB);
	(g = newImage.createGraphics()).drawImage(image, 0, 0, null);
	g.dispose();
	return newImage;
}

// Processor Code -------------------------------------------------------------
// Converting the request map to something that is accessible as JS object
var req = require("java-utils").convertToJS(request);

// 'image' is an instance of BufferedImage
var image = req.image, filter = new String(req.filter);

if (!filter) throw "No filter has been specified, please set 'filter=...'.";

// Ensure source is BufferedImage.TYPE_INT_ARGB
if (image.type != BufferedImage.TYPE_INT_ARGB) image = copyImage(image);

// Apply all filters
var prev, prevImage = null, filters = [], c = {};
var filterExpressions = filter.split(/\)\s*,\s*/g);
for (var i = 0; i < filterExpressions.length; i++) {
	var nameAndParams = filterExpressions[i].split(/[()+]/g), filterName = nameAndParams[0], isConfigHolder = "c" == filterName;

	// Handle swapping of image buffers
	if (filterName == "swap") {
		var _i = prevImage;
		if (c.image) {
			_i = c.image;
			c.image = image;
		} else
			prevImage = image;

		image = _i;
		continue;
	}

	// Create instance
	var filterInstance = isConfigHolder ? {image:image} : newFilterInstance(filterName);

	// Build filter chain (for config sharing and holding)
	filters.push(filterInstance);
	if (filters[filterName]) filterInstance.prev = filters[filterName];
	filters[filterName] = filterInstance;

	// Configure and apply
	if (nameAndParams.length > 1) configureFilter(filterInstance, nameAndParams[1]);
	if (isConfigHolder)
		c = filterInstance;
	else {
		prevImage = image;
		try { image = filterInstance.filter(image, null);} catch (e) { throw "Filter '" + filterName + "' failed with: " + e;}
	}

	prev = filterInstance;
}
