/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

/*
 * Implements "http://wiki.commonjs.org/wiki/Modules/1.1".
 */

var exports = {}, module = {uri:scriptUri, id:scriptUri};

/**
 * Implements 'require()' as specified in "http://wiki.commonjs.org/wiki/Modules/1.1".
 *
 * @param moduleName the name of a module to load. (= relative file path excluding extension)
 * @return {*} The exports that are defined by the loaded module.
 */
var require = function (moduleName) {
	var currentScriptPath = scriptPath, modulePath = "" + (moduleName.charAt(0) != '.' ? moduleName : scriptPath + "/" + moduleName);

	if (modulePath.indexOf(".js") != modulePath.length - 3) modulePath += ".js";

	var cachedExports = require.modules[modulePath];
	if (cachedExports != null) return cachedExports;

	// Handle libraries within the library path
	if (!globals.isResolvable(modulePath)) {
		for (var i = 0; i < require.paths.length; i++) {
			var libraryModulePath = require.paths[i] + "/" + modulePath;
			if (globals.isResolvable(libraryModulePath)) {
				modulePath = libraryModulePath;
				break;
			}
		}
	}

	modulePath = globals.resolvePath(modulePath);
	cachedExports = require.modules[modulePath];
	if (cachedExports != null) return cachedExports;

	try {
		scriptPath = "" + modulePath.toString();
		if (scriptPath.indexOf("/") != -1) scriptPath = scriptPath.substring(0, scriptPath.lastIndexOf("/"));

		var exports = {}, moduleUri = "" + modulePath, module = {uri:moduleUri, id:moduleUri},
				moduleScript = "" + globals.fetchUrlText(modulePath);

		var __evalSuccess = false;
		try {
			eval(moduleScript);
			__evalSuccess = true;
			return require.modules[modulePath] = exports;
		} finally {
			if (!__evalSuccess) {
				println("");
				println("Failed evaluating script module " + modulePath + ":");
				println("---------------------------------------------------");
				var lines = moduleScript.replace("\r\n", "\n").replace("\r", "\n").split("\n");
				for (var i = 0; i < lines.length; i++) println(java.lang.String.format("%03.0f ", [i + 1]) + lines[i]);
				println("---------------------------------------------------");
			}
		}
	} finally {
		scriptPath = currentScriptPath;
	}
};

/**
 * Is a map of modules that have been required already.
 *
 * Requiring the same module multiple times returns the same exported instances.
 *
 * @type {Object}
 */
require.modules = {};

/**
 * Is a list of paths to look for modules when requiring them.
 *
 * If the required module is not found in this list of paths then, the resolution logic tries
 * to find the requested module relative to the location of the script that called require().
 *
 * @type {Array} A list of paths to look for modules to require.
 */
require.paths = ["classpath:/org/tinyjee/maven/dim/extensions/script/commonjs"];

/**
 * Registering a shutdown hook to clean the script context before the script interpreter ends.
 */
require("shutdown-hook").hooks.push(function() {
	delete exports;
	delete module;
	require.modules = {};
});
