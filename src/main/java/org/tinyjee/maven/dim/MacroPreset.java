/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim;

import org.codehaus.plexus.util.StringUtils;

import java.util.*;

/**
 * Holds macro presets as definable within {@link InitializeMacroMojo}.
 *
 * @author Juergen_Kellerer, 2012-02-21
 */
public class MacroPreset {

	static final String KEY_PREFIX = "org.tinyjee.maven.dim.preset.";
	static final String ALIAS_KEY = "org.tinyjee.maven.dim.preset-aliases";

	private String name = "";
	private boolean alias;
	private Map<String, String> properties = new HashMap<String, String>();

	public MacroPreset() {
	}

	public MacroPreset(String name, boolean alias) {
		this.name = name;
		this.alias = alias;
	}

	public String getName() {
		return name;
	}

	public boolean isAlias() {
		return alias;
	}

	public Map<String, String> getProperties() {
		return properties;
	}

	void encodeTo(Properties props) {
		props.setProperty(KEY_PREFIX + name, "true");
		if (alias) {
			final List<String> aliases = getAliases(props);
			if (!aliases.contains(name)) {
				aliases.add(name);
				props.setProperty(ALIAS_KEY, StringUtils.join(aliases.iterator(), ","));
			}
		}

		for (Map.Entry<String, String> entry : properties.entrySet())
			props.put(KEY_PREFIX + name + '.' + entry.getKey(), entry.getValue());
	}

	static List<String> getAliases(Properties props) {
		List<String> aliases = new ArrayList<String>();

		final StringTokenizer tokenizer = new StringTokenizer(props.getProperty(ALIAS_KEY, ""), ",");
		while (tokenizer.hasMoreTokens())
			aliases.add(tokenizer.nextToken());

		return aliases;
	}

	static MacroPreset decodeFrom(String name, Properties props) {
		String prefix = KEY_PREFIX + name;
		if (!Boolean.getBoolean(prefix)) return null;

		boolean alias = getAliases(props).contains(name);
		final MacroPreset preset = new MacroPreset(name, alias);
		prefix += ".";
		for (Map.Entry<Object, Object> entry : props.entrySet()) {
			final String key = entry.getKey().toString();
			if (key.startsWith(prefix))
				preset.getProperties().put(key.substring(prefix.length()), String.valueOf(entry.getValue()));
		}

		return preset;
	}
}
