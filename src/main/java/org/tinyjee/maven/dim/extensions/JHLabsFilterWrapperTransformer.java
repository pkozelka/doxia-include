/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.spi.ExtensionInformation;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.RequestParameterTransformer;

import java.util.Map;

import static org.tinyjee.maven.dim.extensions.AbstractParameterTransformer.makeUniqueName;

/**
 * Provides support for standard image filters that may be used within {@link ImageLoader}.
 * <p/>
 * Technically it is implemented via a bundled javascript image processor that can configure and use image filters
 * that derive from {@link java.awt.image.BufferedImageOp} and expose all configuration parameters as java bean properties.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|image-source=image.png|jh-filter=gaussian}
 * %{include|image-source=create:text-effect.png|width=200|height=40|jh-filter=text(font:"serif-italic-30", text:"Hello World"), shadow()}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 *
 * @author Juergen_Kellerer, 2012-05-30
 */
@ExtensionInformation(displayName = "JHLabs Filter Wrapper", documentationUrl = "/templatesImageProcessing.html")
public class JHLabsFilterWrapperTransformer implements RequestParameterTransformer {

	private static final Log log = Globals.getLog();
	private static final String IMAGE_PROCESSOR = "script:classpath:/org/tinyjee/maven/dim/extensions/JHLabsFilterWrapper.js";
	private static final String IMAGE_PROCESSOR_FILTER_PARAM = "filter";

	/**
	 * Sets one or more filter operations to apply to the loaded image using the Java image filter library provided by JHLabs.
	 * <p/>
	 * Available Filters:
	 * All filters provided within the library are available and accessible by name.
	 * E.g. if a filter is named "com.jhlabs.image.DiffuseFilter" it may be accessed via "diffuse", "DiffuseFilter" and
	 * its fully qualified class name.
	 * <p/>
	 * Filter properties:
	 * Filters that implement properties using the JavaBean standard can be setup using the following format:
	 * "<code>filterName(javaBeanProperty:value, anotherProperty:value)</code>"
	 * <p/>
	 * Examples:<ul>
	 * <li><code>%{include|image-source=image.png|jh-filter=gaussian}</code></li>
	 * <li><code>%{include|image-source=image.png|jh-filter=gaussian(radius:10)}</code></li>
	 * <li><code>%{include|image-source=image.png|jh-filter=diffuse(scale:5), rotate(angle:10)}</code></li>
	 * </ul>
	 * <p/>
	 * For a reference on available filters and filter properties, visit:
	 * <a href="http://www.jhlabs.com/ip/filters/index.html">http://www.jhlabs.com/ip/filters/index.html</a>
	 */
	public static final String PARAM_JH_FILTER = "jh-filter";

	/**
	 * Same as "jh-filter" but run as post processor.
	 */
	public static final String PARAM_JH_POST_FILTER = "jh-post-filter";


	public void transformParameters(Map<String, Object> params) {
		final Object imageSource = params.get(ImageLoader.PARAM_IMAGE),
				filterExpression = params.get(PARAM_JH_FILTER), postFilterExpression = params.get(PARAM_JH_POST_FILTER);
		if (imageSource != null && (filterExpression != null || postFilterExpression != null)) {
			params.put(filterExpression == null ? ImageLoader.PARAM_POST_PROCESSOR : ImageLoader.PARAM_PROCESSOR, IMAGE_PROCESSOR);
			params.put(IMAGE_PROCESSOR_FILTER_PARAM, filterExpression == null ? postFilterExpression : filterExpression);

			Object attach = params.get(ImageLoader.PARAM_ATTACH);
			if (attach == null) {
				String source = makeUniqueName(imageSource, filterExpression);

				if (log.isDebugEnabled()) log.debug("Fixing missing unique name for attaching the image, using '" + source + '\'');
				params.put(ImageLoader.PARAM_ATTACH, source);
			}
		}
	}
}
