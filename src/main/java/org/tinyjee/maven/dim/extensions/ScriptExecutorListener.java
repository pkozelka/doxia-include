/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.tinyjee.maven.dim.spi.backport.ServiceLoader;

/**
 * Defines an interface for implementations that listen to script executions.
 *
 * @author Juergen_Kellerer, 2012-06-18
 */
public interface ScriptExecutorListener {
	/**
	 * Iterates all registered listener implementations.
	 */
	Iterable<ScriptExecutorListener> LISTENERS = ServiceLoader.load(ScriptExecutorListener.class);

	/**
	 * Is called just before the actual script is executed inside the script engine.
	 *
	 * @param scriptExecutor  the executor that runs the script.
	 * @param scriptExtension the script file extension.
	 * @param scriptContent   the script.
	 * @param scriptName      the absolute file path of the script.
	 */
	void beforeExecute(ScriptExecutor scriptExecutor, String scriptExtension, String scriptContent, String scriptName);

	/**
	 * Is called after the actual script was executed inside the script engine.
	 *
	 * @param scriptExecutor  the executor that runs the script.
	 * @param scriptExtension the script file extension.
	 * @param scriptContent   the script.
	 * @param scriptName      the absolute file path of the script.
	 */
	void afterExecute(ScriptExecutor scriptExecutor, String scriptExtension, String scriptContent, String scriptName);
}
