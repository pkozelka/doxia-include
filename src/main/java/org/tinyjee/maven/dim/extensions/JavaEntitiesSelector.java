/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.tinyjee.maven.dim.extensions.utils.JavaEntitiesList;

/**
 * Defines a selector that can be used to access additional entities that relate to the loaded java class.
 * <p/>
 * <b>Usage Examples:</b>
 * <code><pre>
 * #foreach($class in $class.select.packageClasses.selectMatching("public ..*"))
 *    Public class $class.name in package "$class.javaClass.packageName"
 * #end
 * </pre></code>
 * <code><pre>
 * #foreach($annotatedClass in $class.select.annotatedClasses.sort())
 *    $annotatedClass.name is annotated with $class.name
 * #end
 * </pre></code>
 */
public interface JavaEntitiesSelector {
	/**
	 * Returns a selectable list of all classes in the same package.
	 *
	 * @return a selectable list of all classes in the same package.
	 */
	JavaEntitiesList getPackageClasses();

	/**
	 * Returns a selectable list of all classes that are below or in the same package.
	 *
	 * @return a selectable list of all classes that are below or in the same package.
	 */
	JavaEntitiesList getClassesBelowPackage();

	/**
	 * Returns a selectable list of nested classes (inner classes).
	 *
	 * @return a selectable list of nested classes (inner classes).
	 */
	JavaEntitiesList getNestedClasses();

	/**
	 * Returns a selectable list of all super classes (vector up to java.lang.Object).
	 *
	 * @return a selectable list of all super classes (vector up to java.lang.Object).
	 */
	JavaEntitiesList getSuperClasses();

	/**
	 * Returns a selectable list of this class and all super classes (vector up to java.lang.Object).
	 *
	 * @return a selectable list of this class and all super classes (vector up to java.lang.Object).
	 * @since 1.2
	 */
	JavaEntitiesList getSelfAndSuperClasses();

	/**
	 * Returns a selectable list of all implemented interfaces.
	 * <p/>
	 * Note: Interfaces that extend other interfaces actually "implement" them and will
	 * be listed in this list instead of superClasses (even if the keyword inside the code is extends).
	 *
	 * @return a selectable list of all implemented interfaces.
	 */
	JavaEntitiesList getImplementedInterfaces();

	/**
	 * Returns a collection of all parent classes that this class implements or derives from.
	 * <p/>
	 * The collection returned by this method contains all classes that are either directly or indirectly implemented
	 * or are super classes. Effectively this is the opposite of {@link #getDerivedClasses()}.
	 * <p/>
	 * Note: This method does not return marker interfaces or base classes that do not contain methods.
	 *
	 * @return a collection of all parent classes that this class implements or derives from.
	 * @since 1.2
	 */
	JavaEntitiesList getParentClasses();

	/**
	 * Returns a collection of this class and all parent classes that this class implements or derives from.
	 * <p/>
	 * The collection returned by this method contains all classes that are either directly or indirectly implemented
	 * or are super classes. Effectively this is the opposite of {@link #getDerivedClasses()}.
	 * <p/>
	 * Note: This method does not return marker interfaces or base classes that do not contain methods.
	 *
	 * @return a collection of this class and all parent classes that this class implements or derives from.
	 * @since 1.2
	 */
	JavaEntitiesList getSelfAndParentClasses();

	/**
	 * Returns a selectable list of all classes that are created within this class (where {@code new ClassName(...)} is called).
	 *
	 * @return a selectable list of all classes that are created within this class (where {@code new ClassName(...)} is called).
	 * @since 1.2
	 */
	JavaEntitiesList getCreatedClasses();

	/**
	 * Returns a selectable list of all known classes that derive either directly or in-directly from this class.
	 *
	 * @return a selectable list of all known classes that derive either directly or in-directly from this class.
	 */
	JavaEntitiesList getDerivedClasses();

	/**
	 * If this class is an annotation, it returns a selectable list of all known classes that are directly annotated
	 * with this class.
	 * An {@link IllegalStateException} will be thrown if the property is accessed on non-Annotation classes.
	 *
	 * @return a selectable list of all known classes that are directly annotated with this class..
	 */
	JavaEntitiesList getAnnotatedClasses();
}
