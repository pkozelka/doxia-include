/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.graph;

import org.tinyjee.maven.dim.spi.Globals;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import static java.util.Collections.singletonMap;
import static org.tinyjee.maven.dim.extensions.graph.GraphBuilder.GRAPH_PROPERTY_LABEL;
import static org.tinyjee.maven.dim.extensions.graph.GraphBuilder.GRAPH_PROPERTY_STYLE;

/**
 * Trivial parser for the TGF (Trivial graph format).
 * <p/>
 * In addition to the default syntax, this parser allows to encode a node and edge style definition into the
 * edge or node labels using {@code {@style styleClass;styleAttribute=value;...}}.
 *
 * @author Juergen_Kellerer, 2012-07-22
 */
public class TrivialGraphFormatParser {

	boolean supportNonStandardStyleSyntax;
	private static final Pattern WHITESPACE = Pattern.compile("\\s+");

	public TrivialGraphFormatParser(boolean supportNonStandardStyleSyntax) {
		this.supportNonStandardStyleSyntax = supportNonStandardStyleSyntax;
	}

	public void parseGraph(URL graphSource, GraphBuilder builder) throws IOException {
		parseGraph(Globals.getInstance().fetchUrlText(graphSource), builder);
	}

	public void parseGraph(String graphData, GraphBuilder builder) {
		final StringTokenizer tokenizer = new StringTokenizer(graphData, "\r\n");

		boolean isEdgeMode = false;
		while (tokenizer.hasMoreTokens()) {
			final String token = tokenizer.nextToken();

			if ("#".equals(token.trim()))
				isEdgeMode = true;
			else {
				int maxParts = isEdgeMode ? 3 : 2;
				String[] parts = WHITESPACE.split(token, maxParts);
				Map<String, Object> properties = buildProperties(maxParts, parts);

				if (isEdgeMode)
					builder.addEdge(parts[0], parts[1], properties);
				else
					builder.addVertex(parts[0], properties);
			}
		}
	}

	private Map<String, Object> buildProperties(int maxParts, String[] parts) {
		Map<String, Object> properties = null;

		if (parts.length == maxParts) {
			String label = parts[maxParts - 1];
			if (supportNonStandardStyleSyntax && label.contains("{@")) {
				properties = tokenize(label);
			} else {
				if (supportNonStandardStyleSyntax) label = label.replace("\\n", "\n");
				properties = singletonMap(GRAPH_PROPERTY_LABEL, (Object) label);
			}
		}

		return properties;
	}

	private static Map<String, Object> tokenize(String line) {
		line = line.trim();

		int index = 0;
		final StringBuilder label = new StringBuilder();
		final Map<String, Object> result = new HashMap<String, Object>();

		while (index < line.length()) {
			final int nextIndex = line.indexOf("{@", index);

			if (nextIndex > index)
				label.append(line.substring(index, nextIndex));

			if (nextIndex == -1) {
				label.append(line.substring(index));
				break;
			} else {
				index = line.indexOf('}', nextIndex);
				if (index == -1) break;

				String[] tag = WHITESPACE.split(line.substring(nextIndex + 2, index), 2);
				result.put(tag[0], tag[tag.length == 2 ? 1 : 0]);
				index++;
			}
		}

		if (label.length() > 0) result.put(GRAPH_PROPERTY_LABEL, label.toString().trim().replace("\\n", "\n"));

		return result;
	}
}
