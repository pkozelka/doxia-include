/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.apache.maven.doxia.logging.Log;
import org.codehaus.plexus.util.FileUtils;
import org.tinyjee.maven.dim.extensions.image.ImageCapture;
import org.tinyjee.maven.dim.extensions.image.ImageUtil;
import org.tinyjee.maven.dim.spi.ExtensionInformation;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.ResourceResolver;
import org.tinyjee.maven.dim.utils.AbstractAliasHandler;
import org.tinyjee.maven.dim.utils.ClassUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;
import static org.tinyjee.maven.dim.IncludeMacroSignature.*;

/**
 * Loads and transcodes image content and includes it.
 * <p/>
 * In addition to image file loading this loader supports also invoking classes that generate dynamic images
 * see the "{@code image}" parameter below.
 * <p/>
 * This class makes use of {@link javax.imageio.ImageIO} for reading and writing image files, therefore it supports any
 * format that ImageIO offers. Format support can be extended by loading extensions to ImageIO.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|source-image=file.png}
 * %{include|source-image=class:my.app.Screens.getMainWindow()|lnf=nimbus|attach=mainWindow.png}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 * Full call syntax:<code><pre>
 * %{include|source-class=org.tinyjee.maven.dim.extensions.ImageLoader|image=file.(png|jpeg|tiff)}
 * </pre></code>
 *
 * @author Juergen_Kellerer, 2011-11-05
 */
@ExtensionInformation(displayName = "Image Loader", documentationUrl = "/extensionsImageLoader.html")
public class ImageLoader extends HashMap<String, Object> {

	private static final Log log = Globals.getLog();
	private static final long serialVersionUID = -6282606147512841025L;

	private static final String CLASS_PREFIX = "class:";
	private static final String SCREEN_PREFIX = "screen:";
	private static final String SCRIPT_PREFIX = "script:";
	private static final String CREATE_PREFIX = "create:";

	/**
	 * Utility method that loads the given image content.
	 *
	 * @return a buffered image containing the rendered output.
	 * @throws Exception in case of rendering failed.
	 */
	public static BufferedImage load(Object image, Map<String, Object> parameters) throws Exception {
		if (parameters == null) parameters = Collections.emptyMap();
		parameters = new HashMap<String, Object>(parameters);

		parameters.put(PARAM_ATTACH, false);
		if (image != null) parameters.put(PARAM_IMAGE, image);

		ImageLoader loader = new ImageLoader(Globals.getInstance().getBasedir(), parameters);
		return (BufferedImage) loader.get(OUT_PARAM_IMAGE);
	}

	/**
	 * Implements the "{@link org.tinyjee.maven.dim.extensions.ImageLoader#PARAM_ALIAS}" alias functionality.
	 */
	public static class AliasHandler extends AbstractAliasHandler {
		/**
		 * Constructs the handler (Note: Is called by {@link org.tinyjee.maven.dim.spi.RequestParameterTransformer#TRANSFORMERS}).
		 */
		public AliasHandler() {
			super(PARAM_ALIAS, PARAM_IMAGE, ImageLoader.class.getName());
		}
	}

	/**
	 * Defines a shortcut for the request properties 'source-class' and 'image'.
	 * <p/>
	 * If this parameter is present inside the request parameters list, the system will effectively behave as if
	 * 'source-class' and 'image' where set separately.
	 */
	public static final String PARAM_ALIAS = "source-image";

	/**
	 * Sets the URL, local path or expression to the image resource to load or create.
	 * <p/>
	 * Possible values:<ul>
	 * <li>{@code path/to/image.file}: Loads the specified image file.</li>
	 * <li>{@code create:my-image.png}: Creates an empty image.</li>
	 * <li>{@code screen:10,10,-20,-20}: Captures the screen.</li>
	 * <li>{@code class:my.package.ImageProducer}: Creates an image using the specified image producing class or method.</li>
	 * </ul>
	 * <p/>
	 * When a path or URL is specified, the image file is resolved in exactly the same way as when using the
	 * parameter "<code>source</code>" (see Macro reference).
	 * <p/>
	 * If the parameter value is prefixed with "{@code create:}", an empty transparent image is created and further processing
	 * is delegated to an image processor. The content after the prefix is treated as the image file name of the created image.
	 * <p/>
	 * If the parameter value is prefixed with "{@code screen:}" the remaining part of the specified value is considered to be a rectangle
	 * to capture from the screen following the format "{@code screen:x,y,width,height}".
	 * <p/>
	 * If the parameter value is prefixed with "{@code class:}" the remaining part of the specified value is considered to be a fully
	 * qualified class name to a class that either provides a {@link Component}, a {@link Rectangle}, an {@link Image} or any subclasses
	 * of the these.<br/>
	 * Components are either rendered using their {@code paint()} method or by capturing the on-screen appearance
	 * (for heavyweight components like frames and windows).<br/>
	 * Rectangles trigger a desktop screen capture of the specified region that works in the same was as the {@code screen:} option.
	 * <p/>
	 * The specified class is resolved in exactly the same way as when using the parameter "{@code source-class}" (see Macro reference).
	 * <p/>
	 * In addition to fully qualified class names, this parameter supports also factory methods that create the classes:
	 * <ul>
	 * <li>my.package.MyClass</li>
	 * <li>my.package.MyClass.getInstance()</li>
	 * <li>my.package.MyClassFactory.getMyClass()</li>
	 * <li>my.package.MyClassFactory.getInstance().getMyClass()</li>
	 * </ul>
	 * Methods and constructors are resolved against one of the supported signatures that are declared inside {@link ClassUtils}.
	 * <p/>
	 * Note: Screen capturing is in general not supported inside a <b>headless environment</b>. While most component rendering
	 * can fallback to lightweight {@code paint()}, screen rectangles and any window/frame classes will fail the build process when
	 * the build machine is headless.
	 */
	public static final String PARAM_IMAGE = "image";

	/**
	 * <b>Optional Parameter</b>, Sets the filename to use for attaching an image.
	 * <p/>
	 * Possible values:<ul>
	 * <li>undefined: Attaches the image using the original filename (extended with a format specific file extension if required).</li>
	 * <li>'%s-suffix': Attaches the image using the original filename with a suffix.</li>
	 * <li>'[filename]': Attaches the image using the specified filename.</li>
	 * <li>'false': Does not attach the image.</li>
	 * </ul>
	 * <p/>
	 * Use this if you attempt to render multiple results out of the same image source and make sure every
	 * differing result is also named differently.
	 */
	public static final String PARAM_ATTACH = "attach";

	/**
	 * <b>Optional Parameter</b>, Sets the image format to use (e.g. 'png', 'jpg', etc.). If not set this parameter is assumed
	 * using the filename extension of {@code image} or {@code attach}.
	 */
	public static final String PARAM_FORMAT = "format";

	/**
	 * <b>Optional Parameter</b>, Sets a class or script to invoke on the loaded {@link BufferedImage} that can
	 * manipulate it before it's being attached.
	 * <ul>
	 * <li>"class:my.package.ImageProcessor" or "class:my.package.ImageProcessor.process()" invokes the
	 * specified class or method with the loaded image being stored inside the request parameters. Alternatively the specified class
	 * or method may return an implementation deriving from {@link BufferedImageOp} that is then applied on the loaded image.</li>
	 * <li>"script:script.file" or "script:eval[script-type]:script" uses {@link ScriptInvoker} to run the
	 * specified script to do image manipulations.</li>
	 * </ul>
	 */
	public static final String PARAM_PROCESSOR = "processor";

	/**
	 * <b>Optional Parameter</b>, Same as "processor" but executed after all other operations were applied.
	 */
	public static final String PARAM_POST_PROCESSOR = "post-processor";

	/**
	 * <b>Optional Parameter</b>, Specifies the amount of milliseconds to wait before capturing a full screen image.
	 * (Default: auto -> 0 for rectangles, 2000 for heavyweight components)
	 */
	public static final String PARAM_FULL_SCREEN_CAPTURE_DELAY = "capture-delay";

	/**
	 * <b>Optional Parameter</b>, Disables any screen captures (except for rectangles) and forces the use of
	 * lightweight paint even on heavyweight components (may produce less exact results).
	 */
	public static final String PARAM_NO_CAPTURE = "no-capture";

	/**
	 * <b>Optional Parameter</b>, Swallow headless exceptions instead of breaking the build.
	 */
	public static final String PARAM_NO_HEADLESS_EXCEPTIONS = "no-headless-exceptions";


	/**
	 * <b>Optional Parameter</b>, sets the look'n'feel of the Swing subsystem.
	 * Possible values ('native', 'universal', 'nimbus', 'metal', etc.)
	 */
	public static final String PARAM_LNF = "lnf";

	/**
	 * <b>Optional Parameter</b>, crops the image.
	 * <p/>
	 * Possible values:<ul>
	 * <li>'true' / 'auto': Removes transparent / background sections surrounding the image (uses lower, right pixel to
	 * detect what content can be cropped)</li>
	 * <li>'pixel:x,y': Detects surrounding pixels of the same color as the given pixel and removes them.</li>
	 * <li>'color:#ffffff': Detects surrounding pixels of the given color and removes them.</li>
	 * <li>'rect:x,y,width,height': Crops by the given rectangle.</li>
	 * </ul>
	 */
	public static final String PARAM_CROP = "crop";

	/**
	 * <b>Optional Parameter</b>, sets the method to use for scaling.
	 * (Default: BICUBIC)
	 * Possible values ('NEIGHBOR', 'BILINEAR', 'BICUBIC')
	 */
	public static final String PARAM_SCALER = "scaler";

	/**
	 * <b>Optional Parameter</b>, sets the width in pixel.
	 */
	public static final String PARAM_WIDTH = "width";

	/**
	 * <b>Optional Parameter</b>, sets the height in pixel.
	 */
	public static final String PARAM_HEIGHT = "height";

	/**
	 * Is set with a reference to a {@link BufferedImage} instance straight after loading / creating the image succeeded.
	 * <p/>
	 * Image processors can access the buffered image via this parameter.
	 */
	public static final String OUT_PARAM_IMAGE = "image";

	/**
	 * Is set with the encoded image file bytes.
	 */
	public static final String OUT_PARAM_IMAGE_BYTES = "imageBytes";

	/**
	 * Is set with the with the HREF to the attached image file.
	 */
	public static final String OUT_PARAM_IMAGE_HREF = "imageHref";

	/**
	 * Constructs a new image loader with the given baseDir and request parameters.
	 *
	 * @param baseDir    the base directory of the currently built module.
	 * @param parameters the macro request parameters.
	 * @throws Exception In case of the image operation failed.
	 */
	public ImageLoader(File baseDir, Map<String, Object> parameters) throws Exception {
		final boolean containsSource = parameters.containsKey(PARAM_SOURCE) || parameters.containsKey(PARAM_SOURCE_CONTENT);

		putAll(parameters);

		// Images almost never loaded in verbatim mode
		put(PARAM_VERBATIM, false);

		BufferedImage image = null;
		String imageSource = "";
		final Object rawImage = parameters.get(PARAM_IMAGE);

		if (!(rawImage instanceof String)) {
			if (log.isDebugEnabled()) log.debug("Detected direct image input, trying to convert it to a buffered image.");
			try {
				image = buildImage(rawImage, imageSource, parameters);
			} catch (IllegalArgumentException e) {
				log.warn("Provided a non-image / non-path '" + rawImage + "' input to the image loader, Retrying using 'path=input.toString()'.", e);
			}
		}

		if (image == null) {
			imageSource = valueOf(rawImage);
			image = loadImage(imageSource, baseDir, parameters);
		}

		if (log.isDebugEnabled()) log.debug("Image loaded, setting it into the context with key '" + OUT_PARAM_IMAGE + "'");
		put(OUT_PARAM_IMAGE, image);

		image = processIfRequested(baseDir, parameters, image);
		image = scaleIfRequested(image, parameters);
		image = filterIfRequested(image, baseDir, parameters);
		image = cropIfRequested(image, parameters);
		image = postProcessIfRequested(baseDir, parameters, image);

		if (log.isDebugEnabled()) log.debug("Image processed, updating the context with the final image.");
		put(OUT_PARAM_IMAGE, image);

		attachImage(image, imageSource, containsSource, parameters);
	}

	protected BufferedImage loadImage(String imageSource, File baseDir, Map<String, Object> parameters) {
		BufferedImage image;

		if (imageSource.startsWith(CLASS_PREFIX)) {
			image = buildImage(baseDir, imageSource.substring(CLASS_PREFIX.length()), parameters);
		} else if (imageSource.startsWith(SCREEN_PREFIX)) {
			image = captureScreen(imageSource.substring(SCREEN_PREFIX.length()), parameters);
		} else if (imageSource.startsWith(CREATE_PREFIX)) {
			if (!parameters.containsKey(PARAM_WIDTH) || !parameters.containsKey(PARAM_HEIGHT))
				throw new IllegalStateException("Specified '" + imageSource + "' image without setting 'width' and 'height'.");

			image = new BufferedImage(
					parseInt(valueOf(parameters.get(PARAM_WIDTH))), parseInt(valueOf(parameters.get(PARAM_HEIGHT))), TYPE_INT_ARGB);
		} else {
			image = loadImage(baseDir, imageSource);
		}

		if (image == null)
			throw new IllegalArgumentException("Loading '" + imageSource + "' lead to a 'null' result. Please check your implementations.");

		return image;
	}

	protected void attachImage(BufferedImage image, String imageSource,
							   boolean containsSource, Map<String, Object> parameters) throws IOException {
		if (imageSource.startsWith(CLASS_PREFIX)) {
			imageSource = imageSource.replaceAll("[:.()]+", "_");
		} else if (imageSource.startsWith(CREATE_PREFIX))
			imageSource = imageSource.replace(CREATE_PREFIX, "");

		String imageFilename = imageSource;
		Object rawAttach = parameters.get(PARAM_ATTACH);
		boolean doAttach = !"false".equalsIgnoreCase(valueOf(rawAttach));

		if (rawAttach != null) {
			String ext = FileUtils.extension(imageSource),
					baseName = ext.length() == 0 ? imageSource : imageSource.substring(0, Math.max(0, imageSource.length() - ext.length() - 1));
			imageFilename = valueOf(rawAttach).replace("%s", baseName);
		} else if (imageSource.startsWith(SCREEN_PREFIX) && doAttach)
			throw new IllegalStateException("The parameter '" + PARAM_ATTACH + "' must be set with screen captures like " + imageSource);

		String format = FileUtils.extension(imageFilename);
		if ("".equals(format) || parameters.containsKey(PARAM_FORMAT)) {
			format = valueOf(parameters.get("format"));
			if ("null".equals(format)) format = "png";
		}

		if (!imageFilename.endsWith(format)) imageFilename += '.' + format;

		final byte[] bytes = ImageUtil.createImageFileContent(image, format);
		put(OUT_PARAM_IMAGE_BYTES, bytes);

		if (doAttach) {
			String href = Globals.getInstance().attachBinaryContent(imageFilename, bytes);
			put(OUT_PARAM_IMAGE_HREF, href);

			if (!containsSource) put(PARAM_SOURCE_CONTENT, "<img src='$esc.xml($imageHref)' alt=''/>");
		}
	}

	protected static Rectangle parseRectangleExpression(String rectangleExpression) {
		Scanner scanner = new Scanner(rectangleExpression).useDelimiter(Pattern.compile("[\\s,;:\\[\\]]+"));
		return new Rectangle(scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
	}

	protected BufferedImage captureScreen(String rectangleExpression, Map<String, Object> parameters) {
		Rectangle rectangle = parseRectangleExpression(rectangleExpression);
		return desktopToImage(rectangle, parameters);
	}

	protected BufferedImage loadImage(File baseDir, String imageSource) {
		return ImageUtil.loadImage(ResourceResolver.findSource(baseDir, imageSource));
	}

	protected BufferedImage buildImage(File baseDir, String imageSource, Map<String, Object> parameters) {
		LookAndFeel originalLNF = null;
		try {
			originalLNF = handleLookAndFeel(parameters);

			Object image = ClassUtils.invoke(imageSource, baseDir, parameters);
			return buildImage(image, imageSource, parameters);
		} catch (RuntimeException e) {
			if (Boolean.parseBoolean(valueOf(parameters.get(PARAM_NO_HEADLESS_EXCEPTIONS)))) {
				for (Throwable t = e; t != null; t = t.getCause()) {
					if (t instanceof HeadlessException) {
						log.error("Ignoring headless error for image " + imageSource + ", will render little black square instead.");
						return new BufferedImage(10, 10, TYPE_INT_ARGB);
					}
				}
			}
			throw e;
		} finally {
			restoreLookAndFeel(originalLNF);
		}
	}

	protected BufferedImage buildImage(Object image, String imageSource, Map<String, Object> parameters) {
		try {
			if (image instanceof Component) {
				final Component component = (Component) image;
				return componentToImage(component, parameters);
			} else if (image instanceof Rectangle) {
				return desktopToImage((Rectangle) image, parameters);
			} else if (image instanceof Image) {
				final Image sourceImage = (Image) image;
				return imageToBufferedImage(imageSource, sourceImage);
			} else if (image instanceof ImageProducer) {
				final ImageProducer producer = (ImageProducer) image;
				return producerToImage(producer);
			}
		} finally {
			if (image instanceof Closeable) {
				try {
					((Closeable) image).close();
				} catch (IOException e) {
					if (log.isDebugEnabled()) log.debug("Failed closing image " + image, e);
				}
			}
		}

		throw new IllegalArgumentException("The given image source '" + imageSource + "' did not resolve to a supported instance " +
				"(java.awt.Component of java.awt.Image) but was '" + image + "' instead.");
	}

	protected static BufferedImage imageToBufferedImage(String imageSource, Image sourceImage) {
		if (!(sourceImage instanceof BufferedImage)) {
			if (!waitOnImage(sourceImage)) {
				throw new IllegalArgumentException("Failed loading " + imageSource + ", due some internal async. AWT image loading error. " +
						"(maybe the code references to an invalid resource when using Toolkit.getImage(...)?");
			}

			BufferedImage buffer = new BufferedImage(sourceImage.getWidth(null), sourceImage.getHeight(null), TYPE_INT_ARGB);
			Graphics graphics = buffer.getGraphics();
			try {
				graphics.drawImage(sourceImage, 0, 0, null);
			} finally {
				graphics.dispose();
			}
			sourceImage = buffer;
		}
		return (BufferedImage) sourceImage;
	}

	protected static BufferedImage desktopToImage(final Rectangle rectangle, Map<String, Object> parameters) {
		try {
			if (log.isDebugEnabled()) log.debug("Creating a desktop screenshot of " + rectangle);
			if (parameters.containsKey(PARAM_FULL_SCREEN_CAPTURE_DELAY))
				Thread.sleep(parseInt(valueOf(parameters.get(PARAM_FULL_SCREEN_CAPTURE_DELAY))));
			return ImageCapture.createImage(rectangle);
		} catch (AWTException e) {
			throw new RuntimeException(e);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException(e);
		}
	}

	protected static BufferedImage producerToImage(final ImageProducer producer) {
		PixelGrabber grabber = new PixelGrabber(producer, 0, 0, -1, -1, null, 0, 0);
		try {
			grabber.grabPixels();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException("Image fetch was interrupted.", e);
		}

		if ((grabber.status() & ImageObserver.ABORT) != 0) throw new RuntimeException("Image fetch aborted");
		if ((grabber.status() & ImageObserver.ERROR) != 0) throw new RuntimeException("Image fetch error");

		BufferedImage image = new BufferedImage(grabber.getWidth(), grabber.getHeight(), TYPE_INT_ARGB);
		image.setRGB(0, 0, grabber.getWidth(), grabber.getHeight(), (int[]) grabber.getPixels(), 0, grabber.getWidth());

		return image;

	}

	protected static BufferedImage componentToImage(final Component component, Map<String, Object> parameters) {
		final boolean debug = log.isDebugEnabled();

		if (debug) log.debug("Rendering component " + component);

		final boolean showing = component.isShowing(), noScreenCapture = Boolean.parseBoolean(valueOf(parameters.get(PARAM_NO_CAPTURE)));
		final AtomicReference<BufferedImage> imageReference = new AtomicReference<BufferedImage>();

		try {
			final boolean isLightWeightPaint = component instanceof JComponent;

			if (!showing) {
				SwingUtilities.invokeAndWait(new Runnable() {
					public void run() {
						if (component instanceof Frame) {
							((Frame) component).setAlwaysOnTop(true);
						}
						component.setVisible(true);
					}
				});

				boolean willCaptureScreen = !noScreenCapture && !isLightWeightPaint && !GraphicsEnvironment.isHeadless();
				if (willCaptureScreen) {
					int millis = 2000;
					if (parameters.containsKey(PARAM_FULL_SCREEN_CAPTURE_DELAY))
						millis = parseInt(valueOf(parameters.get(PARAM_FULL_SCREEN_CAPTURE_DELAY)));

					if (debug) {
						log.debug("The component is rendered by taking a desktop screenshot, will wait " +
								millis + "ms to let the component finish drawing itself.");
					}

					Thread.sleep(millis);
				}
			}

			SwingUtilities.invokeAndWait(new Runnable() {
				public void run() {
					try {
						imageReference.set(isLightWeightPaint ?
								ImageCapture.createImage((JComponent) component) :
								ImageCapture.createImage(component, noScreenCapture));
					} catch (AWTException e) {
						throw new RuntimeException(e);
					}
				}
			});
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		} finally {
			if (!showing) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						component.setVisible(false);
						component.removeNotify();
					}
				});
			}
		}

		return imageReference.get();
	}

	protected static boolean waitOnImage(Image sourceImage) {
		if (sourceImage.getHeight(null) == -1) {
			final Component component = new Panel();
			MediaTracker tracker = new MediaTracker(component);
			tracker.addImage(sourceImage, 1);
			try {
				return tracker.waitForAll(0);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				throw new RuntimeException(e);
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	protected BufferedImage processIfRequested(File baseDir, Map<String, Object> parameters, BufferedImage image) throws Exception {
		if (parameters.containsKey(PARAM_PROCESSOR)) {
			String processor = valueOf(parameters.get(PARAM_PROCESSOR));
			if (log.isDebugEnabled()) log.debug("Processing loaded image with " + processor);

			if (processor.startsWith(CLASS_PREFIX)) {
				final Object result = ClassUtils.invoke(processor.substring(CLASS_PREFIX.length()), baseDir, this);
				if (result instanceof Map)
					putAll((Map<? extends String, ?>) result);

				if (result instanceof BufferedImageOp)
					image = ((BufferedImageOp) result).filter(image, null);
				else
					image = (BufferedImage) get(OUT_PARAM_IMAGE);
			} else if (processor.startsWith(SCRIPT_PREFIX)) {
				put(ScriptInvoker.PARAM_SCRIPT, processor.substring(SCRIPT_PREFIX.length()));
				putAll(new ScriptInvoker(baseDir, this));
				image = (BufferedImage) get(OUT_PARAM_IMAGE);
			}
		}
		return image;
	}

	protected BufferedImage postProcessIfRequested(File baseDir, Map<String, Object> parameters, BufferedImage image) throws Exception {
		parameters = new HashMap<String, Object>(parameters);
		parameters.put(PARAM_PROCESSOR, parameters.remove(PARAM_POST_PROCESSOR));
		parameters.put("isPostProcess", true);
		return processIfRequested(baseDir, parameters, image);
	}

	protected static BufferedImage scaleIfRequested(BufferedImage image, Map<String, Object> parameters) {
		if (image != null) {
			Object rawWidth = parameters.get(PARAM_WIDTH), rawHeight = parameters.get(PARAM_HEIGHT);
			if (rawWidth != null || rawHeight != null) {
				Object rawScaler = parameters.get(PARAM_SCALER);
				if ("NEIGHBOR".equalsIgnoreCase(valueOf(rawScaler)))
					rawScaler = RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR;
				else if ("BILINEAR".equalsIgnoreCase(valueOf(rawScaler)))
					rawScaler = RenderingHints.VALUE_INTERPOLATION_BILINEAR;
				else if ("BICUBIC".equalsIgnoreCase(valueOf(rawScaler)))
					rawScaler = RenderingHints.VALUE_INTERPOLATION_BICUBIC;

				image = ImageUtil.scale(image,
						rawWidth == null ? null : parseInt(valueOf(rawWidth)),
						rawHeight == null ? null : parseInt(valueOf(rawHeight)), false, rawScaler);
			}
		}
		return image;
	}

	protected BufferedImage filterIfRequested(BufferedImage image, File baseDir, Map<String, Object> parameters) {
		// This can be overridden in subclasses or might get some implementation later.
		return image;
	}

	private BufferedImage cropIfRequested(BufferedImage image, Map<String, Object> parameters) {
		Object rawCrop = parameters.get(PARAM_CROP);
		if (rawCrop != null) {
			String crop = valueOf(rawCrop);
			if ("true".equalsIgnoreCase(crop) || "auto".equalsIgnoreCase(crop)) {
				image = ImageUtil.shrinkByBackgroundColor(image, null);
			} else if (crop.startsWith("pixel:")) {
				String[] parts = crop.split("[:,;]+");
				int[] pixel = image.getRaster().getPixel(parseInt(parts[1]), parseInt(parts[2]), (int[]) null);
				image = ImageUtil.shrinkByBackgroundColor(image, pixel);
			} else if (crop.startsWith("color:")) {
				// TODO
			} else if (crop.startsWith("rect:")) {
				final Rectangle rect = parseRectangleExpression(crop.substring(5));
				image = image.getSubimage(rect.x, rect.y,
						Math.min(image.getWidth() - rect.x, rect.width), Math.min(image.getHeight() - rect.y, rect.height));
			}
		}
		return image;
	}

	protected static void restoreLookAndFeel(LookAndFeel originalLookAndFeel) {
		try {
			if (originalLookAndFeel != null) {
				if (log.isDebugEnabled()) log.debug("Restoring previous Look'N'Feel " + originalLookAndFeel.getName());
				UIManager.setLookAndFeel(originalLookAndFeel);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected static LookAndFeel handleLookAndFeel(Map<String, Object> parameters) {
		Object rawLNF = parameters.get(PARAM_LNF);
		if (rawLNF != null) {
			final LookAndFeel originalLookAndFeel = UIManager.getLookAndFeel();
			if (log.isDebugEnabled()) {
				log.debug("Changing Look'N'Feel from " +
						(originalLookAndFeel == null ? "<unknown>" : originalLookAndFeel.getName()) + " to " + rawLNF);
			}

			String lnfClass = null;
			List<String> allNames = new ArrayList<String>();
			allNames.addAll(Arrays.asList("native", "universal"));

			if ("native".equals(rawLNF))
				lnfClass = UIManager.getSystemLookAndFeelClassName();
			else if ("universal".equals(rawLNF))
				lnfClass = UIManager.getCrossPlatformLookAndFeelClassName();
			else {
				for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
					allNames.add(info.getName());
					if (valueOf(rawLNF).equalsIgnoreCase(info.getName()))
						lnfClass = info.getClassName();
				}
			}

			if (lnfClass == null)
				throw new IllegalArgumentException("Didn't find look'n'feel '" + rawLNF + "' within the installed look'n'feels " + allNames);

			try {
				UIManager.setLookAndFeel(lnfClass);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

			return originalLookAndFeel;
		}

		return null;
	}
}
