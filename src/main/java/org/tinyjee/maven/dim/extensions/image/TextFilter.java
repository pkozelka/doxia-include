/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.image;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;
import java.util.Arrays;

import static java.awt.RenderingHints.*;

/**
 * Implements a filter that renders a text overlay.
 *
 * @author Juergen_Kellerer, 2012-06-02
 */
public class TextFilter extends OverlayingFilter {

	Font font;
	String text = "";
	String color, background, verticalAlign = "center", align = "center";

	boolean smooth = true;

	private final Insets insets = new Insets(0, 0, 0, 0);

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public String getFont() {
		return font == null ? null : font.getName() + "-" +
				(font.isItalic() ? (font.isBold() ? "boldItalic-" : "italic-") : font.isBold() ? "bold-" : "") + font.getSize();
	}

	public void setFont(String font) {
		this.font = font == null ? null : Font.decode(font);
	}

	public String getVerticalAlign() {
		return verticalAlign;
	}

	public void setVerticalAlign(String verticalAlign) {
		this.verticalAlign = verticalAlign;
	}

	public String getAlign() {
		return align;
	}

	public void setAlign(String align) {
		this.align = align;
	}

	public boolean isSmooth() {
		return smooth;
	}

	public void setSmooth(boolean smooth) {
		this.smooth = smooth;
	}

	public Insets getInsets() {
		return insets;
	}

	public void setTopMargin(int pixel) {
		insets.top = pixel;
	}

	public void setLeftMargin(int pixel) {
		insets.left = pixel;
	}

	public void setRightMargin(int pixel) {
		insets.right = pixel;
	}

	public void setBottomMargin(int pixel) {
		insets.bottom = pixel;
	}

	@Override
	protected BufferedImage doFilter(BufferedImage source, BufferedImage destination) {
		JLabel label = new JLabel(text, SwingConstants.CENTER);
		label.setOpaque(false);

		if (font != null) label.setFont(font);
		if (color != null) label.setForeground((Color) ImageUtil.convertToPaint(color));
		if (background != null) {
			label.setBackground((Color) ImageUtil.convertToPaint(background));
			label.setOpaque(true);
		}

		label.setHorizontalAlignment(textToConstant(align));
		label.setHorizontalTextPosition(textToConstant(align));
		label.setVerticalAlignment(textToConstant(verticalAlign));
		label.setVerticalTextPosition(textToConstant(verticalAlign));

		label.setLocation(insets.left, insets.top);
		label.setSize(destination.getWidth() - insets.right - insets.left, destination.getHeight() - insets.bottom - insets.top);

		final Graphics2D graphics = destination.createGraphics();
		try {
			graphics.setRenderingHint(KEY_TEXT_ANTIALIASING, smooth ? VALUE_TEXT_ANTIALIAS_ON : VALUE_TEXT_ANTIALIAS_OFF);
			if (smooth) graphics.setRenderingHint(KEY_FRACTIONALMETRICS, VALUE_FRACTIONALMETRICS_ON);
			label.paint(graphics);
		} finally {
			graphics.dispose();
		}

		return super.doFilter(source, destination);
	}

	private static int textToConstant(String text) {
		for (Field field : SwingConstants.class.getFields()) {
			if (field.getName().equalsIgnoreCase(text))
				try {
					return (Integer) field.get(null);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
		}
		throw new IllegalArgumentException(text + " was not contained in " + Arrays.toString(SwingConstants.class.getFields()));
	}
}
