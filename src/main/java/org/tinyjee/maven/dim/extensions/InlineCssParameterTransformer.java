/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.tinyjee.maven.dim.spi.ExtensionInformation;

import java.util.Map;

import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_SOURCE_CONTENT;

/**
 * Adds an inline CSS definition to the current page.
 * Use this extension to define short-lived style classes or adjust existing styles for just one page.
 *
 * @author Juergen_Kellerer, 2011-11-06
 */
@ExtensionInformation(displayName = "Inline CSS", documentationUrl = "/extensionsMiscellaneous.html#aInline_CSS_Definition")
public class InlineCssParameterTransformer extends AbstractParameterTransformer {

	/**
	 * Enables this extension and sets the content to place inside the inlined "&lt;style&gt;" element.
	 * <p/>
	 * <b>Note:</b> Setting this property has no effect in case of "<code>source</code>" or "<code>source-class</code>" were
	 * specified with the macro call. This behaviour ensures that the parameter "<code>inline-css</code>" can still be used
	 * with ordinary macro calls where a source or source-class was set.
	 * <p/>
	 * Using this parameter has no effect when the used Sink does not support HTML markup.
	 * <p/>
	 * This parameter supports the usage of "{@code classpath:/path/to/css/to/inline}" to load the css from the classpath instead
	 * of defining it with the macro call.
	 * <p/>
	 * As the default brace style of "{}" is used to call macros in APT, the extension supports substituting the default CSS braces
	 * with "[]". This brace style is optional but it must be used when more than one style class is defined with the macro call
	 * as the APT parser swallows the "}" character, leading to invalid results.
	 * <p/>
	 * Examples:<ul>
	 * <li>"<code>%{include|inline-css=.my-class { background-color:red; }}</code>"</li>
	 * <li>"<code>%{include|inline-css=.section th { background-color:#999999; color:white; }}</code>"
	 * - changes the appearance of table headers inside sections.</li>
	 * <li>"<code>%{include|inline-css=.white [ color:white; ] .red [ color:red; ]}</code>"
	 * - Define more than one class at once using [] instead of {}.</li>
	 * </ul>
	 */
	public static final String PARAM_INLINE_CSS = "inline-css";

	@Override
	protected boolean doTransformParameters(Map<String, Object> requestParams) {
		String inlineCss = (String) requestParams.get(PARAM_INLINE_CSS);
		if (inlineCss != null) {
			// Using velocity instead of a direct call to "attachCss()" as parameter transformers shouldn't do anything else than
			// what they are named after.
			requestParams.put(PARAM_SOURCE_CONTENT, "#set($success = $globals.attachCss(null, ${inline-css}))");

			inlineCss = inlineCss.replace('[', '{').replace(']', '}').trim();
			if (!inlineCss.endsWith("}")) inlineCss += "}";
			requestParams.put(PARAM_INLINE_CSS, inlineCss);

			return true;
		}
		return false;
	}
}
