/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.tinyjee.maven.dim.IncludeMacroSignature;
import org.tinyjee.maven.dim.spi.ExtensionInformation;

import java.util.Map;

/**
 * Generates diagram out of the specified selection of Maven root POMs.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|maven-modules-diagram=pom.xml}
 * %{include|maven-modules-diagram=module1/pom.xml,module2/pom.xml}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 *
 * @author Juergen_Kellerer, 2012-07-08
 */
@ExtensionInformation(displayName = "Maven Modules Diagram", documentationUrl = "/templatesMavenModulesDiagram.html")
public class MavenModulesDiagramParameterTransformer extends AbstractParameterTransformer {

	/**
	 * Is a comma separated list of root poms to use for building the diagram.
	 */
	public static final String PARAM_MAVEN_MODULES_DIAGRAM = "maven-modules-diagram";

	/**
	 * Regular expression that must be matched by scope.
	 * (Default: "{@code ^(?!test|provided).*$}" -> anything except 'test' and 'provided')
	 */
	public static final String PARAM_SCOPE = "scope";

	/**
	 * Boolean, indicating whether the module graph is drawn based on the inter-module dependencies.
	 * (Default: 'false')
	 */
	public static final String PARAM_SHOW_DEPENDENCIES = "show-dependencies";

	/**
	 * Boolean, indicating whether the module graph is drawn based on all direct dependencies that are declared in {@code project/dependencies}.
	 * (Default: 'false')
	 * <br/>
	 * <i>Note:</i> This option only includes directly declared dependencies. Dependencies that are added via the module hierarchy are not shown.
	 */
	public static final String PARAM_SHOW_ALL_DEPENDENCIES = "show-all-dependencies";

	/**
	 * Boolean, indicating whether the module graph is drawn based on the module hierarchy (= forward module references).
	 * (Default: 'true')
	 */
	public static final String PARAM_SHOW_HIERARCHY = "show-hierarchy";

	/**
	 * Group modules by their groupId using a group box.
	 */
	public static final String PARAM_GROUP = "group";

	/**
	 * Shows the groupId vertex and connects it with modules under this group.
	 */
	public static final String PARAM_SHOW_GROUP = "show-group";

	/**
	 * Specifies a regular expression matching "groupId:artifactId" of modules that should be included
	 * with their incoming and outgoing dependencies.
	 */
	public static final String PARAM_CENTRIC_TO = "centric-to";

	/**
	 * Specifies a regular expression matching "groupId:artifactId" of modules that may be included.
	 * (Default: '.*')
	 */
	public static final String PARAM_INCLUDES = "includes";

	/**
	 * Specifies a regular expression matching "groupId:artifactId" of modules that must not be included.
	 */
	public static final String PARAM_EXCLUDES = "excludes";

	/**
	 * Specifies the layout of the class diagram. This can be anything that is supported by {@link org.tinyjee.maven.dim.extensions.GraphLoader},
	 * however in most cases only "hierarchical-vertical" and "hierarchical" are useful layout engines for class diagrams.
	 * (Default: 'hierarchical')
	 */
	public static final String PARAM_LAYOUT = "layout";

	private static final String INPUT_PARAM_POMS = "poms";

	@Override
	protected boolean doTransformParameters(Map<String, Object> requestParams) {
		Object input = requestParams.get(PARAM_MAVEN_MODULES_DIAGRAM);
		if (input != null) {
			requestParams.put(INPUT_PARAM_POMS, input);
			requestParams.put(IncludeMacroSignature.PARAM_SOURCE, "classpath:/templates/dim/maven-modules-diagram.xdoc.vm");


			String name = makeUniqueName("modules-diagram.svg",
					requestParams.get(PARAM_MAVEN_MODULES_DIAGRAM),
					requestParams.get(PARAM_SCOPE),
					requestParams.get(PARAM_SHOW_DEPENDENCIES),
					requestParams.get(PARAM_SHOW_ALL_DEPENDENCIES),
					requestParams.get(PARAM_SHOW_HIERARCHY),
					requestParams.get(PARAM_GROUP),
					requestParams.get(PARAM_SHOW_GROUP),
					requestParams.get(PARAM_CENTRIC_TO),
					requestParams.get(PARAM_INCLUDES),
					requestParams.get(PARAM_EXCLUDES),
					requestParams.get(PARAM_LAYOUT)
			);
			requestParams.put(GraphLoader.PARAM_ALIAS, "create:" + name);

			return true;
		}
		return false;
	}
}
