/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

/**
 * Base class for parameter transformers that select classes via '{@code /templates/dim/commons-class-selection.vm}'.
 *
 * @author Juergen_Kellerer, 2012-10-20
 */
public abstract class AbstractClassSelectionParameterTransformer extends AbstractParameterTransformer {
	/**
	 * Specifies a regular expression that must be matched by all elements that should be included.
	 */
	public static final String PARAM_INCLUDES = "includes";

	/**
	 * Specifies a regular expression that must NOT be matched by elements that should be included.
	 */
	public static final String PARAM_EXCLUDES = "excludes";

	/**
	 * Specifies the default regular expression that must NOT be matched by elements that should be included.
	 * <p/>
	 * (Default: "(^java.+|^.+\WTest[A-Z].+|.+[a-z]Test$|.+\.Log(|ger)$)" - exclude standard java types, test classes and loggers)
	 */
	public static final String PARAM_DEFAULT_EXCLUDES = "default-excludes";
}
