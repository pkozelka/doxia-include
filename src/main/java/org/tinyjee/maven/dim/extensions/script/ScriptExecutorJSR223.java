/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.script;

import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.extensions.ScriptExecutor;
import org.tinyjee.maven.dim.extensions.ScriptExecutorListener;
import org.tinyjee.maven.dim.spi.Globals;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.SimpleBindings;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.util.Map;

import static javax.script.ScriptContext.ENGINE_SCOPE;
import static javax.script.ScriptEngine.FILENAME;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_SOURCE_CONTENT;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_VERBATIM;

/**
 * Implements the JSR-223 bindings.
 *
 * @author Juergen_Kellerer, 2012-02-27
 */
public class ScriptExecutorJSR223 implements ScriptExecutor {

	private static final Log log = Globals.getLog();
	private WeakReference<ClassLoader> classLoader = new WeakReference<ClassLoader>(null);
	private ScriptEngineManager manager;
	private ThreadLocal<Bindings> currentBindings = new ThreadLocal<Bindings>();

	public synchronized ScriptEngineManager getManager() {
		final ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		if (manager == null || classLoader.get() != contextClassLoader) {
			if (log.isDebugEnabled()) log.debug("Initializing the script engine manager...");
			manager = new ScriptEngineManager(contextClassLoader);
			classLoader = new WeakReference<ClassLoader>(contextClassLoader);
		}

		return manager;
	}

	@SuppressWarnings("unchecked")
	public void execute(Map<String, Object> context, String scriptExtension, String scriptContent, String scriptName) throws Exception {
		final Bindings currentBindings = this.currentBindings.get();
		if (currentBindings != null) {
			if (currentBindings == context) {
				((ScriptEngine)currentBindings.get(SCRIPT_ENGINE)).eval(scriptContent);
			} else
				throw new IllegalStateException("Cannot run recursively with different script bindings.");
		} else {
			final Bindings bindings = new SimpleBindings(context);
			bindings.put(REQUEST, new SimpleBindings((Map<String, Object>) bindings.get(REQUEST)));
			execute(bindings, scriptExtension, scriptContent, scriptName);
		}
	}

	public Map<String, Object> getScriptContext() {
		return currentBindings.get();
	}

	protected void execute(Bindings bindings, String scriptExtension, String scriptContent, String scriptName) throws Exception {
		final Log log = Globals.getLog();
		final StringWriter errorWriter = new StringWriter(), outputWriter = new StringWriter();

		final ScriptEngine engine = createEngine(bindings, scriptExtension, scriptName, outputWriter, errorWriter);
		try {
			bindings.put(SCRIPT_ENGINE, engine);
			currentBindings.set(bindings);

			for (ScriptExecutorListener listener : ScriptExecutorListener.LISTENERS)
				listener.beforeExecute(this, scriptExtension, scriptContent, scriptName);

			engine.eval(scriptContent);
		} catch (Exception e) {
			String scriptOutput = outputWriter.getBuffer().length() > 0 ? outputWriter.toString() : "<no-output>";
			String scriptErrorOutput = errorWriter.getBuffer().length() > 0 ? errorWriter.toString() : "<no-error-output>";
			log.error("Failed executing script '" + scriptName + "':\n" +
					"---- Script-Source:\n" +
					scriptContent + "\n" +
					"---- Script-Output (until error occurred):\n" +
					scriptOutput + "\n" +
					"---- Script-Error-Output (until error occurred):\n" +
					scriptErrorOutput + "\n" +
					"----\n" +
					", caused by: ", e);
			throw e;
		} finally {
			try {
				for (ScriptExecutorListener listener : ScriptExecutorListener.LISTENERS)
					listener.afterExecute(this, scriptExtension, scriptContent, scriptName);
			} finally {
				currentBindings.set(null);
				bindings.remove(SCRIPT_ENGINE);
			}

			Object sourceContent = bindings.get(PARAM_SOURCE_CONTENT);
			if (outputWriter.getBuffer().length() > 0 && sourceContent == null) {
				bindings.put(PARAM_SOURCE_CONTENT, sourceContent = outputWriter.toString());
				if (bindings.get(PARAM_VERBATIM) == null) bindings.put(PARAM_VERBATIM, false);
			}

			if (errorWriter.getBuffer().length() > 0) {
				String content = errorWriter.toString();
				log.error(content);

				content = "ERROR:" + content.replace("\n", "\nERROR:");
				if (sourceContent != null) content = content + "\n\n" + sourceContent;
				bindings.put(PARAM_SOURCE_CONTENT, content);
			}
		}
	}

	protected ScriptEngine createEngine(Bindings bindings, String scriptExtension, String scriptName,
	                                    StringWriter outputWriter, StringWriter errorWriter) {
		final ScriptEngineManager manager = getManager();
		ScriptEngine engine = manager.getEngineByExtension(scriptExtension);
		if (engine == null) engine = manager.getEngineByName(scriptExtension);
		if (engine == null) engine = manager.getEngineByMimeType(scriptExtension);

		if (engine == null) {
			throw new IllegalArgumentException("No script engine exists that is named '" + scriptExtension + "' " +
					"or processes scripts if this file extension.");
		}

		if (log.isDebugEnabled()) log.debug("Starting to configure script engine " + engine);

		engine.setBindings(bindings, ENGINE_SCOPE);
		engine.put(FILENAME, scriptName);

		engine.getContext().setWriter(new PrintWriter(outputWriter));
		engine.getContext().setErrorWriter(new PrintWriter(errorWriter));

		return engine;
	}
}
