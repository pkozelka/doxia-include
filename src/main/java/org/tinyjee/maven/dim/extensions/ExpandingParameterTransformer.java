/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.spi.ExtensionInformation;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.RequestParameterTransformer;
import org.tinyjee.maven.dim.spi.UrlFetcher;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

/**
 * Expands macro parameter values of a supported expandable format (e.g. loads the parameter value from a file).
 * <p/>
 * The following formats are supported:
 * <ul>
 * <li><code>%{include|...|param=#load:[value.txt]}</code>:
 * Expands "param" with the contents of 'value.txt'.</li>
 * <li><code>%{include|...|param=#load:[values.properties:keyA]}</code>:
 * Expands "param" with the contents of 'keyA' contained inside 'values.properties'.</li>
 * <li><code>%{include|...|param=#system:[java.version]}</code>:
 * Expands "param" with the contents of 'java.version' contained inside the system properties.</li>
 * <li><code>%{include|...|param=#env:[home]}</code>:
 * Expands "param" with the contents of '$home' contained inside the system's environment.</li>
 * </ul>
 * <p/>
 * Expansion in this context means that the original value of a parameter is replaced with the expanded value.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|..|anyName=#(load|system|env):[file and/or key]}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 *
 * @author juergen kellerer, 2012-05-26
 * @since 1.2
 */
@ExtensionInformation(displayName = "Parameter Expander", documentationUrl = "/extensionsMiscellaneous.html#a1._Parameter_Expander")
public class ExpandingParameterTransformer implements RequestParameterTransformer {

	private static final String LOAD_PREFIX = "#load:[";
	private static final String SYSTEM_PREFIX = "#system:[";
	private static final String ENVIRONMENT_PREFIX = "#env:[";
	private static final String SUFFIX = "]";

	private static final String PROPERTIES_KEY = ".properties:";

	public void transformParameters(Map<String, Object> requestParams) {
		for (Map.Entry<String, Object> entry : requestParams.entrySet()) {
			boolean modified;
			do {
				modified = expandLoadableProperties(entry);
				modified |= expandSystemProperties(entry);
				modified |= expandEnvironmentProperties(entry);

			} while (modified);
		}
	}

	private boolean expandSystemProperties(Map.Entry<String, Object> entry) {
		final Globals globals = Globals.getInstance();
		final Log logger = globals.getLogger();
		final String key = entry.getKey(), value = String.valueOf(entry.getValue());

		if (value.startsWith(SYSTEM_PREFIX) && value.endsWith(SUFFIX)) {
			String propertyKey = extractValue(SYSTEM_PREFIX, value), propertyValue = System.getProperty(propertyKey);

			logger.debug("Expanding property['" + key + "']: '" + value + "', replacing with system property['" + propertyKey + "']: '" + propertyValue + "'");
			entry.setValue(propertyValue);
			return true;
		}

		return false;
	}

	private boolean expandEnvironmentProperties(Map.Entry<String, Object> entry) {
		final Globals globals = Globals.getInstance();
		final Log logger = globals.getLogger();
		final String key = entry.getKey(), value = String.valueOf(entry.getValue());

		if (value.startsWith(ENVIRONMENT_PREFIX) && value.endsWith(SUFFIX)) {
			String envName = extractValue(ENVIRONMENT_PREFIX, value), envValue = System.getenv(envName);

			logger.debug("Expanding property['" + key + "']: '" + value + "', replacing with environment variable ['" + envName + "']: '" + envValue + "'");
			entry.setValue(envValue);
			return true;
		}

		return false;
	}

	private boolean expandLoadableProperties(Map.Entry<String, Object> entry) {
		final Globals globals = Globals.getInstance();
		final Log logger = globals.getLogger();
		final String key = entry.getKey(), value = String.valueOf(entry.getValue());

		if (value.startsWith(LOAD_PREFIX) && value.endsWith(SUFFIX)) {
			String resourceToLoad = extractValue(LOAD_PREFIX, value);

			logger.debug("Expanding property['" + key + "']: '" + value + "', loading resource '" + resourceToLoad + "'");

			try {
				if (resourceToLoad.contains(PROPERTIES_KEY)) {
					int index = resourceToLoad.indexOf(PROPERTIES_KEY);
					String valueKey = resourceToLoad.substring(index + PROPERTIES_KEY.length());
					resourceToLoad = resourceToLoad.substring(0, index + PROPERTIES_KEY.length() - 1);

					Properties properties = new Properties();
					properties.load(UrlFetcher.getSource(globals.resolvePath(resourceToLoad)));
					String newValue = properties.getProperty(valueKey);

					if (newValue == null) {
						throw new IllegalArgumentException("The requested key '" + valueKey +
								"' was not found inside '" + resourceToLoad + "' on the attempt to " +
								"expand property['" + key + "']: '" + value + "'");
					}

					entry.setValue(newValue);

				} else {
					String newValue = globals.fetchText(resourceToLoad);
					logger.debug("Replacing value '" + value + "' with '" + newValue + "'");
					entry.setValue(newValue);
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}

			return true;
		}

		return false;
	}

	private static String extractValue(String prefix, String value) {
		return value.substring(prefix.length(), value.length() - SUFFIX.length());
	}
}
