/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.tinyjee.maven.dim.spi.ExtensionInformation;
import org.tinyjee.maven.dim.spi.RequestParameterTransformer;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.tinyjee.maven.dim.IncludeMacroSignature.*;

/**
 * Draws a message box containing freely definable text and an icon indicating the severity.
 * <p/>
 * The message box extension is implemented as a {@link RequestParameterTransformer} that is triggered when the parameter
 * "<code>message-box</code>" is set with the macro call. When "<code>message-box=text to display</code>" is present, the transformer
 * sets "<code>source</code>" to a bundled velocity template that performs the actual rendering.
 * <p/>
 * <b>Note:</b> Message boxes are rendered as pure <i>HTML</i> and will only work if a XhtmlSink or derived class
 * is used for rendering the output (=> any HTML render target should be fine, including the standard Maven site generation).
 * Furthermore all built-in icons are rendered using embedded <i>SVG</i> graphics, which is currently supported in:
 * IE 9+, FF 4+, Chrome, Safari and Opera.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|message-box=Hello World inside a Message Box}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 *
 * @author Juergen_Kellerer, 2011-10-06
 */
@ExtensionInformation(displayName = "Message Box", documentationUrl = "/templatesMessageBox.html")
public class MessageBoxParameterTransformer implements RequestParameterTransformer {

	/**
	 * Enables this extension and sets the text to display inside the message box.
	 * <p/>
	 * <b>Note:</b> Setting this property has no effect in case of "<code>source</code>" or "<code>source-class</code>" were
	 * specified with the macro call. This behaviour ensures that the parameter "<code>message-box</code>" can still be used
	 * with ordinary macro calls where a source or source-class was set.
	 * <p/>
	 * <b>Markup:</b> The message box is rendered as pure HTML and text is added to the box <b>as-is</b>.
	 * As a consequence HTML can be used as markup inside the given message and it is also the only valid
	 * markup that may be used here.
	 * <br/>
	 * E.g. <code>%{include|message-box=Hello &lt;b&gt;World!&lt;/b&gt;}</code> can be used with the APT format,
	 * to have "<b>World!</b>" formatted in strong text style.
	 */
	public static final String PARAM_MESSAGE_BOX = "message-box";

	/**
	 * <i>Optional parameter</i> specifying the type of message box to display.
	 * <ul>
	 * <li><b>info</b>: displays a message box with an information icon (= default).</li>
	 * <li><b>warning</b>: displays a message box showing the given warning with a corresponding icon.</li>
	 * <li><b>alert</b>: displays a message box showing the given alert with a corresponding icon.</li>
	 * </ul>
	 */
	public static final String PARAM_TYPE = "type";

	/**
	 * <i>Optional parameter</i> overriding the text color of the message box.
	 */
	public static final String PARAM_TEXT_COLOR = "text-color";

	/**
	 * <i>Optional parameter</i> overriding the border color of the message box.
	 */
	public static final String PARAM_COLOR = "color";

	/**
	 * <i>Optional parameter</i> overriding the color of the message box background.
	 */
	public static final String PARAM_BACKGROUND_COLOR = "background-color";

	/**
	 * <i>Optional parameter</i> setting a site resource path to the image that is used with the message box instead of
	 * the built-in SVG graphics.
	 * <p/>
	 * Supported formats include GIF, PNG, JPG and SVG. The path has to be chosen relative to the site's web root, e.g.:<br/>
	 * <code>%{include|icon=images/alert.png|message-box=This is an alert message}</code>
	 * <p/>
	 * <i>Note:</i> If no <code>type</code> is specified, it will be set accordingly when the image path contains one of the
	 * keywords "<code>alert</code>" or "<code>warn(ing)</code>" ("info" remains the default message box type).
	 */
	public static final String PARAM_ICON = "icon";

	private static final AtomicInteger idSequence = new AtomicInteger();

	private static boolean isSourceSet(Map<String, Object> requestParams) {
		return requestParams.containsKey(PARAM_SOURCE) && requestParams.containsKey(PARAM_SOURCE_CLASS);
	}

	public void transformParameters(Map<String, Object> requestParams) {
		Object messageBoxText = requestParams.get(PARAM_MESSAGE_BOX);
		if (messageBoxText != null && !isSourceSet(requestParams)) {
			requestParams.put("idSequence", idSequence.incrementAndGet());
			requestParams.put(PARAM_SOURCE, "classpath:/templates/dim/message-box.html.vm");
			requestParams.put(PARAM_VERBATIM, false);

			requestParams.put("text", messageBoxText);

			if (requestParams.containsKey(PARAM_BACKGROUND_COLOR))
				requestParams.put("backgroundColor", requestParams.get(PARAM_BACKGROUND_COLOR));
			if (requestParams.containsKey(PARAM_TEXT_COLOR))
				requestParams.put("textColor", requestParams.get(PARAM_TEXT_COLOR));
		}
	}
}
