package org.tinyjee.maven.dim.extensions;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * Is the interface of a loaded file reference that is put inside "files".
 * <p/>
 * Example Usage:
 * <code><pre>
 * #foreach($fileReference in $files)
 *    &lt;pre&gt;First Line of "$fileReference.filePath": $fileReference.textLines.get(0)&lt;/pre&gt;
 * #end
 * </pre></code>
 */
public interface FileReference extends Comparable<FileReference> {
	/**
	 * Returns the source file as {@link java.io.File} instance.
	 *
	 * @return the file the reference points at.
	 */
	File getFile();

	/**
	 * Returns the file path as relative path string.
	 *
	 * @return the file path as relative path string.
	 */
	String getFilePath();

	/**
	 * Returns the file path as file url.
	 *
	 * @return the file path as file url.
	 */
	URL getFileUrl();

	/**
	 * Loads the file's content as text using automatic charset detection.
	 *
	 * @return the file's content as text.
	 */
	String getText();

	/**
	 * Loads the file's content as text using the specified charset.
	 *
	 * @param charset the charset to use for loading the file.
	 * @return the file's content as text.
	 */
	String getText(String charset);

	/**
	 * Returns the file's text lines.
	 *
	 * @return the file's text lines.
	 */
	List<String> getTextLines();

	/**
	 * Returns the file's text lines.
	 *
	 * @param charset the charset to use for loading the file.
	 * @return the file's text lines.
	 */
	List<String> getTextLines(String charset);

	/**
	 * Returns the file's content in binary form.
	 *
	 * @return the file's content in binary form.
	 */
	byte[] getBytes();

	/**
	 * Instantiates and returns the associated loader extension.
	 *
	 * @return An extension that is known to work on the file type or 'null' if not available.
	 */
	Map<String, Object> getLoader();

	/**
	 * Instantiates and returns the associated loader extension.
	 *
	 * @param properties additional properties to pass to the loader.
	 * @return An extension that is known to work on the file type or 'null' if not available.
	 */
	Map<String, Object> getLoader(Map<String, Object> properties);
}
