/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.tinyjee.maven.dim.spi.ExtensionInformation;
import org.tinyjee.maven.dim.spi.RequestParameterTransformer;

import java.util.Map;

import static org.tinyjee.maven.dim.IncludeMacroSignature.*;

/**
 * Renders a tabbed panel from document sections (hierarchical headlines in Doxia).
 * <p/>
 * Tabbed panels are useful in improving the screen appearance of large content by creating multiple pages using tabs.
 * When printing a document, the content renders in the same way as before (the panel is hidden and all tabs are visible).
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|tabbed-panel=*}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 *
 * @author Juergen_Kellerer, 2011-11-06
 */
@ExtensionInformation(displayName = "Tabbed Panel", documentationUrl = "/templatesTabbedPanel.html")
public class TabbedPanelParameterTransformer extends AbstractParameterTransformer {

	/**
	 * Enables this extension and sets a regular expression matching the section titles that should be transformed into a tabs.
	 * <p/>
	 * <b>Note:</b> Setting this property has no effect in case of "<code>source</code>" or "<code>source-class</code>" were
	 * specified with the macro call. This behaviour ensures that the parameter "<code>tabbed-panel</code>" can still be used
	 * with ordinary macro calls where a source or source-class was set.
	 * <p/>
	 * This parameter expects a regular expression that is used to select what sections turn into tabs and what not by matching
	 * the given titles (headlines).<br/>
	 * By default the javascript looks for the first section that follows the include position and continues with all
	 * siblings (sections of the same level). Neither parent nor child sections are processed, therefore it's mostly safe
	 * to use the <i>match all</i> expression <code>.*</code> if all sub-sequent sections of the same nesting level should
	 * turn into tabs.
	 * <p/>
	 * Examples:<ul>
	 * <li>"<code>%{include|tabbed-panel=.*}</code>" - Transforms all following sections (of the same level) to tabs.</li>
	 * <li>"<code>%{include|tabbed-panel=.*(Example).*}</code>" - Transforms all following sections to tabs that contain
	 * the word "Example".</li>
	 * </ul>
	 */
	public static final String PARAM_TABBED_PANEL = "tabbed-panel";

	/**
	 * Toggles whether section titles are hidden once they were transformed into a tab (defaults to 'true').
	 */
	public static final String PARAM_HIDE_TITLES = "hide-titles";

	/**
	 * <b>Optional Parameter:</b> Specifies a minimum tab height to improve scrolling behaviour when switching tabs.
	 */
	public static final String PARAM_MIN_HEIGHT = "min-height";

	/**
	 * <b>Optional Parameter:</b> Adds an additional style class to the tabbed panel tabs container.
	 * Supported style classes:<ul>
	 *     <li>'default'</li>
	 *     <li>'plain'</li>
	 * </ul>
	 */
	public static final String PARAM_STYLE_CLASS = "style-class";

	private static final String PARAM_TABS = "tabs";

	@Override
	protected boolean doTransformParameters(Map<String, Object> requestParams) {
		Object tabs = requestParams.get(PARAM_TABBED_PANEL);
		if (tabs != null) {
			requestParams.put(PARAM_SOURCE, "classpath:/templates/dim/tabbed-panel.html.vm");
			requestParams.put(PARAM_VERBATIM, false);
			requestParams.put(PARAM_TABS, "*".equals(tabs) ? ".*" : tabs);

			if (!requestParams.containsKey(PARAM_HIDE_TITLES)) requestParams.put(PARAM_HIDE_TITLES, true);
			return true;
		}
		return false;
	}
}
