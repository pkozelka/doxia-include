/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.spi.ExtensionInformation;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.ResourceResolver;
import org.tinyjee.maven.dim.utils.AbstractAliasHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static java.awt.image.BufferedImage.TYPE_3BYTE_BGR;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_SOURCE_CONTENT;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_VERBATIM;
import static org.tinyjee.maven.dim.extensions.image.ImageUtil.createImageFileContent;
import static org.tinyjee.maven.dim.extensions.image.ImageUtil.shrinkByBackgroundColor;

/**
 * Renders ODS files (open document spreadsheets) to PNG or JPG images using "<a href="http://www.jopendocument.org/">jOpenDocument</a>".
 * <p/>
 * This is a very thin wrapper around a 3rd party rendering library, check
 * "<a href="http://www.jopendocument.org/">jopendocument.org</a>" for a list of supported rendering features.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|render-ods=file.ods}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 * Full call syntax:<code><pre>
 * %{include|source-class=org.tinyjee.maven.dim.extensions.OpenDocumentSpreadsheetRenderer|render=file.ods}
 * </pre></code>
 *
 * @author Juergen_Kellerer, 2012-05-25
 */
@ExtensionInformation(displayName = "ODS Renderer", documentationUrl = "/extensionsODSRenderer.html")
public class OpenDocumentSpreadsheetRenderer extends HashMap<String, Object> {

	private static final Log log = Globals.getLog();
	private static final long serialVersionUID = -6282606147512841025L;

	private static final int MAX_IMAGE_SIZE = 4 * 1024;

	/**
	 * Implements the "{@link org.tinyjee.maven.dim.extensions.SvgLoader#PARAM_ALIAS}" alias functionality.
	 */
	public static class AliasHandler extends AbstractAliasHandler {
		/**
		 * Constructs the handler (Note: Is called by {@link org.tinyjee.maven.dim.spi.RequestParameterTransformer#TRANSFORMERS}).
		 */
		public AliasHandler() {
			super(PARAM_ALIAS, PARAM_RENDER, OpenDocumentSpreadsheetRenderer.class.getName());
		}
	}

	/**
	 * Defines a shortcut for the request properties 'source-class' and 'render'.
	 * <p/>
	 * If this parameter is present inside the request parameters list, the system will effectively behave as if
	 * 'source-class' and 'render' where set separately.
	 */
	public static final String PARAM_ALIAS = "render-ods";

	/**
	 * Sets the URL or local path to the ODS file to load and render.
	 * <p/>
	 * The specified file is resolved in exactly the same way as when using the parameter "<code>source</code>"
	 * (see Macro reference).
	 */
	public static final String PARAM_RENDER = "render";

	/**
	 * <b>Optional Parameter</b>, Sets the image format to use (can be anything that is supported by ImageIO).
	 * (Default: 'png')
	 */
	public static final String PARAM_FORMAT = "format";

	/**
	 * <b>Optional Parameter</b>, Sets the filename to use for attaching the rendered image.
	 */
	public static final String PARAM_ATTACH = "attach";

	/**
	 * <b>Optional Parameter</b>, Toggles shrinking of the generated image by removing surrounding margins.
	 * (Default: 'true')
	 */
	public static final String PARAM_SHRINK = "shrink";

	/**
	 * <b>Optional Parameter</b>, Sets the ZOOM factor in percent.
	 * (Default: '100')
	 */
	public static final String PARAM_ZOOM = "zoom";

	/**
	 * <b>Optional Parameter</b>, Sets the page number, starting at 0.
	 * (Default: '0')
	 */
	public static final String PARAM_PAGE = "page";

	/**
	 * <b>Optional Parameter</b>, sets the width in pixel that is used to scale the generated output.
	 */
	public static final String PARAM_WIDTH = "width";

	/**
	 * <b>Optional Parameter</b>, sets the height in pixel that is used to scale the generated output.
	 */
	public static final String PARAM_HEIGHT = "height";

	public OpenDocumentSpreadsheetRenderer(File baseDir, Map<String, Object> requestParams) throws Exception {
		final Globals globals = Globals.getInstance();

		final Object rawFormat = requestParams.get(PARAM_FORMAT);
		final String format = rawFormat == null ? "png" : valueOf(rawFormat);

		final String name = (String) requestParams.get(PARAM_RENDER);
		final URL source = ResourceResolver.findSource(baseDir, name);

		final Object rawZoomValue = requestParams.get(PARAM_ZOOM);
		final double zoomValue = rawZoomValue == null ? 100D : parseDouble(valueOf(rawZoomValue));

		final Object rawPageValue = requestParams.get(PARAM_PAGE);
		final int pageValue = rawPageValue == null ? 0 : parseInt(valueOf(rawPageValue));

		try {
			final Object document = loadODS(source);
			final JComponent renderer = initRenderer(document, pageValue, zoomValue);
			BufferedImage image = renderImage(requestParams, renderer);
			image = ImageLoader.scaleIfRequested(image, requestParams);

			putAll(requestParams);

			put("odsUrl", source);
			put("ods", source.toString());
			put("width", image.getWidth());
			put("height", image.getHeight());

			String fileName = requestParams.containsKey(PARAM_ATTACH) ? valueOf(requestParams.get(PARAM_ATTACH)) : name;
			if (!fileName.endsWith(format))
				fileName += '.' + format;

			final String imageReference = globals.attachBinaryContent(fileName, createImageFileContent(image, format));
			put("href", imageReference);

			put(PARAM_VERBATIM, false);
			put(PARAM_SOURCE_CONTENT, "<img src='$esc.xml($href)' width='$width' height='$height' alt='$esc.xml($render)'/>");
		} catch (UnsupportedClassVersionError e) {
			final String javaVersion = System.getProperty("java.version");
			put("javaVersion", javaVersion);
			Globals.getLog().error("ODS rendering is not supported within the used JDK " +
					javaVersion + "; " + e.getMessage(), e);
			put(PARAM_SOURCE_CONTENT, "ODS rendering is not supported within the used JDK '$javaVersion'");
		}
	}

	private static BufferedImage renderImage(Map<String, Object> requestParams, JComponent renderer) throws Exception {
		int pageWidth = parseInt(BeanUtils.getProperty(renderer, "pageWidthInPixel"));
		int pageHeight = parseInt(BeanUtils.getProperty(renderer, "pageHeightInPixel"));

		if (log.isDebugEnabled()) log.debug("About to render ODS pageWidth=" + pageWidth + " x pageHeight=" + pageHeight);
		BufferedImage image = new BufferedImage(Math.min(MAX_IMAGE_SIZE, pageWidth), Math.min(MAX_IMAGE_SIZE, pageHeight), TYPE_3BYTE_BGR);
		final Graphics2D graphics = image.createGraphics();
		try {
			renderer.paint(graphics);
		} finally {
			graphics.dispose();
		}

		if (!Boolean.FALSE.equals(requestParams.get(PARAM_SHRINK))) {
			image = shrinkByBackgroundColor(image, null);
		}
		return image;
	}

	private static JComponent initRenderer(Object document, int pageValue, double zoomValue) throws Exception {
		if (log.isDebugEnabled()) log.debug("About to init ODS renderer with zoom=" + zoomValue + " and page=" + pageValue);
		final JComponent renderer = (JComponent) Class.forName("org.jopendocument.renderer.ODTRenderer").
				getConstructor(document.getClass()).newInstance(document);
		BeanUtils.setProperty(renderer, "currentPage", pageValue);
		BeanUtils.setProperty(renderer, "ignoreMargins", true);
		BeanUtils.setProperty(renderer, "paintMaxResolution", true);
		BeanUtils.setProperty(renderer, "resizeFactor", ((100D * 360D) / zoomValue));
		return renderer;
	}

	private static Object loadODS(URL source) throws Exception {
		if (log.isDebugEnabled()) log.debug("About to load ODS from " + source);
		final Object document = Class.forName("org.jopendocument.model.OpenDocument").newInstance();
		document.getClass().getMethod("loadFrom", URL.class).invoke(document, source);

		return document;
	}
}
