/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.tinyjee.maven.dim.IncludeMacroSignature;
import org.tinyjee.maven.dim.spi.ExtensionInformation;

import java.util.Map;

/**
 * Generates an table of class names and descriptions that corresponds to a class diagram and can be used as a class legend.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|class-diagram-legend=my.package.MyClass}
 * %{include|class-diagram-legend=my.package}
 * %{include|class-diagram-legend=my.package1, my.package2}
 * %{include|class-diagram-legend=annotated:@XmlType}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 *
 * @author Juergen_Kellerer, 2012-07-08
 */
@ExtensionInformation(displayName = "Class Diagram Legend", documentationUrl = "/templatesClassDiagram.html")
public class ClassDiagramLegendParameterTransformer extends AbstractClassSelectionParameterTransformer {

	/**
	 * Is a comma separated list of fully qualified class or package names to display inside the class diagram legend.
	 * Possible values:<ul>
	 * <li>"package.prefix" <i>= all in this package</i></li>
	 * <li>"package.prefix.." <i>= all classes and packages below this package</i></li>
	 * <li>"package.prefix1, package.prefix2, package.prefix3"</li>
	 * <li>"my.package.MyClass"</li>
	 * <li>"my.package.MyClass1, my.package.MyClass2"</li>
	 * <li>"derived:my.package.MySuperClassOrInterface"</li>
	 * <li>"annotated:my.package.MyAnnotation"</li>
	 * </ul>
	 */
	public static final String PARAM_CLASS_DIAGRAM_LEGEND = "class-diagram-legend";

	/**
	 * Specifies whether related classes are automatically added to the class diagram.
	 * Possible values:<ul>
	 * <li>"false" <i>(default, if more than one class was specified)</i></li>
	 * <li>"true" <i>(default, if a single class was specified)</i></li>
	 * <li>"package.prefix" <i>= all in exactly this package</i></li>
	 * <li>"package.prefix.." <i>= all below this package</i></li>
	 * <li>"package.prefix1, package.prefix2, package.prefix3"</li>
	 * </ul>
	 */
	public static final String PARAM_ADD_RELATED = "add-related";

	/**
	 * Is a boolean specifying whether only the first line of the class description is used in the legend instead of
	 * the full description.
	 * (Default: "true")
	 */
	public static final String PARAM_COMPACT = "compact";

	private static final String INPUT_PARAM_CLASSES = "classes";

	@Override
	protected boolean doTransformParameters(Map<String, Object> requestParams) {
		Object input = requestParams.get(PARAM_CLASS_DIAGRAM_LEGEND);
		if (input != null) {
			requestParams.put(INPUT_PARAM_CLASSES, input);
			requestParams.put(IncludeMacroSignature.PARAM_SOURCE, "classpath:/templates/dim/class-diagram-legend.xdoc.vm");

			return true;
		}
		return false;
	}
}
