/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.image;

import com.jhlabs.composite.MiscComposite;
import com.jhlabs.image.ImageUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.util.Arrays;

/**
 * Simple image overlay filter with various blending rules and additional alpha.
 * <p/>
 * Available Rules: "Normal", "Add", "Subtract", "Difference", "Multiply", "Darken", "Burn", "Color Burn",
 * "Screen", "Lighten", "Dodge", "Color Dodge", "Hue", "Saturation", "Brightness", "Color",
 * "Overlay", "Soft Light", "Hard Light", "Pin Light", "Exclusion", "Negation", "Average", "Stencil", "Silhouette"
 */
public class OverlayFilter extends AbstractImageFilter {

	float alpha = 1f;
	int compositeId = MiscComposite.BLEND;
	BufferedImage image;

	public float getAlpha() {
		return alpha;
	}

	public void setAlpha(float alpha) {
		this.alpha = alpha;
	}

	public int getCompositeId() {
		return compositeId;
	}

	public void setCompositeId(int compositeId) {
		this.compositeId = compositeId;
	}

	public String getRule() {
		return MiscComposite.RULE_NAMES[compositeId];
	}

	public void setRule(String ruleName) {
		ruleName = String.valueOf(ruleName).toLowerCase().replaceAll("[^a-z]+", "");
		for (int i = 0; i < MiscComposite.RULE_NAMES.length; i++) {
			if (MiscComposite.RULE_NAMES[i].toLowerCase().replaceAll("[^a-z]+", "").equals(ruleName)) {
				compositeId = i;
				return;
			}
		}

		throw new IllegalArgumentException(ruleName + " not found within " + Arrays.toString(MiscComposite.RULE_NAMES));
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	@Override
	public BufferedImage createCompatibleDestImage(BufferedImage source, ColorModel destinationColorModel) {
		return ImageUtils.cloneImage(source);
	}

	@Override
	protected BufferedImage doFilter(BufferedImage source, BufferedImage destination) {
		if (image == null) throw new IllegalStateException("overlay 'image' was not set.");

		final Graphics2D graphics = destination.createGraphics();
		try {
			graphics.setComposite(MiscComposite.getInstance(compositeId, alpha));
			graphics.drawImage(image, 0, 0, null);
		} finally {
			graphics.dispose();
		}

		return destination;
	}
}
