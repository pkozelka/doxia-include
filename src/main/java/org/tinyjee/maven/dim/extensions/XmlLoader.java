/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.apache.maven.doxia.logging.Log;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.introspect.AnnotatedField;
import org.codehaus.jackson.map.ser.CustomSerializerFactory;
import org.codehaus.jackson.xc.DomElementJsonSerializer;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;
import org.tinyjee.maven.dim.DebugConstants;
import org.tinyjee.maven.dim.sources.TemplateSource;
import org.tinyjee.maven.dim.sources.UrlSource;
import org.tinyjee.maven.dim.spi.ExtensionInformation;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.UrlFetcher;
import org.tinyjee.maven.dim.utils.*;
import org.w3c.dom.*;
import org.xml.sax.SAXParseException;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.beans.XMLEncoder;
import java.io.*;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;
import java.util.zip.GZIPInputStream;

import static java.lang.Boolean.parseBoolean;
import static java.lang.String.valueOf;
import static java.util.Arrays.asList;
import static java.util.Locale.ENGLISH;
import static org.codehaus.jackson.map.SerializationConfig.Feature;
import static org.tinyjee.maven.dim.IncludeMacroSignature.*;
import static org.tinyjee.maven.dim.spi.ResourceResolver.findSource;
import static org.tinyjee.maven.dim.utils.DebugUtils.dumpReaderWithLineNumbers;
import static org.tinyjee.maven.dim.utils.XPathEvaluatorImplementation.serializeNode;

/**
 * Loads and transforms XML/JSON content and provides XPath and DOM oriented access to the document elements when used
 * in combination with a velocity template.
 * <p/>
 * The loader supports remote URLs (e.g. REST-services) and local paths to get the XML (or JSON) content (similar to the standard
 * inclusion engine).
 * <p/>
 * Use this loader when you want to access the parsed content of XML (or JSON) markup using the DOM API with the option to
 * apply optional XSL transformations. This extension is not needed if your aim is to include code snippets that are
 * selected using XPath expressions.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|source=template.vm|source-xml=file.xml}
 * %{include|source-xml=file.xml|xsl=transform-to-html.xsl}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 * Full call syntax:<code><pre>
 * %{include|source=template.vm|source-class=org.tinyjee.maven.dim.extensions.XmlLoader|xml=file.xml}
 * </pre></code>
 *
 * @author Juergen_Kellerer, 2011-10-06
 */
@ExtensionInformation(displayName = "XML Loader", documentationUrl = "/extensionsXmlLoader.html")
public class XmlLoader extends HashMap<String, Object> {

	private static final Log log = Globals.getLog();
	private static final long serialVersionUID = -3702740619423786753L;
	private static final String LOCAL_ONLY = "local-only";

	/**
	 * Utility method that loads the given XML content.
	 *
	 * @return a Document containing the loaded XML.
	 * @throws Exception in case of loading failed.
	 */
	public static Document load(Object xml, boolean namespaceAware, Map<String, Object> parameters) throws Exception {
		if (parameters == null) parameters = Collections.emptyMap();
		parameters = new HashMap<String, Object>(parameters);

		if (xml != null) parameters.put(PARAM_XML, xml);
		if (namespaceAware) parameters.put(PARAM_NAMESPACE_AWARE, true);

		XmlLoader loader = new XmlLoader(Globals.getInstance().getBasedir(), parameters);
		return (Document) loader.get(OUT_PARAM_DOCUMENT);
	}

	/**
	 * Implements the "{@link org.tinyjee.maven.dim.extensions.XmlLoader#PARAM_ALIAS}" alias functionality.
	 */
	public static class AliasHandler extends AbstractAliasHandler {
		/**
		 * Constructs the handler (Note: Is called by {@link org.tinyjee.maven.dim.spi.RequestParameterTransformer#TRANSFORMERS}).
		 */
		public AliasHandler() {
			super(PARAM_ALIAS, PARAM_XML, XmlLoader.class.getName());
		}
	}

	/**
	 * Defines a shortcut for the request properties 'source-class' and 'xml'.
	 * <p/>
	 * If this parameter is present inside the request parameters list, the system will effectively behave as if
	 * 'source-class' and 'xml' where set separately.
	 */
	public static final String PARAM_ALIAS = "source-xml";

	/**
	 * Sets the URL or local path to the XML file to load inside the request parameters.
	 * <p/>
	 * The specified file is resolved in exactly the same way as when using the parameter "<code>source</code>" (see Macro reference).
	 * <p/>
	 * <i>Notes:</i><ul>
	 * <li>When no further source is specified, the xml loader sets the serialized representation of
	 * this file as the content that is included by the macro. If a source IS specified, the defined out parameters
	 * are set and allow DOM or XPath based access to the document structure.</li>
	 * <li>GZIP compressed XML files are detected and automatically uncompressed.</li>
	 * <li>The XML file is assumed to be a velocity template if the referenced URL or local path ends with "{@code .vm}".
	 * When present, the file is evaluated using all specified macro parameters (this allow to build XML dynamically
	 * using velocity).</li>
	 * <li>If the parameter value is prefixed with "script:", {@code ScriptInvoker} is used to run the script and the
	 * output is processed as XML.</li>
	 * <li>The parameter value is interpreted as the XML content to parse when it starts with "{@code <?xml}".</li>
	 * </ul>
	 */
	public static final String PARAM_XML = "xml";

	/**
	 * Sets a fully qualified class name to a class that either provides a {@link Document} instance, a {@link javax.xml.transform.Source}
	 * or is annotated with {@link javax.xml.bind.annotation.XmlRootElement}.
	 * <p/>
	 * The specified class is resolved in exactly the same way as when using the parameter "{@code source-class}" (see Macro reference).
	 * <p/>
	 * In addition to fully qualified class names, this parameter supports also factory methods that create the classes:
	 * <ul>
	 * <li>my.package.MyClass</li>
	 * <li>my.package.MyClass.getInstance()</li>
	 * <li>my.package.MyClassFactory.getMyClass()</li>
	 * <li>my.package.MyClassFactory.getInstance().getMyClass()</li>
	 * </ul>
	 * <p/>
	 * Methods and constructors are resolved against one of the supported signatures that are declared inside {@link ClassUtils}.
	 * <p/>
	 * Source class is implemented as a convenience parameter to "{@code xml=class:}", thus using the
	 * {@code xml=class:my.pa..} is exactly the same as using {@code xml-class=my.pa..}. Effectively this means that
	 * if this loader is used with the alias syntax, xml classes can be used via {@code source-xml=class:my.package.MyClass}.
	 */
	public static final String PARAM_XML_CLASS = "xml-class";

	/**
	 * <i>Experimental:</i> Sets the URL or local path to JSON encoded content that is translated to XML and included.
	 * <p/>
	 * JSON to XML transcoding is currently experimental and is primarily meant to be used for XSL or XPath matching on
	 * a JSON source (e.g. from a REST web service).
	 * See {@link PositioningJsonDocumentBuilder} for details on how JSON is mapped to XML.
	 */
	public static final String PARAM_JSON = "json";

	/**
	 * <b>Optional parameter</b>, sets the path to an XSL file that is used to transform the given input.
	 * <p/>
	 * The specified file is resolved in exactly the same way as when using the parameter "<code>source</code>"
	 * (see Macro reference).
	 * <p/>
	 * When a transformation is performed, all given macro call parameters are passed to the XSL processor making them
	 * available as xsl params when using global definitions like:<br/>
	 * <code>&lt;xsl:param name="my-macro-param" select="'default-value'"&gt;</code>
	 */
	public static final String PARAM_XSL = "xsl";

	/**
	 * <b>Optional parameter</b>, sets a relative or absolute URL to an XSL file that is added as standard template
	 * to the document being included but it is not executed.
	 * <p/>
	 * This is useful to place <code>&lt;?xml-stylesheet ...</code> declarations in included content.
	 */
	public static final String PARAM_ADD_XSL = "add-xsl";

	/**
	 * <b>Optional parameter</b>, enables the inclusion of the XML declaration (<code>&lt;?xml...</code>) if set to "true".
	 */
	public static final String PARAM_ADD_DECLARATION = "add-declaration";

	/**
	 * Enum parameter that specifies whether XML schemata are loaded in order to include them with the XML content.
	 * (Default: false)
	 * Possible values for this parameter are: (true|false|local-only)
	 * <p/>
	 * Note: Enabling this option does not enable schema validation, it enables loading schema content into the
	 * output parameter {@code schemaMap}.
	 */
	public static final String PARAM_LOAD_SCHEMA = "load-schema";

	/**
	 * <b>Optional parameter</b>, toggles whether the XML is indented before inclusion.
	 */
	public static final String PARAM_INDENT = "indent";

	/**
	 * <b>Optional parameter</b>, sets the target line width for serialized XML documents.
	 * When this value is greater 0, lines are broken at the previous whitespace when the line
	 * length is exceeded.
	 */
	public static final String PARAM_LINE_WIDTH = "line-width";

	/**
	 * Specifies how the XML is serialized.
	 * Possible values are:
	 * <ul>
	 * <li>xml: XML/XHTML output format.</li>
	 * <li>html: XSL HTML output format.</li>
	 * <li>json: Serializes XML-DOM to JSON &amp; JAXB using JAXB-TO-JSON conversion.</li>
	 * <li>json-dom: Uses XML-DOM to JSON serialization for any input data.</li>
	 * </ul>
	 */
	public static final String PARAM_SERIALIZE_AS = "serialize-as";

	/**
	 * <b>Optional parameter</b>, toggles whether the XML parser handles namespaces.
	 * <p/>
	 * The default value for this option is 'true' when an XML transformation is specified (= <code>xsl</code> is set).
	 * Without a xml transformation namespace awareness is turned off by default to simplify xpath based matching.
	 */
	public static final String PARAM_NAMESPACE_AWARE = "namespace-aware";

	/**
	 * Is set with the parsed document object ({@link Document}) and can be accessed from velocity templates.
	 */
	public static final String OUT_PARAM_DOCUMENT = "document";

	/**
	 * Is set with the original parsed document as it existed before applying any transformations.
	 * When <code>xsl</code> is not set, <code>originalDocument</code> and <code>document</code> are the same.
	 */
	public static final String OUT_PARAM_ORIGINAL_DOCUMENT = "originalDocument";

	/**
	 * If filled with a map of all loaded XML schemata (in case of schema loading is enabled).
	 * <p/>
	 * The map contains entries of the format:<code><pre>[
	 *     "schema-system-id": {@link Document},
	 *     ...
	 * ]</pre></code>
	 * <p/>
	 * <i>Note:</i> Schema loading works with any input type that defines a schema (including schema generation on JAXB classes).
	 * <p/>
	 * <i>Tipp:</i> Schema content can either be included in the site, parts can be included via XPath matching or finally schema
	 * files can also be linked using: <code><pre>
	 * Referenced XML schemata:&lt;ul&gt;
	 * #foreach($entry in $schemaMap.entrySet())
	 *    #if($entry.key.startWith("http"))
	 *        #set($href=$entry.key)
	 *    #else
	 *        #set($href=$globals.attachContent($entry.key, $xpath.serialize($entry.value)))
	 *    #end
	 *    &lt;li&gt;&lt;a href="$href"&gt;$href&lt;/a&gt;&lt;/li&gt;
	 * #end &lt;/ul&gt;
	 * </pre></code>
	 */
	public static final String OUT_PARAM_SCHEMA_MAP = "schemaMap";

	/**
	 * Is set with an implementation of "{@link org.tinyjee.maven.dim.utils.XPathEvaluator}" that is configured to match xpath expressions
	 * against DOM tree of {@code document}.
	 * <p/>
	 * Usage (from within a velocity template):<br/>
	 * <code>Title of <b>$document.documentURI</b> is: <b>$xpath.findText("/html/head/title")</b></code>
	 * <p/>
	 * <b>Note:</b> When <code>namespace-aware</code> is set to true, elements are matched using namespace prefixes.
	 * This is true also for the default namespace configured via <code>xmlns="http://some/namespace"</code>. The xpath evaluator
	 * assigns the prefix "<code>default</code>" for the default namespace in order to make it accessible for an xpath evaluation.
	 * <br/>
	 * E.g. the XHTML example from above changes to <code>$xpath.findText("/default:html/default:head/default:title")</code>.
	 * As a consequence, other namespace URIs that use a prefix of "default" will be ignored and are therefore unsupported.
	 */
	public static final String OUT_PARAM_XPATH = "xpath";

	/**
	 * Is the same as "<code>xpath</code>" but configured to match on the original document.
	 */
	public static final String OUT_PARAM_ORIGINAL_XPATH = "originalXpath";

	final transient TransformerFactory transformerFactory = TransformerFactory.newInstance();

	private Object jaxbInstance;

	/**
	 * Constructs a new XML loader.
	 *
	 * @param baseDir       the base dir to of the maven module.
	 * @param requestParams the request params of the macro call.
	 * @throws Exception In case of loading or transforming fails.
	 */
	public XmlLoader(File baseDir, Map<String, Object> requestParams) throws Exception {
		final boolean debug = log.isDebugEnabled();

		putAll(requestParams);

		final boolean containsSource = containsKey(PARAM_SOURCE) || containsKey(PARAM_SOURCE_CONTENT);

		if (containsKey(PARAM_XML_CLASS)) put(PARAM_XML, "class:" + get(PARAM_XML_CLASS));

		Document document, originalDocument;
		Object rawSource = get(containsKey(PARAM_JSON) ? PARAM_JSON : PARAM_XML);
		final Map<String, Document> schemaMap = containsKey(PARAM_LOAD_SCHEMA) &&
				!"false".equalsIgnoreCase(valueOf(get(PARAM_LOAD_SCHEMA))) ? new LinkedHashMap<String, Document>() : null;

		if (rawSource instanceof String) {
			String sourcePath = (String) rawSource;
			Object rawNamespaceAware = containsKey(PARAM_NAMESPACE_AWARE) ? get(PARAM_NAMESPACE_AWARE) : containsKey(PARAM_XSL);
			final boolean namespaceAware = parseBoolean(valueOf(rawNamespaceAware));

			if (sourcePath.startsWith("script:")) {
				put(ScriptInvoker.PARAM_SCRIPT, sourcePath.substring(7));
				sourcePath = "class:" + ScriptInvoker.class.getName();
				put(PARAM_XML, sourcePath);
			}

			if (sourcePath.startsWith("class:")) {
				document = buildDocument(baseDir, sourcePath.substring(6), schemaMap);
			} else if (sourcePath.contains("<?xml")) {
				document = instanceToDocument(String.class, sourcePath, schemaMap);
			} else {
				document = readDocument(baseDir, sourcePath, containsKey(PARAM_JSON) || sourcePath.endsWith(".json"), namespaceAware);
			}
			originalDocument = document;

		} else if (rawSource instanceof Document) {
			if (debug) log.debug("Found raw source is already a document, using this instance instead of loading the document.");
			originalDocument = document = (Document) rawSource;
		} else
			throw new IllegalArgumentException("The specified xml-source '" + rawSource + "' is not supported (use path or an instance of Document).");

		put("document-uri", document.getDocumentURI());

		if (containsKey(PARAM_XSL))
			document = transformDocument(baseDir, document);

		if (containsKey(PARAM_ADD_XSL)) {
			ProcessingInstruction pi = document.createProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"" + get(PARAM_ADD_XSL) + '"');
			document.insertBefore(pi, document.getDocumentElement());
		}

		put(OUT_PARAM_DOCUMENT, document);
		XPathEvaluatorImplementation documentXpathEvaluator = new XPathEvaluatorImplementation(document);
		put(OUT_PARAM_XPATH, documentXpathEvaluator);
		put(OUT_PARAM_ORIGINAL_DOCUMENT, originalDocument);
		put(OUT_PARAM_ORIGINAL_XPATH, new XPathEvaluatorImplementation(originalDocument));

		put(OUT_PARAM_SCHEMA_MAP, schemaMap);
		if (schemaMap != null) {
			if (debug) log.debug("About to load all referenced xml schema documents.");
			loadSchema(document, schemaMap);
		}

		if (debug) {
			if (DebugConstants.PRINT_XML) {
				if (document != originalDocument) {
					log.debug("Original Document:\n" + serializeNode(originalDocument, true, true));
					log.debug("Transformed Document:\n" + serializeNode(document, true, true));
				} else
					log.debug("Document:\n" + serializeNode(document, true, true));
			}
		}

		if (!containsSource) {
			serializeDocument(document);
		}
	}

	private void serializeDocument(Document document) throws IOException {
		boolean debug = log.isDebugEnabled();

		final boolean containedHighlightType = containsKey(PARAM_HIGHLIGHT_TYPE);
		if (!containedHighlightType) put(PARAM_HIGHLIGHT_TYPE, "xml");
		if (!containsKey(PARAM_SOURCE_CONTENT_TYPE)) put(PARAM_SOURCE_CONTENT_TYPE, "xml");

		if (debug) {
			log.debug("Did not find any 'source', assuming the loaded XML document is the content to include. " +
					"(" + PARAM_VERBATIM + '=' + get(PARAM_VERBATIM) + ", " +
					PARAM_HIGHLIGHT_TYPE + '=' + get(PARAM_HIGHLIGHT_TYPE) + ", " +
					PARAM_SOURCE_CONTENT_TYPE + '=' + get(PARAM_SOURCE_CONTENT_TYPE) + ')');
		}

		Object rawLineWidth = get(PARAM_LINE_WIDTH);
		int lineWidth = rawLineWidth == null ? 0 : Integer.parseInt(rawLineWidth.toString());

		String serializeAs = valueOf(get(PARAM_SERIALIZE_AS)).toLowerCase();
		if ("null".equals(serializeAs)) serializeAs = "xml";

		if (asList("xml", "html").contains(serializeAs)) {
			put(PARAM_SOURCE_CONTENT, serializeNode(document, !parseBoolean(valueOf(get(PARAM_ADD_DECLARATION))), true, serializeAs, lineWidth));
		} else {
			if (!containedHighlightType) put(PARAM_HIGHLIGHT_TYPE, "js");

			ObjectMapper mapper = new ObjectMapper();
			SerializationConfig config = mapper.getSerializationConfig().with(Feature.INDENT_OUTPUT).with(Feature.WRITE_ENUMS_USING_TO_STRING);
			mapper.setSerializationConfig(config);

			if ("json-dom".equals(serializeAs) || jaxbInstance == null) {
				CustomSerializerFactory sf = new CustomSerializerFactory();
				sf.addGenericMapping(Element.class, new DomElementJsonSerializer());
				mapper.setSerializerFactory(sf);
				put(PARAM_SOURCE_CONTENT, mapper.writeValueAsString(document.getDocumentElement()));
			} else {
				AnnotationIntrospector introspector = new JaxbAnnotationIntrospector() {
					@Override
					public String findSerializablePropertyName(AnnotatedField af) {
						final String name = super.findSerializablePropertyName(af);
						return af.getAnnotation(XmlAttribute.class) == null ? name : '@' + name;
					}
				};
				mapper.setSerializationConfig(config.withAnnotationIntrospector(introspector));
				put(PARAM_SOURCE_CONTENT, mapper.writeValueAsString(jaxbInstance));
			}
		}
	}

	Document transformDocument(File baseDir, Document document) throws Exception {
		final boolean debug = log.isDebugEnabled();
		if (!containsKey(PARAM_VERBATIM)) put(PARAM_VERBATIM, false);

		URL xslURL = findSource(baseDir, (String) get(PARAM_XSL));
		if (debug) log.debug("About to transform previously loaded XML content using XSL " + xslURL);
		Transformer transformer = transformerFactory.newTransformer(new StreamSource(xslURL.openStream(), xslURL.toString()));
		try {
			for (Map.Entry<String, Object> entry : entrySet()) {
				if (entry.getValue() == null) continue;
				transformer.setParameter(entry.getKey(), entry.getValue());
			}
			document = sourceToDocument(transformer, new DOMSource(document));
		} catch (TransformerException e) {
			log.error("Failed transforming document '" + document.getDocumentURI() + "' using XSL '" + xslURL + "'. " +
					"Run with -X to see the content that failed.", e);
			if (debug) log.debug("Document that failed:\n" + serializeNode(document, true, true));
			throw e;
		}
		return beautifyDocument(document, false);
	}

	Document readDocument(File baseDir, String sourcePath, boolean sourceIsJson, boolean namespaceAware) throws Exception {
		final boolean debug = log.isDebugEnabled();
		final URL xmlURL = findSource(baseDir, sourcePath);
		if (debug)
			log.debug("About to create XML content (namespaceAware=" + namespaceAware + ") from " + (sourceIsJson ? "JSON" : "XML") + " encoded in " + xmlURL);

		final AbstractPositioningDocumentBuilder documentBuilder = sourceIsJson ?
				new PositioningJsonDocumentBuilder() : new PositioningDocumentBuilder(namespaceAware);

		final Reader reader = openReader(xmlURL);
		try {
			return beautifyDocument(documentBuilder.parse(xmlURL, reader), false);
		} catch (SAXParseException e) {
			logSaxParseException(xmlURL, e);
			throw e;
		} catch (TransformerException e) {
			if (e.getCause() instanceof SAXParseException) logSaxParseException(xmlURL, (SAXParseException) e.getCause());
			throw e;
		} finally {
			reader.close();
		}
	}

	void logSaxParseException(URL xmlURL, SAXParseException e) throws IOException {
		log.error("Failed parsing XML '" + xmlURL + "', line '" + e.getLineNumber() + "' column '" + e.getColumnNumber() + "' error " + e.getMessage());
		dumpReaderWithLineNumbers(openReader(xmlURL));
	}

	Reader openReader(URL xmlURL) throws IOException {
		Reader reader;
		if (xmlURL.getPath().endsWith(".vm")) {
			TemplateSource source = new TemplateSource(xmlURL, new UrlSource(xmlURL, this));
			reader = source.getContent().openReader();
		} else {
			InputStream inputStream = UrlFetcher.getSource(xmlURL);
			inputStream.mark(4 * 1024);
			try {
				inputStream = new GZIPInputStream(inputStream);
			} catch (IOException e) {
				if (log.isDebugEnabled()) log.debug(xmlURL + " doesn't seem to be GZIP compressed, using standard read mode.");
				inputStream.reset();
			}
			Charset sourceCharset = UrlFetcher.getSourceCharset(xmlURL);
			reader = new InputStreamReader(inputStream, sourceCharset);
		}
		return reader;
	}

	Document buildDocument(File baseDir, String classExpression, Map<String, Document> schemaMap) throws Exception {
		if (log.isDebugEnabled()) log.debug("About to create XML content from expression '" + classExpression + '\'');

		Object xmlInstance = ClassUtils.invoke(classExpression, baseDir, this);
		return instanceToDocument(xmlInstance.getClass(), xmlInstance, schemaMap);
	}


	void loadSchema(Document document, Map<String, Document> schemaMap) throws Exception {
		final boolean debug = log.isDebugEnabled();
		final boolean localOnly = LOCAL_ONLY.equalsIgnoreCase(valueOf(get(PARAM_LOAD_SCHEMA)));

		for (Node node : new NodeListAdapter(document.getElementsByTagName("*"))) {
			NamedNodeMap attributes = node.getAttributes();
			if (attributes != null) {
				List<String> systemIds = null;

				Node schemaLocation = attributes.getNamedItem("xsi:schemaLocation");
				if (schemaLocation == null) schemaLocation = attributes.getNamedItemNS("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation");

				if (schemaLocation != null) {
					String[] locations = schemaLocation.getNodeValue().split("\\s+");
					if (locations.length % 2 != 0) {
						throw new IllegalArgumentException("Cannot load xml schemata, the location(s) " + Arrays.toString(locations) +
								" specified in element '<" + node.getNodeName() + "/>' are not formatted as expected, " +
								" 'namespace systemId nextNamespace nextSystemId..'");
					}
					systemIds = new ArrayList<String>(locations.length / 2);
					for (int i = 1; i < locations.length; i += 2) systemIds.add(locations[i]);
				}

				schemaLocation = attributes.getNamedItem("xsi:noNamespaceSchemaLocation");
				if (schemaLocation != null) {
					String[] locations = schemaLocation.getNodeValue().split("\\s+");
					if (systemIds == null) systemIds = new ArrayList<String>(locations.length);
					Collections.addAll(systemIds, locations);
				}

				if (systemIds == null)
					continue;

				final Transformer transformer = transformerFactory.newTransformer();
				for (String systemId : systemIds) {
					if (schemaMap.containsKey(systemId)) {
						if (debug) log.debug("Not loading schema '" + systemId + "\' as it was already loaded.");
						continue;
					}

					if (debug) log.debug("About to lookup schema '" + systemId + '\'');
					String documentURI = document.getDocumentURI();
					URI schemaUri = documentURI == null ? URI.create(systemId) : URI.create(documentURI).resolve(systemId);
					if (localOnly && valueOf(schemaUri.getScheme()).toLowerCase(ENGLISH).startsWith("http")) continue;

					systemId = schemaUri.toASCIIString();
					if (debug) log.debug("Loading schema '" + systemId + '\'');
					schemaMap.put(systemId, beautifyDocument(sourceToDocument(transformer, new StreamSource(systemId)), false));
				}
			}
		}
	}

	boolean requiresBeautification() {
		return parseBoolean(valueOf(get(PARAM_INDENT)));
	}

	Document beautifyDocument(Document document, boolean force) throws Exception {
		final boolean debug = log.isDebugEnabled();
		if (!force && !requiresBeautification()) {
			if (debug) log.debug("XML beautification not required, returning original document " + document.getDocumentURI() + '.');
			return document;
		}

		if (debug) log.debug("Beautifying document " + document.getDocumentURI() + '.');
		final Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

		ByteArrayOutputStream buffer = new ByteArrayOutputStream(64 * 1024);
		transformer.transform(new DOMSource(document), new StreamResult(buffer));
		return sourceToDocument(transformer, new StreamSource(new ByteArrayInputStream(buffer.toByteArray()), document.getDocumentURI()));
	}

	Document sourceToDocument(String systemId, javax.xml.transform.Source source) throws Exception {
		source.setSystemId(systemId);
		return sourceToDocument(transformerFactory.newTransformer(), source);
	}

	Document sourceToDocument(Transformer transformer, javax.xml.transform.Source source) throws Exception {
		Document document;
		DOMResult domResult = new DOMResult(null, source.getSystemId());
		transformer.transform(source, domResult);
		document = (Document) domResult.getNode();
		document.setDocumentURI(source.getSystemId());
		return document;
	}

	@SuppressWarnings("unchecked")
	Document instanceToDocument(Class<?> xmlClass, Object xmlInstance, Map<String, Document> schemaMap) throws Exception {
		final boolean debug = log.isDebugEnabled();

		final String className = xmlClass.getName();
		final Document document;

		// Pre-Process XML strings or other source classes that provided XML output
		if (xmlInstance instanceof String) {
			xmlInstance = new StreamSource(new StringReader((String) xmlInstance));
		} else if (xmlInstance instanceof Map) {
			final Object sourceContent = ((Map) xmlInstance).get(PARAM_SOURCE_CONTENT);
			if (sourceContent != null) xmlInstance = new StreamSource(new StringReader((String) sourceContent));
		}

		if (xmlInstance instanceof Document) {
			if (debug) log.debug("Found that class is an instance of 'Document', using it directly");
			document = (Document) xmlInstance;
		} else if (xmlInstance instanceof javax.xml.transform.Source) {
			if (debug) log.debug("Found that class is an instance of 'javax.xml.transform.Source', transforming it to a 'Document'.");
			document = sourceToDocument(className, (javax.xml.transform.Source) xmlInstance);
		} else if (xmlInstance instanceof JAXBElement || xmlClass.isAnnotationPresent(javax.xml.bind.annotation.XmlRootElement.class)) {
			if (debug)
				log.debug("Found that class is an instance of JAXBElement or annotated with 'XmlRootElement', transforming it to a 'Document'.");

			Class<Object> jaxbClass;
			if (xmlInstance instanceof JAXBElement) {
				jaxbClass = ((JAXBElement) xmlInstance).getDeclaredType();
				jaxbInstance = ((JAXBElement) xmlInstance).getValue();
			} else {
				jaxbClass = (Class<Object>) xmlClass;
				jaxbInstance = xmlInstance;
			}

			JaxbXmlSerializer<Object> serializer = new JaxbXmlSerializer<Object>(jaxbClass);
			document = serializer.serialize(xmlInstance, schemaMap != null);
			if (schemaMap != null) {
				for (Map.Entry<String, Document> entry : serializer.generateSchema().entrySet())
					schemaMap.put(entry.getKey(), beautifyDocument(entry.getValue(), true));
			}
		} else {
			if (debug) log.debug("Did not find any supported XML type, using Bean encoding to transform it to a 'Document'.");
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			XMLEncoder encoder = new XMLEncoder(buffer);
			encoder.writeObject(xmlInstance);
			encoder.close();
			document = sourceToDocument(className, new StreamSource(new ByteArrayInputStream(buffer.toByteArray())));
		}

		if (document.getDocumentURI() == null) {
			final String documentURI = xmlClass.getSimpleName().replaceAll("(?!^)([A-Z]+)", "-$1").toLowerCase() + ".xml";
			if (debug) log.debug("The generated document contained no document URI, using '" + documentURI + "' instead.");
			document.setDocumentURI(documentURI);
		}

		return beautifyDocument(document, true);
	}
}
