/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.tinyjee.maven.dim.spi.ExtensionInformation;

import java.util.Map;

import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_SOURCE;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_VERBATIM;

/**
 * Re-arranges selected document sections (hierarchical headlines in Doxia) using a table of configurable columns and rows.
 * <p/>
 * The actual layout is created using client side javascript based on the specified parameters and does gracefully fallback
 * to the standard layout if scripting is not available.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|table-layout=*|columns=3}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 *
 * @author Juergen_Kellerer, 2011-11-08
 */
@ExtensionInformation(displayName = "Table Layout", documentationUrl = "/templatesTableLayout.html")
public class TableLayoutParameterTransformer extends AbstractParameterTransformer {

	/**
	 * Enables this extension and sets a regular expression matching the section titles that should be transformed into a tabs.
	 * <p/>
	 * <b>Note:</b> Setting this property has no effect in case of "<code>source</code>" or "<code>source-class</code>" were
	 * specified with the macro call. This behaviour ensures that the parameter "<code>tabbed-panel</code>" can still be used
	 * with ordinary macro calls where a source or source-class was set.
	 * <p/>
	 * This parameter expects a regular expression that is used to select what sections are placed inside the table layout by matching
	 * the given section titles (headlines).<br/>
	 * By default the javascript looks for the first section that follows the include position and continues with all
	 * siblings (sections of the same level). Neither parent nor child sections are processed, therefore it's mostly safe
	 * to use the <i>match all</i> expression <code>.*</code> if all sub-sequent sections of the same nesting level should
	 * be formatted with table layout.
	 * <p/>
	 * Examples:<ul>
	 * <li>"<code>%{include|table-layout=.*|columns=2}</code>" - Transforms all following sections (of the same level) to tabs.</li>
	 * <li>"<code>%{include|table-layout=.*(Example).*|columns=*}</code>" - Formats all sections containing the word "Example"
	 * using a side by side view.</li>
	 * </ul>
	 */
	public static final String PARAM_TABLE_LAYOUT = "table-layout";

	/**
	 * Specifies the number of columns to render (defaults to '{@code *}').
	 * <p/>
	 * If this parameter is set to asterisk ("{@code *}") the number of columns is unlimited and
	 * calculates out of the number of sections divided by the defined rows. If both {@code columns} and {@code rows}
	 * is set to asterisk the number of columns equals the number of sections while the number of rows becomes "1".
	 */
	public static final String PARAM_COLUMNS = "columns";

	/**
	 * Specifies the number of rows to render (defaults to '{@code *}').
	 * <p/>
	 * If this parameter is set to asterisk ("{@code *}") the number of rows is unlimited and
	 * calculates out of the number of sections divided by the defined number of columns.
	 */
	public static final String PARAM_ROWS = "rows";

	/**
	 * Defines the direction for placing the sections in the table layout (defaults to '{@code top-down}').
	 * <p/>
	 * Possible values:<ul>
	 * <li>{@code top-down} - Places sections below each other and wraps when "{@code rows}" number of sections were placed.</li>
	 * <li>{@code left-right} or {@code ltr} - Places sections next to each other starting from <i>left</i> and wraps when
	 * "{@code columns}" number of sections were placed.</li>
	 * <li>{@code right-left} or {@code rtl} - Places sections next to each other starting from <i>right</i> and wraps when
	 * "{@code columns}" number of sections were placed.</li>
	 * </ul>
	 */
	public static final String PARAM_DIRECTION = "direction";

	/**
	 * Defines the gab (margin) between the table cells (defaults to '{@code 10pt}').
	 * <p/>
	 * Note: The gap can also be defined for the horizontal or vertical direction using {@code horizontal-gap} and {@code vertical-gap}.
	 */
	public static final String PARAM_GAP = "gap";

	/**
	 * Defines the width of the layout (defaults to '{@code 100%}').
	 */
	public static final String PARAM_WIDTH = "width";

	/**
	 * <b>Optional Parameter:</b> Defines the CSS style of the container hosting the elements.
	 * <p/>
	 * Example - Creates a right aligned floating box of all "<i>Hints</i>":<code><pre>
	 * %{include|table-layout=.*(Hint).*|columns=1|width=200|style=border: 1px solid gray; background-color:#eee; float: right;}</pre></code>
	 */
	public static final String PARAM_STYLE = "style";

	/**
	 * <b>Optional Parameter:</b> Defines the CSS style class of the container hosting the elements.
	 */
	public static final String PARAM_STYLE_CLASS = "style-class";

	/**
	 * <b>Optional Parameter:</b> Defines the CSS style of the TD elements.
	 */
	public static final String PARAM_CELL_STYLE = "cell-style";

	/**
	 * <b>Optional Parameter:</b> Specifies a comma separates list of width values (CSS format) to apply with the columns.
	 * The number of width values doesn't need to match the number of actual columns as missing values are interpolated.
	 */
	public static final String PARAM_COLUMN_WIDTHS = "column-widths";

	/**
	 * <b>Optional Parameter:</b> Specifies a comma separates list of classes to apply with the columns. The number of classes
	 * doesn't need to match the number of actual columns as missing values are interpolated.
	 */
	public static final String PARAM_COLUMN_CLASSES = "column-classes";

	/**
	 * <b>Optional Parameter:</b> Specifies a comma separates list of height values (CSS format) to apply with the rows.
	 * The number of height values doesn't need to match the number of actual rows as missing values are interpolated.
	 */
	public static final String PARAM_ROW_HEIGHTS = "row-heights";

	/**
	 * <b>Optional Parameter:</b> Specifies a comma separates list of classes to apply with the rows. The number of classes
	 * doesn't need to match the number of actual rows as missing values are interpolated.
	 */
	public static final String PARAM_ROW_CLASSES = "row-classes";

	private static final String OUT_PARAM_SECTIONS = "sections";


	@Override
	protected boolean doTransformParameters(Map<String, Object> params) {
		Object sections = params.get(PARAM_TABLE_LAYOUT);
		if (sections != null) {
			params.put(PARAM_SOURCE, "classpath:/templates/dim/table-layout.html.vm");
			params.put(PARAM_VERBATIM, false);
			params.put(OUT_PARAM_SECTIONS, "*".equals(sections) ? ".*" : sections);

			if ("left-right".equals(params.get(PARAM_DIRECTION)) || "left-to-right".equals(params.get(PARAM_DIRECTION)))
				params.put(PARAM_DIRECTION, "ltr");
			else if ("right-left".equals(params.get(PARAM_DIRECTION)) || "right-to-left".equals(params.get(PARAM_DIRECTION)))
				params.put(PARAM_DIRECTION, "rtl");

			return true;
		}
		return false;
	}
}
