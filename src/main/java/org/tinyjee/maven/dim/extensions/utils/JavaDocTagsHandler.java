/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.utils;

import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaMethod;
import org.apache.maven.doxia.logging.Log;
import org.codehaus.plexus.util.StringUtils;
import org.tinyjee.maven.dim.DebugConstants;
import org.tinyjee.maven.dim.spi.Globals;
import org.w3c.tidy.Tidy;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Map;
import java.util.Queue;
import java.util.StringTokenizer;

import static org.tinyjee.maven.dim.extensions.JavaSourceLoader.PARAM_API_DOCS;

/**
 * Implements a handler that can process javadoc comments and replace tags with XHTML markup.
 */
public class JavaDocTagsHandler implements Serializable {

	private static final Log log = Globals.getLog();
	private static final long serialVersionUID = 689877916746238765L;

	private JavaClass javaClass;
	private String javaDocRootPath;
	private Map<String, Object> packageMappings;
	private Tidy tidy = new Tidy();

	{
		tidy.setXmlOut(true);
		tidy.setTabsize(4);
		tidy.setWraplen(Integer.MAX_VALUE);//disable line wrapping
		tidy.setFixBackslash(true);
		tidy.setFixUri(true);
		tidy.setFixComments(true);
		tidy.setNumEntities(true);
		tidy.setForceOutput(true);
		tidy.setPrintBodyOnly(true);
		tidy.setDropEmptyParas(false);
		tidy.setTrimEmptyElements(false);
		tidy.setBreakBeforeBR(false);
		tidy.setIndentContent(false);
		tidy.setIndentCdata(false);
		tidy.setIndentAttributes(false);
		tidy.setLiteralAttribs(true);
	}

	private DocumentBuilder xmlParser;

	{
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		try {
			xmlParser = documentBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		}
	}

	public JavaDocTagsHandler() {
	}

	public JavaDocTagsHandler(JavaClass javaClass, String javaDocRootPath, Map<String, Object> packageMappings) {
		this.javaClass = javaClass;
		this.javaDocRootPath = javaDocRootPath;
		this.packageMappings = packageMappings == null ? Collections.<String, Object>emptyMap() : packageMappings;

		if (log.isDebugEnabled()) {
			if (DebugConstants.PRINT_JAVADOC) {
				log.debug("Created new JavaDocTagsHandler for class '" + javaClass.getFullyQualifiedName() + "' linking to " +
						"javaDocRootPath='" + javaDocRootPath + "'.");
				for (Map.Entry<String, Object> entry : this.packageMappings.entrySet()) {
					if (entry.getKey().startsWith(PARAM_API_DOCS) && !PARAM_API_DOCS.equals(entry.getKey()))
						log.debug("Package '" + entry.getKey().substring(PARAM_API_DOCS.length() + 1) + "' is linked to:" + entry.getValue());
				}
			}
		}
	}

	/**
	 * Processes all inline tags inside the javadoc comment of the method that is on top of the provided inheritance stack.
	 *
	 * @param methodStack a stack of methods that override each other. The top of the stack is the method to create the javadoc comment for.
	 * @return a comment with all common inline tags being processed.
	 */
	public String processJavaDocComment(Queue<JavaMethod> methodStack) {
		JavaMethod topMethod = methodStack.poll();
		if (topMethod == null) return null;

		String comment = topMethod.getComment();
		if (comment == null || comment.contains("{@inheritDoc}")) {
			String parentComment = processJavaDocComment(methodStack);
			comment = comment == null ? parentComment : comment.replace("{@inheritDoc}", parentComment == null ? "" : parentComment);
		}

		return processJavaDocComment(comment);
	}

	/**
	 * Processes all inline tags inside the javadoc comment.
	 *
	 * @param comment the comment to process.
	 * @return a comment with all common inline tags being processed.
	 */
	public String processJavaDocComment(String comment) {
		if (comment == null) return comment;

		final boolean debug = log.isDebugEnabled() && DebugConstants.PRINT_JAVADOC;
		if (debug) log.debug("About to process inline javadoc tags in comment '" + comment + '\'');

		final StringBuilder buffer = new StringBuilder(comment.length());
		final StringTokenizer tokenizer = new StringTokenizer(comment, "{}", true);

		int openBraceCount = 0;
		boolean previousIsOpenBrace = false;
		String currentInlineTag = null;
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			switch (token.charAt(0)) {
				case '{':
					openBraceCount++;
					previousIsOpenBrace = true;
					break;
				case '}':
					openBraceCount--;
					if (openBraceCount <= 0) {
						openBraceCount = 0;
						if (currentInlineTag != null)
							token = handleJavaDocInlineTagClose(currentInlineTag);
						currentInlineTag = null;
					}
				default:
					if (previousIsOpenBrace) {
						if (currentInlineTag == null && token.startsWith("@")) {
							boolean containsWhitespace = token.contains(" ");
							currentInlineTag = containsWhitespace ? token.substring(1, token.indexOf(' ')) : token.substring(1);
							token = token.substring((containsWhitespace ? 2 : 1) + currentInlineTag.length());
							token = handleJavaDocInlineTagContent(currentInlineTag, token, true);
						} else {
							if (currentInlineTag == null) openBraceCount = 0;
							buffer.append('{');
						}
					} else if (currentInlineTag != null)
						token = handleJavaDocInlineTagContent(currentInlineTag, token, false);

					buffer.append(token);
					previousIsOpenBrace = false;
			}
		}

		if (debug) log.debug("Process result '" + buffer + '\'');

		return createValidXml(buffer.toString());
	}

	String createValidXml(String comment) {
		final String xmlCommentSource = "<comment>" + comment + "</comment>";
		try {
			xmlParser.parse(new InputSource(new StringReader(xmlCommentSource)));
			return comment;
		} catch (SAXException ignored) {
			log.warn("The comment '" + xmlCommentSource + "' is not well formed XML, will apply HTML tidy to correct this problem.");
		} catch (IOException ignored) {
			return comment;
		}

		final ByteArrayOutputStream errorBuffer = new ByteArrayOutputStream();
		tidy.setErrout(new PrintWriter(errorBuffer));

		final StringWriter output = new StringWriter(comment.length() + 64);
		tidy.parse(new StringReader(comment), output);
		final String tidiedComment = output.toString();

		if (errorBuffer.size() > 0) {
			log.error("The javadoc comment '" + comment + "' contained XHTML errors:\n" + errorBuffer.toString());
			log.error("Returning cleaned-up comment '" + tidiedComment + "' instead.");
		}

		return tidiedComment.trim();
	}

	String handleJavaDocInlineTagContent(String tag, String content, boolean first) {
		boolean isPlainLink = "linkplain".equalsIgnoreCase(tag);
		if ("link".equals(tag) || isPlainLink) {
			try {
				return first ? createApiDocsLink(content, isPlainLink) : "";
			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException(e);
			}
		} else if ("docRoot".equalsIgnoreCase(tag)) {
			return javaDocRootPath;
		} else {
			boolean isCode = "code".equals(tag);
			if (isCode || "literal".equals(tag)) {
				content = content.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;");
				if (isCode) content = content.replaceAll("(\\n\\r|\\n|\\r)", "<br/>\n");
				return first ? (isCode ? "<code>" : "") + content : content;
			}
		}

		return content;
	}

	String handleJavaDocInlineTagClose(String tag) {
		if ("code".equals(tag)) return "</code>";
		return "";
	}

	String createApiDocsLink(String content, boolean isPlainLink) throws UnsupportedEncodingException {
		String linkText = "";
		if (content.contains(" ")) {
			final int firstWhitespace = content.indexOf(' ', Math.max(0, content.indexOf(')')));
			if (firstWhitespace != -1) {
				linkText = content.substring(firstWhitespace + 1).trim();
				content = content.substring(0, firstWhitespace).trim();
			}
		}

		int anchorIndex = content.indexOf('#');
		String fullyQualifiedType = anchorIndex == 0 ? javaClass.getFullyQualifiedName() :
				javaClass.resolveType(anchorIndex == -1 ? content : content.substring(0, anchorIndex));

		if (fullyQualifiedType == null)
			fullyQualifiedType = content;
		else if (anchorIndex != -1)
			fullyQualifiedType += content.substring(anchorIndex);

		String linkBase = findLinkBaseForType(fullyQualifiedType);
		String linkTarget = fullyQualifiedType.replace('.', '/');
		if (StringUtils.isEmpty(linkText))
			linkText = linkTarget.contains("/") ? linkTarget.substring(linkTarget.lastIndexOf('/') + 1) : linkTarget;

		anchorIndex = linkTarget.indexOf('#');
		if (anchorIndex != -1) {
			String fragment = linkTarget.substring(anchorIndex + 1);
			linkTarget = linkTarget.substring(0, anchorIndex) + ".html#" + URLEncoder.encode(fragment, "UTF-8");
		} else {
			linkTarget += ".html";
		}

		if (!isPlainLink) linkText = "<code>" + linkText + "</code>";
		return "<a href=\"" + linkBase + linkTarget.replace('$', '.') + "\" title=\"" + fullyQualifiedType + "\">" + linkText + "</a>";
	}

	String findLinkBaseForType(String fullyQualifiedType) {
		String linkBase = javaDocRootPath;
		StringBuilder packageName = new StringBuilder(fullyQualifiedType.length()).append(PARAM_API_DOCS).append('-');
		for (String part : fullyQualifiedType.split("\\.")) {
			if (packageName.length() > PARAM_API_DOCS.length() + 1) packageName.append('.');
			packageName.append(part);
			Object otherLinkBase = packageMappings.get(packageName.toString());
			//System.out.println("Checking: " + packageName + " resulted in: " + otherLinkBase);
			if (otherLinkBase != null)
				linkBase = otherLinkBase.toString();
		}
		return linkBase;
	}

	@Override
	public String toString() {
		return "JavaDocTagsHandler{" +
				"javaClass=" + javaClass +
				", javaDocRootPath='" + javaDocRootPath + '\'' +
				", packageMappings=" + packageMappings +
				'}';
	}
}
