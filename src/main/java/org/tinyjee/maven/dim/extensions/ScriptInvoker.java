/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.apache.maven.doxia.logging.Log;
import org.codehaus.plexus.util.FileUtils;
import org.tinyjee.maven.dim.extensions.script.ScriptContextAwareResourceLoader;
import org.tinyjee.maven.dim.spi.ExtensionInformation;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.utils.AbstractAliasHandler;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Boolean.parseBoolean;
import static java.lang.String.valueOf;
import static org.tinyjee.maven.dim.IncludeMacroSignature.*;
import static org.tinyjee.maven.dim.extensions.ScriptExecutor.*;
import static org.tinyjee.maven.dim.spi.ResourceResolver.findSource;
import static org.tinyjee.maven.dim.spi.UrlFetcher.getReadableSource;
import static org.tinyjee.maven.dim.spi.UrlFetcher.readerToString;

/**
 * Provides a runtime environment that allows to load and execute script code using the JSR-223 interface (scripting for java).
 * <p/>
 * Scripts can either provide input for velocity templates or can produce content directly. When invoked, this extension first
 * populates a defined set of global variables within the script context, evaluates the script and copies any changes or newly allocated
 * variables back into the original macro parameters map.
 * <p/>
 * When {@code source} or {@code source-content} is specified in addition to {@code source-script}, the specified source content
 * is loaded after the script finished execution and all final script variables are accessible via {@code $variableName}.
 * (Source content is by default interpreted as a velocity template unless template processing is explicitly disabled.)
 * <p/>
 * When <b>no</b> {@code source} and {@code source-content} is specified with the macro call, any text output that is produced by scripts
 * is directly copied into the generated page ("non-verbatim" by default, thus HTML markup is the assumed output format).
 * <p/>
 * The following global variables are exposed in the script context. Please note that the standard template parameters are
 * NOT exposed within scripts, use {@code request.get("paramName")} to access macro call parameters:
 * <ul>
 * <li>"{@code request}" Contains a map of the original macro request parameters.</li>
 * <li>"{@code globals}" points to an instance of "{@link Globals}" which allows to access base path, path resolution logic,
 * loading &amp; attaching content, class loading, etc.</li>
 * <li>"{@code scriptName}" is a string that is filled with the absolute path to the script file.</li>
 * <li>"{@code scriptPath}" is a string that is filled with the absolute path containing the script file.</li>
 * <li>"{@code scriptFile}" is a string that is filled with the name of the script file.</li>
 * <li>"{@code scriptUri}" is a string that is filled with URI pointing to the script file.</li>
 * </ul>
 * <p/>
 * This extension is only useful when JSR-223 is available. Starting with Java 6, JSR-223 including a JavaScript interpreter is
 * bundled and available by default. Adding other script engines to the maven dependency set of your module or the site-plugin enables
 * scripting languages like: Groovy, Ruby, Python and others that offer integration with JSR-223.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|source=template.vm|source-script=my-script.js}
 * %{include|source-script=my-script.js}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 * Full call syntax:<code><pre>
 * %{include|source=template.vm|source-class=org.tinyjee.maven.dim.extensions.ScriptInvoker|script=my-script.js}
 * </pre></code>
 * <p/>
 * <b>Notes:</b>
 * <ul>
 * <li>"{@code source-script}" vs "{@code source-class}": Under normal circumstances it is preferable to use your own Java class via
 * 'source-class' for creating dynamic content as it adds unit testing and compiler checks. Scripting however, when combined with
 * "{@code site:run}" allows to create content with scripts very quickly as no additional compilation step is needed to see live
 * changes to the site.</li>
 * <li>Scripts run in a request scope with global bindings (global variables) being shared amongst executions and script engines.
 * Whether this has an effect on the scripts depends on the implementation of the script interpreter and the language itself
 * (see JSR-223 specs.).</li>
 * </ul>
 *
 * @author Juergen_Kellerer, 2011-10-15
 */
@ExtensionInformation(displayName = "Script Invoker", documentationUrl = "/extensionsScriptInvoker.html")
public class ScriptInvoker extends HashMap<String, Object> {

	private static final Log log = Globals.getLog();
	private static final long serialVersionUID = 8955658194210732920L;

	private static final Pattern EVAL_PREFIX_PATTERN = Pattern.compile("^eval\\[([^\\[\\]\\s]+)\\]:(.+)$", Pattern.DOTALL);

	/**
	 * Implements the "{@link org.tinyjee.maven.dim.extensions.ScriptInvoker#PARAM_ALIAS}" alias functionality.
	 */
	public static class AliasHandler extends AbstractAliasHandler {
		/**
		 * Constructs the handler (Note: Is called by {@link org.tinyjee.maven.dim.spi.RequestParameterTransformer#TRANSFORMERS}).
		 */
		public AliasHandler() {
			super(PARAM_ALIAS, PARAM_SCRIPT, ScriptInvoker.class.getName());
		}
	}

	/**
	 * Defines a shortcut for the request properties 'source-class' and 'script'.
	 * <p/>
	 * If this parameter is present inside the request parameters list, the system will effectively behave as if
	 * 'source-class' and 'script' where set separately.
	 */
	public static final String PARAM_ALIAS = "source-script";

	/**
	 * Sets the URL or local path to the script file to load and evaluate.
	 * <p/>
	 * The specified file is resolved in exactly the same way as when using the parameter "<code>source</code>" (see Macro reference).
	 * <p/>
	 * <i>Notes:</i><ul>
	 * <li>In addition to setting a file path, the parameter can be used to define an inline script snippet using the syntax
	 * "{@code eval[type]:script...}".
	 * E.g. for javascript this looks like {@code %{include|source-script=eval[js]:print("Hello World");}}.</li>
	 * <li>When no further source is specified, any text output produced by the script (e.g. via {@code print "some text"})
	 * is included into the current page. STDERR is always sent to both locations, the build logs and the page. Unhandled script errors
	 * will break the build.</li>
	 * <li>As velocity template processing is applied at a later stage, the script output is processed by velocity unless the parameter
	 * "{@code source-is-template}" is set to <i>false</i> or another "{@code source}" was specified. Thus a script can dynamically
	 * generate a velocity template and provide the parameters for the template context (though this might be kind of a overkill).</li>
	 * </ul>
	 */
	public static final String PARAM_SCRIPT = "script";

	/**
	 * Optional boolean parameter indicating that the resulting final script variables context should not be copied into the macro
	 * parameters map to avoid any further processing and or collisions within the macro engine.
	 * <p/>
	 * When set to "true" the script must produce the "source content" of the page by printing to STDOUT.
	 *
	 * @since 1.2
	 */
	public static final String PARAM_ISOLATE = "isolate";

	/**
	 * Optional parameter containing the commandline to pass to the script.
	 *
	 * @since 1.2
	 */
	public static final String PARAM_ARGV = "argv";

	/**
	 * Optional parameter specifying whether all macro parameters are registered as global variables within the script.
	 * <p/>
	 * <b>Note:</b> In DIM 1.1 this used to be the default. However when scripting is used more seriously, it's too easy that
	 * parameters overlap with macro parameters thus the recommended setting is to leave this at the default value and access
	 * macro parameters via {@code request.get("paramName")} instead of just {@code paramName}.
	 *
	 * @since 1.2
	 */
	public static final String PARAM_REGISTER_GLOBALS = "register-globals";

	private static ScriptExecutor executor;

	private static synchronized ScriptExecutor getExecutor() {
		if (executor == null) {
			try {
				final String executorClass = System.getProperty("dim.script.invoker.engine.class",
						"org.tinyjee.maven.dim.extensions.script.ScriptExecutorJSR223");
				executor = (ScriptExecutor) Class.forName(executorClass).newInstance();
			} catch (Throwable e) {
				StringWriter buffer = new StringWriter();
				e.printStackTrace(new PrintWriter(buffer));
				final String errorMessage = e.toString(), stackTrace = buffer.toString();

				executor = new ScriptExecutor() {
					public void execute(Map<String, Object> context, String scriptExtension, String scriptContent, String scriptName) throws Exception {
						log.error("Failed executing script '" + scriptName + "':\n----\n" + scriptContent + "\n----\n, " +
								"caused by: " + errorMessage + "\n" + stackTrace);
						context.put(PARAM_SOURCE_CONTENT, "Failed executing script '" + scriptName + "', caused by: " + errorMessage);
					}

					public Map<String, Object> getScriptContext() {
						return null;
					}
				};
			}
		}
		return executor;
	}

	private static SoftReference<ScriptContextAwareResourceLoader> resourceLoader = new SoftReference<ScriptContextAwareResourceLoader>(null);

	private static synchronized ScriptContextAwareResourceLoader getClassLoader() {
		final ClassLoader parentLoader = Globals.getInstance().getClassLoader();

		ScriptContextAwareResourceLoader loader = resourceLoader.get();
		if (loader == null || loader.getParent() != parentLoader) {
			loader = new ScriptContextAwareResourceLoader(parentLoader);
			resourceLoader = new SoftReference<ScriptContextAwareResourceLoader>(loader);
		}

		return loader;
	}

	/**
	 * Constructs a new Script Invoker.
	 *
	 * @param baseDir       the base dir to of the maven module.
	 * @param requestParams the request params of the macro call.
	 * @throws Exception In case of loading or transforming fails.
	 */
	public ScriptInvoker(File baseDir, Map<String, Object> requestParams) throws Exception {

		putAll(requestParams);

		final String script = (String) get(PARAM_SCRIPT);
		if (script == null) throw new IllegalArgumentException("No script (parameter '" + PARAM_SCRIPT + "') was defined.");

		final String scriptExtension, scriptContent, scriptName;

		final Matcher evalMatcher = EVAL_PREFIX_PATTERN.matcher(script);
		final URL scriptURL;
		if (evalMatcher.matches()) {
			scriptExtension = evalMatcher.group(1);
			scriptContent = evalMatcher.group(2);
			scriptName = baseDir.getAbsolutePath() + File.separator + "local." + scriptExtension;
			scriptURL = new File(scriptName).toURI().toURL();
		} else {
			scriptURL = findSource(baseDir, script);
			scriptContent = readerToString(getReadableSource(scriptURL), true);
			scriptName = "file".equals(scriptURL.getProtocol()) ? new File(scriptURL.toURI()).getAbsolutePath() : scriptURL.toString();
			scriptExtension = FileUtils.extension(scriptName);
		}

		final HashMap<String, Object> scriptContext = new HashMap<String, Object>();
		scriptContext.put(PARAM_VERBATIM, get(PARAM_VERBATIM));
		scriptContext.put(PARAM_SOURCE_CONTENT, get(PARAM_SOURCE_CONTENT));

		scriptContext.put(REQUEST, requestParams);
		scriptContext.put(GLOBALS, Globals.getInstance());
		scriptContext.put(SCRIPT_NAME, scriptName);
		scriptContext.put(SCRIPT_URI, scriptURL);

		if ("true".equalsIgnoreCase(valueOf(get(PARAM_REGISTER_GLOBALS))))
			scriptContext.putAll(this);

		int index = scriptName.replace('\\', '/').lastIndexOf('/');
		scriptContext.put(SCRIPT_PATH, index > 0 ? scriptName.substring(0, index) : "");
		scriptContext.put(SCRIPT_FILE, scriptName.substring(index + 1));

		final Thread thread = Thread.currentThread();
		final ClassLoader loader = thread.getContextClassLoader();
		try {
			final ScriptContextAwareResourceLoader classLoader = getClassLoader();
			classLoader.setScriptContextPath(new File((String) scriptContext.get(SCRIPT_PATH)));
			thread.setContextClassLoader(classLoader);

			for (ScriptEnvironment environment : ScriptEnvironment.ENVIRONMENTS) environment.build(classLoader);

			getExecutor().execute(scriptContext, scriptExtension, scriptContent, scriptName);

			if (parseBoolean(valueOf(get(PARAM_ISOLATE)))) {
				if (get(PARAM_SOURCE) != null || get(PARAM_FILE) != null) {
					throw new IllegalArgumentException("'source' must not be set with the macro call, when scripts run with " +
							"the option 'isolate'. The script extension cannot remove a previously specified 'source' setting, thus " +
							"not the script output would be included but the content of '" + get(PARAM_SOURCE) + "' instead.");
				}
				// Removing parameters that would change the output.
				put(PARAM_SNIPPET, null);
				put(PARAM_VERBATIM, scriptContext.get(PARAM_VERBATIM));
				put(PARAM_SOURCE_CONTENT, scriptContext.get(PARAM_SOURCE_CONTENT));
			} else
				putAll(scriptContext);
		} finally {
			thread.setContextClassLoader(loader);
			for (ScriptEnvironment environment : ScriptEnvironment.ENVIRONMENTS) environment.cleanUp();
		}
	}
}
