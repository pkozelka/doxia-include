/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.image;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Convenience class to create a  BufferedImage of an area on the screen.
 * Generally there are  four different scenarios. Create an image of:
 * <ul>
 * <li>an entire component</li>
 * <li>a region of the component</li>
 * <li>the entire desktop</li>
 * <li>a region of the desktop</li>
 * </ul>
 * The first two use the Swing paint() method to draw the
 * component image to the BufferedImage. The latter two use the
 * AWT Robot to create the BufferedImage.
 * <p/>
 * Although this class was originally designed to create an image of a
 * component on the screen it can be used to create an image of components
 * not displayed on a GUI. Behind the scenes the component will be given a
 * size and the component will be layed out. The default size will be the
 * preferred size of the component although you can invoke the setSize()
 * method on the component before invoking a createImage(...) method. The
 * default functionality should work in most cases. However the only
 * foolproof way to get a image to is make sure the component has been
 * added to a realized window with code something like the following:
 * <p/>
 * JFrame frame = new JFrame();
 * frame.setContentPane( someComponent );
 * frame.pack();
 * ImageCapture.createImage( someComponent );
 *
 * @author Rob Camick, http://tips4java.wordpress.com/2008/10/13/screen-image/
 */
public class ImageCapture {
	private ImageCapture() {
	}

	/*
	 *  Create a BufferedImage for Swing components.
	 *  The entire component will be captured to an image.
	 *
	 *  @param  component Swing component to create image from
	 *  @return	image the image for the given region
	*/
	public static BufferedImage createImage(JComponent component) {
		Dimension dimension = component.getSize();

		if (dimension.width == 0 || dimension.height == 0) {
			dimension = component.getPreferredSize();
			component.setSize(dimension);
		}

		Rectangle region = new Rectangle(0, 0, dimension.width, dimension.height);
		return createImage(component, region);
	}

	/*
	 *  Create a BufferedImage for Swing components.
	 *  All or part of the component can be captured to an image.
	 *
	 *  @param  component Swing component to create image from
	 *  @param  region The region of the component to be captured to an image
	 *  @return	image the image for the given region
	*/
	public static BufferedImage createImage(JComponent component, Rectangle region) {
		//  Make sure the component has a size and has been layed out.
		//  (necessary check for components not added to a realized frame)
		layoutIfRequired(component);
		return paintComponent(component, region);
	}

	private static void layoutIfRequired(Component component) {
		if (!component.isDisplayable()) {
			Dimension dimension = component.getSize();

			if (dimension.width == 0 || dimension.height == 0) {
				dimension = component.getPreferredSize();
				component.setSize(dimension);
			}

			layoutComponent(component);
		}
	}

	private static BufferedImage paintComponent(Component component, Rectangle region) {
		BufferedImage image = new BufferedImage(region.width, region.height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics = image.createGraphics();
		try {
			graphics.translate(-region.x, -region.y);
			component.paint(graphics);
		} finally {
			graphics.dispose();
		}
		return image;
	}

	/**
	 * Convenience method to create a BufferedImage of the desktop
	 *
	 * @return image the image for the given region
	 * @throws AWTException see Robot class constructors
	 * @throws IOException  if an error occurs during writing
	 */
	public static BufferedImage createDesktopImage() throws AWTException, IOException {
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		return createImage(new Rectangle(dimension));
	}

	/*
	 *  Create a BufferedImage for AWT components.
	 *
	 *  @param  component AWT component to create image from
	 *  @return	image the image for the given region
	 *  @exception AWTException see Robot class constructors
	*/
	public static BufferedImage createImage(Component component, boolean forceNoScreenCapture) throws AWTException {
		if (forceNoScreenCapture || GraphicsEnvironment.isHeadless()) {
			layoutComponent(component);
			return paintComponent(component, component.getBounds());
		} else {
			Point point = new Point(0, 0);
			SwingUtilities.convertPointToScreen(point, component);
			Rectangle region = component.getBounds();
			region.setLocation(point);
			return createImage(region);
		}
	}

	/**
	 * Create a BufferedImage from a rectangular region on the screen.
	 * This will include Swing components JFrame, JDialog and JWindow
	 * which all extend from Component, not JComponent.
	 *
	 * @param region region on the screen to create image from
	 * @return image the image for the given region
	 * @throws AWTException see Robot class constructors
	 */
	public static BufferedImage createImage(Rectangle region) throws AWTException {
		if (region.width < 0 || region.height < 0) {
			final DisplayMode displayMode = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode();
			if (region.width < 0) region.width = Math.max(0, displayMode.getWidth() + region.width);
			if (region.height < 0) region.height = Math.max(0, displayMode.getHeight() + region.height);
		}
		return new Robot().createScreenCapture(region);
	}

	static void layoutComponent(Component component) {
		synchronized (component.getTreeLock()) {
			component.doLayout();

			if (component instanceof Container) {
				for (Component child : ((Container) component).getComponents()) {
					layoutComponent(child);
				}
			}
		}
	}
}
