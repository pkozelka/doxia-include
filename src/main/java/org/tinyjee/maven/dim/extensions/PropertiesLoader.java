/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.apache.maven.doxia.logging.Log;
import org.codehaus.plexus.util.FileUtils;
import org.tinyjee.maven.dim.spi.ExtensionInformation;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.utils.AbstractAliasHandler;
import org.tinyjee.maven.dim.utils.SelectableArrayList;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TreeSet;

import static org.tinyjee.maven.dim.spi.ResourceResolver.findSource;

/**
 * Loads properties files and extends or overwrites the macro request parameters by the properties contained in the file.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|source=template.vm|source-properties=props.(xml|properties)}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 * Full call syntax:<code><pre>
 * %{include|source=template.vm|source-class=org.tinyjee.maven.dim.extensions.PropertiesLoader|properties=props.(xml|properties)}
 * </pre></code>
 *
 * @author Juergen_Kellerer, 2010-09-08
 */
@ExtensionInformation(displayName = "Properties Loader", documentationUrl = "/extensionsMiscellaneous.html#a3._Properties_Loader")
public class PropertiesLoader extends LinkedHashMap<String, Object> {

	private static final long serialVersionUID = 7451224247313497738L;

	/**
	 * Implements the "{@link PropertiesLoader#PARAM_ALIAS}" alias functionality.
	 */
	public static class AliasHandler extends AbstractAliasHandler {
		/**
		 * Constructs the handler (Note: Is called by
		 * {@link org.tinyjee.maven.dim.spi.RequestParameterTransformer#TRANSFORMERS}).
		 */
		public AliasHandler() {
			super(PARAM_ALIAS, PARAM_PROPERTIES, PropertiesLoader.class.getName());
		}
	}

	/**
	 * Defines a shortcut for the request properties '{@code source-class}' and '{@code properties}'.
	 * <p/>
	 * If this parameter is present inside the request parameters list, the system
	 * will effectively behave as if 'source-class' and 'properties' where set
	 * separately.
	 */
	public static final String PARAM_ALIAS = "source-properties";

	/**
	 * Sets the path to the properties file to load inside the request parameters.
	 * <p/>
	 * The specified file is retrieved in exactly the same way as if the "{@code source}"
	 * parameter would have been used (see Macro reference).
	 * <p/>
	 * <i>Note:</i> Properties can use the traditional {@code .properties} or
	 * the newer {@code .xml} format. Format selection is performed by file extension.
	 */
	public static final String PARAM_PROPERTIES = "properties";

	/**
	 * Is filled with the loaded {@link Properties} instance.
	 * <p/>
	 * Note: All loaded properties are exposed in the standard scope as request properties and are accessible
	 * like any other parameters specified with the macro call.
	 * <br/>
	 * This additional map based access allows to query properties whose names are not compatible with the variable naming
	 * scheme and it also allows to iterate all loaded properties using {@code $loadedProperties.entrySet()}.
	 */
	public static final String OUT_PARAM_LOADED_PROPERTIES = "loadedProperties";

	/**
	 * Is filled with a sorted, regex-enabled list of property keys (=
	 * <code>new {@link SelectableArrayList}&lt;String&gt;(new TreeSet&lt;String&gt;(properties.keySet()));</code>).
	 */
	public static final String OUT_PARAM_LOADED_PROPERTY_KEYS = "loadedPropertyKeys";

	/**
	 * Constructs a new properties loader searching for properties using the given base path.
	 *
	 * @param baseDir       the base dir to of the maven module.
	 * @param requestParams the request params of the macro call.
	 */
	@SuppressWarnings("unchecked")
	public PropertiesLoader(File baseDir, Map<String, Object> requestParams) {
		final Log log = Globals.getLog();
		final boolean debug = log.isDebugEnabled();

		putAll(requestParams);

		URL propertiesURL = findSource(baseDir, (String) requestParams.get(PARAM_PROPERTIES));
		try {
			Properties properties = new Properties();
			boolean isXml = "xml".equalsIgnoreCase(FileUtils.extension(propertiesURL.getFile()));
			if (debug) log.debug("About to load properties from '" + propertiesURL + "'. Used format: " + (isXml ? "xml" : "properties"));
			if (isXml)
				properties.loadFromXML(propertiesURL.openStream());
			else
				properties.load(propertiesURL.openStream());

			put(OUT_PARAM_LOADED_PROPERTIES, properties);
			SelectableArrayList sortedKeys = new SelectableArrayList(new TreeSet(properties.keySet()));
			put(OUT_PARAM_LOADED_PROPERTY_KEYS, sortedKeys);

			if (debug) log.debug("Loaded properties:");
			for (Object key : sortedKeys) {
				String propertyKey = key.toString(), propertyValue = properties.getProperty(propertyKey);
				put(propertyKey, propertyValue);
				if (debug) log.debug(propertyKey + "=" + propertyValue);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
