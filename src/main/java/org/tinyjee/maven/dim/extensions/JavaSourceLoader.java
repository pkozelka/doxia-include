/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import com.thoughtworks.qdox.JavaClassContext;
import com.thoughtworks.qdox.model.*;
import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.DebugConstants;
import org.tinyjee.maven.dim.IncludeMacroSignature;
import org.tinyjee.maven.dim.extensions.utils.JavaDocTagsHandler;
import org.tinyjee.maven.dim.extensions.utils.JavaEntitiesList;
import org.tinyjee.maven.dim.spi.ExtensionInformation;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.utils.*;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Boolean.parseBoolean;
import static java.lang.String.valueOf;
import static java.util.Collections.emptyMap;
import static java.util.Collections.synchronizedMap;
import static org.tinyjee.maven.dim.utils.AbstractSelectableJavaEntitiesList.isAnnotation;

/**
 * Loads a java class (at source level including JavaDoc) and makes the class details available to velocity templates.
 * It can effectively be used to include content into maven sites that is similar to (and in sync with) JavaDoc, with the
 * difference that only those portions are included that are important within a certain scope.
 * <p/>
 * Use this loader when you want to access the parsed content of Java sources within velocity templates using the variables that are
 * exposed by this loader. This extension is not needed if your aim is to include code snippets.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|source=template.vm|source-java=net.sf.MyClass}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 * Full call syntax:<code><pre>
 * %{include|source=template.vm|source-class=org.tinyjee.maven.dim.extensions.JavaSourceLoader|java-source=net.sf.MyClass}
 * </pre></code>
 * <p/>
 * <b>Notes:</b><ul>
 * <li>
 * All lists used by this extension derive from {@link org.tinyjee.maven.dim.utils.AbstractSelectableJavaEntitiesList} which means
 * they allow chained calls to {@code selectMatching(..)}, {@code selectNonMatching(..)}, {@code selectAnnotated(..)}, etc. in order to
 * further limit the amount of returned java entities. Example:
 * <code><pre>##Select all properties starting with "my" but not containing "Internal":
 * #set($myPublicProperties = $class.properties.selectMatching("..my*").selectNonMatching("..*Internal*"))
 * </pre></code>
 * </li>
 * <li>
 * All maps used by this extension derive from {@link PrintableMap} which logs any failed key access including the
 * existing map content and defines additional methods like "{@link PrintableMap#getContentAsString()}" and
 * "{@link PrintableMap#printContent()}" that may be used print the map structure.
 * </li>
 * <li>
 * Applied doclet tags are parsed and translated to XHTML markup. Non-inline tags are stripped from the comments and copied
 * into a map of the format {@code Map<String,(String|Map<String,String>)>}, where all tags are mapped as {@code String}
 * key and value pair with one exception of "{@code param}" being mapped to a Map of {@code paramName} and {@code paramDescription}.
 * </li>
 * <li>
 * Applied annotations are pre-processed for simplified access within a velocity template and are stored within list rows
 * using a map (usually referenced via "annotations"). The simple name of the mapped annotation starting with "{@code @}" is used as
 * map key and the values are maps of annotation parameter name and value pairs. (values are always serialized to String)
 * </li>
 * </ul>
 *
 * @author Juergen_Kellerer, 2010-09-08
 */
@ExtensionInformation(displayName = "Java Source Loader", documentationUrl = "/extensionsJavaSourceLoader.html")
public class JavaSourceLoader extends HashMap<String, Object> {

	private static final Log log = Globals.getLog();
	private static final long serialVersionUID = -4069212921446859127L;

	private static final String JAVA_EXT = ".java";

	/**
	 * Implements the "{@link JavaSourceLoader#PARAM_ALIAS}" alias functionality.
	 */
	public static class AliasHandler extends AbstractAliasHandler {
		/**
		 * Constructs the handler (Note: Is called by
		 * {@link org.tinyjee.maven.dim.spi.RequestParameterTransformer#TRANSFORMERS}).
		 */
		public AliasHandler() {
			super(PARAM_ALIAS, PARAM_JAVA_SOURCE, JavaSourceLoader.class.getName());
		}
	}

	/**
	 * Defines a shortcut for the request properties 'source-class' and 'java-source'.
	 * <p/>
	 * If this parameter is present inside the request parameters list, the system
	 * will effectively behave as if 'source-class' and 'java-source' where set
	 * separately.
	 */
	public static final String PARAM_ALIAS = "source-java";

	/**
	 * Is the expected input parameter that points to the class or interface to scan.
	 * <p/>
	 * The class can be specified as file (with .java as extension) or as fully qualified class name.
	 * Valid examples:<ul>
	 * <li>your.package.YourJavaClass</li>
	 * <li>your/package/YourJavaClass.java</li>
	 * <li>src/main/java/your/package/YourJavaClass.java</li>
	 * </ul>
	 * <p/>
	 * If only a package name or package file path is given, the extension loads the package information
	 * instead. When this feature is used most output parameters will be empty.
	 * Valid examples:<ul>
	 * <li>your.package</li>
	 * <li>your.package</li>
	 * <li>your/package</li>
	 * <li>src/main/java/your/package</li>
	 * </ul>
	 */
	public static final String PARAM_JAVA_SOURCE = "java-source";

	/**
	 * Optional input parameter specifying the standard path to the apidocs to use when linking via
	 * {@literal {@link}} or {@literal {@linkplain}}.
	 * <p/>
	 * Defaults to "./apidocs/", package specific, alternate link paths can be specified using
	 * "{@code apidocs-my.package=http://some-host/apidocs/}".
	 */
	public static final String PARAM_API_DOCS = "apidocs";

	/**
	 * Optional boolean input parameter specifying whether the extension overwrites other input parameters.
	 * <br/>
	 * If not set (=default) nothing is overwritten except "{@code $class}".
	 */
	public static final String PARAM_OVERWRITE = "overwrite";

	/**
	 * Optional boolean input parameter specifying whether property and method lists should include
	 * all reachable methods from superclasses (does not influence {@code $select} in any way).
	 * <br/>
	 * (defaults to false).
	 */
	public static final String PARAM_INCLUDE_PARENTS = "include-parents";

	/**
	 * Optional input parameter specifying the minimum visibility level of java elements inside the
	 * properties, constants, methods and fields lists and also for the class selector (e.g. classesBelowPackage,
	 * superClasses, etc).
	 * <br/>
	 * One of "'public', 'protected', 'package-local' (or 'pl') and private" (defaults to 'protected').
	 */
	public static final String PARAM_VISIBILITY = "visibility";

	/**
	 * Optional boolean input parameter specifying whether the extension registers legacy indexed list-like access to maps.
	 * <br/>
	 * (defaults to false).
	 */
	public static final String PARAM_LEGACY_MODE = "legacy-mode";


	/**
	 * Is set with a reference to this loader instance.
	 *
	 * @since 1.2
	 */
	public static final String OUT_PARAM_LOADER = "javaLoader";

	/**
	 * Is filled with the fully qualified name of the specified class.
	 */
	public static final String OUT_PARAM_NAME = "name";

	/**
	 * Is filled with the simple name of the specified class.
	 */
	public static final String OUT_PARAM_SIMPLE_NAME = "simpleName";

	/**
	 * Is filled with a map containing all other output parameters that are set by this extension.
	 * Therefore calls like {@code $class.simpleName} and {@code $simpleName} lead to exactly the same results.
	 */
	public static final String OUT_PARAM_CLASS = "class";

	/**
	 * One of "class", "interface", "enum" or "annotation".
	 */
	public static final String OUT_PARAM_TYPE = "classType";

	/**
	 * Is filled with the {@link com.thoughtworks.qdox.model.JavaClass} instance of the specified class and allows
	 * accessing the QDox reflection model directly.
	 */
	public static final String OUT_PARAM_JAVA_CLASS = "javaClass";

	/**
	 * Is filled with an implementation of {@link JavaEntitiesSelector} that may be used to access nested, super, implemented and derived classes.
	 * Example Usage with Velocity:<code><pre>
	 * #foreach($derivedClass in $class.select.derivedClasses)
	 *     "$derivedClass.name" derives from $class.name
	 * #end
	 * </pre></code>
	 */
	public static final String OUT_PARAM_SELECTOR = "select";

	/**
	 * Is filled with a the JavaDoc comment of the specified class.
	 */
	public static final String OUT_PARAM_COMMENT = "comment";

	/**
	 * Is filled with a the JavaDoc Doclet-Tags of the specified class.
	 * <p/>
	 * The tags map has a format like <code><pre>[
	 *     "tagName": "tagValue",
	 *     ...
	 *     !! Special treatment for '@param' !!
	 *     "param": [
	 *         "parameterName": "JavaDoc comment",
	 *         ...
	 *     ]
	 * ]</pre></code>
	 * Please note that any leading "{@code @}" characters are removed from the map keys, thus "{@code @since}"
	 * becomes {@code $class.tags.since}.
	 */
	public static final String OUT_PARAM_TAGS = "tags";

	/**
	 * Is filled with a map of annotations that are applied to the class.
	 * <p/>
	 * Applied annotations are pre-processed for simplified access within a velocity template by translating the QDox model into a map.
	 * The simple name of the mapped annotation starting with "{@code @}" is used as map key and the values are maps of annotation
	 * parameter name and value pairs. All values are serialized to string, which includes arrays &amp; annotation hierarchies that were
	 * set as values.
	 * <p/>
	 * The annotation map has a format like <code><pre>[
	 *     "@SimpleAnnotationName": [
	 *         "parameterName": "value",
	 *         ...
	 *     ],
	 *     ...
	 * ]</pre></code>
	 * Accessing the default value of an annotation works as expected, e.g.:
	 * <code><pre>
	 * Method suppresses: $class.methods.get(0).annotations.get("@SuppressWarnings").value</pre></code>
	 */
	public static final String OUT_PARAM_ANNOTATIONS = "annotations";

	/**
	 * Is filled with a selectable list of public or protected class fields.
	 * Every list row is a map that follows the format:
	 * <code><pre>[
	 *     "type": type (signature-string),
	 *     "typeClass": Map&lt;String, Object&gt; contains the loaded type.
	 *     "genericType": [
	 *         "type": "fullyQualifiedClassName"
	 *         "typeClass": Map&lt;String, Object&gt; contains the loaded type.
	 *         "parameters": [ ["type": "...", "typeClass": ..., "parameters": [...], ... ]
	 *     ],
	 *     "declaringType": the type that declares this field (class-name),
	 *     "declaringClass": Map&lt;String, Object&gt; contains the loaded declaring class.
	 *     "name": simple field name,
	 *     "value": initialization expression,
	 *     "comment": JavaDoc comment,
	 *     "tags": Map&lt;String,(String|Map&lt;String,String&gt;)&gt;
	 *     "field": {@link com.thoughtworks.qdox.model.JavaField},
	 *     "annotations": Map&lt;String,Map&lt;String,String&gt;&gt;
	 * ]</pre></code>
	 */
	public static final String OUT_PARAM_FIELDS = "fields";

	/**
	 * Contains the same content as <code>"fields"</code> presented as Map&lt;String,Map&lt;?,?&gt;&gt; (mapping "rows" by name).
	 */
	public static final String OUT_PARAM_FIELDS_MAP = "fieldsMap";

	/**
	 * Is filled with a selectable list of all class fields that are not static and final.
	 * Every list row is a map that follows the same format as used in {@code "fields"}.
	 */
	public static final String OUT_PARAM_DECLARED_FIELDS = "declaredFields";

	/**
	 * Contains the same content as <code>"declaredFields"</code> presented as Map&lt;String,Map&lt;?,?&gt;&gt; (mapping "rows" by name).
	 */
	public static final String OUT_PARAM_DECLARED_FIELDS_MAP = "declaredFieldsMap";

	/**
	 * Is filled with a selectable list of visible (see visibility) class constants (static final fields).
	 * Every list row is a map that follows the format:
	 * <code><pre>[
	 *     "type": type (signature-string),
	 *     "typeClass": Map&lt;String, Object&gt; contains the loaded type.
	 *     "genericType": [
	 *         "type": "fullyQualifiedClassName"
	 *         "typeClass": Map&lt;String, Object&gt; contains the loaded type.
	 *         "parameters": [ ["type": "...", "typeClass": ..., "parameters": [...], ... ]
	 *     ],
	 *     "declaringType": the type that declares this constant (class-name),
	 *     "declaringClass": Map&lt;String, Object&gt; contains the loaded declaring class.
	 *     "name": simple field name,
	 *     "value": initialization expression,
	 *     "comment": JavaDoc comment,
	 *     "tags": Map&lt;String,(String|Map&lt;String,String&gt;)&gt;
	 *     "field": {@link com.thoughtworks.qdox.model.JavaField},
	 *     "annotations": Map&lt;String,Map&lt;String,String&gt;&gt;
	 * ]</pre></code>
	 */
	public static final String OUT_PARAM_CONSTANTS = "constants";

	/**
	 * Contains the same content as <code>"constants"</code> presented as Map&lt;String,Map&lt;?,?&gt;&gt; (mapping "rows" by name).
	 */
	public static final String OUT_PARAM_CONSTANTS_MAP = "constantsMap";

	/**
	 * Is filled with a selectable list of all class constants (static final fields).
	 * Every list row is a map that follows the same format as used in {@code "constants"}.
	 */
	public static final String OUT_PARAM_DECLARED_CONSTANTS = "declaredConstants";

	/**
	 * Contains the same content as <code>"declaredConstants"</code> presented as Map&lt;String,Map&lt;?,?&gt;&gt; (mapping "rows" by name).
	 */
	public static final String OUT_PARAM_DECLARED_CONSTANTS_MAP = "declaredConstantsMap";

	/**
	 * Is filled with a selectable list of bean properties.
	 * Every list row is a map that follows the format:
	 * <code><pre>[
	 *     "type": property type (signature-string),
	 *     "typeClass": Map&lt;String, Object&gt; contains the loaded type.
	 *     "genericType": [
	 *         "type": "fullyQualifiedClassName"
	 *         "typeClass": Map&lt;String, Object&gt; contains the loaded type.
	 *         "parameters": [ ["type": "...", "typeClass": ..., "parameters": [...], ... ]
	 *     ],
	 *     "declaringType": the type that declares this property (class-name),
	 *     "declaringClass": Map&lt;String, Object&gt; contains the loaded declaring class.
	 *     "name": simple property name (equals the name of the field),
	 *     "comment": getter or field JavaDoc comment (field comment is used when getter contains no comment),
	 *     "tags": Map&lt;String,(String|Map&lt;String,String&gt;)&gt;
	 *     "setterComment": setter JavaDoc comment,
	 *     "setterTags": Map&lt;String,(String|Map&lt;String,String&gt;)&gt;
	 *     "property": {@link com.thoughtworks.qdox.model.BeanProperty},
	 *     "getter": {@link com.thoughtworks.qdox.model.JavaMethod},
	 *     "setter": {@link com.thoughtworks.qdox.model.JavaMethod},
	 *     "field": {@link com.thoughtworks.qdox.model.JavaField},
	 *     "value": field initialization expression,
	 *     "annotations": Map&lt;String,Map&lt;String,String&gt;&gt;
	 * ]</pre></code>
	 * <p/>
	 * <b>Note:</b> The annotations map contains a combined map of all annotations that were either specified on field, setter,
	 * or getter (using this precedence on conflicts).
	 */
	public static final String OUT_PARAM_PROPERTIES = "properties";

	/**
	 * Contains the same content as <code>"properties"</code> presented as Map&lt;String,Map&lt;?,?&gt;&gt; (mapping "rows" by name).
	 */
	public static final String OUT_PARAM_PROPERTIES_MAP = "propertiesMap";

	/**
	 * Is filled with a selectable list of abstract or interface methods (=all visible (see visibility) methods that are
	 * declared but not implemented by the loaded class).
	 * <p/>
	 * Every list row is a map that follows the format:
	 * <code><pre>[
	 *     "type": return type (signature-string),
	 *     "typeClass": Map&lt;String, Object&gt; contains the loaded type.
	 *     "genericType": [
	 *         "type": "fullyQualifiedClassName"
	 *         "typeClass": Map&lt;String, Object&gt; contains the loaded type.
	 *         "parameters": [ ["type": "...", "typeClass": ..., "parameters": [...], ... ]
	 *     ],
	 *     "declaringType": the type that declares this method (class-name),
	 *     "declaringClass": Map&lt;String, Object&gt; contains the loaded declaring class.
	 *     "name": simple method name,
	 *     "signature": method call signature (signature-string),
	 *     "comment": JavaDoc comment,
	 *     "tags": Map&lt;String,(String|Map&lt;String,String&gt;)&gt;
	 *     "method": {@link com.thoughtworks.qdox.model.JavaMethod},
	 *     "superMethods": is filled with a selectable list of all super or interface {@link com.thoughtworks.qdox.model.JavaMethod},
	 *     "annotations": Map&lt;String,Map&lt;String,String&gt;&gt;,
	 *     "parameters": "name" => [
	 *         "name": name of the parameter,
	 *         "type": parameter type (signature-string),
	 *         "typeClass": Map&lt;String, Object&gt; contains the loaded type.
	 *         "genericType": [
	 *             "type": "fullyQualifiedClassName"
	 *             "typeClass": Map&lt;String, Object&gt; contains the loaded type.
	 *             "parameters": [ ["type": "...", "typeClass": ..., "parameters": [...], ... ]
	 *         ],
	 *         "parameter": {@link com.thoughtworks.qdox.model.JavaParameter},
	 *         "comment": JavaDoc comment,
	 *     ],
	 *     "parameterAnnotations": Map&lt;String,Map&lt;String,Map&lt;String,String&gt;&gt;&gt;
	 * ]</pre></code>
	 * <p/>
	 * This list does not contain property related methods like getters and setters.
	 */
	public static final String OUT_PARAM_INTERFACE_METHODS = "interfaceMethods";

	/**
	 * Contains the same content as <code>"interfaceMethods"</code> presented as Map&lt;String,Map&lt;?,?&gt;&gt; (mapping "rows" by name).
	 */
	public static final String OUT_PARAM_INTERFACE_METHODS_MAP = "interfaceMethodsMap";

	/**
	 * Is filled with a selectable list of visible (see visibility) methods.
	 * Every list row is a map that follows the same format as used in {@code "interfaceMethods"}.
	 */
	public static final String OUT_PARAM_METHODS = "methods";

	/**
	 * Is filled with a selectable list of all declared class methods.
	 * Every list row is a map that follows the same format as used in {@code "methods"}.
	 */
	public static final String OUT_PARAM_DECLARED_METHODS = "declaredMethods";

	/**
	 * Contains the same content as <code>"methods"</code> presented as Map&lt;String,Map&lt;?,?&gt;&gt; (mapping "rows" by name).
	 */
	public static final String OUT_PARAM_METHODS_MAP = "methodsMap";

	/**
	 * Contains the same content as <code>"declaredMethods"</code> presented as Map&lt;String,Map&lt;?,?&gt;&gt; (mapping "rows" by name).
	 */
	public static final String OUT_PARAM_DECLARED_METHODS_MAP = "declaredMethodsMap";

	private static final List<String> visibilityModifiers = Arrays.asList("public", "protected", "package-local", "private");

	JavaClass javaClass;
	String javaDocRootPath = "./apidocs/";
	JavaDocTagsHandler javaDocTagsHandler;

	final Map<String, JavaMethod[]> allReachableMethodsCache = new HashMap<String, JavaMethod[]>();

	int includedVisibility = visibilityModifiers.indexOf("protected");

	/**
	 * Constructs a new interface scanner.
	 *
	 * @param baseDir       the base dir to of the maven module.
	 * @param requestParams the request params of the macro call.
	 */
	public JavaSourceLoader(File baseDir, Map<String, Object> requestParams) {
		try {
			final boolean debug = log.isDebugEnabled();
			final String classOrPackageName = (String) requestParams.get(PARAM_JAVA_SOURCE);
			try {
				if (debug) log.debug("Attempting to load source for '" + classOrPackageName + '\'');
				final JavaSource source = getSource(baseDir, classOrPackageName);
				init(source, requestParams);
			} catch (IllegalArgumentException e) {
				if (debug) log.debug("Direct match was not found, trying to interpret the name as package.");
				final JavaPackage[] javaPackages = getPackages(baseDir, classOrPackageName);
				if (javaPackages.length == 0) {
					if (debug) {
						log.debug("Direct match on package name was not found either, looking for expression or " +
								"class name from dependencies.");
					}

					AbstractSelectableJavaEntitiesList<Map<?, ?>> entities = null;
					String commonPackageName = JavaClassLoader.findCommonPackageName();
					if (commonPackageName != null && !"".equals(commonPackageName)) {
						final JavaSourceLoader packageLoader = new JavaSourceLoader(baseDir, commonPackageName);
						final JavaEntitiesSelector selector = (JavaEntitiesSelector) packageLoader.get(OUT_PARAM_SELECTOR);

						entities = selector.getClassesBelowPackage().selectMatching(classOrPackageName);
						if (entities.isEmpty()) {
							try {
								Globals.getInstance().getClassLoader().loadClass(classOrPackageName);
								entities = packageLoader.convert(classOrPackageName);
							} catch (ClassNotFoundException ignored) {
							}
						}
					}

					if (entities != null && !entities.isEmpty())
						init((JavaClass) entities.get(0).get(OUT_PARAM_JAVA_CLASS), requestParams);
					else {
						throw new IllegalArgumentException('\'' + classOrPackageName + "' " +
								"doesn't seem to be a valid package name nor is it a resolvable java source.", e);
					}
				} else {
					String packageName = classOrPackageName;
					if (packageName.indexOf(':') != -1) packageName = packageName.substring(packageName.lastIndexOf(':') + 1);
					init(packageName, javaPackages, requestParams);
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Constructs a new interface scanner for the given interface or abstract class.
	 *
	 * @param baseDir   the base dir to of the maven module.
	 * @param className the class describing the interface.
	 */
	public JavaSourceLoader(File baseDir, String className) {
		this(baseDir, Collections.singletonMap(PARAM_JAVA_SOURCE, (Object) className));
	}

	/**
	 * Constructs a new interface scanner for the given java class.
	 *
	 * @param javaClass     the java class to scan.
	 * @param requestParams optional request params.
	 */
	protected JavaSourceLoader(JavaClass javaClass, Map<String, Object> requestParams) {
		init(javaClass, requestParams);
	}

	/**
	 * Initializes the map values.
	 *
	 * @param source        the source to scan.
	 * @param requestParams optional additional request params.
	 */
	protected final void init(JavaSource source, Map<String, Object> requestParams) {
		if (log.isDebugEnabled()) log.debug("About to load first java class of source '" + source.getURL() + "'.");

		final JavaClass[] classes = source.getClasses();
		if (classes.length == 0)
			throw new IllegalArgumentException("The given source '" + source.getURL() + "' did not contain a single class.");
		JavaClass javaClass = classes[0];

		// Support direct inner class references (using the $ delimiter).
		String className = requestParams == null ? null : (String) requestParams.get(PARAM_JAVA_SOURCE);
		if (className != null && className.contains("$")) {
			className = className.substring(className.indexOf('$') + 1).replace('$', '.');
			javaClass = javaClass.getNestedClassByName(className);
			if (javaClass == null)
				throw new IllegalArgumentException("Did not find nested class: " + className + " in " + classes[0]);
		}

		init(javaClass, requestParams);
	}

	private static Map<List, SoftReference<LoaderIntrospection>> parsedClassesCache =
			synchronizedMap(new WeakHashMap<List, SoftReference<LoaderIntrospection>>());

	/**
	 * Initializes the map values.
	 *
	 * @param javaClass     the java class to scan.
	 * @param requestParams optional additional request params.
	 */
	protected final void init(final JavaClass javaClass, Map<String, Object> requestParams) {
		final Log log = Globals.getLog();
		final boolean debug = log.isDebugEnabled();
		this.javaClass = javaClass;

		LoaderIntrospection selfIntrospection, cachedSelfIntrospection = null;

		final String fullyQualifiedName = javaClass.getFullyQualifiedName();
		@SuppressWarnings("unchecked")
		final List<Serializable> cacheKey = Arrays.asList(fullyQualifiedName, new HashMap(requestParams));


		Object overwrite = null;

		if (requestParams != null) {
			final SoftReference<LoaderIntrospection> reference = parsedClassesCache.get(cacheKey);
			if (reference != null) cachedSelfIntrospection = reference.get();

			overwrite = requestParams.get(PARAM_OVERWRITE);
			putAll(requestParams);
			String apiDocsPath = (String) requestParams.get(PARAM_API_DOCS);
			if (apiDocsPath != null && apiDocsPath.trim().length() != 0) javaDocRootPath = apiDocsPath;

			String visibility = valueOf(requestParams.get(PARAM_VISIBILITY)).toLowerCase();
			if ("pl".equals(visibility)) visibility = "package-local";
			int v = visibilityModifiers.indexOf(visibility);
			if (v != -1) includedVisibility = v;
		}

		javaDocTagsHandler = new JavaDocTagsHandler(javaClass, javaDocRootPath, requestParams);

		if (debug) log.debug("About to load reflection details on class '" + fullyQualifiedName + '\'');

		if (cachedSelfIntrospection != null) {
			final Long lastLoaded = (Long) cachedSelfIntrospection.get("javaClassesLastLoaded");
			if (lastLoaded != JavaClassLoader.getInstance().getLastModifiedTime())
				cachedSelfIntrospection = null;
		}

		if (cachedSelfIntrospection == null) {
			scanFieldsAndConstants(javaClass);
			scanProperties(javaClass);
			scanMethods(javaClass);

			put(OUT_PARAM_NAME, fullyQualifiedName);
			put(OUT_PARAM_SIMPLE_NAME, javaClass.getName());
			put(OUT_PARAM_JAVA_CLASS, javaClass);
			put(OUT_PARAM_COMMENT, javaDocTagsHandler.processJavaDocComment(javaClass.getComment()));
			put(OUT_PARAM_ANNOTATIONS, asMap(javaClass.getAnnotations()));
			put(OUT_PARAM_TAGS, asMap(javaClass.getTags()));
			put(OUT_PARAM_SELECTOR, new SelectorImplementation(javaClass));
			put(OUT_PARAM_LOADER, this);

			if (isAnnotation(javaClass)) put(OUT_PARAM_TYPE, "annotation");
			else if (javaClass.isInterface()) put(OUT_PARAM_TYPE, "interface");
			else if (javaClass.isEnum()) put(OUT_PARAM_TYPE, "enum");
			else put(OUT_PARAM_TYPE, "class");

			put("javaClassesLastLoaded", JavaClassLoader.getInstance().getLastModifiedTime());

			if (overwrite == null) putAll(requestParams);

			selfIntrospection = new LoaderIntrospection(this, "Reflection details on class '" + fullyQualifiedName + '\'');
			put(OUT_PARAM_CLASS, selfIntrospection);

			if (requestParams != null && !parseBoolean(IncludeMacroSignature.PARAM_NO_CACHE))
				parsedClassesCache.put(cacheKey, new SoftReference<LoaderIntrospection>(selfIntrospection));

			if (debug) {
				if (DebugConstants.PRINT_JAVA_AST) {
					log.debug("Loaded: $" + OUT_PARAM_CLASS + " = " + selfIntrospection.getContentAsString());
				} else {
					log.debug("Loaded: $" + OUT_PARAM_CLASS + " = <<<" + javaClass.getFullyQualifiedName() + ">>>");
				}
			}
		} else {
			putAll(cachedSelfIntrospection);
			put(OUT_PARAM_CLASS, cachedSelfIntrospection);

			if (debug) log.debug("Using Cached: $" + OUT_PARAM_CLASS + " = <<<" + javaClass.getFullyQualifiedName() + ">>>");
		}

		if (overwrite != null && !parseBoolean(valueOf(overwrite))) putAll(requestParams);
	}

	/**
	 * Initializes the map values when loading a package.
	 *
	 * @param javaPackage   the package that is loaded.
	 * @param requestParams the request params that were specified with the macro call.
	 */
	protected final void init(String requestedPackageName, JavaPackage[] javaPackage, Map<String, Object> requestParams) {
		if (requestParams != null) {
			putAll(extractRequestParamsForNewLoader(requestParams));
		}

		put(OUT_PARAM_NAME, requestedPackageName);
		put(OUT_PARAM_SIMPLE_NAME, requestedPackageName);
		put(OUT_PARAM_ANNOTATIONS, javaPackage.length == 1 ? asMap(javaPackage[0].getAnnotations()) : asMap(new Annotation[0]));
		put(OUT_PARAM_SELECTOR, newMultiPackageSelector(javaPackage));
		put(OUT_PARAM_LOADER, this);
	}

	/**
	 * Loads the given class from the module's source directory.
	 *
	 * @param baseDir   the module's base dir (containing the 'src/main/java' folder).
	 * @param className the name of the class.
	 * @return The parsed JavaSource instance.
	 * @throws IOException in case of the java source cannot be found or read.
	 */
	protected final JavaSource getSource(File baseDir, String className) throws IOException {
		className = className.replace('.', '/');
		if (className.contains("$"))
			className = className.substring(0, className.indexOf('$'));

		if (className.endsWith("/java"))
			className = className.substring(0, className.length() - 5) + JAVA_EXT;
		else if (!className.endsWith(JAVA_EXT))
			className += JAVA_EXT;

		return JavaClassLoader.getInstance().addSource(baseDir, className);
	}

	/**
	 * Loads the given package from the module's source directory.
	 *
	 * @param baseDir     the module's base dir (containing the 'src/main/java' folder).
	 * @param packageName the name of the package.
	 * @return The parsed JavaPackage instance or an empty array if no java package was found.
	 * @throws IOException in case of the java source cannot be read.
	 */
	protected static JavaPackage[] getPackages(File baseDir, String packageName) throws IOException {
		String[] names = packageName.split("\\s*,\\s*");
		Set<JavaPackage> packages = new LinkedHashSet<JavaPackage>(names.length * 2);
		for (String name : names) {
			try {
				Collections.addAll(packages, JavaClassLoader.getInstance().addPackageSource(baseDir, name));
			} catch (IllegalArgumentException ignore) {
			}
		}

		return packages.toArray(new JavaPackage[packages.size()]);
	}

	public JavaEntitiesSelector getSelector() {
		return (JavaEntitiesSelector) get(OUT_PARAM_SELECTOR);
	}

	/**
	 * Converts the given collection of loaded java classes to a selectable list of {@link JavaSourceLoader} instances.
	 *
	 * @param classes the classes to convert.
	 * @return a selectable list of {@link JavaSourceLoader} instances.
	 */
	public JavaEntitiesList convertToSelectableList(Collection<? extends Map<?, ?>> classes) {
		return classes instanceof JavaEntitiesList ? (JavaEntitiesList) classes : new JavaEntitiesList(classes, OUT_PARAM_JAVA_CLASS);
	}

	/**
	 * Converts the given JavaClass instances to a selectable list of {@link JavaSourceLoader} instances.
	 *
	 * @param classes the classes to convert.
	 * @return a selectable list of {@link JavaSourceLoader} instances.
	 */
	public JavaEntitiesList convert(Collection<JavaClass> classes) {
		return convert(true, true, classes.toArray(new JavaClass[classes.size()]));
	}

	/**
	 * Converts the given JavaClass instances to a selectable list of {@link JavaSourceLoader} instances.
	 *
	 * @param classes the classes to convert.
	 * @return a selectable list of {@link JavaSourceLoader} instances.
	 */
	public JavaEntitiesList convert(JavaClass... classes) {
		return convert(true, false, classes);
	}

	protected JavaEntitiesList convert(boolean applyVisibility, boolean lazy, JavaClass... classes) {
		final Map<String, Object> requestParams = extractRequestParamsForNewLoader(this);
		final List<Map<String, ?>> sourceLoaders = new ArrayList<Map<String, ?>>(classes.length);

		if (lazy) {
			for (JavaClass aClass : classes) {
				final boolean include = !applyVisibility || isVisible(aClass);
				if (include) sourceLoaders.add(getLazyLoaderReference(aClass));
			}
		} else {
			for (JavaClass aClass : classes) {
				final boolean include = !applyVisibility || isVisible(aClass);
				if (include) sourceLoaders.add(new LoaderIntrospection(new JavaSourceLoader(aClass, requestParams), aClass.toString()));
			}
		}

		return new JavaEntitiesList(sourceLoaders, OUT_PARAM_JAVA_CLASS);
	}

	/**
	 * Converts the given class names to a selectable list of {@link JavaSourceLoader} instances.
	 *
	 * @param classes the classes to convert.
	 * @return a selectable list of {@link JavaSourceLoader} instances.
	 */
	public JavaEntitiesList convert(String... classes) {
		final JavaClassLoader instance = JavaClassLoader.getInstance();
		final List<JavaClass> javaClasses = new ArrayList<JavaClass>(classes.length);
		final JavaClassContext classContext = javaClass == null ? null : javaClass.getJavaClassContext();

		for (String type : classes) {
			JavaClass jc = null;

			if (type != null && type.contains(".")) {
				jc = instance.resolveClass(type);
				if (jc.getPackage() == null) jc = null;
			}

			if (jc == null && classContext != null) {
				String resolvedType = javaClass.resolveType(type);
				if (resolvedType == null) {
					log.info("Failed resolving given java type '" + type +
							"' it is probably not a valid type in the context of '" + javaClass.getFullyQualifiedName() + "'");
					continue;
				} else
					type = resolvedType;

				jc = classContext.getClassByName(type);
			}

			if (jc == null) jc = instance.resolveClass(type);
			if (jc != null) javaClasses.add(jc);
		}

		return convert(javaClasses);
	}

	Map<String, Object> extractRequestParamsForNewLoader(Map<String, Object> requestParams) {
		final Map<String, Object> params = new HashMap<String, Object>();
		for (Map.Entry<String, Object> entry : requestParams.entrySet()) {
			final String key = entry.getKey();
			if (key.startsWith(PARAM_API_DOCS) || key.equals(PARAM_VISIBILITY) || key.equals(PARAM_INCLUDE_PARENTS))
				params.put(key, entry.getValue());
		}
		return params;
	}

	/**
	 * Checks whether the given java entity is visible according to the specified minimum visibility.
	 *
	 * @param javaEntity the java entitiy to check.
	 * @return true when visible.
	 */
	protected boolean isVisible(AbstractJavaEntity javaEntity) {
		if (javaEntity == null) return true;
		switch (includedVisibility) {
			case 0:
				return javaEntity.isPublic();
			case 1:
				return javaEntity.isPublic() || javaEntity.isProtected();
			case 2:
				return !javaEntity.isPrivate();
			default:
				return true;
		}
	}

	/**
	 * Converts the given instance of {@link JavaMethod} to a method-row-item map as used in the methods and interfaceMethods lists.
	 *
	 * @param method the method to convert.
	 * @return a method-row-item map as used in the methods and interfaceMethods lists.
	 */
	public Map<?, ?> asMap(JavaMethod method) {
		final String callSignature = method.getCallSignature();
		final SelectableJavaEntitiesList<JavaMethod> superMethodsAndSelf = findSuperMethods(method, true);

		// Create the tags map.
		List<DocletTag> methodTags = new ArrayList<DocletTag>();
		for (JavaMethod javaMethod : superMethodsAndSelf) methodTags.addAll(Arrays.asList(javaMethod.getTags()));
		Map<String, Object> tags = asMap(methodTags.toArray(new DocletTag[methodTags.size()]));

		// Assemble parameters and parameter-annotations of the method
		Map<String, Map> parameters = new PrintableMap<String, Map>("Parameters :: " + callSignature);
		Map<String, Map> parameterAnnotations = new PrintableMap<String, Map>("Parameter-Annotations :: " + callSignature);
		if (method.getParameters() != null) {
			Map paramTags = (Map) (tags.containsKey("param") ? tags.get("param") : emptyMap());
			for (JavaParameter parameter : method.getParameters()) {
				String parameterComment = paramTags.containsKey(parameter.getName()) ? (String) paramTags.get(parameter.getName()) : null;
				String typeString = typeToString(parameter.getType());
				parameters.put(parameter.getName(), asMap(
						"name", parameter.getName(),
						"type", typeString,
						"typeClass", getLazyLoaderReference(parameter.getType().getJavaClass()),
						"genericType", genericTypeToMap(typeString),
						"parameter", parameter,
						"comment", parameterComment));
				parameterAnnotations.put(parameter.getName(), asMap(parameter.getAnnotations()));
			}
		}


		String typeString = typeToString(method.getReturnType());
		return asMap(
				"type", typeString,
				"typeClass", getLazyLoaderReference(method.getReturnType().getJavaClass()),
				"declaringType", typeToString(method.getParentClass().asType()),
				"genericType", genericTypeToMap(typeString),
				"declaringClass", getLazyLoaderReference(method.getParentClass()),
				"name", method.getName(),
				"signature", callSignature,
				OUT_PARAM_COMMENT, javaDocTagsHandler.processJavaDocComment(new LinkedList<JavaMethod>(superMethodsAndSelf)),
				OUT_PARAM_TAGS, tags,
				"method", method,
				"superMethods", superMethodsAndSelf,
				OUT_PARAM_ANNOTATIONS, asMap(method.getAnnotations()),
				"parameters", parameters,
				"parameterAnnotations", parameterAnnotations);
	}

	/**
	 * Finds all super methods of the specified method (= vector up the declarations in super classes and interfaces).
	 *
	 * @param method  The method to inspect.
	 * @param addSelf Indicates whether the given method is added as first element to the returned list.
	 * @return A list of all super methods (optionally beginning with the given method).
	 */
	public SelectableJavaEntitiesList<JavaMethod> findSuperMethods(final JavaMethod method, boolean addSelf) {
		final JavaClass methodOwner = method.getParentClass();
		final SelectableJavaEntitiesList<JavaMethod> methodList = new SelectableJavaEntitiesList<JavaMethod>();

		final String ownerName = methodOwner.getFullyQualifiedName();
		JavaMethod[] javaMethods = findAllReachableMethods(methodOwner, ownerName);

		final JavaParameter[] parameters = method.getParameters();
		final Type[] parameterTypes = new Type[parameters.length];
		for (int i = 0; i < parameters.length; i++)
			parameterTypes[i] = parameters[i].getType();

		for (JavaMethod javaMethod : javaMethods) {
			if (javaMethod.signatureMatches(method.getName(), parameterTypes, method.isVarArgs())) {
				if (addSelf || !ownerName.equals(method.getParentClass().getFullyQualifiedName()))
					methodList.add(javaMethod);
			}
		}

		return methodList;
	}

	private JavaMethod[] findAllReachableMethods(JavaClass methodOwner) {
		final String ownerName = methodOwner.getFullyQualifiedName();
		return findAllReachableMethods(methodOwner, ownerName);
	}

	private JavaMethod[] findAllReachableMethods(JavaClass methodOwner, String ownerName) {
		JavaMethod[] javaMethods = allReachableMethodsCache.get(ownerName);
		if (javaMethods == null) {
			List<JavaMethod> allMethods = new ArrayList<JavaMethod>(64);
			collectAllMethods(methodOwner, allMethods);
			allReachableMethodsCache.put(ownerName, javaMethods = allMethods.toArray(new JavaMethod[allMethods.size()]));
		}
		return javaMethods;
	}

	private static void collectAllMethods(JavaClass startClass, List<JavaMethod> methodList) {
		methodList.addAll(Arrays.asList(startClass.getMethods()));

		for (JavaClass interfaceClass : startClass.getImplementedInterfaces())
			collectAllMethods(interfaceClass, methodList);

		final JavaClass javaSuperClass = fastGetSuperClass(startClass);
		if (javaSuperClass != null) collectAllMethods(javaSuperClass, methodList);
	}

	/**
	 * Scans all relevant methods of the specified class and sets the results as map values.
	 *
	 * @param javaClass the java class to scan.
	 */
	protected void scanMethods(JavaClass javaClass) {
		final List<Map<?, ?>> interfaceMethods = new JavaEntitiesList("method");
		put(OUT_PARAM_INTERFACE_METHODS, interfaceMethods);
		final List<Map<?, ?>> methods = new JavaEntitiesList("method");
		put(OUT_PARAM_METHODS, methods);
		final List<Map<?, ?>> declaredMethods = new JavaEntitiesList("method");
		put(OUT_PARAM_DECLARED_METHODS, declaredMethods);

		final boolean superclasses = parseBoolean(valueOf(get(PARAM_INCLUDE_PARENTS))), classIsInterface = javaClass.isInterface();

		for (JavaMethod method : javaClass.getMethods(superclasses)) {
			if (method.isConstructor() || method.isPropertyAccessor() || method.isPropertyMutator())
				continue;

			Map<?, ?> row = asMap(method);

			declaredMethods.add(row);

			if (classIsInterface || isVisible(method)) {
				if (classIsInterface || method.isAbstract())
					interfaceMethods.add(row);
				else
					methods.add(row);
			}
		}

		put(OUT_PARAM_INTERFACE_METHODS_MAP, asMap(interfaceMethods, "name"));
		put(OUT_PARAM_METHODS_MAP, asMap(methods, "name"));
		put(OUT_PARAM_DECLARED_METHODS_MAP, asMap(declaredMethods, "name"));
	}

	/**
	 * Converts the given instance of {@link BeanProperty} to a property-row-item map as used in the properties list.
	 *
	 * @param javaClass the class that contains the property.
	 * @param property  the property to convert.
	 * @return a property-row-item map as used in the properties list.
	 */
	public Map<?, ?> asMap(JavaClass javaClass, BeanProperty property) {
		final JavaMethod getter = property.getAccessor();
		final JavaMethod setter = property.getMutator();
		final JavaField field = javaClass.getFieldByName(property.getName());

		final Map<String, Map> annotations = new PrintableMap<String, Map>("Annotations");
		if (getter != null) annotations.putAll(asMap(getter.getAnnotations()));
		if (setter != null) annotations.putAll(asMap(setter.getAnnotations()));
		if (field != null) annotations.putAll(asMap(field.getAnnotations()));

		String typeString = typeToString(property.getType());
		return asMap(
				"type", typeString,
				"typeClass", getLazyLoaderReference(property.getType().getJavaClass()),
				"genericType", genericTypeToMap(typeString),
				"declaringType", typeToString(javaClass.asType()),
				"declaringClass", getLazyLoaderReference(javaClass),
				"name", property.getName(),
				OUT_PARAM_COMMENT, javaDocTagsHandler.processJavaDocComment(getter == null ? "" : getter.getComment() == null ?
				(field == null ? "" : field.getComment()) : getter.getComment()),
				OUT_PARAM_TAGS, getter == null ? field == null ? emptyMap() : asMap(field.getTags()) : asMap(getter.getTags()),
				"setterComment", javaDocTagsHandler.processJavaDocComment(setter == null ? "" : setter.getComment()),
				"setterTags", setter == null ? emptyMap() : asMap(setter.getTags()),
				"property", property,
				"getter", getter,
				"setter", setter,
				"field", field,
				"value", field == null ? "" : field.getInitializationExpression(),
				OUT_PARAM_ANNOTATIONS, annotations);
	}

	/**
	 * Scans all relevant bean properties of the specified class and sets the results as map values.
	 *
	 * @param javaClass the java class to scan.
	 */
	protected void scanProperties(JavaClass javaClass) {
		final List<Map<?, ?>> properties = new JavaEntitiesList("property");
		put(OUT_PARAM_PROPERTIES, properties);

		final boolean superclasses = parseBoolean(valueOf(get(PARAM_INCLUDE_PARENTS)));
		for (BeanProperty property : javaClass.getBeanProperties(superclasses)) {
			if (!isVisible(property.getAccessor()) && !isVisible(property.getMutator())) continue;

			final Map<?, ?> row = asMap(javaClass, property);
			properties.add(row);
		}

		put(OUT_PARAM_PROPERTIES_MAP, asMap(properties, "name"));
	}


	/**
	 * Converts the given instance of {@link JavaField} to a field-row-item map as used in the constants and fields lists.
	 *
	 * @param field the field to convert.
	 * @return a field-row-item map as used in the constants and fields lists.
	 */
	public Map<?, ?> asMap(JavaField field) {
		String typeString = typeToString(field.getType());
		return asMap(
				"type", typeString,
				"typeClass", getLazyLoaderReference(field.getType().getJavaClass()),
				"genericType", genericTypeToMap(typeString),
				"declaringType", typeToString(field.getParentClass().asType()),
				"declaringClass", getLazyLoaderReference(field.getParentClass()),
				"name", field.getName(),
				"value", field.getInitializationExpression(),
				OUT_PARAM_COMMENT, javaDocTagsHandler.processJavaDocComment(field.getComment()),
				OUT_PARAM_TAGS, asMap(field.getTags()),
				"field", field,
				OUT_PARAM_ANNOTATIONS, asMap(field.getAnnotations()));
	}

	/**
	 * Scans all relevant fields and constants of the specified class and sets the results as map values.
	 *
	 * @param javaClass the java class to scan.
	 */
	protected void scanFieldsAndConstants(JavaClass javaClass) {
		final List<Map<?, ?>> constants = new JavaEntitiesList("field"), declaredConstants = new JavaEntitiesList("field");
		put(OUT_PARAM_CONSTANTS, constants);
		put(OUT_PARAM_DECLARED_CONSTANTS, declaredConstants);

		final List<Map<?, ?>> fields = new JavaEntitiesList("field"), declaredFields = new JavaEntitiesList("field");
		put(OUT_PARAM_FIELDS, fields);
		put(OUT_PARAM_DECLARED_FIELDS, declaredFields);

		boolean classIsInterface = javaClass.isInterface();

		for (JavaField field : javaClass.getFields()) {
			boolean fieldIsConstant = classIsInterface || (field.isFinal() && field.isStatic());

			Map<?, ?> row = asMap(field);

			(fieldIsConstant ? declaredConstants : declaredFields).add(row);

			if (!isVisible(field) && !classIsInterface)
				continue;

			(fieldIsConstant ? constants : fields).add(row);
		}

		put(OUT_PARAM_CONSTANTS_MAP, asMap(constants, "name"));
		put(OUT_PARAM_DECLARED_CONSTANTS_MAP, asMap(declaredConstants, "name"));
		put(OUT_PARAM_FIELDS_MAP, asMap(fields, "name"));
		put(OUT_PARAM_DECLARED_FIELDS_MAP, asMap(declaredFields, "name"));
	}

	private boolean isInLegacyMode() {
		return parseBoolean(valueOf(get(PARAM_LEGACY_MODE)));
	}

	/**
	 * Converts the given doclet tags to a map of key->value pairs with any inline doclet tags being converted to XHTML markup.
	 *
	 * @param tags the tags to convert.
	 * @return a key & value
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> asMap(DocletTag... tags) {
		Map<String, Object> result = new PrintableMap<String, Object>("Doclet-Tags");
		for (DocletTag tag : tags) {
			Map targetMap = result;
			String name = tag.getName(), value = tag.getValue();

			if ("see".equalsIgnoreCase(name)) {
				value = "{@link " + value + '}';
			} else if ("param".equalsIgnoreCase(name)) {
				if (tag.getParameters().length > 0) {
					name = tag.getParameters()[0];
					value = value.substring(value.indexOf(name) + name.length());
					if (result.containsKey("param")) targetMap = (Map) result.get("param");
					else result.put("param", targetMap = new PrintableMap<String, Object>("Method-Params-Tags"));
				} else
					continue;
			}

			// Remove extra WS
			if (value != null) value = value.trim();
			// Don't override existing comments
			if ((value == null || value.length() == 0) && targetMap.containsKey(name)) continue;

			targetMap.put(name, javaDocTagsHandler.processJavaDocComment(value));
		}
		return result;
	}

	/**
	 * Converts the given type to string.
	 *
	 * @param type the type to convert.
	 * @return the string representation or an empty string.
	 */
	public static String typeToString(Type type) {
		String value = type == null ? "" : type.toGenericString();

		// This is a workaround for a QDox bug in "type.toGenericString()" that adds too many array brackets.
		if (value.endsWith("[][]")) value = value.substring(0, value.length() - 2);

		return value;
	}

	public Map<String, Object> genericTypeToMap(String genericTypeString) {
		genericTypeString = genericTypeString.replace("? extends ", "").replace("? super ", "").trim();
		final Map<String, Object> result = new PrintableMap<String, java.lang.Object>("GenericTypeMap for '" + genericTypeString + '\'') {

			private static final long serialVersionUID = -6681810977406575171L;

			@Override
			public Object get(Object key) {
				if (!containsKey(key) && "typeClass".equals(key)) {
					JavaEntitiesList entitiesList = convert((String) get("type"));
					put((String) key, entitiesList.isEmpty() ? null : entitiesList.get(0));
				}
				return super.get(key);
			}
		};

		int genericsPosition = genericTypeString.indexOf('<');

		if (genericsPosition == -1) {
			result.put("type", genericTypeString);
			result.put("parameters", Collections.emptyList());
		} else {
			result.put("type", genericTypeString.substring(0, genericsPosition).trim());
			String rawParameters = genericTypeString.substring(genericsPosition + 1, genericTypeString.lastIndexOf('>'));
			ArrayList<Map<String, Object>> parameters = new ArrayList<Map<String, Object>>(3);

			int position = 0, capturedPosition = 0, nextComma, nextBrace, nextEndBrace, nestingLevel = 0;
			while (position < rawParameters.length()) {
				nextComma = rawParameters.indexOf(',', position);
				nextBrace = rawParameters.indexOf('<', position);
				if (nestingLevel == 0) {
					if (nextComma < nextBrace || nextBrace == -1) {
						if (nextComma == -1) {
							parameters.add(genericTypeToMap(rawParameters.substring(position, rawParameters.length())));
							position = rawParameters.length();
						} else {
							if (position != nextComma)
								parameters.add(genericTypeToMap(rawParameters.substring(position, nextComma)));
							position = nextComma + 1;
						}
					} else {
						capturedPosition = position;
						nestingLevel++;
						position = nextBrace + 1;
					}
				} else {
					nextEndBrace = rawParameters.indexOf('>', position);
					if (nextEndBrace < nextBrace || nextBrace == -1) {
						nestingLevel--;
						if (nestingLevel == 0)
							parameters.add(genericTypeToMap(rawParameters.substring(capturedPosition, nextEndBrace + 1)));
						position = nextEndBrace + 1;
					} else {
						nestingLevel++;
						position = nextBrace + 1;
					}
				}
			}

			result.put("parameters", parameters);
		}

		return result;
	}

	private static Field superClassField, isAnnotationField;
	private static volatile Type OBJECT, ENUM;

	static {
		try {
			superClassField = JavaClass.class.getDeclaredField("superClass");
			superClassField.setAccessible(true);
		} catch (NoSuchFieldException e) {
			log.error("Reflective access to JavaClass & Type not available, will need to use slow getter method.", e);
		}
	}

	/**
	 * Implements a reflection based replacement to the original getter {@link JavaClass#getSuperJavaClass()}
	 * as this is very slow due to the use of regular expression matching.
	 *
	 * @param javaClass the class to extract the super class for.
	 * @return the super class of the given type.
	 */
	protected static JavaClass fastGetSuperClass(JavaClass javaClass) {
		if (superClassField == null) return javaClass.getSuperJavaClass();

		Type superType;

		try {
			// Measured speed difference: (~50 times compared to original, still ~15 times when superClass = 'java.lang.Object')
			if (javaClass.isEnum()) {
				if (ENUM == null) ENUM = javaClass.getJavaClassContext().getClassByName("java.lang.Enum").asType();
				superType = ENUM;
			} else {
				superType = (Type) superClassField.get(javaClass);
				if (superType == null) {
					boolean derivesFromObject = (!javaClass.isInterface() &&
							!isAnnotation(javaClass) &&
							!"java.lang.Object".equals(javaClass.getFullyQualifiedName()));

					if (derivesFromObject) {
						if (OBJECT == null) OBJECT = javaClass.getJavaClassContext().getClassByName("java.lang.Object").asType();
						superType = OBJECT;
					}
				}
			}

			return superType == null ? null : superType.getJavaClass();
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Converts the given set of annotations to a map, mapping the simple name of the annotation
	 * against a key=>value map of the specified parameters (the simple name starts with '@').
	 *
	 * @param annotations the annotations to convert to a map.
	 * @return a map mapping the simple name of the annotation to its parameters.
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Map> asMap(Annotation... annotations) {
		final Map<String, Map> result = new PrintableMap<String, Map>("Annotations");
		for (Annotation annotation : annotations) {
			String name = getSimpleAnnotation(annotation);

			Map<Object, Object> map = new PrintableMap<Object, Object>("Annotation-Parameters");
			map.putAll(annotation.getNamedParameterMap());
			for (Map.Entry<Object, Object> entry : map.entrySet()) {
				Object value = entry.getValue();
				if (value != null) entry.setValue(annotationValueToString(value));
			}

			result.put(name, map);
		}

		return result;
	}

	private static Object annotationValueToString(Object value) {
		if (value instanceof List) {
			@SuppressWarnings("unchecked")
			final List valuesList = (List) value;
			StringBuilder result = new StringBuilder(64).append('[');

			for (Object listValue : valuesList) {
				if (listValue instanceof Annotation) {
					Annotation annotation = (Annotation) listValue;
					result.append(getSimpleAnnotation(annotation)).append('(');

					final Map parameterMap = annotation.getNamedParameterMap();
					if (!parameterMap.isEmpty()) {
						for (Object o : parameterMap.entrySet()) result.append(annotationValueToString(o)).append(',');
						result.deleteCharAt(result.length() - 1);
					}

					result.append("),");
				} else
					result.append(annotationValueToString(listValue)).append(",");
			}

			value = result.deleteCharAt(result.length() - 1).append(']').toString();
		} else
			value = value.toString().trim();

		return value;
	}

	private static String getSimpleAnnotation(Annotation annotation) {
		String name = AbstractSelectableJavaEntitiesList.fastGetName(annotation.getType(), false);
		name = "@".concat(name.substring(name.lastIndexOf('.') + 1));
		return name;
	}

	/**
	 * Converts the given key and value pairs to a Map that is linked by key and index (=> associative array).
	 *
	 * @param keyValuePairs the key and value pairs to convert.
	 * @return a Map that is linked by key and index.
	 */
	protected Map<Object, Object> asMap(Object... keyValuePairs) {
		if (keyValuePairs.length % 2 != 0)
			throw new IllegalArgumentException("The key and value input array length must be a multiple of 2.");

		final boolean addIndexKeys = isInLegacyMode();
		final Map<Object, Object> map = new PrintableMap<Object, Object>("Key-Value-Pairs"),
				assocMap = new LinkedHashMap<Object, Object>(keyValuePairs.length / 2);

		for (int i = 0; i < keyValuePairs.length; i += 2) {
			Object key = valueOf(keyValuePairs[i]), value = keyValuePairs[i + 1];
			if (addIndexKeys) map.put(i / 2, value);
			assocMap.put(key, value);
		}

		map.putAll(assocMap);

		return map;
	}

	/**
	 * Converts the given named rows to a map by name.
	 *
	 * @param namedRows the named rows to map.
	 * @param nameKey   the key to use to extract the name.
	 * @return the mapped rows.
	 */
	protected static Map<String, Map<?, ?>> asMap(Iterable<Map<?, ?>> namedRows, String nameKey) {
		final Map<String, Map<?, ?>> map = new PrintableMap<String, Map<?, ?>>("Named-Class-Elements");
		for (Map<?, ?> namedRow : namedRows)
			map.put((String) namedRow.get(nameKey), namedRow);

		return map;
	}

	@Override
	public String toString() {
		return "JavaSourceLoader{" +
				"packageOrClass=" + get(OUT_PARAM_NAME) +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Map)) return false;
		Map that = (Map) o;
		return nameKeyEquals(this, that);
	}

	static boolean nameKeyEquals(Map first, Map second) {
		String name = (String) first.get(OUT_PARAM_NAME), thatName = (String) second.get(OUT_PARAM_NAME);
		return !(name != null ? !name.equals(thatName) : thatName != null);
	}

	@Override
	public int hashCode() {
		return valueOf(get(OUT_PARAM_NAME)).hashCode();
	}

	/**
	 * Implements {@link JavaEntitiesSelector}.
	 */
	private final class SelectorImplementation implements JavaEntitiesSelector {

		private final JavaClass javaClass;
		private final JavaPackage javaPackage;

		private SelectorImplementation(JavaClass javaClass) {
			this.javaClass = javaClass;
			javaPackage = javaClass.getPackage();
		}

		private SelectorImplementation(JavaPackage javaPackage) {
			javaClass = null;
			this.javaPackage = javaPackage;
		}

		public JavaEntitiesList getPackageClasses() {
			final JavaClass[] classes = javaPackage == null ? new JavaClass[0] : javaPackage.getClasses();
			return convert(true, true, classes);
		}

		public JavaEntitiesList getClassesBelowPackage() {
			final List<JavaClass> classes = new ArrayList<JavaClass>();
			if (javaPackage == null)
				classes.addAll(JavaClassLoader.getInstance().getClasses());
			else {
				String prefix = javaPackage.getName() + '.';
				for (JavaPackage candidate : JavaClassLoader.getInstance().getPackages()) {
					if (candidate.equals(javaPackage) || candidate.getName().startsWith(prefix))
						classes.addAll(Arrays.asList(candidate.getClasses()));
				}
			}

			return convert(classes);
		}

		public JavaEntitiesList getNestedClasses() {
			final JavaClass[] nestedClasses = javaClass == null ? new JavaClass[0] : javaClass.getNestedClasses();
			return convert(true, true, nestedClasses);
		}

		public JavaEntitiesList getSuperClasses() {
			final List<JavaClass> knownSuperClasses = new ArrayList<JavaClass>();
			if (javaClass != null) {
				for (JavaClass superClass = fastGetSuperClass(javaClass); superClass != null; superClass = fastGetSuperClass(superClass))
					knownSuperClasses.add(superClass);
			}

			return convert(true, true, knownSuperClasses.toArray(new JavaClass[knownSuperClasses.size()]));
		}

		public JavaEntitiesList getSelfAndSuperClasses() {
			final JavaEntitiesList superClasses = getSuperClasses();
			superClasses.add(0, new LoaderIntrospection(JavaSourceLoader.this, javaClass.toString()));
			return superClasses;
		}

		public JavaEntitiesList getImplementedInterfaces() {
			final JavaClass[] interfaces = javaClass == null ? new JavaClass[0] : javaClass.getImplementedInterfaces();
			return convert(true, true, interfaces);
		}

		public JavaEntitiesList getParentClasses() {
			Map<String, JavaClass> parentClasses = new LinkedHashMap<String, JavaClass>();
			for (JavaMethod method : findAllReachableMethods(javaClass)) {
				JavaClass parentClass = method.getParentClass();
				parentClasses.put(parentClass.getFullyQualifiedName(), parentClass);
			}

			parentClasses.remove(javaClass.getFullyQualifiedName());

			return convert(parentClasses.values());
		}

		public JavaEntitiesList getSelfAndParentClasses() {
			JavaEntitiesList parentClasses = getParentClasses();
			parentClasses.add(0, JavaSourceLoader.this);
			return parentClasses;
		}

		public JavaEntitiesList getDerivedClasses() {
			final JavaClass[] classes = javaClass == null ? new JavaClass[0] : javaClass.getDerivedClasses();
			return convert(true, true, classes);
		}

		public JavaEntitiesList getAnnotatedClasses() {
			if (!isAnnotation(javaClass)) {
				throw new IllegalStateException("Cannot look after classes that are annotated with '" + javaClass +
						"' as this class is no annotation.");
			}

			final SortedSet<JavaClass> allClasses = JavaClassLoader.getInstance().getClasses();
			final SelectableJavaEntitiesList<JavaClass> entitiesList = new SelectableJavaEntitiesList<JavaClass>(allClasses);
			return convert(entitiesList.selectAnnotated('@' + javaClass.getFullyQualifiedName()));
		}

		public JavaEntitiesList getCreatedClasses() {
			final Pattern newStatementFindPattern = Pattern.compile("\\Wnew\\W([\\.\\w]+)\\s*[\\(<]");
			final Matcher matcher = newStatementFindPattern.matcher(javaClass.getCodeBlock());

			int position = 0;
			Set<String> createdTypes = new LinkedHashSet<String>();
			while (matcher.find(position)) {
				createdTypes.add(matcher.group(1));
				position = matcher.end(1);
			}

			return convert(createdTypes.toArray(new String[createdTypes.size()]));
		}

		@Override
		public String toString() {
			return "SelectorImplementation{" +
					"javaClass=" + javaClass +
					", javaPackage=" + javaPackage +
					'}';
		}
	}

	/**
	 * Implements a special selector that delegates calls to multiple package selectors.
	 *
	 * @param javaPackages the java packages to create the selector for.
	 * @return a selector that delegates calls to multiple selector instances.
	 */
	private JavaEntitiesSelector newMultiPackageSelector(JavaPackage[] javaPackages) {
		if (javaPackages.length == 1)
			return new SelectorImplementation(javaPackages[0]);
		else {
			final JavaEntitiesSelector[] delegates = new JavaEntitiesSelector[javaPackages.length];
			for (int i = 0; i < delegates.length; i++)
				delegates[i] = new SelectorImplementation(javaPackages[i]);

			return (JavaEntitiesSelector) Proxy.newProxyInstance(getClass().getClassLoader(),
					new Class[]{JavaEntitiesSelector.class}, new MultipleSelectorInvocationHandler(delegates));
		}
	}

	private static class MultipleSelectorInvocationHandler implements InvocationHandler {
		private final JavaEntitiesSelector[] delegates;

		MultipleSelectorInvocationHandler(JavaEntitiesSelector[] delegates) {
			this.delegates = delegates;
		}

		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			if (JavaEntitiesList.class.equals(method.getReturnType())) {
				JavaEntitiesList result = null;
				final Set<Object> includedNames = new HashSet<Object>();

				for (JavaEntitiesSelector delegate : delegates) {
					final JavaEntitiesList list = (JavaEntitiesList) method.invoke(delegate, args);

					if (result == null) {
						result = list;
					} else if (list != null && !list.isEmpty()) {
						result.ensureCapacity(list.size());
						for (Map<?, ?> map : list) {
							// Avoid duplicates
							if (!includedNames.contains(map.get(OUT_PARAM_NAME))) result.add(map);
						}
					}

					if (list != null) {
						for (Map<?, ?> map : list) includedNames.add(map.get(OUT_PARAM_NAME));
					}
				}
				return result;
			} else
				return method.invoke(delegates[0], args);
		}
	}


	private Map<String, Object> getLazyLoaderReference(JavaClass javaClass) {
		if (javaClass != null && javaClass.equals(this.javaClass))
			return new LazyLoaderReference(javaClass, this);
		return new LazyLoaderReference(javaClass, null);
	}

	/**
	 * Is a map that lazily initializes a loader to fill it when first accessed.
	 */
	private class LazyLoaderReference extends AbstractMap<String, Object> {

		private Map map, genericType;
		private String name, typeString;
		private final JavaClass javaClass;

		private LazyLoaderReference(JavaClass javaClass, Map loader) {
			this.javaClass = javaClass;
			map = loader;
		}

		@SuppressWarnings("unchecked")
		private Map<?, ?> getMap() {
			if (map == null) {
				if (javaClass == null)
					map = new HashMap();
				else
					map = convert(false, false, javaClass).get(0);
			}
			return map;
		}

		@Override
		@SuppressWarnings("unchecked")
		public Set<Entry<String, Object>> entrySet() {
			return (Set) getMap().entrySet();
		}

		@Override
		public boolean containsKey(Object key) {
			return getMap().containsKey(key);
		}

		private String getName() {
			if (name == null) name = javaClass.getFullyQualifiedName();
			return name;
		}

		private String getTypeString() {
			if (typeString == null) typeString = typeToString(javaClass.asType());
			return typeString;
		}

		private Map getGenericType() {
			if (genericType == null) genericType = genericTypeToMap(getTypeString());
			return genericType;
		}

		@Override
		public Object get(Object key) {
			if (map == null && javaClass != null) {
				if (OUT_PARAM_NAME.equals(key)) return getName();
				if (OUT_PARAM_SIMPLE_NAME.equals(key)) return javaClass.getName();
				if (OUT_PARAM_JAVA_CLASS.equals(key)) return javaClass;
				if ("type".equals(key)) return getTypeString();
				if ("genericType".equals(key)) return getGenericType();
			}
			return getMap().get(key);
		}

		@Override
		public String toString() {
			return "JavaSourceLoaderReference{" +
					"javaClass=" + javaClass +
					'}';
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (!(o instanceof Map)) return false;
			Map that = (Map) o;
			return nameKeyEquals(this, that);
		}

		@Override
		public int hashCode() {
			return valueOf(get(OUT_PARAM_NAME)).hashCode();
		}
	}

	/**
	 * Is a printable map that wraps (copies) the contents of a loader.
	 */
	private static class LoaderIntrospection extends PrintableMap<String, Object> {

		private static final long serialVersionUID = -2365869208760038404L;

		private LoaderIntrospection(JavaSourceLoader source, String fullyQualifiedName) {
			super(source, "Reflection details on class '" + fullyQualifiedName + '\'');
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (!(o instanceof Map)) return false;
			Map that = (Map) o;
			return nameKeyEquals(this, that);
		}

		@Override
		public int hashCode() {
			return valueOf(get(OUT_PARAM_NAME)).hashCode();
		}
	}
}
