/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.script;

import org.apache.maven.doxia.logging.Log;
import org.codehaus.plexus.util.FileUtils;
import org.tinyjee.maven.dim.extensions.ScriptEnvironment;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.utils.ClassPathUtils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.io.File.separator;
import static java.util.regex.Pattern.compile;

/**
 * Creates a jython environment if this is not set from external using {@code -Dpython.home=/path/to/python}
 * and {@code -Dpython.executable=/path/to/bin/python}.
 * <p/>
 * A jython environment is required to allow the installation and usage of site-packages.
 * Please note that jython does not import "site" by default, thus a call to {@code import site} is required.
 * <p/>
 * When the environment is not specified, it will be created below the <i>maven repository</i> location that contains
 * the downloaded python interpreter.
 *
 * @author juergen_kellerer, 2012-06-06
 */
public class ScriptEnvironmentJython implements ScriptEnvironment {

	private static final Log log = Globals.getLog();

	private static boolean hasPythonEnvironment() {
		return System.getProperty("python.home") != null;
	}

	public void cleanUp() {
		// we do not cleanup..
	}

	/**
	 * Builds a valid python environment next to the location where 'jython-standalone.jar' resides.
	 * <p/>
	 * If you don't want this, simply set "python.home" to the location of your python installation.
	 *
	 * @param classLoader the current modules classloader.
	 */
	public void build(ClassLoader classLoader) {
		if (hasPythonEnvironment()) return;

		final File[] pythonPaths = findPythonPaths(classLoader);
		if (pythonPaths != null) {
			final File pythonHome = pythonPaths[0];
			final File jythonJar = pythonPaths[1];
			doBuild(classLoader, pythonHome, jythonJar);
		}
	}

	static void doBuild(ClassLoader classLoader, File pythonHome, File jythonJar) {
		log.info("Setting up (java) python-" + getPythonVersion(pythonHome) + " environment below " + pythonHome);
		try {
			handleLibraries(pythonHome);
		} catch (IOException e) {
			log.error("Failed setting the python library path.", e);
		}

		final String executable = isPosix() ? "dim-python.sh" : "dim-python.bat";
		final File executableFile = new File(pythonHome, executable);

		System.setProperty("python.home", pythonHome.getAbsolutePath());
		System.setProperty("python.executable", executableFile.getAbsolutePath());
		System.setProperty("python.cachedir", makeDirectory(pythonHome, "Cache").getAbsolutePath());
		System.setProperty("python.cachedir.skip", "false");

		log.info("Creating jython executable: " + executableFile);
		try {
			final URL resource = classLoader.getResource("org/tinyjee/maven/dim/extensions/script/" + executable);
			final String content = Globals.getInstance().fetchUrlText(resource).
					replace("${java.home}", System.getProperty("java.home")).
					replace("${python.home}", System.getProperty("python.home")).
					replace("${jython.jar}", jythonJar.getCanonicalPath());

			FileUtils.fileWrite(executableFile.getAbsolutePath(), content);

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Handles the inclusion of the library folder 3rd party site packages.
	 */
	static void handleLibraries(File pythonHome) throws IOException {
		makeDirectory(pythonHome, "Lib");

		String sitePackages = "Lib" + separator + "site-packages", scripts = isPosix() ? "bin" : "Scripts";
		makeDirectory(pythonHome, sitePackages);
		makeDirectory(pythonHome, scripts);
	}

	static final Pattern MAVEN_VERSION_PATTERN = compile(".*[\\\\/]([0-9]+\\.[0-9]+)[^\\\\/]*([\\\\/].*|$)");

	static String getPythonVersion(File pythonHome) {
		final Matcher matcher = MAVEN_VERSION_PATTERN.matcher(pythonHome.getAbsolutePath());
		String version = null;
		while (matcher.find()) version = matcher.group(1);

		return version;
	}

	static File makeDirectory(File pythonHome, String directoryName) {
		final File directory = new File(pythonHome, directoryName);
		if (!directory.isDirectory()) {
			if (!directory.mkdirs()) log.error("Failed creating python directory: " + directory);
		}
		return directory;
	}

	static File[] findPythonPaths(ClassLoader classLoader) {
		File pythonHome = null, pythonJar = null;
		for (String url : ClassPathUtils.listPaths(classLoader)) {
			if (url.contains("jython") && url.endsWith(".jar") && url.startsWith("file:")) {
				try {
					pythonJar = new File(URI.create(url));
					pythonHome = pythonJar.getParentFile().getCanonicalFile();
					if (pythonHome.isDirectory()) break;
				} catch (Exception e) {
					log.warn("Failed to convert potential python home folder " + url + " to a local file path.", e);
				}

				pythonJar = pythonHome = null;
			}
		}

		return pythonHome == null ? null : new File[]{pythonHome, pythonJar};
	}

	static boolean isPosix() {
		return File.separatorChar == '/';
	}
}
