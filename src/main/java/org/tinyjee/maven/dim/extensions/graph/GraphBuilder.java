/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.graph;

import com.mxgraph.model.mxGraphModel;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;
import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.extensions.ImageLoader;
import org.tinyjee.maven.dim.extensions.SvgLoader;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.utils.SelectableArrayList;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.*;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableMap;

/**
 * Graph builder implements a utility that can be used from <b>velocity templates</b> and <b>scripts</b>
 * to <b>generate graphs dynamically</b>.
 * <p/>
 * It is accessible in velocity and script contexts via {@code $graphBuilder} and {@code request.get("graphBuilder");}.
 */
public class GraphBuilder implements Serializable {

	private static final Log log = Globals.getLog();
	private static final long serialVersionUID = 5486710880354867517L;

	/**
	 * Sets the display label of a vertex, edge or group.
	 */
	public static final String GRAPH_PROPERTY_LABEL = "label";

	/**
	 * Allows to specify an image to display in place of the vertex. The image can be anything that can be loaded by
	 * {@link ImageLoader} and {@link SvgLoader}. If the specified image is a string, it is either interpreted as a path or
	 * as SVG content when XML tags are detected.
	 * Otherwise the value can be one of: {@code JComponent}, {@code Image}, {@code ImageProducer}.
	 * <p/>
	 * Note: SVG is preserved when the graph is later rendered and attached as SVG.
	 */
	public static final String GRAPH_PROPERTY_IMAGE = "image";

	/**
	 * Is a boolean indicating whether an edge is directed or not (defaults to true).
	 */
	public static final String GRAPH_PROPERTY_DIRECTED = "directed";

	/**
	 * Sets the style of the vertex, edge or group. (See style information)
	 */
	public static final String GRAPH_PROPERTY_STYLE = "style";

	/**
	 * Sets the initial x coordinate of the vertex.
	 */
	public static final String GRAPH_PROPERTY_X = "x";

	/**
	 * Sets the initial y coordinate of the vertex.
	 */
	public static final String GRAPH_PROPERTY_Y = "y";

	/**
	 * Sets the initial width of the vertex.
	 */
	public static final String GRAPH_PROPERTY_WIDTH = "width";

	/**
	 * Sets the initial height of the vertex.
	 */
	public static final String GRAPH_PROPERTY_HEIGHT = "height";

	/**
	 * Sets the initial border width of a group.
	 */
	public static final String GRAPH_PROPERTY_GROUP_BORDER = "group-border";

	protected final Map<String, Map<String, Object>> vertexProperties = new LinkedHashMap<String, Map<String, Object>>();
	protected final Map<String, SelectableArrayList<String>> vertexGroups = new LinkedHashMap<String, SelectableArrayList<String>>();
	protected final Map<List<String>, Map<String, Object>> edgeProperties = new LinkedHashMap<List<String>, Map<String, Object>>();

	private Map<String, BufferedImage> imageCache = new Hashtable<String, BufferedImage>();

	public Map<String, BufferedImage> getImageCache() {
		return imageCache;
	}

	public void setImageCache(Map<String, BufferedImage> imageCache) {
		this.imageCache = imageCache;
	}

	private Map<String, Object> setVertexProperties(String vertexName, Map<String, Object> vertexProperties) {
		if (vertexProperties == null) vertexProperties = Collections.emptyMap();
		vertexProperties = new HashMap<String, Object>(vertexProperties);
		this.vertexProperties.put(vertexName, vertexProperties);

		return vertexProperties;
	}

	/**
	 * Returns true if the named group was added to the graph model.
	 *
	 * @param groupName the name of the group to test.
	 * @return true if the named group was added to the graph model.
	 */
	public boolean containsGroup(String groupName) {
		return vertexGroups.containsKey(groupName);
	}

	/**
	 * Adds the specified vertex to the specified group.
	 *
	 * @param groupName  the group to add the vertex to.
	 * @param vertexName the name of the vertex to add.
	 */
	public void addToGroup(String groupName, String vertexName) {
		addToGroup(groupName, vertexName, null);
	}

	/**
	 * Adds the specified vertex to the specified group.
	 *
	 * @param groupName       the group to add the vertex to.
	 * @param vertexName      the name of the vertex to add.
	 * @param groupProperties Sets properties for the vertex that displays the group.
	 */
	public void addToGroup(String groupName, String vertexName, Map<String, Object> groupProperties) {
		setVertexProperties(groupName, groupProperties);

		SelectableArrayList<String> list = vertexGroups.get(groupName);
		if (list == null) vertexGroups.put(groupName, list = new SelectableArrayList<String>());

		if (!list.contains(vertexName))
			list.add(vertexName);
	}

	/**
	 * Returns all group names as selectable list.
	 *
	 * @return all group names as selectable list.
	 */
	public SelectableArrayList<String> getGroups() {
		return new SelectableArrayList<String>(vertexGroups.keySet());
	}

	/**
	 * Removes the named group.
	 *
	 * @param groupName        the name the group to remove.
	 * @param includingMembers whether group members are removed as well.
	 */
	public void removeGroup(String groupName, boolean includingMembers) {
		vertexProperties.remove(groupName);

		final SelectableArrayList<String> vertexesAndGroupsToRemove = vertexGroups.remove(groupName);
		if (vertexesAndGroupsToRemove != null) {
			if (includingMembers) {
				for (String name : vertexesAndGroupsToRemove) {
					removeGroup(name, true);
					removeVertexes(vertexesAndGroupsToRemove);
				}
			}
			removeOrphanedEdges();
		}
	}

	/**
	 * Removes all groups that are not containing any vertexes.
	 */
	public void removeOrphanedGroups() {
		for (Map.Entry<String, SelectableArrayList<String>> entry :
				new ArrayList<Map.Entry<String, SelectableArrayList<String>>>(vertexGroups.entrySet())) {

			boolean hasExistingVertexes = false;
			for (String vertex : entry.getValue())
				hasExistingVertexes |= containsVertex(vertex) || containsGroup(vertex);

			if (!hasExistingVertexes) removeGroup(entry.getKey(), false);
		}
	}

	/**
	 * Returns true if the named vertex was added to the graph model.
	 *
	 * @param vertexName the name of the vertex to test.
	 * @return true if the named vertex was added to the graph model.
	 */
	public boolean containsVertex(String vertexName) {
		return vertexProperties.containsKey(vertexName);
	}

	/**
	 * Adds a new vertex (=node) to the graph.
	 *
	 * @param vertexName the name of the vertex to add.
	 */
	public void addVertex(String vertexName) {
		addVertex(vertexName, null);
	}

	/**
	 * Adds a new vertex (=node) to the graph.
	 *
	 * @param vertexName       the name of the vertex to add.
	 * @param vertexProperties Sets properties for the vertex.
	 */
	public void addVertex(String vertexName, Map<String, Object> vertexProperties) {
		vertexProperties = setVertexProperties(vertexName, vertexProperties);
		Map<String, Object> styles = getStyles(vertexProperties);

		Object imageProperty = convertImageProperty(vertexProperties.get(GRAPH_PROPERTY_IMAGE));
		if (imageProperty instanceof BufferedImage) {
			BufferedImage image = (BufferedImage) imageProperty;

			vertexProperties.put(GRAPH_PROPERTY_WIDTH, image.getWidth());
			vertexProperties.put(GRAPH_PROPERTY_HEIGHT, image.getHeight());

			String imageUrl = "file:///./" + vertexName;
			getImageCache().put(imageUrl, image);

			styles.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_IMAGE);
			styles.put(mxConstants.STYLE_IMAGE, imageUrl);
			styles.put(mxConstants.STYLE_IMAGE_WIDTH, image.getWidth());
			styles.put(mxConstants.STYLE_IMAGE_HEIGHT, image.getHeight());
			styles.put(mxConstants.STYLE_NOLABEL, "true");

			vertexProperties.put(GRAPH_PROPERTY_STYLE, styles);
		}
	}

	/**
	 * Returns all vertex properties mapped against the vertex name.
	 *
	 * @return all vertex properties mapped against the vertex name.
	 */
	public Map<String, Map<String, Object>> getVertexProperties() {
		return unmodifiableMap(vertexProperties);
	}

	/**
	 * Returns all vertex names.
	 *
	 * @return all vertex names.
	 */
	public SelectableArrayList<String> getVertexes() {
		return getVertexes(false);
	}

	/**
	 * Returns all vertex names.
	 *
	 * @param includeGroups specified whether group vertexes are included.
	 * @return all vertex names.
	 */
	public SelectableArrayList<String> getVertexes(boolean includeGroups) {
		final SelectableArrayList<String> names = new SelectableArrayList<String>(vertexProperties.keySet());
		if (!includeGroups) names.removeAll(vertexGroups.keySet());
		return names;
	}

	/**
	 * Returns all vertexes that do not have edges.
	 *
	 * @return all vertexes that do not have edges.
	 */
	public List<String> getUnrelatedVertexes() {
		return getUnrelatedVertexes(false);
	}

	/**
	 * Returns all vertexes that do not have edges.
	 *
	 * @param includeGroups specified whether group vertexes are included.
	 * @return all vertexes that do not have edges.
	 */
	public SelectableArrayList<String> getUnrelatedVertexes(boolean includeGroups) {
		final SelectableEdgeList edges = getEdges();
		final SelectableArrayList<String> result = new SelectableArrayList<String>();

		for (String name : vertexProperties.keySet()) {
			if (edges.selectFrom(name).isEmpty() && edges.selectTo(name).isEmpty()) {
				if (includeGroups || !containsGroup(name))
					result.add(name);
			}
		}

		return result;
	}

	/**
	 * Returns all elements in the named group as selectable list.
	 *
	 * @param groupName the name of the group.
	 * @return all elements in the named group as selectable list.
	 */
	public SelectableArrayList<String> getVertexesInGroup(String groupName) {
		return vertexGroups.get(groupName);
	}

	/**
	 * Removes the named vertex and related edges.
	 *
	 * @param vertex the vertex to remove.
	 */
	public void removeVertex(String vertex) {
		vertexProperties.remove(vertex);
		removeOrphanedEdges();
		removeOrphanedGroups();
	}

	/**
	 * Remove all vertexes that match the specified names.
	 *
	 * @param vertexes the vertexes to remove.
	 */
	public void removeVertexes(Collection<String> vertexes) {
		vertexProperties.keySet().removeAll(vertexes);
		removeOrphanedEdges();
		removeOrphanedGroups();
	}


	/**
	 * Returns true if the specified edge was added to the graph model.
	 *
	 * @param fromVertex the vertex to draw the edge from.
	 * @param toVertex   the vertex to draw the edge to.
	 * @return true if the specified edge was added to the graph model.
	 */
	public boolean containsEdge(String fromVertex, String toVertex) {
		return edgeProperties.containsKey(asList(fromVertex, toVertex));
	}

	/**
	 * Adds an edge between 2 vertices.
	 *
	 * @param fromVertex the vertex to draw the edge from.
	 * @param toVertex   the vertex to draw the edge to.
	 */
	public void addEdge(String fromVertex, String toVertex) {
		addEdge(fromVertex, toVertex, null);
	}

	/**
	 * Adds an edge between 2 vertices.
	 *
	 * @param fromVertex     the vertex to draw the edge from.
	 * @param toVertex       the vertex to draw the edge to.
	 * @param edgeProperties the properties for the edge.
	 */
	public void addEdge(String fromVertex, String toVertex, Map<String, Object> edgeProperties) {
		if (edgeProperties == null) edgeProperties = Collections.emptyMap();
		edgeProperties = new HashMap<String, Object>(edgeProperties);
		Map<String, Object> styles = getStyles(edgeProperties);

		final Object directed = edgeProperties.get(GRAPH_PROPERTY_DIRECTED);
		if (Boolean.FALSE.toString().equalsIgnoreCase(String.valueOf(directed))) {
			styles.put(mxConstants.STYLE_ENDARROW, mxConstants.NONE);
			edgeProperties.put(GRAPH_PROPERTY_STYLE, styles);
		}

		this.edgeProperties.put(asList(fromVertex, toVertex), edgeProperties);
	}

	/**
	 * Returns all edge properties mapped against the edge id.
	 *
	 * @return all edge properties mapped against the edge id.
	 */
	public Map<List<String>, Map<String, Object>> getEdgeProperties() {
		return unmodifiableMap(edgeProperties);
	}

	/**
	 * Returns all edge ids as selectable list.
	 *
	 * @return all edge ids as selectable list.
	 */
	public SelectableEdgeList getEdges() {
		return new SelectableEdgeList(edgeProperties.keySet());
	}

	/**
	 * Remove all edges that are unrelated.
	 */
	public void removeOrphanedEdges() {
		for (Iterator<List<String>> iterator = edgeProperties.keySet().iterator(); iterator.hasNext(); ) {
			List<String> next = iterator.next();
			if ((!containsGroup(next.get(0)) && !containsVertex(next.get(0))) || (!containsGroup(next.get(1)) && !containsVertex(next.get(1))))
				iterator.remove();
		}
	}

	/**
	 * Updates the model in the specified graph with the vertex, edge and group information that was build in this builder.
	 *
	 * @param graph the graph to update.
	 */
	public void buildGraph(mxGraph graph) {
		graph.getModel().beginUpdate();
		try {
			((mxGraphModel) graph.getModel()).clear();

			final Object parent = graph.getDefaultParent();

			final Map<String, Object> vertexHandles = new HashMap<String, Object>();
			for (Map.Entry<String, Map<String, Object>> entry : vertexProperties.entrySet()) {
				if (vertexGroups.containsKey(entry.getKey())) continue;

				Map<String, Object> properties = entry.getValue();
				addVertexToGraph(graph, parent, vertexHandles, entry.getKey(), properties);
			}

			for (Map.Entry<String, SelectableArrayList<String>> entry : vertexGroups.entrySet()) {
				List<String> vertexNames = entry.getValue();
				List<Object> cells = new ArrayList<Object>(vertexNames.size());
				for (String vertexName : vertexNames) {
					Object cell = vertexHandles.get(vertexName);
					if (cell == null)
						log.warn("Didn't find group vertex " + vertexName);
					else
						cells.add(cell);
				}

				String group = entry.getKey();
				Map<String, Object> properties = vertexProperties.get(group);
				Object groupCell = addVertexToGraph(graph, parent, vertexHandles, group, properties);
				graph.groupCells(groupCell, getProperty(properties, GRAPH_PROPERTY_GROUP_BORDER, 1D), cells.toArray());
			}

			for (Map.Entry<List<String>, Map<String, Object>> entry : edgeProperties.entrySet()) {
				Map<String, Object> properties = entry.getValue();
				String edgeDisplayName = getProperty(properties, GRAPH_PROPERTY_LABEL, "");

				Object fromVertex = vertexHandles.get(entry.getKey().get(0)), toVertex = vertexHandles.get(entry.getKey().get(1));
				if (fromVertex == null || toVertex == null) {
					log.warn("Didn't find edge vertexes " + entry.getKey());
					continue;
				}

				GraphStyles.logInvalidStyles(graph, getStyles(properties));

				String style = getFlatStyles(properties);
				graph.insertEdge(parent, entry.getKey().toString(), edgeDisplayName, fromVertex, toVertex, style);
			}
		} finally {
			graph.getModel().endUpdate();
		}
	}

	private static Object addVertexToGraph(
			mxGraph graph, Object parent, Map<String, Object> vertexHandles, String vertexName, Map<String, Object> properties) {

		String vertexDisplayName = getProperty(properties, GRAPH_PROPERTY_LABEL, vertexName);

		int x = getProperty(properties, GRAPH_PROPERTY_X, 0), y = getProperty(properties, GRAPH_PROPERTY_Y, 0);
		int width = getProperty(properties, GRAPH_PROPERTY_WIDTH, 0), height = getProperty(properties, GRAPH_PROPERTY_HEIGHT, 0);

		GraphStyles.logInvalidStyles(graph, getStyles(properties));

		String style = getFlatStyles(properties);
		Object cell = graph.insertVertex(parent, vertexName, vertexDisplayName, x, y, width, height, style);
		vertexHandles.put(vertexName, cell);

		return cell;
	}

	@SuppressWarnings("unchecked")
	protected static <T> T getProperty(Map properties, String key, T defaultValue) {
		Object value = properties.get(key);
		if (value == null)
			return defaultValue;

		if (defaultValue != null && !value.getClass().equals(defaultValue.getClass())) {
			try {
				return (T) defaultValue.getClass().getMethod("valueOf", value.getClass()).invoke(null, value);
			} catch (Exception ignored) {
			}
		}
		return (T) value;
	}

	@SuppressWarnings("unchecked")
	protected static Map<String, Object> getStyles(Map properties) {
		Object instance = properties.get(GRAPH_PROPERTY_STYLE);
		if (instance == null) properties.put(GRAPH_PROPERTY_STYLE, instance = new LinkedHashMap<String, Object>());

		return GraphStyles.getMappedStyles(instance);
	}

	protected static String getFlatStyles(Map properties) {
		Map<String, Object> styles = getStyles(properties);
		return GraphStyles.getFlatStyles(styles);
	}

	protected static Object convertImageProperty(Object imageProperty) {
		if (imageProperty == null) return null;

		try {
			if (imageProperty instanceof String) {
				String text = (String) imageProperty;
				if ((text.startsWith("<?xml") && text.contains("<svg")) || text.startsWith("<svg")) {
					return SvgLoader.render(null, text);
				} else if (text.matches(".+\\.(svg|svgz|xml)(|.vm|.gz)$")) {
					return SvgLoader.render(Collections.singletonMap(SvgLoader.PARAM_SVG, (Object) text), null);
				}
			}

			return ImageLoader.load(imageProperty, null);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String toString() {
		return "GraphBuilder{" +
				"vertexProperties=" + vertexProperties +
				", vertexGroups=" + vertexGroups +
				", edgeProperties=" + edgeProperties +
				", imageCache=" + imageCache +
				'}';
	}
}
