/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.graph;

import com.mxgraph.io.mxCodec;
import com.mxgraph.io.mxStylesheetCodec;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxXmlUtils;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;
import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.spi.Globals;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

import static java.lang.String.valueOf;

public class GraphStyles {

	private static final Log log = Globals.getLog();
	private static final SortedMap<String, List<String>> AVAILABLE_STYLES;

	private GraphStyles() {
	}

	static {
		SortedMap<String, String> constants = new TreeMap<String, String>();
		for (Field field : mxConstants.class.getFields()) {
			if (Modifier.isStatic(field.getModifiers())) {
				try {
					constants.put(field.getName(), valueOf(field.get(null)));
				} catch (Exception e) {
					if (log.isDebugEnabled()) log.debug("Failed converting graph library field " + field, e);
				}
			}
		}

		AVAILABLE_STYLES = new TreeMap<String, List<String>>();
		for (Map.Entry<String, String> entry : constants.entrySet()) {
			String[] parts = entry.getKey().split("_", 2);
			if ("STYLE".equals(parts[0])) {
				ArrayList<String> valuesList = new ArrayList<String>();
				findParameters(parts[1], constants.tailMap(parts[1]).entrySet(), valuesList);
				if (valuesList.isEmpty()) {
					parts[1] = parts[1].replaceAll("^(END|START|IMAGE_(VERTICAL_|)|ENTRY_|EXIT_|VERTICAL_)", "").replace("STYLE", "");
					findParameters(parts[1], constants.tailMap(parts[1]).entrySet(), valuesList);
				}

				AVAILABLE_STYLES.put(entry.getValue(), valuesList);
			}
		}

		AVAILABLE_STYLES.get(mxConstants.STYLE_FONTSTYLE).addAll(Arrays.asList(
				"bold", valueOf(mxConstants.FONT_BOLD), "italic", valueOf(mxConstants.FONT_ITALIC),
				"shadow", valueOf(mxConstants.FONT_SHADOW), "underline", valueOf(mxConstants.FONT_UNDERLINE)));
	}

	private static void findParameters(String valueKeyName, Set<Map.Entry<String, String>> entries, ArrayList<String> valuesList) {
		String[] prefixes = {valueKeyName + '_', valueKeyName + "STYLE_"};

		nextElement:
		for (Map.Entry<String, String> valueEntry : entries) {
			String key = valueEntry.getKey();
			for (String prefix : prefixes) {
				if (key.startsWith(prefix)) {
					valuesList.add(valueEntry.getValue());
					continue nextElement;
				}
			}

			if (key.equals(valueKeyName))
				valuesList.add(valueEntry.getValue());
			else
				break;
		}

		// Remove number only entries
		for (Iterator<String> iterator = valuesList.iterator(); iterator.hasNext(); ) {
			String value = iterator.next();
			if (value.matches("^[0-9]+$")) iterator.remove();
		}
	}

	public static void logInvalidStyleKey(String styleKey, String styleValue) {
		List<String> styleValues = AVAILABLE_STYLES.get(styleKey);
		if (styleValues == null) {
			List<String> tailKeys = new ArrayList<String>(AVAILABLE_STYLES.tailMap(styleKey).keySet()),
					headKeys = new ArrayList<String>(AVAILABLE_STYLES.headMap(styleKey).keySet());
			tailKeys = tailKeys.subList(0, Math.min(tailKeys.size(), 3));
			headKeys = headKeys.subList(Math.max(0, headKeys.size() - 3), headKeys.size());
			log.error("The style key '" + styleKey + "' is unknown. Known similar style keys are '" + headKeys + "' < 'unknown' < '" + tailKeys + '\'');
			if (log.isDebugEnabled()) log.debug("All available styles are: " + AVAILABLE_STYLES.keySet());
		} else if (!styleValues.isEmpty() && !styleValues.contains(styleValue) && !mxConstants.NONE.equals(styleValue)) {
			log.warn("The style value '" + styleValue + "' is unknown for style key '" + styleKey + "', known values are: " + styleValues);
		}
	}

	public static void logUndefinedStyleName(mxGraph graph, String styleName) {
		Set<String> styleNames = graph.getStylesheet().getStyles().keySet();
		if (!styleNames.contains(styleName)) {
			log.warn("The style class '" + styleName + "' is not defined. Defined style names are: '" + styleNames + "'");
		}
	}

	public static void defineStyleClass(mxGraph graph, String styleName, String styleDefinition) {
		defineStyleClass(graph, styleName, getMappedStyles(styleDefinition));
	}

	public static void defineStyleClass(mxGraph graph, String styleName, Map<String, Object> styleProperties) {
		logInvalidStyles(graph, styleProperties);
		// resolving style classes
		styleProperties = graph.getStylesheet().getCellStyle(getFlatStyles(styleProperties), null);
		// setting styleName
		graph.getStylesheet().putCellStyle(styleName, styleProperties);
	}

	public static void changeStyleClass(mxGraph graph, String styleName, String styleDefinition) {
		changeStyleClass(graph, styleName, getMappedStyles(styleDefinition));
	}

	public static void changeStyleClass(mxGraph graph, String styleName, Map<String, Object> styleProperties) {
		logInvalidStyles(graph, styleProperties);
		Map<String, Object> existingStyle = graph.getStylesheet().getCellStyle(styleName, null),
			providedStyle = graph.getStylesheet().getCellStyle(getFlatStyles(styleProperties), null);

		if (existingStyle == null || existingStyle.isEmpty()) {
			defineStyleClass(graph, styleName, styleProperties);
		} else {
			existingStyle.putAll(providedStyle);
			graph.getStylesheet().putCellStyle(styleName, existingStyle);
		}
	}

	public static String preProcessStyle(String styleKey, String styleValue) {
		if (styleValue != null && styleKey != null) {
			// Convert (bold|italic|shadow|underline) to the numeric representations.
			if (mxConstants.STYLE_FONTSTYLE.equals(styleKey)) {
				List<String> values = AVAILABLE_STYLES.get(styleKey);
				for (int i = 0; i < values.size() - 1; i += 2) {
					if (styleValue.equals(values.get(i))) {
						styleValue = values.get(i + 1);
						break;
					}
				}
			}
		}
		return styleValue;
	}

	public static void logInvalidStyles(mxGraph graph, Map<String, Object> styleProperties) {
		for (Map.Entry<String, Object> entry : styleProperties.entrySet()) {
			if (entry.getValue() != null)
				logInvalidStyleKey(entry.getKey(), valueOf(entry.getValue()));
			else
				logUndefinedStyleName(graph, entry.getKey());
		}
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getMappedStyles(Object style) {
		if (style instanceof Map)
			return (Map<String, Object>) style;
		else {
			Map<String, Object> styles = new LinkedHashMap<String, Object>();
			for (String styleElement : style.toString().split(";")) {
				String[] pair = styleElement.split("=", 2);
				if (pair.length == 2) {
					pair[1] = preProcessStyle(pair[0], pair[1]);
					styles.put(pair[0], pair[1]);
				} else
					styles.put(pair[0], null);
			}
			return styles;
		}
	}

	public static String getFlatStyles(Map<String, Object> styles) {
		StringBuilder builder = new StringBuilder(64);
		for (Map.Entry<String, Object> entry : styles.entrySet()) {
			if (builder.length() > 0) builder.append(';');
			builder.append(entry.getKey());
			if (entry.getValue() != null) builder.append('=').append(entry.getValue());
		}
		return builder.toString();
	}

	public static mxStylesheet loadStyles(String stylesheet) {
		mxStylesheetCodec codec = new mxStylesheetCodec() {
			@Override
			public boolean processInclude(mxCodec dec, Node node, Object into) {
				if (node instanceof Element && "include".equalsIgnoreCase(node.getNodeName())) {
					Node xml = GraphStyles.decodeNode(((Element) node).getAttribute("name"));
					if (xml != null) dec.decode(xml, into);
					return true;
				}

				return super.processInclude(dec, node, into);
			}
		};

		return (mxStylesheet) codec.decode(new mxCodec(), decodeNode(stylesheet), null);
	}

	private static Node decodeNode(String path) {
		if (path != null && Globals.getInstance().isResolvable(path)) {
			try {
				return mxXmlUtils.parseXml(Globals.getInstance().fetchText(path)).getDocumentElement();
			} catch (Exception e) {
				log.error("Failed loading jgraphx stylesheet " + path, e);
			}
		}

		return null;
	}
}
