/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.script;

import org.apache.maven.doxia.logging.Log;
import org.codehaus.plexus.util.IOUtil;
import org.tinyjee.maven.dim.extensions.ScriptExecutor;
import org.tinyjee.maven.dim.extensions.ScriptExecutorListener;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.utils.CompositeInputStream;

import java.io.IOException;
import java.io.InputStream;

/**
 * Listens to javascript executions and provides a subset of CommonJS (in particular "Modules/1.1").
 * <p/>
 * Implemented CommonJS components:<ul>
 * <li>http://wiki.commonjs.org/wiki/Modules/1.1</li>
 * </ul>
 *
 * @author Juergen_Kellerer, 2012-06-18
 */
public class ScriptEnvironmentCommonJS implements ScriptExecutorListener {

	private static final Log log = Globals.getLog();

	private static final String commonJsScriptEnvironment, cleanupJsScriptEnvironment;

	static {
		final ClassLoader loader = ScriptEnvironmentCommonJS.class.getClassLoader();
		final InputStream inputStream = new CompositeInputStream(
				loader.getResourceAsStream("org/tinyjee/maven/dim/extensions/script/commonjs/modules-1.1.js"));
		try {
			commonJsScriptEnvironment = IOUtil.toString(inputStream, "UTF-8");
			cleanupJsScriptEnvironment = "require(\"shutdown-hook\").run();";
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				inputStream.close();
			} catch (IOException ignored) {
			}
		}
	}

	public void beforeExecute(ScriptExecutor scriptExecutor, String scriptExtension, String scriptContent, String scriptName) {
		if (isJavaScript(scriptExtension)) {
			try {
				scriptExecutor.execute(scriptExecutor.getScriptContext(), scriptExtension, commonJsScriptEnvironment, "commonjs.js");
			} catch (Exception e) {
				log.error("Failed to execute pre-configuration-script.", e);
				if (log.isDebugEnabled()) log.debug("Script that failed:\n" + commonJsScriptEnvironment);
			}
		}
	}

	public void afterExecute(ScriptExecutor scriptExecutor, String scriptExtension, String scriptContent, String scriptName) {
		if (isJavaScript(scriptExtension)) {
			try {
				scriptExecutor.execute(scriptExecutor.getScriptContext(), scriptExtension, cleanupJsScriptEnvironment, "cleanup.js");
			} catch (Exception e) {
				log.error("Failed to execute pre-configuration-script.", e);
				if (log.isDebugEnabled()) log.debug("Script that failed:\n" + cleanupJsScriptEnvironment);
			}
		}
	}

	private static boolean isJavaScript(String scriptExtension) {
		return "js".equalsIgnoreCase(scriptExtension);
	}
}
