/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.image;

import com.jhlabs.image.AbstractBufferedImageOp;

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;

/**
 * Base class for DIM-extension image filters.
 *
 * @author Juergen_Kellerer, 2012-06-02
 */
public abstract class AbstractImageFilter extends AbstractBufferedImageOp {

	@Override
	public BufferedImage createCompatibleDestImage(BufferedImage source, ColorModel destinationColorModel) {
		if (destinationColorModel == null) destinationColorModel = source.getColorModel();

		final Rectangle2D rectangle = getBounds2D(source);
		final WritableRaster raster = destinationColorModel.createCompatibleWritableRaster((int) rectangle.getWidth(), (int) rectangle.getHeight());

		return new BufferedImage(destinationColorModel, raster, destinationColorModel.isAlphaPremultiplied(), null);
	}

	public final BufferedImage filter(BufferedImage source, BufferedImage destination) {
		if (destination == null) destination = createCompatibleDestImage(source, null);
		return doFilter(source, destination);
	}


	protected abstract BufferedImage doFilter(BufferedImage source, BufferedImage destination);
}
