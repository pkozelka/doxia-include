/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.apache.batik.dom.svg.SVGDOMImplementation;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.TranscodingHints;
import org.apache.batik.transcoder.image.ImageTranscoder;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.batik.transcoder.image.TIFFTranscoder;
import org.apache.maven.doxia.logging.Log;
import org.codehaus.plexus.util.FileUtils;
import org.tinyjee.maven.dim.extensions.image.ImageUtil;
import org.tinyjee.maven.dim.spi.ExtensionInformation;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.ResourceResolver;
import org.tinyjee.maven.dim.utils.AbstractAliasHandler;
import org.tinyjee.maven.dim.utils.NamedNodeMapListAdapter;
import org.tinyjee.maven.dim.utils.NodeListAdapter;
import org.tinyjee.maven.dim.utils.XPathEvaluatorImplementation;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.Boolean.parseBoolean;
import static java.lang.String.valueOf;
import static org.apache.batik.transcoder.SVGAbstractTranscoder.*;
import static org.apache.batik.transcoder.image.ImageTranscoder.KEY_BACKGROUND_COLOR;
import static org.apache.batik.transcoder.image.JPEGTranscoder.KEY_QUALITY;
import static org.tinyjee.maven.dim.IncludeMacroSignature.*;
import static org.tinyjee.maven.dim.extensions.image.ImageUtil.convertToPaint;

/**
 * Loads and transforms SVG content and includes it either directly or rendered as PNG.
 * <p/>
 * The loader is an extension of {@link XmlLoader} and supports remote URLs (e.g. services) and local paths to get the SVG source content.
 * In particular anything that is supported by XmlLoader is also supported in this class to load and transform the SVG-XML source.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|source-svg=file.svg}
 * %{include|source-svg=file.xml|xsl=transform-to-svg.xsl}
 * %{include|source-svg=file.svg.vm|someParam=someValue}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 * Full call syntax:<code><pre>
 * %{include|source-class=org.tinyjee.maven.dim.extensions.SvgLoader|svg=file.svg}
 * </pre></code>
 *
 * @author Juergen_Kellerer, 2011-10-06
 */
@ExtensionInformation(displayName = "SVG Loader", documentationUrl = "/extensionsSvgLoader.html")
public class SvgLoader extends HashMap<String, Object> {

	private static final Log log = Globals.getLog();
	private static final long serialVersionUID = -6282606147512841025L;

	/**
	 * Utility method that renders the given SVG content.
	 *
	 * @param parameters any additional parameters to pass to the loader.
	 * @param svgText    the svg content to render or 'null' if a source ('svg=/path/to/svg') is defined within the properties.
	 * @return a buffered image containing the rendered output.
	 * @throws Exception in case of rendering failed.
	 */
	public static BufferedImage render(Map<String, Object> parameters, String svgText) throws Exception {
		if (parameters == null) parameters = Collections.emptyMap();
		parameters = new HashMap<String, Object>(parameters);

		final File basedir = Globals.getInstance().getBasedir();

		if (svgText != null) {
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			builderFactory.setNamespaceAware(true);
			Document document = builderFactory.newDocumentBuilder().parse(new InputSource(new StringReader(svgText)));

			String documentURI = parameters.containsKey(PARAM_SVG) ? valueOf(parameters.get(PARAM_SVG)) : null;
			document.setDocumentURI(documentURI);
			parameters.put(PARAM_SVG, document);
		}

		parameters.put(PARAM_ATTACH, false);
		parameters.put(PARAM_RENDER, "png");

		SvgLoader loader = new SvgLoader(basedir, parameters);
		return ImageUtil.loadImage((byte[]) loader.get(OUT_PARAM_RENDER_RESULT));
	}

	/**
	 * Implements the "{@link org.tinyjee.maven.dim.extensions.SvgLoader#PARAM_ALIAS}" alias functionality.
	 */
	public static class AliasHandler extends AbstractAliasHandler {
		/**
		 * Constructs the handler (Note: Is called by {@link org.tinyjee.maven.dim.spi.RequestParameterTransformer#TRANSFORMERS}).
		 */
		public AliasHandler() {
			super(PARAM_ALIAS, PARAM_SVG, SvgLoader.class.getName());
		}
	}

	/**
	 * Defines a shortcut for the request properties 'source-class' and 'svg'.
	 * <p/>
	 * If this parameter is present inside the request parameters list, the system will effectively behave as if
	 * 'source-class' and 'svg' where set separately.
	 */
	public static final String PARAM_ALIAS = "source-svg";

	/**
	 * Sets the URL or local path to the SVG file to load inside the request parameters.
	 * <p/>
	 * The specified file is resolved in exactly the same way as when using the parameter "<code>source</code>"
	 * (see Macro reference).
	 * <p/>
	 * <i>Notes:</i><ul>
	 * <li>When no further source is specified, the loader uses a built-in template to add the loaded SVG into
	 * the output. (Either as SVG or rendered using Apache Batik when '{@code render}' is set)</li>
	 * <li>GZIP compressed SVG files are detected and automatically uncompressed.</li>
	 * <li>The SVG file is assumed to be a velocity template if the referenced URL or local path ends with "{@code .vm}".
	 * When present, the file is evaluated using all specified macro parameters (this allows building SVG dynamically using velocity).</li>
	 * <li>The parameter value is interpreted as the SVG content to parse when it starts with "{@code <?xml}".</li>
	 * </ul>
	 */
	public static final String PARAM_SVG = "svg";

	/**
	 * <b>Optional Parameter</b>, transform the given input (e.g. XML) to an SVG file using an XML stylesheet.
	 * <p/>
	 * See {@link XmlLoader} for details.
	 */
	public static final String PARAM_XSL = "xsl";

	/**
	 * <b>Optional Parameter</b>, sets the width in pixel.
	 */
	public static final String PARAM_WIDTH = "width";

	/**
	 * <b>Optional Parameter</b>, sets the height in pixel.
	 */
	public static final String PARAM_HEIGHT = "height";

	/**
	 * <b>Optional Parameter</b>, sets the link title when rendering and attaching TIFF images.
	 */
	public static final String PARAM_TITLE = "title";

	/**
	 * Specifies whether the SVG is rendered to an image and included.
	 * <p/>
	 * Possible values:<ul>
	 * <li>'true' or 'png': Renders a 32bit PNG image (including alpha channel).</li>
	 * <li>'jpg' or "jpeg": Renders a 24bit JPEG image.</li>
	 * <li>'tiff': Renders to a TIFF image and includes it as a download link.</li>
	 * <li>'false': Does not render an image.</li>
	 * </ul>
	 * <p/>
	 * Note: When an additional {@code source} is specified the render result is stored inside {@code renderResult}
	 * instead of being attached and included.
	 */
	public static final String PARAM_RENDER = "render";

	/**
	 * <b>Optional Parameter</b>, Specifies a background color to use when rendering SVG to an image.
	 * (Format: #000000)
	 */
	public static final String PARAM_RENDER_BACKGROUND = "render-background";

	/**
	 * <b>Optional Parameter</b>, Specifies the "dots-per-inch" value that is used when converting sizes (e.g. cm)
	 * to pixels in the rendered SVG.
	 */
	public static final String PARAM_RENDER_DPI = "render-dpi";

	/**
	 * <b>Optional Parameter</b>, Specifies the CSS media when rendering an SVG containing CSS stylesheets with
	 * alternating media styles.
	 * <p/>
	 * E.g. "print", "screen"
	 */
	public static final String PARAM_RENDER_MEDIA = "render-media";

	/**
	 * <b>Optional Parameter</b>, Specifies a custom CSS file that is used to override values of the original CSS when
	 * rendering the SVG.
	 */
	public static final String PARAM_RENDER_CSS = "render-css";

	/**
	 * <b>Optional Parameter</b>, Toggles whether {@code onload="scriptMethod()"} is executed.
	 * (Defaults to "true")
	 */
	public static final String PARAM_RENDER_ONLOAD = "render-onload";

	/**
	 * <b>Optional Parameter</b>, Sets the filename to use for attaching an SVG directly or rendered as image.
	 * <p/>
	 * Possible values:<ul>
	 * <li>'true': Attaches the SVG using the original filename.</li>
	 * <li>'[filename]': Attaches the SVG using given filename.</li>
	 * <li>'%s-suffix': Attaches the SVG using the original filename with a suffix.</li>
	 * <li>'false': Does not attach the SVG.</li>
	 * </ul>
	 * <p/>
	 * <i>Note:</i> When an SVG is attached, it cannot be further processed by velocity, thus any velocity
	 * expressions inside the included SVG are only processed with inline SVG graphics and the addition of the macro
	 * parameter "{@code source-is-template=true}".
	 * <p/>
	 * When {@code render} is set, this property can be used to override the default image filename.
	 * Use this if you attempt to render multiple results out of the same SVG source and make sure every differing result
	 * is also named differently.
	 */
	public static final String PARAM_ATTACH = "attach";

	/**
	 * Is set with a reference to this loader instance.
	 */
	public static final String OUT_PARAM_LOADER = "loader";

	/**
	 * Is set with the parsed document object ({@link org.w3c.dom.Document org.w3c.dom.svg.SVGDocument}) and can be accessed
	 * from velocity templates.
	 */
	public static final String OUT_PARAM_DOCUMENT = "document";

	/**
	 * Is set with an XPath evaluator, see {@link XmlLoader} for details.
	 */
	public static final String OUT_PARAM_XPATH = "xpath";

	/**
	 * Is filled with a byte array containing the render result (= the binary file content) when {@code render} was set and
	 * {@code source} points to a velocity template.
	 * <p/>
	 * Use this within velocity templates to attach the binary content.
	 */
	public static final String OUT_PARAM_RENDER_RESULT = "renderResult";

	/**
	 * Is filled with the <i>href</i> to the render result when {@code render} was set, {@code source} points to a velocity template and
	 * {@code attach} defines the filename to use for attaching the content.
	 * <p/>
	 * Use this within velocity templates to create HTML images or links.
	 */
	public static final String OUT_PARAM_RENDER_HREF = "renderedHref";

	private static final String[][] parameterTranslations = {
			{PARAM_SVG, XmlLoader.PARAM_XML},
			{PARAM_XSL, XmlLoader.PARAM_XSL},
			{XmlLoader.OUT_PARAM_DOCUMENT, OUT_PARAM_DOCUMENT},
			{XmlLoader.OUT_PARAM_XPATH, OUT_PARAM_XPATH},
	};

	private static final ExecutorService transcodePool = Executors.newSingleThreadExecutor();

	/**
	 * Creates a new SVG loader for the given base directory and macro request parameters.
	 *
	 * @param baseDir       the base directory of the current module.
	 * @param requestParams the macro request parameters.
	 * @throws Exception in case of the SVG loading & transformation failed.
	 */
	public SvgLoader(final File baseDir, Map<String, Object> requestParams) throws Exception {
		final boolean containsSource = requestParams.containsKey(PARAM_SOURCE) || requestParams.containsKey(PARAM_SOURCE_CONTENT);

		// SVG is usually not loaded in verbatim mode
		if (!requestParams.containsKey(PARAM_VERBATIM)) requestParams.put(PARAM_VERBATIM, false);

		loadAndTransform(baseDir, requestParams);
		if (isRendered())
			render(baseDir, containsSource);
		else if (isAttached() && !containsSource)
			attach();

		put(OUT_PARAM_LOADER, this);
	}

	/**
	 * Renders and attaches the loaded SVG.
	 * <p/>
	 * This method may be called within velocity templates via {@code $href = $loader.renderAndAttach('png', 'mysvg.png')} after applying
	 * programmatic transformations to the SVG document.
	 *
	 * @param renderFormat the format to use for rendering.
	 * @param filename     the filename to use for attaching.
	 * @return the HREF of the rendered and attached PNG.
	 * @throws Exception In case of rendering and attaching failed.
	 */
	public String renderAndAttach(String renderFormat, String filename) throws Exception {
		put(PARAM_RENDER, renderFormat);
		put(PARAM_ATTACH, filename);
		render(Globals.getInstance().getBasedir(), true);
		return (String) get(OUT_PARAM_RENDER_HREF);
	}

	protected void loadAndTransform(File baseDir, Map<String, Object> requestParams) throws Exception {
		requestParams = new HashMap<String, Object>(requestParams);

		requestParams.put(XmlLoader.PARAM_NAMESPACE_AWARE, true);

		applyParameterTranslations(requestParams);
		XmlLoader xmlLoader = new XmlLoader(baseDir, requestParams);
		putAll(xmlLoader);
		applyParameterTranslations(this);

		Document originalDocument = (Document) get(OUT_PARAM_DOCUMENT);
		Document document = convertToSVGDocument(originalDocument);
		put(OUT_PARAM_DOCUMENT, document);
		put(OUT_PARAM_XPATH, new XPathEvaluatorImplementation(document));
	}

	private Document convertToSVGDocument(Document originalDocument) {
		DOMImplementation implementation = SVGDOMImplementation.getDOMImplementation();
		Document document = implementation.createDocument(SVGDOMImplementation.SVG_NAMESPACE_URI, "svg", null);

		document.setDocumentURI(originalDocument.getDocumentURI());
		if (document.getDocumentURI() == null) {
			document.setDocumentURI(new File(Globals.getInstance().getBasedir(), "unknown.svg").toURI().toString());
			if (log.isDebugEnabled()) log.debug("SVG document contained no document URI, using " + document.getDocumentURI());
		}

		Element svg = document.getDocumentElement();
		for (Node node : new NamedNodeMapListAdapter(originalDocument.getDocumentElement().getAttributes())) {
			Attr attributeNode = (Attr) document.importNode(node, true);
			if (!"xmlns".equalsIgnoreCase(attributeNode.getName())) svg.setAttributeNode(attributeNode);
		}

		for (Node node : new NodeListAdapter(originalDocument.getDocumentElement().getChildNodes())) {
			svg.appendChild(document.importNode(node, true));
		}

		return document;
	}

	protected String getAttachedName() {
		String fileName = valueOf(get(PARAM_SVG));
		if (fileName.endsWith(".vm")) fileName = fileName.substring(0, fileName.length() - 3);

		if (containsKey(PARAM_ATTACH) && !"true".equalsIgnoreCase(valueOf(get(PARAM_ATTACH)))) {
			String ext = FileUtils.extension(fileName), baseName = fileName.substring(0, fileName.length() - ext.length() - 1);
			fileName = valueOf(get(PARAM_ATTACH)).replace("%s", baseName);
		}

		return fileName;
	}

	protected boolean isAttached() {
		return get(PARAM_ATTACH) != null && !isNotAttached();
	}

	protected boolean isNotAttached() {
		return "false".equalsIgnoreCase(valueOf(get(PARAM_ATTACH)));
	}

	protected boolean isRendered() {
		Object value = get(PARAM_RENDER);
		return value != null && !"false".equalsIgnoreCase(valueOf(value));
	}

	protected void attach() throws IOException {
		String fileName = getAttachedName();
		String href = Globals.getInstance().attachContent(fileName, (String) get(PARAM_SOURCE_CONTENT));

		put("svgHref", href);
		put(PARAM_SOURCE_CONTENT, "<embed src=\"${esc.xml($svgHref)}\" type=\"image/svg+xml\"/>");
	}

	protected void render(File baseDir, boolean containsSource) throws TranscoderException, IOException {
		String renderFormat = valueOf(get(PARAM_RENDER));

		final ImageTranscoder transcoder;
		if ("jpg".equalsIgnoreCase(renderFormat) || "jpeg".equalsIgnoreCase(renderFormat))
			transcoder = new JPEGTranscoder();
		else if ("tiff".equalsIgnoreCase(renderFormat))
			transcoder = new TIFFTranscoder();
		else {
			transcoder = new PNGTranscoder();
			renderFormat = "png";
		}

		if (transcoder instanceof JPEGTranscoder) transcoder.addTranscodingHint(KEY_QUALITY, .95f);

		configureTranscoder(baseDir, transcoder);

		byte[] renderResult;
		try {
			// Note: We must call the transcoder in an own Thread to ensure Rhino makes a new context that does not
			//       interfere with the one we created for highlighting snippets.
			renderResult = transcodePool.submit(new Callable<byte[]>() {
				public byte[] call() throws Exception {
					Document document = (Document) get(OUT_PARAM_DOCUMENT);
					TranscoderInput input = new TranscoderInput(document);
					ByteArrayOutputStream buffer = new ByteArrayOutputStream(64 * 1024);
					transcoder.transcode(input, new TranscoderOutput(buffer));
					return buffer.toByteArray();
				}
			}).get();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException(e);
		} catch (ExecutionException e) {
			if (e.getCause() instanceof IOException) throw (IOException) e.getCause();
			throw new RuntimeException(e);
		}

		if (isNotAttached() || (containsSource && get(PARAM_ATTACH) == null)) {
			put(OUT_PARAM_RENDER_RESULT, renderResult);
		} else {
			String fileName = getAttachedName();
			if (!fileName.endsWith(renderFormat)) fileName += '.' + renderFormat;

			final boolean createLink = containsKey(PARAM_TITLE) || "tiff".equalsIgnoreCase(renderFormat);
			final String href = Globals.getInstance().attachBinaryContent(fileName, renderResult);

			put(OUT_PARAM_RENDER_HREF, href);
			if (!containsKey(PARAM_TITLE)) put(PARAM_TITLE, new File(href).getName());

			if (!containsSource) {
				if (createLink) {
					put(PARAM_SOURCE_CONTENT, "<a href='${esc.xml($renderedHref)}'>${esc.xml($title)}</a>");
				} else {
					put(PARAM_SOURCE_CONTENT, "<img src='${esc.xml($renderedHref)}' alt='${esc.xml($title)}'/>");
				}
			}
		}
	}

	protected void configureTranscoder(final File baseDir, ImageTranscoder transcoder) {
		addIfDefined(transcoder, KEY_BACKGROUND_COLOR, PARAM_RENDER_BACKGROUND, new Callable<Object>() {
			public Object call() throws Exception {
				String value = valueOf(get(PARAM_RENDER_BACKGROUND));
				return convertToPaint(value);
			}
		});

		// Working around a batik bug, allowed script types must not contain any spaces.
		transcoder.addTranscodingHint(KEY_ALLOWED_SCRIPT_TYPES, DEFAULT_ALLOWED_SCRIPT_TYPES.replaceAll("\\s*", ""));
		// Enabling the execution of onload scripts.
		transcoder.addTranscodingHint(KEY_EXECUTE_ONLOAD, get(PARAM_RENDER_ONLOAD) == null || parseBoolean(valueOf(get(PARAM_RENDER_ONLOAD))));

		addIfDefined(transcoder, KEY_WIDTH, PARAM_WIDTH, new Callable<Object>() {
			public Object call() throws Exception {
				return Float.parseFloat(valueOf(get(PARAM_WIDTH)));
			}
		});

		addIfDefined(transcoder, KEY_HEIGHT, PARAM_HEIGHT, new Callable<Object>() {
			public Object call() throws Exception {
				return Float.parseFloat(valueOf(get(PARAM_HEIGHT)));
			}
		});

		addIfDefined(transcoder, KEY_PIXEL_UNIT_TO_MILLIMETER, PARAM_RENDER_DPI, new Callable<Object>() {
			public Object call() throws Exception {
				float dpi = Float.parseFloat(valueOf(get(PARAM_RENDER_DPI)));
				return (2.41f * 10) / dpi;
			}
		});

		addIfDefined(transcoder, KEY_MEDIA, PARAM_RENDER_MEDIA, new Callable<Object>() {
			public Object call() throws Exception {
				return get(PARAM_RENDER_MEDIA);
			}
		});

		addIfDefined(transcoder, KEY_USER_STYLESHEET_URI, PARAM_RENDER_CSS, new Callable<Object>() {
			public Object call() throws Exception {
				return ResourceResolver.findSource(baseDir, valueOf(get(PARAM_RENDER_CSS)));
			}
		});
	}

	protected void addIfDefined(ImageTranscoder transcoder, TranscodingHints.Key transcoderKey, String valueKey, Callable<Object> valueGetter) {
		if (get(valueKey) != null) try {
			transcoder.addTranscodingHint(transcoderKey, valueGetter.call());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected static void applyParameterTranslations(Map<String, Object> requestParams) {
		for (String[] parameterTranslation : parameterTranslations) {
			if (requestParams.containsKey(parameterTranslation[0]))
				requestParams.put(parameterTranslation[1], requestParams.get(parameterTranslation[0]));
		}
	}

	@Override
	public String toString() {
		return "SvgLoader{" +
				"svg=" + get(PARAM_SVG) +
				'}';
	}
}
