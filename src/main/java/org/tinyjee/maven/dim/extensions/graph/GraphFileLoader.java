/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.graph;

import com.mxgraph.io.mxCodec;
import com.mxgraph.io.mxGdCodec;
import com.mxgraph.io.mxGraphMlCodec;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.util.mxXmlUtils;
import com.mxgraph.util.png.mxPngTextDecoder;
import com.mxgraph.view.mxGraph;
import org.apache.maven.doxia.logging.Log;
import org.codehaus.plexus.util.FileUtils;
import org.tinyjee.maven.dim.extensions.GraphLoader;
import org.tinyjee.maven.dim.extensions.XmlLoader;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.UrlFetcher;
import org.w3c.dom.Document;

import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Loads graph files of various types.
 *
 * @author juergen kellerer, 2012-07-07
 */
public class GraphFileLoader implements Callable<Boolean> {

	private static final Log log = Globals.getLog();

	protected final String file;
	protected final mxGraph graph;
	protected final Map<String, Object> parameters;

	public GraphFileLoader(mxGraph graph, String file, Map<String, Object> parameters) {
		this.file = file;
		this.graph = graph;
		this.parameters = parameters;
	}

	private Document parseXml(String file) throws Exception {
		return XmlLoader.load(file, true, null);
	}

	protected void openPNGWithEmbeddedMXE(URL file) throws IOException {
		Map<String, String> text = mxPngTextDecoder.decodeCompressedText(UrlFetcher.getSource(file));

		if (text != null) {
			String value = text.get("mxGraphModel");
			if (value != null) {
				Document document = mxXmlUtils.parseXml(URLDecoder.decode(value, "UTF-8"));
				openMXE(document);
				return;
			}
		}

		throw new IllegalArgumentException("Image contains not diagram data.");
	}

	protected void openMXE(Document document) {
		mxCodec codec = new mxCodec(document);

		clearGraph();
		codec.decode(document.getDocumentElement(), graph.getModel());
	}

	protected void openMXE(String file) throws Exception {
		openMXE(parseXml(file));
	}

	protected void openGD(URL file) throws IOException {
		clearGraph();
		mxGdCodec.decode(Globals.getInstance().fetchUrlText(file), graph);
	}

	protected void openTGF(URL file) throws IOException {
		final GraphBuilder builder = new GraphBuilder();
		TrivialGraphFormatParser parser = new TrivialGraphFormatParser(true);
		parser.parseGraph(file, builder);
		builder.buildGraph(graph);
	}

	protected void openGraphML(String file) throws Exception {
		Document document = parseXml(file);

		clearGraph();
		mxGraphMlCodec.decode(document, graph);
	}

	private void clearGraph() {
		((mxGraphModel) graph.getModel()).clear();
	}

	public void run() {
	}

	public Boolean call() throws Exception {
		final boolean debug = log.isDebugEnabled();

		String extension = FileUtils.extension(file);
		if (parameters.containsKey(GraphLoader.PARAM_INPUT_FORMAT))
			extension = String.valueOf(parameters.get(GraphLoader.PARAM_INPUT_FORMAT));

		if ("mxe".equalsIgnoreCase(extension)) {
			if (debug) log.debug("Loading mxGraph native format from " + file);
			openMXE(file);
		} else if ("png".equalsIgnoreCase(extension)) {
			if (debug) log.debug("Loading mxGraph native format embedded inside PNG from " + file);
			openPNGWithEmbeddedMXE(Globals.getInstance().resolvePath(file));
		} else if ("vdx".equalsIgnoreCase(extension)) {
			if (debug) log.debug("Loading XML drawing format (*.vdx) from " + file);
			openMXE(file);
		} else if ("txt".equalsIgnoreCase(extension)) {
			if (debug) log.debug("Loading Graph Drawing (*.txt) from " + file);
			openGD(Globals.getInstance().resolvePath(file));
		} else if ("graphml".equalsIgnoreCase(extension) || "graphmlz".equalsIgnoreCase(extension)) {
			if (debug) log.debug("Loading GraphML format from " + file);
			openGraphML(file);
		} else if ("tgf".equalsIgnoreCase(extension)) {
			if (debug) log.debug("Loading Trivial Graph format (*.tgf) from " + file);
			openTGF(Globals.getInstance().resolvePath(file));
		} else
			throw new IllegalArgumentException("Cannot open a graph of format '" + extension + "' from " + file);

		return true;
	}
}
