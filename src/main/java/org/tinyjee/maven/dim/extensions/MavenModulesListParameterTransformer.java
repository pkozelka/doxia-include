/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.tinyjee.maven.dim.IncludeMacroSignature;
import org.tinyjee.maven.dim.spi.ExtensionInformation;

import java.util.Map;

/**
 * Generates list out of the specified selection of Maven root POMs.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|maven-modules-list=pom.xml}
 * %{include|maven-modules-list=module1/pom.xml,module2/pom.xml}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 *
 * @author Juergen_Kellerer, 2012-07-08
 */
@ExtensionInformation(displayName = "Maven Modules List", documentationUrl = "/templatesMavenModulesDiagram.html")
public class MavenModulesListParameterTransformer extends AbstractParameterTransformer {

	/**
	 * Is a comma separated list of root poms to use for building the list.
	 */
	public static final String PARAM_MAVEN_MODULES_LIST = "maven-modules-list";

	/**
	 * Regular expression that must be matched by scope.
	 * (Default: "{@code ^(?!test|provided).*$}" -> anything except 'test' and 'provided')
	 */
	public static final String PARAM_SCOPE = "scope";

	/**
	 * Specifies a regular expression matching "groupId:artifactId" of modules that may be included.
	 * (Default: '.*')
	 */
	public static final String PARAM_INCLUDES = "includes";

	/**
	 * Specifies a regular expression matching "groupId:artifactId" of modules that must not be included.
	 */
	public static final String PARAM_EXCLUDES = "excludes";

	private static final String INPUT_PARAM_POMS = "poms";

	@Override
	protected boolean doTransformParameters(Map<String, Object> requestParams) {
		Object input = requestParams.get(PARAM_MAVEN_MODULES_LIST);
		if (input != null) {
			requestParams.put(INPUT_PARAM_POMS, input);
			requestParams.put(IncludeMacroSignature.PARAM_SOURCE, "classpath:/templates/dim/maven-modules-list.xdoc.vm");

			return true;
		}
		return false;
	}
}
