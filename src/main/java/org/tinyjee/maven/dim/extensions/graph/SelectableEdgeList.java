/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.graph;

import org.tinyjee.maven.dim.utils.SelectableArrayList;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Implements a selectable list for edges. A list of this type is returned by {@code $graphBuilder.edges}.
 * <p/>
 * <b>Usage:</b><code><pre>
 * ## Get all vertexes that are referenced from vertex "a"
 * $graphBuilder.edges.selectFrom("a").toVertexes()
 * <p/>
 * ## Get all vertexes that are referencing vertex "a"
 * $graphBuilder.edges.selectTo("a").fromVertexes()
 * <p/>
 * ## Get all vertexes that are referencing vertex "a" and whose names contain "b"
 * $graphBuilder.edges.selectTo("a").selectMatchingFrom(".*b.*").fromVertexes()
 *   ..or..
 * $graphBuilder.edges.selectTo("a").fromVertexes().selectMatching(".*b.*")
 * <p/>
 * ## Remove all vertexes that do not reference the vertexes "a" or "b"
 * #set($edges = $graphBuilder.edges.selectTo(["a", "b"]).selectFrom(["a", "b"]))
 * #set($removableVertexes = $graphBuilder.vertexes.remove($edges.toVertexes().append($edges.fromVertexes())))
 * $graphBuilder.removeVertexes($removableVertexes)
 * </pre></code>
 *
 * @author Juergen_Kellerer, 2012-07-21
 */
public class SelectableEdgeList extends SelectableArrayList<List<String>> {

	private static final long serialVersionUID = 2274876025638765581L;

	public SelectableEdgeList(Collection<? extends List<String>> lists) {
		super(lists);
	}

	/**
	 * Returns all the vertexes that the edges originate from.
	 *
	 * @return all the vertexes that the edges originate from.
	 */
	public SelectableArrayList<String> fromVertexes() {
		return extractNames(0);
	}

	/**
	 * Returns all the vertexes that the edges direct to.
	 *
	 * @return all the vertexes that the edges direct to.
	 */
	public SelectableArrayList<String> toVertexes() {
		return extractNames(1);
	}

	/**
	 * Selects all edges whose originating vertex matches the specified regular expression.
	 *
	 * @param fromVertexName a regular expression matching the vertex name.
	 * @return all edges whose originating vertex matches the specified regular expression
	 */
	public SelectableEdgeList selectMatchingFrom(String... fromVertexName) {
		return (SelectableEdgeList) select(matchEdgeName(0, fromVertexName));
	}

	public SelectableEdgeList selectMatchingFrom(String fromVertexName) {
		return selectMatchingFrom(new String[]{fromVertexName});
	}

	public SelectableEdgeList selectMatchingFrom(List<String> fromVertexNames) {
		return selectMatchingFrom(fromVertexNames.toArray(new String[fromVertexNames.size()]));
	}

	/**
	 * Selects all edges whose originating vertex does not match the specified regular expression.
	 *
	 * @param fromVertexName a regular expression matching the vertex name.
	 * @return all edges whose originating vertex does not match the specified regular expression
	 */
	public SelectableEdgeList selectNotMatchingFrom(String... fromVertexName) {
		return (SelectableEdgeList) select(createInvertSelector(matchEdgeName(0, fromVertexName)));
	}

	public SelectableEdgeList selectNotMatchingFrom(String fromVertexName) {
		return selectNotMatchingFrom(new String[]{fromVertexName});
	}

	public SelectableEdgeList selectNotMatchingFrom(List<String> fromVertexNames) {
		return selectNotMatchingFrom(fromVertexNames.toArray(new String[fromVertexNames.size()]));
	}

	/**
	 * Selects all edges whose originating vertex equals the specified name.
	 *
	 * @param fromVertexName the vertex name.
	 * @return all edges whose originating vertex equals the specified name
	 */
	public SelectableEdgeList selectFrom(String... fromVertexName) {
		return (SelectableEdgeList) select(edgeNameEquals(0, fromVertexName));
	}

	public SelectableEdgeList selectFrom(String fromVertexName) {
		return selectFrom(new String[]{fromVertexName});
	}

	public SelectableEdgeList selectFrom(List<String> fromVertexNames) {
		return selectFrom(fromVertexNames.toArray(new String[fromVertexNames.size()]));
	}

	/**
	 * Selects all edges whose originating vertex is not equals the specified name.
	 *
	 * @param fromVertexName the vertex name.
	 * @return all edges whose originating vertex is not equals the specified name
	 */
	public SelectableEdgeList selectNotFrom(String... fromVertexName) {
		return (SelectableEdgeList) select(createInvertSelector(edgeNameEquals(0, fromVertexName)));
	}

	public SelectableEdgeList selectNotFrom(String fromVertexName) {
		return selectNotFrom(new String[]{fromVertexName});
	}

	public SelectableEdgeList selectNotFrom(List<String> fromVertexNames) {
		return selectNotFrom(fromVertexNames.toArray(new String[fromVertexNames.size()]));
	}

	/**
	 * Selects all edges whose target vertex matches the specified regular expression.
	 *
	 * @param toVertexName a regular expression matching the vertex name.
	 * @return all edges whose target vertex matches the specified regular expression
	 */
	public SelectableEdgeList selectMatchingTo(String... toVertexName) {
		return (SelectableEdgeList) select(matchEdgeName(1, toVertexName));
	}

	public SelectableEdgeList selectMatchingTo(String toVertexName) {
		return selectMatchingTo(new String[]{toVertexName});
	}

	public SelectableEdgeList selectMatchingTo(List<String> toVertexNames) {
		return selectMatchingTo(toVertexNames.toArray(new String[toVertexNames.size()]));
	}

	/**
	 * Selects all edges whose target vertex does not match the specified regular expression.
	 *
	 * @param toVertexName a regular expression matching the vertex name.
	 * @return all edges whose target vertex does not match the specified regular expression
	 */
	public SelectableEdgeList selectNotMatchingTo(String... toVertexName) {
		return (SelectableEdgeList) select(createInvertSelector(matchEdgeName(1, toVertexName)));
	}

	public SelectableEdgeList selectNotMatchingTo(String toVertexName) {
		return selectNotMatchingTo(new String[]{toVertexName});
	}

	public SelectableEdgeList selectNotMatchingTo(List<String> toVertexNames) {
		return selectNotMatchingTo(toVertexNames.toArray(new String[toVertexNames.size()]));
	}

	/**
	 * Selects all edges whose target vertex equals the specified name.
	 *
	 * @param toVertexName the vertex name.
	 * @return all edges whose target vertex equals the specified name
	 */
	public SelectableEdgeList selectTo(String... toVertexName) {
		return (SelectableEdgeList) select(edgeNameEquals(1, toVertexName));
	}

	public SelectableEdgeList selectTo(String toVertexName) {
		return selectTo(new String[]{toVertexName});
	}

	public SelectableEdgeList selectTo(List<String> toVertexNames) {
		return selectTo(toVertexNames.toArray(new String[toVertexNames.size()]));
	}

	/**
	 * Selects all edges whose target vertex is not equals the specified name.
	 *
	 * @param toVertexName the vertex name.
	 * @return all edges whose target vertex is not equals the specified name
	 */
	public SelectableEdgeList selectNotTo(String... toVertexName) {
		return (SelectableEdgeList) select(createInvertSelector(edgeNameEquals(1, toVertexName)));
	}

	public SelectableEdgeList selectNotTo(String toVertexName) {
		return selectNotTo(new String[]{toVertexName});
	}

	public SelectableEdgeList selectNotTo(List<String> toVertexNames) {
		return selectNotTo(toVertexNames.toArray(new String[toVertexNames.size()]));
	}

	private SelectableArrayList<String> extractNames(int index) {
		Set<String> names = new LinkedHashSet<String>(size());
		for (List<String> edge : this) names.add(edge.get(index));
		return new SelectableArrayList<String>(names);
	}

	private Selector<List<String>> edgeNameEquals(final int index, final String... fromVertexNames) {
		return new Selector<List<String>>() {
			public boolean accept(List<String> element) {
				for (String fromVertexName : fromVertexNames) {
					if (fromVertexName.equals(element.get(index))) return true;
				}
				return false;
			}
		};
	}

	@SuppressWarnings("unchecked")
	private Selector<List<String>> matchEdgeName(final int index, final String... toVertexNames) {
		final Selector[] regexSelectors = new Selector[toVertexNames.length];
		for (int i = 0; i < regexSelectors.length; i++) regexSelectors[i] = createRegularExpressionSelector(toVertexNames[i]);

		return new Selector<List<String>>() {

			public boolean accept(List<String> element) {
				for (Selector selector : regexSelectors) {
					if (selector.accept(element.get(index))) return true;
				}
				return false;
			}
		};
	}
}
