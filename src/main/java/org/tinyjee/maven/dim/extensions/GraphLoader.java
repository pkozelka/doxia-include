/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import com.mxgraph.canvas.mxICanvas;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.layout.*;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.swing.handler.mxGraphHandler;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.view.mxInteractiveCanvas;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource;
import com.mxgraph.util.mxRectangle;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxGraph;
import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.extensions.graph.GraphBuilder;
import org.tinyjee.maven.dim.extensions.graph.GraphFileLoader;
import org.tinyjee.maven.dim.extensions.graph.GraphStyles;
import org.tinyjee.maven.dim.spi.ExtensionInformation;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.utils.AbstractAliasHandler;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.List;

import static java.lang.Double.parseDouble;
import static java.lang.Math.floor;
import static java.lang.Math.max;
import static java.lang.String.valueOf;
import static java.util.Collections.singletonMap;
import static org.tinyjee.maven.dim.extensions.image.ImageUtil.convertToPaint;

/**
 * Loads, generates or transcodes graphs and attaches or renders it as PNG or SVG or a supported Graph format.
 * <p/>
 * In addition to graph file loading this loader supports also generating dynamic graphs via velocity, scripts or
 * java classes see the "{@code graph}" parameter below.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|source-graph=file.(tgf|graphml)}
 * %{include|source-graph=my-graph.xml|xsl=convert-to-graphml.xsl|input-format=graphml}
 * %{include|source-graph=script:graph-builder.(py|rb|js|groovy|..)}
 * %{include|source-graph=create:my-graph.png|source=graph-builder.vm}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 * Full call syntax:<code><pre>
 * %{include|source-class=org.tinyjee.maven.dim.extensions.GraphLoader|graph=file.(tgf|graphml)}
 * </pre></code>
 *
 * @author Juergen_Kellerer, 2011-11-05
 */
@ExtensionInformation(displayName = "Graph Loader", documentationUrl = "/extensionsGraphLoader.html")
public class GraphLoader extends HashMap<String, Object> {

	private static final Log log = Globals.getLog();
	private static final long serialVersionUID = 48462397821801651L;

	private static final String SCRIPT_PREFIX = "script:";
	private static final String CREATE_PREFIX = "create:";


	/**
	 * Implements the "{@link org.tinyjee.maven.dim.extensions.GraphLoader#PARAM_ALIAS}" alias functionality.
	 */
	public static class AliasHandler extends AbstractAliasHandler {
		/**
		 * Constructs the handler (Note: Is called by {@link org.tinyjee.maven.dim.spi.RequestParameterTransformer#TRANSFORMERS}).
		 */
		public AliasHandler() {
			super(PARAM_ALIAS, PARAM_GRAPH, GraphLoader.class.getName());
		}
	}

	/**
	 * Defines a shortcut for the request properties 'source-class' and 'graph'.
	 * <p/>
	 * If this parameter is present inside the request parameters list, the system will effectively behave as if
	 * 'source-class' and 'graph' where set separately.
	 */
	public static final String PARAM_ALIAS = "source-graph";

	/**
	 * Sets the URL, local path or expression to the graph resource to load or create.
	 * <p/>
	 * Possible values:<ul>
	 * <li>{@code path/to/graph.file}: Loads the specified graph file.</li>
	 * <li>{@code create:my-graph}: Creates an empty graph that may be assembled using {@code $graphBuilder} from a velocity template.</li>
	 * <li>{@code script:my-graph-builder.js}: Creates an empty graph that may be assembled using {@code graphBuilder} from the specified script.</li>
	 * </ul>
	 * <p/>
	 * When a path or URL is specified, the graph file is resolved in exactly the same way as when using the
	 * parameter "<code>source</code>" (see Macro reference).
	 * <p/>
	 * If the parameter value is prefixed with "{@code create:}", an empty graph is created and graph building and attaching is
	 * assumed to be performed by by the a velocity template that was specified within {@code source} parameter. Example usage:
	 * <code><pre>
	 * %{include|source=graph-builder.apt.vm|source-graph=create:my-graph-filename|format=png}
	 * </pre></code>
	 * graph-builder.apt.vm
	 * <code><pre>
	 * $graphBuilder.addVertex("a")
	 * $graphBuilder.addVertex("b")
	 * $graphBuilder.addVertex("c", {"style": "shape=ellipse;strokeColor=red;strokeWidth=3"})
	 * $graphBuilder.addEdge("a", "c")
	 * $graphBuilder.addEdge("b", "c")
	 * <p/>
	 * #set($graphLoader.width = 500)
	 * #set($graphLoader.height = 300)
	 * [$graphLoader.attach()]
	 * </pre></code>
	 * <p/>
	 * If the parameter value is prefixed with "{@code script:}" the remaining part of the specified value is considered to be a path to a
	 * script that is supported by {@link ScriptInvoker}.
	 * Graph building follows the same principal as if the prefix had been "{@code create:}", which means "graphBuilder" and "graphLoader"
	 * are accessible within the script context. In difference to "{@code create:}", {@code graphLoader.attach()} doesn't need to be called.
	 */
	public static final String PARAM_GRAPH = "graph";

	/**
	 * Sets the filename to use for attaching an rendered graph as image.
	 * <p/>
	 * Possible values:<ul>
	 * <li>undefined: Attaches the image using the original filename (extended with a format specific file extension).</li>
	 * <li>'%s-suffix': Attaches the image using the original filename with a suffix.</li>
	 * <li>'[filename]': Attaches the image using the specified filename.</li>
	 * <li>'false': Does not attach the image.</li>
	 * </ul>
	 * <p/>
	 */
	public static final String PARAM_ATTACH = "attach";

	/**
	 * Sets the image format to use (e.g. 'png', 'jpg', etc.). If not set this parameter is assumed
	 * using the the a default value of 'png' or the filename extension of {@code attach}.
	 * <p/>
	 * Note: Image rendering is performed by {@link ImageLoader} and therefore image loader parameters can be used to change
	 * the way how graph images are rendered.
	 */
	public static final String PARAM_FORMAT = "format";

	/**
	 * specifies the graph input format.
	 * <p/>
	 * Possible values:<ul>
	 * <li>graphml(z): GraphML Format.</li>
	 * <li>tgf: Trivial Graph Format.</li>
	 * <li>(png|mxe|vxd|txt): JGraphX(XML/TXT) Format.</li>
	 * </ul>
	 * <p/>
	 * (Default: <i>file extension</i>)
	 */
	public static final String PARAM_INPUT_FORMAT = "input-format";

	/**
	 * Toggles whether HTML markup within labels is interpreted.
	 * (Default: false)
	 */
	public static final String PARAM_HTML_LABELS = "html-labels";

	/**
	 * Toggles whether cell sizes are automatically optimized.
	 * (Default: 'true' if {@code layout} is not "{@code none}")
	 * <p/>
	 * Possible values:<ul>
	 * <li>"{@code true}", Optimize size if no size is specified.</li>
	 * <li>"{@code force}", Always optimize the size.</li>
	 * <li>"{@code false}", Never optimize the size.</li>
	 * </ul>
	 */
	public static final String PARAM_AUTO_SIZE = "autosize";

	/**
	 * Sets the minimum vertex size as "{@code width x height}", e.g. "{@code 100x35}".
	 */
	public static final String PARAM_MIN_SIZE = "cell-size-min";

	/**
	 * Sets the amount of pixels between cell border and label.
	 * (Default: 2)
	 */
	public static final String PARAM_CELL_PADDING = "cell-padding";

	/**
	 * Sets the amount of pixels between cells when applying a layout.
	 * (Default: <i>layout dependent, between 0 and 30</i>)
	 */
	public static final String PARAM_CELL_SPACING = "cell-spacing";

	/**
	 * Sets the amount of pixels between hierarchical cells when applying a layout (used in hierarchical layout).
	 * <p/>
	 * Possible values:<ul>
	 * <li>"{@code number}", e.g. "{@code 50}": Number of pixels between any hierarchies.</li>
	 * <li>"{@code number:number}", e.g. "{@code 50:60}": First is number of pixels between unconnected hierarchies and second is for connected ones.</li>
	 * </ul>
	 */
	public static final String PARAM_HIERARCHY_SPACING = "hierarchy-spacing";

	/**
	 * Specifies a stylesheet to load.
	 * (Default: "classpath:/org/tinyjee/maven/dim/extensions/graph/default-style.xml")
	 */
	public static final String PARAM_STYLESHEET = "stylesheet";

	/**
	 * Changes the default vertex style using "attribute:value;.." or "stylenName;..".
	 * This property is applied after the stylesheet was loaded.
	 */
	public static final String PARAM_VERTEX_STYLE = "vertex-style";

	/**
	 * Changes the default edge style using "attribute:value;.." or "stylenName;..".
	 * This property is applied after the stylesheet was loaded.
	 */
	public static final String PARAM_EDGE_STYLE = "edge-style";

	/**
	 * Sets the color of the border around the graph.
	 * (Default: transparent)
	 */
	public static final String PARAM_BORDER_COLOR = "border-color";

	/**
	 * Sets the size of the border around the graph.
	 * (Default: 0)
	 */
	public static final String PARAM_BORDER = "border";

	/**
	 * Sets the color of the graph background.
	 * (Default: transparent)
	 */
	public static final String PARAM_BACKGROUND_COLOR = "background-color";

	/**
	 * sets the layout engine to use.
	 * <p/>
	 * Possible values:<ul>
	 * <li>none</li>
	 * <li>hierarchical</li>
	 * <li>organic</li>
	 * <li>tree</li>
	 * <li>stack</li>
	 * <li>partition</li>
	 * <li>circle</li>
	 * </ul>
	 * Modifiers (appended using '-' as delimiter, e.g. "hierarchical-vertical"):<ul>
	 * <li>invert</li>
	 * <li>vertical</li>
	 * <li>horizontal <i>(default)</i></li>
	 * <li>parallelEdges</li>
	 * <li>placeLabels</li>
	 * </ul>
	 * <p/>
	 * (Default: 'hierarchical')
	 */
	public static final String PARAM_LAYOUT = "layout";

	/**
	 * Sets the target width in pixel.
	 */
	public static final String PARAM_WIDTH = "width";

	/**
	 * Sets the target height in pixel.
	 */
	public static final String PARAM_HEIGHT = "height";

	/**
	 * Sets a percentage to down or upscale the graph.
	 * (Default: '100')
	 */
	public static final String PARAM_SCALE = "scale";

	/**
	 * Is set with a reference to this loader instance.
	 */
	public static final String OUT_PARAM_LOADER = "graphLoader";

	/**
	 * Is set with a to {@link GraphBuilder} that may be used within templates or scripts to build an arbitrary graph.
	 */
	public static final String OUT_PARAM_GRAPH_BUILDER = "graphBuilder";

	/**
	 * Is set with a reference to a {@link java.awt.image.BufferedImage} instance straight after the graph was rendered.
	 */
	public static final String OUT_PARAM_IMAGE = "image";

	final URL graphFile;
	final String graphName;

	final mxGraph graph;
	final mxGraphComponent graphComponent;
	final GraphBuilder graphBuilder = new GraphBuilder();

	final Map<String, mxCell> allCells = new HashMap<String, mxCell>();

	public GraphLoader(File baseDir, Map<String, Object> parameters) throws Exception {
		putAll(parameters);

		graph = createGraph();
		graphComponent = createComponent();

		put(OUT_PARAM_LOADER, this);
		put(OUT_PARAM_GRAPH_BUILDER, graphBuilder);

		handleStyles();

		final String graphPath = valueOf(get(PARAM_GRAPH));

		if (graphPath.startsWith(CREATE_PREFIX)) {
			graphFile = null;
			graphName = graphPath.substring(CREATE_PREFIX.length());
		} else if (graphPath.startsWith(SCRIPT_PREFIX)) {
			graphFile = null;
			graphName = graphPath.contains("eval[") ? "unnamed-graph" : graphPath.substring(SCRIPT_PREFIX.length());
			put(ScriptInvoker.PARAM_SCRIPT, graphPath.substring(SCRIPT_PREFIX.length()));
			put(ScriptInvoker.PARAM_ISOLATE, true);
			new ScriptInvoker(baseDir, this);
			attach();
		} else {
			graphFile = Globals.getInstance().resolvePath(graphPath);
			graphName = graphPath;
			createGraphFileLoader(graph, graphPath, this).call();
			attach();
		}
	}

	protected void handleStyles() {
		String stylesheet = containsKey(PARAM_STYLESHEET) ?
				valueOf(get(PARAM_STYLESHEET)) : "classpath:/org/tinyjee/maven/dim/extensions/graph/default-style.xml";
		loadStyleSheet(stylesheet);

		String style = (String) get(PARAM_VERTEX_STYLE);
		if (style != null) changeStyleClass("defaultVertex", style.replace(':', '='));
		style = (String) get(PARAM_EDGE_STYLE);
		if (style != null) changeStyleClass("defaultEdge", style.replace(':', '='));
	}

	public void loadStyleSheet(String stylesheet) {
		graph.setStylesheet(GraphStyles.loadStyles(stylesheet));
	}

	public void defineStyleClass(String styleName, String styleDefinition) {
		GraphStyles.defineStyleClass(graph, styleName, styleDefinition);
	}

	void defineStyleClass(String styleName, Map<String, Object> styleProperties) {
		GraphStyles.defineStyleClass(graph, styleName, styleProperties);
	}

	public void changeStyleClass(String styleName, String styleDefinition) {
		GraphStyles.changeStyleClass(graph, styleName, styleDefinition);
	}

	void changeStyleClass(String styleName, Map<String, Object> styleProperties) {
		GraphStyles.changeStyleClass(graph, styleName, styleProperties);
	}

	public GraphBuilder getBuilder() {
		return graphBuilder;
	}

	public void buildGraph() {
		graphBuilder.buildGraph(graph);
	}

	public mxGraph getGraph() {
		return graph;
	}

	public String attach() throws Exception {
		if (graphFile == null) {
			buildGraph();
		}

		applyLayout((String) get(PARAM_LAYOUT));
		applyBackgroundColor();

		if ("false".equalsIgnoreCase(valueOf(get(PARAM_ATTACH)))) return "";

		Map<String, Object> parameters = new HashMap<String, Object>(this);
		parameters.put(ImageLoader.PARAM_IMAGE, graphComponent);
		if (!parameters.containsKey(PARAM_FORMAT)) parameters.put(PARAM_FORMAT, "png");

		if (!parameters.containsKey(PARAM_ATTACH)) parameters.put(ImageLoader.PARAM_ATTACH, graphName);

		double scale = getScale();
		if (containsKey(PARAM_WIDTH)) parameters.put(ImageLoader.PARAM_WIDTH, Math.round(scale * getLayoutAreaSize().getWidth()));
		if (containsKey(PARAM_HEIGHT)) parameters.put(ImageLoader.PARAM_HEIGHT, Math.round(scale * getLayoutAreaSize().getHeight()));

		putAll(new ImageLoader(Globals.getInstance().getBasedir(), parameters));
		return (String) get(ImageLoader.OUT_PARAM_IMAGE_HREF);
	}

	protected void applyBackgroundColor() {
		graphComponent.getViewport().setOpaque(false);

		final int borderSize = get(PARAM_BORDER) == null ? 0 : (int) parseDouble(valueOf(get(PARAM_BORDER)));
		final Paint borderPaint = convertToPaint((String) get(PARAM_BORDER_COLOR));
		final EmptyBorder border = new EmptyBorder(borderSize, borderSize, borderSize, borderSize);
		if (borderPaint != null)
			graphComponent.setBorder(BorderFactory.createCompoundBorder(new LineBorder((Color) borderPaint), border));
		else
			graphComponent.setBorder(border);

		final Paint backgroundPaint = convertToPaint((String) get(PARAM_BACKGROUND_COLOR));
		if (backgroundPaint == null)
			graphComponent.setOpaque(false);
		else {
			graphComponent.setOpaque(true);
			graphComponent.setBackground((Color) backgroundPaint);
		}
	}

	public double getScale() {
		double scale = 1D;
		if (containsKey(PARAM_SCALE)) {
			scale = parseDouble(valueOf(get(PARAM_SCALE)));
			if (scale > 2.5D) scale /= 100D;
		}

		return scale;
	}

	protected mxGraph createGraph() {
		final mxGraph mxGraph = new mxGraph() {
			@Override
			public void drawState(mxICanvas canvas, mxCellState state, boolean drawLabel) {
				super.drawState(canvas, state, drawLabel);
			}
		};

		mxGraph.setMultigraph(true);
		mxGraph.setAutoOrigin(true);

		mxGraph.addListener(mxEvent.CLEAR, new mxEventSource.mxIEventListener() {
			public void invoke(Object sender, mxEventObject evt) {
				allCells.clear();
			}
		});
		mxGraph.addListener(mxEvent.ADD_CELLS, new CellsAddedListener(mxGraph));

		mxGraph.setHtmlLabels("true".equalsIgnoreCase(valueOf(get(PARAM_HTML_LABELS))));

		return mxGraph;
	}

	protected mxGraphComponent createComponent() {
		GraphComponent component = new GraphComponent();
		component.setFoldingEnabled(false);
		return component;
	}

	protected GraphFileLoader createGraphFileLoader(mxGraph graph, String file, Map<String, Object> parameters) {
		return new GraphFileLoader(graph, file, parameters);
	}

	public void applyLayout(String layoutName) {
		if ("none".equals(layoutName)) return;

		List<mxIGraphLayout> layouts = createLayout(layoutName);
		graph.getModel().beginUpdate();
		try {
			for (mxIGraphLayout graphLayout : layouts)
				graphLayout.execute(graph.getDefaultParent());
		} finally {
			graph.getModel().endUpdate();
		}

		Object[] cells = allCells.values().toArray();
		mxRectangle rectangle = graph.getBoundsForCells(cells, true, false, false);
		if (rectangle != null) graph.getView().scaleAndTranslate(getScale(), -floor(rectangle.getX() - 0.5), -floor(rectangle.getY() - 0.5));
	}

	/**
	 * Creates a layout instance for the given identifiers.
	 */
	protected List<mxIGraphLayout> createLayout(String name) {
		if (name == null) name = "";

		final List<String> layoutAttributes = Arrays.asList(name.toLowerCase().split("-"));
		final boolean horizontal = !layoutAttributes.contains("vertical"), invert = layoutAttributes.contains("invert");
		final mxGraph graph = getGraph();

		Object rawSpacing = get(PARAM_CELL_SPACING), rawHierarchySpacing = get(PARAM_HIERARCHY_SPACING);
		final Double spacing = rawSpacing == null ? null : parseDouble(valueOf(rawSpacing));

		Double hierarchySpacing = null, connectedHierarchySpacing = null;
		if (rawHierarchySpacing != null) {
			final String numbers = valueOf(rawHierarchySpacing);
			if (numbers.contains(":")) {
				hierarchySpacing = parseDouble(numbers.substring(0, numbers.indexOf(':')));
				connectedHierarchySpacing = parseDouble(numbers.substring(numbers.indexOf(':') + 1));
			} else {
				hierarchySpacing = connectedHierarchySpacing = parseDouble(numbers);
			}
		}


		mxIGraphLayout layout;

		if (layoutAttributes.contains("tree")) {
			mxCompactTreeLayout treeLayout = new mxCompactTreeLayout(graph, horizontal, invert);
			if (spacing != null) treeLayout.setNodeDistance(spacing.intValue());
			if (hierarchySpacing != null) treeLayout.setLevelDistance(hierarchySpacing.intValue());
			layout = treeLayout;
		} else if (layoutAttributes.contains("organic")) {
			layout = new mxOrganicLayout(graph) {
				{
					triesPerCell = 8;
					setMaxIterations(5000);
				}
			};
		} else if (layoutAttributes.contains("partition")) {
			layout = new mxPartitionLayout(graph, horizontal, spacing == null ? 0 : spacing.intValue()) {
				@Override
				public mxRectangle getContainerSize() {
					return getLayoutAreaSize();
				}
			};
		} else if (layoutAttributes.contains("stack")) {
			layout = new mxStackLayout(graph, horizontal, spacing == null ? 0 : spacing.intValue()) {
				@Override
				public mxRectangle getContainerSize() {
					return getLayoutAreaSize();
				}
			};
		} else if (layoutAttributes.contains("circle")) {
			layout = new mxCircleLayout(graph, getLayoutAreaSize().getWidth() / 2.2D);
		} else {
			int orientation = horizontal ?
					invert ? SwingConstants.EAST : SwingConstants.WEST :
					invert ? SwingConstants.SOUTH : SwingConstants.NORTH;
			mxHierarchicalLayout hierarchicalLayout = new mxHierarchicalLayout(graph, orientation);

			if (spacing != null) hierarchicalLayout.setIntraCellSpacing(spacing);
			if (hierarchySpacing != null && connectedHierarchySpacing != null) {
				hierarchicalLayout.setInterHierarchySpacing(hierarchySpacing);
				hierarchicalLayout.setInterRankCellSpacing(connectedHierarchySpacing);
			}

			layout = hierarchicalLayout;
		}

		List<mxIGraphLayout> layouts = new ArrayList<mxIGraphLayout>();
		layouts.add(layout);

		if (layoutAttributes.contains("parallelEdges") || layoutAttributes.contains("parallel"))
			layouts.add(new mxParallelEdgeLayout(graph));
		if (layoutAttributes.contains("placeEdgeLabels") || layoutAttributes.contains("placeLabels"))
			layouts.add(new mxEdgeLabelLayout(graph));

		return layouts;
	}

	protected mxRectangle getLayoutAreaSize() {
		Object rawWidth = get(PARAM_WIDTH), rawHeight = get(PARAM_WIDTH);
		if (rawWidth == null) rawWidth = 600;
		if (rawHeight == null) rawHeight = 600;
		return new mxRectangle(0, 0, parseDouble(valueOf(rawWidth)), parseDouble(valueOf(rawHeight)));
	}

	public static void main(String[] args) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>(singletonMap("graph", (Object) (args.length > 0 ? args[0] : "create:")));
		map.put("attach", false);
		GraphLoader loader = new GraphLoader(new File("."), map);

		String value = "<?xml version=\"1.0\"?>\n" +
				"<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"32\" height=\"32\" version=\"1.1\">\n" +
				"\t<defs>\n" +
				"\t\t<linearGradient id=\"strokeGradient-$idSequence\"\n" +
				"\t\t\t\t\t\tgradientUnits=\"objectBoundingBox\" x1=\"0%\" x2=\"0%\" y1=\"0%\" y2=\"100%\">\n" +
				"\t\t\t<stop offset=\"15%\" style=\"stop-color:#F50;\"/>\n" +
				"\t\t\t<stop offset=\"50%\" style=\"stop-color:#E00;\"/>\n" +
				"\t\t\t<stop offset=\"72%\" style=\"stop-color:#700;\"/>\n" +
				"\t\t\t<stop offset=\"110%\" style=\"stop-color:#D33;\"/>\n" +
				"\t\t</linearGradient>\n" +
				"\t\t<linearGradient id=\"innerGradient-$idSequence\"\n" +
				"\t\t\t\t\t\tgradientUnits=\"objectBoundingBox\" x1=\"0%\" x2=\"0%\" y1=\"0%\" y2=\"100%\">\n" +
				"\t\t\t<stop offset=\"66%\" style=\"stop-color:#ffffff;\"/>\n" +
				"\t\t\t<stop offset=\"100%\" style=\"stop-color:#dddddd;\"/>\n" +
				"\t\t</linearGradient>\n" +
				"\t\t<filter id=\"dropShadow-$idSequence\" filterUnits=\"userSpaceOnUse\" x=\"0\" y=\"0\" width=\"100%\" height=\"100%\">\n" +
				"\t\t\t<feGaussianBlur in=\"SourceAlpha\" stdDeviation=\"2.5\" result=\"blur\"/>\n" +
				"\t\t</filter>\n" +
				"\t</defs>\n" +
				"\n" +
				"\t<polygon style=\"fill:black;\" opacity=\"1\" filter=\"url(#dropShadow-$idSequence)\" points=\"3,27 16,4 29,27\"/>\n" +
				"\t<polygon style=\"fill:url(#innerGradient-$idSequence); stroke: url(#strokeGradient-$idSequence);\"\n" +
				"\t\t\t stroke-width=\"3.5\" stroke-linejoin=\"round\" points=\"3,27 16,4 29,27\"/>\n" +
				"\t<polygon style=\"fill:black;\" stroke-linejoin=\"round\" points=\"14,10 18,10 17,18.5 15,18.5\"/>\n" +
				"\t<circle style=\"fill:black; stroke:none\" cx=\"16\" cy=\"22\" r=\"2\"/>\n" +
				"</svg>\n";

		GraphBuilder builder = loader.getBuilder();
		builder.addVertex("Test Node 10");

		builder.addVertex("Test Node 2", singletonMap("image", (Object) value));
		builder.addVertex("Node 3");
		builder.addVertex("Node 4");

		builder.addEdge("Test Node 2", "Test Node 10");
		builder.addEdge("Test Node 2", "Node 3");
		builder.addEdge("Test Node 2", "Node 3", singletonMap("style", (Object) "endFill=true;endArrow=block;editable=false"));
		builder.addEdge("Node 4", "Test Node 10");

		builder.addToGroup("Group A", "Test Node 2");
		builder.addToGroup("Group A", "Node 3");

		if (loader.graphFile == null) loader.buildGraph();
		loader.applyLayout(null);
		loader.applyBackgroundColor();

		final JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setContentPane(loader.graphComponent);
		frame.setBackground(Color.WHITE);
		frame.setSize(500, 500);
		frame.setVisible(true);
	}

	private class CellsAddedListener implements mxEventSource.mxIEventListener {

		private final mxGraph mxGraph;

		CellsAddedListener(mxGraph mxGraph) {
			this.mxGraph = mxGraph;
		}

		double padding, minCellWidth, minCellHeight;

		public void invoke(Object sender, mxEventObject evt) {
			// To avoid timing problems this must be evaluated here

			boolean optimizeSize, forceOptimizeSize = "force".equalsIgnoreCase(valueOf(get(PARAM_AUTO_SIZE)));
			if ("none".equalsIgnoreCase(valueOf(get(PARAM_LAYOUT)))) {
				optimizeSize = forceOptimizeSize || "true".equalsIgnoreCase(valueOf(get(PARAM_AUTO_SIZE)));
			} else {
				optimizeSize = forceOptimizeSize || !"false".equalsIgnoreCase(valueOf(get(PARAM_AUTO_SIZE)));
			}

			padding = containsKey(PARAM_CELL_PADDING) ? parseDouble(valueOf(get(PARAM_CELL_PADDING))) : 2D;
			if (containsKey(PARAM_MIN_SIZE)) {
				String minSize = valueOf(get(PARAM_MIN_SIZE));
				int xIndex = minSize.indexOf('x');
				minCellWidth = xIndex == -1 ? parseDouble(minSize) : xIndex == 0 ? 0 : parseDouble(minSize.substring(0, xIndex));
				minCellHeight = xIndex == -1 ? 0 : parseDouble(minSize.substring(xIndex + 1));
			}

			Object[] cells = (Object[]) evt.getProperty("cells");
			for (Object cell : cells) {
				if (cell instanceof mxCell) {
					mxCell mxCell = (mxCell) cell;

					if (mxCell.isVertex()) {
						if ("".equals(mxCell.getValue()))
							useIdAsLabel(mxCell);
						else if (optimizeSize)
							optimizeCellSize(mxCell, forceOptimizeSize);

						handleMinCellSize(mxCell);
					}

					allCells.put(mxCell.getId(), mxCell);
				}
			}
		}

		private void handleMinCellSize(mxCell mxCell) {
			if (!isImageWithSize(mxCell)) {
				final mxGeometry geometry = mxCell.getGeometry();
				geometry.setWidth(max(minCellWidth, geometry.getWidth()));
				geometry.setHeight(max(minCellHeight, geometry.getHeight()));
			}
		}

		private void useIdAsLabel(mxCell mxCell) {
			mxCell.setValue(mxCell.getId());
			optimizeCellSize(mxCell, true);
		}

		private void optimizeCellSize(mxCell mxCell, boolean force) {
			if (!isImageWithSize(mxCell)) {
				mxGeometry geometry = mxCell.getGeometry();
				if (force || isDefaultCellGeometry(geometry)) {
					mxRectangle sizeForCell = mxGraph.getPreferredSizeForCell(mxCell);
					geometry.setWidth(sizeForCell.getWidth() + 2 * padding);
					geometry.setHeight(sizeForCell.getHeight() + 2 * padding);
				}
			}
		}

		private boolean isDefaultCellGeometry(mxGeometry geometry) {
			return geometry.getWidth() < 1 || geometry.getHeight() < 1;
		}

		private boolean isImageWithSize(mxCell mxCell) {
			String style = valueOf(mxCell.getStyle());
			return style.contains("imageWidth");
		}
	}

	private class GraphComponent extends mxGraphComponent {

		private static final long serialVersionUID = 4782467416580265325L;

		public GraphComponent() {
			super(GraphLoader.this.graph);
		}

		@Override
		protected mxGraphHandler createGraphHandler() {
			return new mxGraphHandler(this) {
				@Override
				protected void installDragGestureHandler() {
					try {
						super.installDragGestureHandler();
					} catch (HeadlessException e) {
						if (log.isDebugEnabled())
							log.debug("Environment is headless, this shouldn't be a problem as the graph component is not interactive.", e);
					}
				}

				@Override
				protected void installDropTargetHandler() {
					try {
						super.installDropTargetHandler();
					} catch (HeadlessException e) {
						if (log.isDebugEnabled())
							log.debug("Environment is headless, this shouldn't be a problem as the graph component is not interactive.", e);
					}
				}
			};
		}

		@Override
		public mxInteractiveCanvas createCanvas() {
			return new mxInteractiveCanvas() {
				{
					graphBuilder.setImageCache(imageCache);
				}
			};
		}

		private HashMap<Object, Object> renderingHints;

		public HashMap<Object, Object> getRenderingHints() {
			if (renderingHints == null) {
				renderingHints = new HashMap<Object, Object>();
				renderingHints.put(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
				renderingHints.put(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
				renderingHints.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				renderingHints.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
				renderingHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
				renderingHints.put(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
				renderingHints.put(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
			}

			return renderingHints;
		}

		@Override
		public void paint(Graphics g) {
			if (g instanceof Graphics2D) ((Graphics2D) g).setRenderingHints(getRenderingHints());

			super.paint(g);
		}
	}
}
