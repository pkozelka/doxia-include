/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.image;

import java.awt.image.BufferedImage;

/**
 * Base for filters that overlay their result over the source image.
 *
 * @author Juergen_Kellerer, 2012-06-02
 */
abstract class OverlayingFilter extends AbstractImageFilter {

	private final OverlayFilter overlayFilter = new OverlayFilter();

	public float getAlpha() {
		return overlayFilter.getAlpha();
	}

	public void setAlpha(float alpha) {
		overlayFilter.setAlpha(alpha);
	}

	public String getRule() {
		return overlayFilter.getRule();
	}

	public void setRule(String ruleName) {
		overlayFilter.setRule(ruleName);
	}

	@Override
	protected BufferedImage doFilter(BufferedImage source, BufferedImage destination) {
		synchronized (overlayFilter) {
			try {
				overlayFilter.setImage(destination);
				return overlayFilter.filter(source, null);
			} finally {
				overlayFilter.setImage(null);
			}
		}
	}
}
