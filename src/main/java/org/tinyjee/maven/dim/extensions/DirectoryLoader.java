/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.codehaus.plexus.util.FileUtils;
import org.tinyjee.maven.dim.spi.ExtensionInformation;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.utils.AbstractAliasHandler;
import org.tinyjee.maven.dim.utils.ClassUtils;
import org.tinyjee.maven.dim.utils.NumberAwareStringComparator;
import org.tinyjee.maven.dim.utils.SelectableArrayList;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * Scans and loads files from the specified directories.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|source=template.vm|source-directories=src/main,src/test}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 * Full call syntax:<code><pre>
 * %{include|source=template.vm|source-class=org.tinyjee.maven.dim.extensions.DirectoryLoader}
 * </pre></code>
 *
 * @author Juergen_Kellerer, 2011-10-09
 */
@ExtensionInformation(displayName = "Directory Loader", documentationUrl = "/extensionsDirectoryLoader.html")
public class DirectoryLoader extends HashMap<String, Object> {

	private static final long serialVersionUID = -3519896231202791446L;

	private static final Map<String, Class<? extends Map<String, Object>>> EXTENSION_TO_LOADER_MAPPING =
			new HashMap<String, Class<? extends Map<String, Object>>>();
	private static final Map<Class, String> FILE_INPUT_PARAM_MAP = new HashMap<Class, String>();

	static {
		EXTENSION_TO_LOADER_MAPPING.put(".xml", XmlLoader.class);
		EXTENSION_TO_LOADER_MAPPING.put(".xsl", XmlLoader.class);
		EXTENSION_TO_LOADER_MAPPING.put(".xdoc", XmlLoader.class);
		EXTENSION_TO_LOADER_MAPPING.put(".xhtml", XmlLoader.class);
		EXTENSION_TO_LOADER_MAPPING.put(".json", XmlLoader.class);

		EXTENSION_TO_LOADER_MAPPING.put(".java", JavaSourceLoader.class);

		EXTENSION_TO_LOADER_MAPPING.put(".properties", PropertiesLoader.class);

		EXTENSION_TO_LOADER_MAPPING.put(".svg", SvgLoader.class);
		EXTENSION_TO_LOADER_MAPPING.put(".svgz", SvgLoader.class);
		EXTENSION_TO_LOADER_MAPPING.put(".svg.gz", SvgLoader.class);

		EXTENSION_TO_LOADER_MAPPING.put(".gif", ImageLoader.class);
		EXTENSION_TO_LOADER_MAPPING.put(".bmp", ImageLoader.class);
		EXTENSION_TO_LOADER_MAPPING.put(".ico", ImageLoader.class);
		EXTENSION_TO_LOADER_MAPPING.put(".wbmp", ImageLoader.class);
		EXTENSION_TO_LOADER_MAPPING.put(".png", ImageLoader.class);
		EXTENSION_TO_LOADER_MAPPING.put(".jpg", ImageLoader.class);

		EXTENSION_TO_LOADER_MAPPING.put(".ods", OpenDocumentSpreadsheetRenderer.class);

		EXTENSION_TO_LOADER_MAPPING.put(".js", ScriptInvoker.class);
		EXTENSION_TO_LOADER_MAPPING.put(".groovy", ScriptInvoker.class);
		EXTENSION_TO_LOADER_MAPPING.put(".py", ScriptInvoker.class);
		EXTENSION_TO_LOADER_MAPPING.put(".rb", ScriptInvoker.class);

		FILE_INPUT_PARAM_MAP.put(XmlLoader.class, XmlLoader.PARAM_XML);
		FILE_INPUT_PARAM_MAP.put(JavaSourceLoader.class, JavaSourceLoader.PARAM_JAVA_SOURCE);
		FILE_INPUT_PARAM_MAP.put(PropertiesLoader.class, PropertiesLoader.PARAM_PROPERTIES);
		FILE_INPUT_PARAM_MAP.put(SvgLoader.class, SvgLoader.PARAM_SVG);
		FILE_INPUT_PARAM_MAP.put(ImageLoader.class, ImageLoader.PARAM_IMAGE);
		FILE_INPUT_PARAM_MAP.put(OpenDocumentSpreadsheetRenderer.class, OpenDocumentSpreadsheetRenderer.PARAM_RENDER);
		FILE_INPUT_PARAM_MAP.put(ScriptInvoker.class, ScriptInvoker.PARAM_SCRIPT);
	}

	/**
	 * Implements the "{@link org.tinyjee.maven.dim.extensions.DirectoryLoader#PARAM_ALIAS}" alias functionality.
	 */
	public static class AliasHandler extends AbstractAliasHandler {
		/**
		 * Constructs the handler (Note: Is called by {@link org.tinyjee.maven.dim.spi.RequestParameterTransformer#TRANSFORMERS}).
		 */
		public AliasHandler() {
			super(PARAM_ALIAS, PARAM_DIRECTORIES, DirectoryLoader.class.getName());
		}
	}

	/**
	 * Defines a shortcut for the request properties 'source-class' and 'directories'.
	 * <p/>
	 * If this parameter is present inside the request parameters list, the system will effectively behave as if
	 * 'source-class' and 'directories' where set separately.
	 */
	public static final String PARAM_ALIAS = "source-directories";

	/**
	 * Sets a comma separated list of URL or local path pointing to directories to load recursively.
	 * <p/>
	 * The specified directories are resolved in exactly the same way as when using the parameter "<code>source</code>"
	 * (see Macro reference).
	 */
	public static final String PARAM_DIRECTORIES = "directories";

	/**
	 * Is set with a selectable list of {@link FileReference} instances of all files that were found in the given directories.
	 * The returned list derives from {@link SelectableArrayList}.
	 * <p/>
	 * Usages:
	 * <p/>
	 * <b>Usage Examples:</b>
	 * <code><pre>
	 * #foreach($file in $files)
	 *    * $file
	 * #end
	 * </pre></code>
	 * <code><pre>
	 * #foreach($xmlFile in $files.selectMatching("(?i).*(\.xml|\.xsl|\.xdoc|\.xhtml|\.json)$"))
	 *    #set($xmlLoader = $xmlFile.loader)
	 * #end
	 * </pre></code>
	 */
	public static final String PARAM_OUT_FILES = "files";

	public DirectoryLoader(Map<String, Object> requestParams) {
		putAll(requestParams);
		final SelectableArrayList<FileReference> files = new SelectableArrayList<FileReference>(128);
		put(PARAM_OUT_FILES, files);

		final String directories = String.valueOf(get(PARAM_DIRECTORIES));
		for (String directory : directories.split("\\s*,\\s*")) {
			final File basePath = Globals.getInstance().resolveLocalPath(directory);
			collectFiles(basePath, files);
		}

		Collections.sort(files);
	}

	@SuppressWarnings("unchecked")
	public SelectableArrayList<FileReference> getFiles() {
		return (SelectableArrayList) get(PARAM_OUT_FILES);
	}

	private void collectFiles(File basePath, SelectableArrayList<FileReference> targetFiles) {
		final File[] files = basePath.listFiles();
		if (files != null) {
			for (File file : files) {
				if (file.isDirectory())
					collectFiles(file, targetFiles);
				else
					targetFiles.add(new FileReferenceImplementation(file));
			}
		}
	}

	private static final Comparator<String> filePathComparator = new NumberAwareStringComparator();

	private class FileReferenceImplementation implements FileReference {

		final File file;
		final String absolutePath;

		private FileReferenceImplementation(File file) {
			this.file = file;
			absolutePath = file.getAbsolutePath();
		}

		public File getFile() {
			return file;
		}

		public String getFilePath() {
			return file.getPath(); // TODO: Make sure it is relative!
		}

		public URL getFileUrl() {
			try {
				return file.toURI().toURL();
			} catch (MalformedURLException e) {
				throw new RuntimeException(e);
			}
		}

		public String getText() {
			try {
				return Globals.getInstance().fetchUrlText(getFileUrl());
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		public List<String> getTextLines() {
			return Arrays.asList(getText().split("(\r\n|\r|\n)"));
		}

		public String getText(String charset) {
			try {
				return FileUtils.fileRead(file, charset);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		public List<String> getTextLines(String charset) {
			return Arrays.asList(getText(charset).split("(\r\n|\r|\n)"));
		}

		public byte[] getBytes() {
			try {
				return Globals.getInstance().fetchUrl(getFileUrl());
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		public Map<String, Object> getLoader() {
			return getLoader(new HashMap<String, Object>());
		}

		@SuppressWarnings("unchecked")
		public Map<String, Object> getLoader(Map<String, Object> properties) {
			String extension = FileUtils.extension(file.getName()).toLowerCase();
			final Class<? extends Map<String, Object>> loaderClass = EXTENSION_TO_LOADER_MAPPING.get(extension);
			if (loaderClass != null) {
				String inputParamKey = FILE_INPUT_PARAM_MAP.get(loaderClass);
				properties.put(inputParamKey, getFileUrl());
				return (Map) ClassUtils.newInstance(loaderClass, Globals.getInstance().getBasedir(), properties);
			}
			return null;
		}

		public int compareTo(FileReference o) {
			String otherPath = o instanceof FileReferenceImplementation ?
					((FileReferenceImplementation) o).absolutePath : o.getFile().getAbsolutePath();
			return filePathComparator.compare(absolutePath, otherPath);
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (!(o instanceof FileReferenceImplementation)) return false;
			FileReferenceImplementation that = (FileReferenceImplementation) o;
			return absolutePath.equals(that.absolutePath);
		}

		@Override
		public int hashCode() {
			return absolutePath.hashCode();
		}

		@Override
		public String toString() {
			return absolutePath; // Must be a clean path, to ensure SelectableArrayList works correctly.
		}
	}
}
