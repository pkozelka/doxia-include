/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import java.util.Map;

/**
 * Defines the invocation interface to enable pluging an executor into {@link ScriptInvoker}.
 */
public interface ScriptExecutor {
	/**
	 * Constant identifying the global variable that is used inside the script context to allow accessing the script engine.
	 */
	String SCRIPT_ENGINE = "scriptEngine";

	String REQUEST = "request";
	String GLOBALS = "globals";
	String SCRIPT_NAME = "scriptName";
	String SCRIPT_PATH = "scriptPath";
	String SCRIPT_FILE = "scriptFile";
	String SCRIPT_URI = "scriptUri";

	/**
	 * Executes the given script.
	 *
	 * @param context         the script context (= global variables).
	 * @param scriptExtension the file extension of the script.
	 * @param scriptContent   the script to execute.
	 * @param scriptName      the absolute path to the script file.
	 * @throws Exception in case of the script execution failed in any way.
	 */
	void execute(Map<String, Object> context, String scriptExtension, String scriptContent, String scriptName) throws Exception;

	/**
	 * Returns the script context of the currently executed script, 'null' if no script is executed.
	 *
	 * @return the script context of the currently executed script, 'null' if no script is executed.
	 */
	Map<String, Object> getScriptContext();
}
