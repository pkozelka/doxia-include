/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.image;

import com.jhlabs.image.ApplyMaskFilter;
import com.jhlabs.image.InvertAlphaFilter;
import com.jhlabs.image.InvertFilter;
import org.apache.commons.codec.binary.Hex;
import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.UrlFetcher;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Pattern;

import static java.lang.Math.min;

/**
 * Collection of image related utilities used inside extensions that deal with image processing.
 *
 * @author Juergen_Kellerer, 2012-05-25
 */
public final class ImageUtil {

	private static final Log log = Globals.getLog();

	private ImageUtil() {
	}

	/**
	 * Shrinks the image by removing all surrounding background pixels.
	 *
	 * @param image           the image to shrink.
	 * @param backgroundPixel an optional pixel color array specifying the background pixel.
	 *                        If not set the implementation assumes the lower right pixel is a background pixel.
	 * @return a smaller image that was shrinked to the actual content or the same image if no shrinking can be applied.
	 */
	public static BufferedImage shrinkByBackgroundColor(BufferedImage image, int[] backgroundPixel) {

		final WritableRaster raster = image.getRaster();

		final int height = image.getHeight(), width = image.getWidth();
		if (backgroundPixel == null) backgroundPixel = raster.getPixel(width - 1, height - 1, (int[]) null);

		int leftMargin = width, rightMargin = 0, topMargin = height, bottomMargin = 0;

		final int[] pixels = new int[width * backgroundPixel.length];
		for (int y = 0; y < height; y++) {
			raster.getPixels(0, y, width, 1, pixels);
			for (int pixelIndex = 0; pixelIndex < pixels.length; pixelIndex += backgroundPixel.length) {
				boolean emptyPixel = true;
				for (int i = 0; i < backgroundPixel.length; i++)
					if (backgroundPixel[i] != pixels[pixelIndex + i]) emptyPixel = false;

				if (!emptyPixel) {
					int x = pixelIndex / backgroundPixel.length;
					if (x < leftMargin) leftMargin = x;
					if (x > rightMargin) rightMargin = x;
					if (y < topMargin) topMargin = y;
					if (y > bottomMargin) bottomMargin = y;
				}
			}
		}

		if (leftMargin == 0 && topMargin == 0 && rightMargin + 1 >= width && bottomMargin + 1 >= height)
			return image;
		return image.getSubimage(leftMargin, topMargin, min(width, (rightMargin - leftMargin) + 1), min(height, (bottomMargin - topMargin) + 1));
	}

	/**
	 * Scales the given image using BICUBIC interpolation.
	 *
	 * @param image               the image to scale.
	 * @param maxWidth            an optional max width.
	 * @param maxHeight           an optional max height.
	 * @param maintainAspectRatio toggles whether the aspect ratio is maintained when scaling.
	 * @return a new scaled instance of the given image.
	 */
	public static BufferedImage scale(BufferedImage image, Integer maxWidth, Integer maxHeight, boolean maintainAspectRatio) {
		return scale(image, maxWidth, maxHeight, maintainAspectRatio, null);
	}


	/**
	 * Scales the given image using BICUBIC interpolation.
	 *
	 * @param image                      the image to scale.
	 * @param maxWidth                   an optional max width.
	 * @param maxHeight                  an optional max height.
	 * @param maintainAspectRatio        toggles whether the aspect ratio is maintained when scaling.
	 * @param interpolationRenderingHint Sets the rendering hint for the interpolation that is used.
	 * @return a new scaled instance of the given image.
	 */
	public static BufferedImage scale(BufferedImage image, Integer maxWidth, Integer maxHeight, boolean maintainAspectRatio,
	                                  Object interpolationRenderingHint) {
		int maxW = maxWidth == null ? (maxHeight == null ? image.getWidth() : Integer.MAX_VALUE) : maxWidth;
		int maxH = maxHeight == null ? (maxWidth == null ? image.getHeight() : Integer.MAX_VALUE) : maxHeight;
		maintainAspectRatio |= (maxHeight == null || maxWidth == null);

		int scaledW = maxW, scaledH = maxH;
		if (maintainAspectRatio) {
			double aspectRatio = (double) image.getWidth() / (double) image.getHeight();
			int testW = (int) Math.round(aspectRatio * (double) scaledH);
			if (testW <= scaledW)
				scaledW = testW;
			else
				scaledH = (int) Math.round((double) scaledW / aspectRatio);
		}

		if (scaledW == image.getWidth() && scaledH == image.getHeight())
			return image;

		BufferedImage scaledInstance = new BufferedImage(scaledW, scaledH, image.getType());

		Graphics2D graphics = scaledInstance.createGraphics();
		try {
			if (interpolationRenderingHint == null) interpolationRenderingHint = RenderingHints.VALUE_INTERPOLATION_BICUBIC;
			graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, interpolationRenderingHint);
			graphics.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
			graphics.scale(scaledW / (double) image.getWidth(), scaledH / (double) image.getHeight());
			graphics.drawImage(image, 0, 0, null);
		} finally {
			graphics.dispose();
		}

		return scaledInstance;
	}

	/**
	 * Creates the image file content inside a byte-array.
	 *
	 * @param image  the image to convert.
	 * @param format the format to create. e.g.: "png", "jpg".
	 * @return the image file content inside a byte-array.
	 */
	public static byte[] createImageFileContent(RenderedImage image, String format) {
		final ByteArrayOutputStream buffer = new ByteArrayOutputStream(64 * 1024);
		try {
			if (("jpg".equalsIgnoreCase(format) || "jpeg".equalsIgnoreCase(format)) && image.getColorModel().hasAlpha()) {
				if (log.isDebugEnabled()) {
					log.debug("Detected an alpha channel on the attempt to create a JPEG image, rendering the given image with " +
							"WHITE background color as JPEG has no alpha support.");
				}

				BufferedImage nonAlpha = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
				final Graphics2D graphics = nonAlpha.createGraphics();
				graphics.setPaint(Color.WHITE);
				graphics.fillRect(0, 0, image.getWidth(), image.getHeight());
				graphics.drawRenderedImage(image, new AffineTransform());
				graphics.dispose();
				image = nonAlpha;
			}

			ImageIO.write(image, format, buffer);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return buffer.toByteArray();
	}

	/**
	 * Loads the image from the specified URL.
	 *
	 * @param imageUrl a local or remote URL pointing to the image to load.
	 * @return the loaded image.
	 */

	public static BufferedImage loadImage(URL imageUrl) {
		try {
			InputStream source = UrlFetcher.getSource(imageUrl);
			try {
				BufferedImage image = ImageIO.read(source);

				if (image == null) {
					throw new RuntimeException("No decoder was found within " + Arrays.toString(ImageIO.getReaderFormatNames()) +
							" that can load the image " + imageUrl);
				}

				return image;
			} finally {
				source.close();
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Loads an image form the specified file content bytes.
	 *
	 * @param imageFileContent the image file content inside a byte-array
	 * @return the loaded image.
	 */
	public static BufferedImage loadImage(byte[] imageFileContent) {
		try {
			BufferedImage image = ImageIO.read(new ByteArrayInputStream(imageFileContent));

			if (image == null) {
				byte[] imageHeader = new byte[256];
				System.arraycopy(imageFileContent, 0, imageHeader, 0, Math.min(imageFileContent.length, imageHeader.length));
				throw new RuntimeException("No decoder was found within " + Arrays.toString(ImageIO.getReaderFormatNames()) +
						" that can load the image content:\n" + new String(Hex.encodeHex(imageHeader)));
			}

			return image;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Converts the given string value to a {@link Paint}.
	 *
	 * @param value the value to convert, e.g HTML color like '#ffffff'.
	 * @return a paint.
	 */
	public static Paint convertToPaint(String value) {
		if (value == null || "transparent".equalsIgnoreCase(value) || "none".equalsIgnoreCase(value)) return null;

		Paint paint = null;

		if (value.startsWith("rgb")) {
			Scanner scanner = new Scanner(value).useDelimiter(Pattern.compile("[^0-9]+"));
			paint = new Color(scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.hasNext() ? scanner.nextInt() : 255);

		} else if (value.startsWith("#") || value.startsWith("0x")) {
			if (value.length() == 4) value = value.replaceAll("([^#])", "$1$1");
			paint = Color.decode(value);
		}

		if (paint == null) {
			try {
				paint = (Paint) Color.class.getField(value).get(null);
			} catch (Exception ignored) {
			}
		}

		return paint;
	}

	public static BufferedImage invert(BufferedImage image) {
		return new InvertFilter().filter(image, null);
	}

	public static BufferedImage invertAlpha(BufferedImage image) {
		return new InvertAlphaFilter().filter(image, null);
	}

	public static BufferedImage applyMask(BufferedImage image, BufferedImage mask) {
		final ApplyMaskFilter maskFilter = new ApplyMaskFilter();
		BufferedImage destination = maskFilter.createCompatibleDestImage(image, null);
		maskFilter.setDestination(destination);
		maskFilter.setMaskImage(mask);
		return maskFilter.filter(image, destination);
	}

	public static BufferedImage extractMask(BufferedImage image) {
		final BufferedImage mask = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);


		int[] pixels = null;
		final WritableRaster raster = image.getAlphaRaster(), maskRaster = mask.getAlphaRaster();

		for (int y = 0, width = raster.getWidth(), height = raster.getHeight(); y < height; y++) {
			pixels = raster.getPixels(0, y, width, 1, pixels);
			maskRaster.setPixels(0, y, width, 1, pixels);
		}

		return image;
	}
}
