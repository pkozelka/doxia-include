/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.apache.commons.codec.binary.Hex;
import org.apache.maven.doxia.logging.Log;
import org.codehaus.plexus.util.FileUtils;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.RequestParameterTransformer;
import org.tinyjee.maven.dim.utils.ReducedVerbosityMap;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static java.lang.String.valueOf;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_SOURCE;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_SOURCE_CLASS;

/**
 * Implements a base for bundled request parameter transformers.
 *
 * @author Juergen_Kellerer, 2011-11-08
 */
public abstract class AbstractParameterTransformer implements RequestParameterTransformer {

	protected abstract boolean doTransformParameters(Map<String, Object> requestParams);

	protected boolean isSourceSet(Map<String, Object> requestParams) {
		return requestParams.containsKey(PARAM_SOURCE) && requestParams.containsKey(PARAM_SOURCE_CLASS);
	}

	public final void transformParameters(Map<String, Object> requestParams) {
		final Log log = Globals.getLog();
		if (!isSourceSet(requestParams)) {
			final Map<String, Object> originalParams = new HashMap<String, Object>(requestParams);
			if (doTransformParameters(requestParams)) {
				if (log.isDebugEnabled()) {
					log.debug("The parameter transformer " + this + " changed the request parameters from " +
							reduceVerbosity(originalParams) + " to " + reduceVerbosity(requestParams));
				}
			} else if (!originalParams.equals(requestParams)) {
				log.warn("The parameter transformer " + this + " changed the request parameters from " + originalParams +
						" to " + requestParams + " without reporting it.");
			}
		}
	}

	private static Map<String, Object> reduceVerbosity(Map<String, Object> originalParams) {
		originalParams = new ReducedVerbosityMap<String, Object>(originalParams);
		originalParams.remove("sourceContent");
		return originalParams;
	}

	protected static String makeUniqueName(Object namePrefix, Object... expressions) {
		MessageDigest md5;
		try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}

		for (int i = 0; i < expressions.length; i++)
			expressions[i] = valueOf(expressions[i]);

		Arrays.sort(expressions);

		for (Object expression : expressions)
			md5.update(valueOf(expression).getBytes());

		String name = namePrefix == null ? "" : valueOf(namePrefix);
		String ext = FileUtils.extension(name);
		name = name.substring(0, name.length() - ext.length() - 1) + '-' + new String(Hex.encodeHex(md5.digest())) + '.' + ext;

		return name;
	}
}
