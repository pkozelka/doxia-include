/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.script;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;

/**
 * Is a special class loader that allows to resolve resources that are located within the thread-locally specified context path.
 * <p/>
 * This is used as a tweak to overcome class-loading issues inside scripting languages on JSR-223, this is namely to support
 * jython's import statements on modules that are located within the same folder as the script that is currently running.
 *
 * @author juergen_kellerer, 2012-06-06
 */
public class ScriptContextAwareResourceLoader extends ClassLoader {

	private final ThreadLocal<File[]> scriptContextPath = new ThreadLocal<File[]>();

	/**
	 * Constructs a new context resource loader using the specified parent to load the real java classes.
	 *
	 * @param parent the parent that loads all java related classes.
	 */
	public ScriptContextAwareResourceLoader(ClassLoader parent) {
		super(parent);
	}

	/**
	 * Gets the thread local context paths that are used to search for resources.
	 *
	 * @return the thread local context paths that are used to search for resources.
	 */
	public File[] getScriptContextPath() {
		return scriptContextPath.get();
	}

	/**
	 * Sets the thread local context paths that are used to search for resources.
	 *
	 * @param value the thread local context paths that are used to search for resources.
	 */
	public void setScriptContextPath(File... value) {
		scriptContextPath.set(value);
	}

	@Override
	protected URL findResource(String name) {
		final File[] files = scriptContextPath.get();
		if (files != null) {
			for (File file : files) {
				final File resource = new File(file, name);
				if (resource.exists()) {
					try {
						return resource.toURI().toURL();
					} catch (MalformedURLException e) {
						throw new RuntimeException(e);
					}
				}
			}
		}
		return null;
	}

	@Override
	protected Enumeration<URL> findResources(String name) throws IOException {
		final URL url = findResource(name);
		return url == null ? super.findResources(name) : Collections.enumeration(Arrays.asList(url));
	}
}
