/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import java.util.Map;

import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_SOURCE;

/**
 * This extension adds an inline Script definition to the current page.
 * Use this extension to define simple scripts or execute pre-formatted blocks containing client side scripts (javascript).
 *
 * @author Juergen_Kellerer, 2012-01-18
 */
public class InlineScriptParameterTransformer extends AbstractParameterTransformer {

	/**
	 * Enables this extension and sets the content to place inside the inlined "&lt;script&gt;" element.
	 * <p/>
	 * <b>Note:</b> Setting this property has no effect in case of "<code>source</code>" or "<code>source-class</code>" were
	 * specified with the macro call. This behaviour ensures that the parameter "<code>inline-script</code>" can still be used
	 * with ordinary macro calls where a source or source-class was set.
	 * <p/>
	 * Using this parameter has no effect when the used Sink does not support HTML markup.
	 * <p/>
	 * This parameter supports the usage of "{@code classpath:/path/to/script/to/inline}" to load the script from the classpath
	 * instead of defining it with the macro call. Furthermore using a value of "." transforms the following verbatim text
	 * section into a script block that is executed on the client side instead of being displayed.
	 * <p/>
	 * Language constructs that require the braces "{}" are not supported from APT documents when adding the script content
	 * to the macro call.
	 * This is a limitation of the APT parser, however you can still use '.' and paste the script code into a verbatim text block
	 * following the macro call.
	 * <p/>
	 * Examples:<ul>
	 * <li>"<code>%{include|inline-script=alert("hello world.")}</code>"</li>
	 * <li>"<code>%{include|inline-script=classpath:/path/to/my-script.js}</code>"</li>
	 * <li><code><pre>
	 * Sample Page with APT
	 *
	 * %{include|inline-script=.}
	 *
	 * ---------
	 * function sayHello() {
	 *     alert("hello world.");
	 * }
	 * sayHello();
	 * ---------
	 * </pre></code></li>
	 * </ul>
	 */
	public static final String PARAM_INLINE_SCRIPT = "inline-script";

	/**
	 * Defines the language of the script to include, defaults to "javascript".
	 */
	public static final String PARAM_SCRIPT_TYPE = "type";

	@Override
	protected boolean doTransformParameters(Map<String, Object> requestParams) {

		String inlineScript = (String) requestParams.get(PARAM_INLINE_SCRIPT);
		if (inlineScript != null) {
			if (!requestParams.containsKey(PARAM_SCRIPT_TYPE))
				requestParams.put(PARAM_SCRIPT_TYPE, "text/javascript");

			requestParams.put(PARAM_SOURCE, "classpath:/templates/dim/inline-script.xdoc.vm");

			return true;
		}
		return false;
	}
}
