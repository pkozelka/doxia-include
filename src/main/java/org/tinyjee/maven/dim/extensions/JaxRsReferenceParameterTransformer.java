/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.tinyjee.maven.dim.spi.ExtensionInformation;

import java.util.Map;

import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_SOURCE;

/**
 * Renders a reference documentation panel for JAX-RS services.
 * <p/>
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|jaxrs-reference=my.rest.services..|application-path=/my-webapp/my-service}
 * %{include|jaxrs-reference=my.RestServiceClass|application-path=/my-webapp/my-service}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 *
 * @author Juergen_Kellerer, 2012-07-29
 */
@ExtensionInformation(displayName = "JAX-RS Reference", documentationUrl = "/templatesJaxRsReference.html")
public class JaxRsReferenceParameterTransformer extends AbstractClassSelectionParameterTransformer {

	/**
	 * Enables this extension and renders documentation panels on one or more package or class names referencing JAX-RS implementation classes.
	 * <p/>
	 * Possible values:<ul>
	 * <li>{@code jaxrs-reference=my.package..}: Includes every JAX-RS service below "my.package".</li>
	 * <li>{@code jaxrs-reference=my.package}: Includes every JAX-RS service in "my.package".</li>
	 * <li>{@code jaxrs-reference=my.packageA,my.packageB}: Includes every JAX-RS service in "my.packageA" and "my.packageB".</li>
	 * <li>{@code jaxrs-reference=derived:my.ParentClass}: Includes every JAX-RS service deriving from "my.ParentClass".</li>
	 * <li>{@code jaxrs-reference=annotated:my.Annotation}: Includes every JAX-RS service annotated with "my.Annotation".</li>
	 * <li>{@code jaxrs-reference=my.Service}: Includes a single JAX-RS service.</li>
	 * </ul>
	 */
	public static final String PARAM_JAX_RS_REFERENCE = "jaxrs-reference";

	/**
	 * Specifies the context path that is used to register the JAX-RS application (e.g. "/my-webapp/my-service").
	 */
	public static final String PARAM_APPLICATION_PATH = "application-path";

	/**
	 * Specifies the path to the generated api docs.
	 * Set it to 'false' if no apidocs link should be generated.
	 * (Default: "./apidocs/")
	 */
	public static final String PARAM_APIDOCS = "apidocs";

	/**
	 * Toggles whether a REST client (HTML form based) is created or not.
	 * (Default: "false")
	 */
	public static final String PARAM_NO_CLIENT = "no-client";

	/**
	 * Specifies the format to use for creating the documentation.
	 * Possible values:<ul>
	 * <li>{@code panel}: Renders a documentation panel with a HTML form based REST client.</li>
	 * <li>{@code document}: Renders a printable documentation which may be embedded in a parent document.</li>
	 * </ul>
	 * (Default: "panel")
	 */
	public static final String PARAM_FORMAT = "format";

	private static final String PARAM_CLASSES = "classes";

	@Override
	protected boolean doTransformParameters(Map<String, Object> requestParams) {
		Object classes = requestParams.get(PARAM_JAX_RS_REFERENCE);
		if (classes != null) {
			if ("document".equals(requestParams.get(PARAM_FORMAT)))
				requestParams.put(PARAM_SOURCE, "classpath:/templates/dim/jaxrs-reference-document.xdoc.vm");
			else
				requestParams.put(PARAM_SOURCE, "classpath:/templates/dim/jaxrs-reference.xdoc.vm");
			requestParams.put(PARAM_CLASSES, classes);

			return true;
		}
		return false;
	}
}
