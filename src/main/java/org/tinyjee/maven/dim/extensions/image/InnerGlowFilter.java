/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.image;

import com.jhlabs.image.*;

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 * Draws an inner glow of a specified color and smoothness.
 *
 * @author Juergen_Kellerer, 2012-06-01
 */
public class InnerGlowFilter extends OverlayingFilter {

	int size = 6;
	float smoothness = 4f;
	String color = "yellow";

	float centerX = 1f, centerY = 1f;

	{
		setAlpha(0.33f);
		setRule("Normal");
	}

	@Override
	public Rectangle2D getBounds2D(BufferedImage src) {
		final Rectangle2D bounds2D = super.getBounds2D(src);
		/*final float extraPixels = size * smoothness;
		bounds2D.add(-extraPixels, -extraPixels);
		bounds2D.add(bounds2D.getWidth() + extraPixels, bounds2D.getHeight() + extraPixels);*/
		return bounds2D;
	}

	@Override
	protected BufferedImage doFilter(BufferedImage source, BufferedImage destination) {
		final int extraPixels = (int) (size * smoothness);
		final int borderSize = size;// + extraPixels;

		BufferedImage maskImage = new BufferedImage(source.getWidth(), source.getHeight(), BufferedImage.TYPE_INT_ARGB);
		maskImage.createGraphics().fillRect(borderSize, borderSize, maskImage.getWidth() - (2 * borderSize), maskImage.getHeight() - (2 * borderSize));
		GaussianFilter gaussianFilter = new GaussianFilter();
		gaussianFilter.setRadius(smoothness * size);
		gaussianFilter.setUseAlpha(true);

		maskImage = gaussianFilter.filter(maskImage, null);

		InvertFilter invertFilter = new InvertFilter();
		maskImage = invertFilter.filter(maskImage, null);

		//BorderFilter borderFilter = new BorderFilter(borderSize, borderSize, extraPixels, extraPixels, ImageUtil.convertToPaint(color));

		ClearFilter clearFilter = new ClearFilter();
		clearFilter.setColor(color);
		//clearFilter.setTransparent(true);

		destination = clearFilter.doFilter(destination, destination);
		ApplyMaskFilter maskFilter = new ApplyMaskFilter();
		maskFilter.setMaskImage(maskImage);
		maskFilter.setDestination(destination);
		destination = maskFilter.filter(destination, null);

		return destination;//super.doFilter(source, destination);
	}
}
