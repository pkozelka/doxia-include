/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.image;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

/**
 * Creates an empty transparent image (optionally filled with a background color).
 *
 * @author Juergen_Kellerer, 2012-06-01
 */
public class ClearFilter extends AbstractImageFilter {

	/**
	 * The background color, may be color name or a hexadecimal color value like "#ffffff".
	 * Set to create a colored image when applying the filter.
	 */
	String color;

	/**
	 * Force creation of a transparent image even when an opaque color is set.
	 */
	boolean transparent;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public boolean isTransparent() {
		return transparent;
	}

	public void setTransparent(boolean transparent) {
		this.transparent = transparent;
	}

	@Override
	protected BufferedImage doFilter(BufferedImage source, BufferedImage destination) {
		if (color != null) {
			final Graphics2D graphics = destination.createGraphics();
			try {
				graphics.setPaint(ImageUtil.convertToPaint(color));
				graphics.fillRect(0, 0, destination.getWidth(), destination.getHeight());
			} finally {
				graphics.dispose();
			}

			if (transparent) {
				final WritableRaster raster = destination.getAlphaRaster();
				int[] pixels = new int[raster.getWidth()];

				for (int y = 0, height = raster.getHeight(); y < height; y++)
					raster.setPixels(0, y, pixels.length, 1, pixels);
			}
		}

		return destination;
	}

	@Override
	public String toString() {
		return "ClearFilter{" +
				"background='" + color + '\'' +
				'}';
	}
}
