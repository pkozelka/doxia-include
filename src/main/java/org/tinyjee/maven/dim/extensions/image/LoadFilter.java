/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.image;

import org.tinyjee.maven.dim.spi.Globals;

import java.awt.image.BufferedImage;

/**
 * Loads the specified image and return it.
 *
 * @author Juergen_Kellerer, 2012-06-02
 */
public class LoadFilter extends NullFilter {

	String path;
	BufferedImage image;
	boolean replace;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public boolean isReplace() {
		return replace;
	}

	public void setReplace(boolean replace) {
		this.replace = replace;
	}

	public BufferedImage getImage() {
		return image;
	}

	@Override
	protected BufferedImage doFilter(BufferedImage source, BufferedImage destination) {
		image = ImageUtil.loadImage(Globals.getInstance().resolvePath(path));
		return replace ? image : source;
	}

	@Override
	public String toString() {
		return "LoadFilter{" +
				"path='" + path + '\'' +
				", image=" + image +
				", replace=" + replace +
				'}';
	}
}
