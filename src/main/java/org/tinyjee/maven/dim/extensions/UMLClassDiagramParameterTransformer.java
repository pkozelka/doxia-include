/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.tinyjee.maven.dim.IncludeMacroSignature;
import org.tinyjee.maven.dim.spi.ExtensionInformation;

import java.util.Map;

/**
 * Generates an UML class diagram of the specified selection of classes and packages.
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|uml-class-diagram=my.package.MyClass}
 * %{include|uml-class-diagram=my.package}
 * %{include|uml-class-diagram=my.package1, my.package2}
 * %{include|uml-class-diagram=annotated:@XmlType}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 *
 * @author Juergen_Kellerer, 2012-07-08
 */
@ExtensionInformation(displayName = "UML Class Diagram", documentationUrl = "/templatesClassDiagram.html")
public class UMLClassDiagramParameterTransformer extends AbstractClassSelectionParameterTransformer {

	/**
	 * Is a comma separated list of fully qualified class or package names to display inside the class diagram.
	 * Possible values:<ul>
	 * <li>"package.prefix" <i>= all in this package</i></li>
	 * <li>"package.prefix.." <i>= all classes and packages below this package</i></li>
	 * <li>"package.prefix1, package.prefix2, package.prefix3"</li>
	 * <li>"my.package.MyClass"</li>
	 * <li>"my.package.MyClass1, my.package.MyClass2"</li>
	 * <li>"derived:my.package.MySuperClassOrInterface"</li>
	 * <li>"annotated:my.package.MyAnnotation"</li>
	 * </ul>
	 */
	public static final String PARAM_UML_CLASS_DIAGRAM = "uml-class-diagram";

	/**
	 * Specifies whether related classes are automatically added to the class diagram.
	 * Possible values:<ul>
	 * <li>"false" <i>(default, if more than one class was specified)</i></li>
	 * <li>"true" <i>(default, if a single class was specified)</i></li>
	 * <li>"package.prefix" <i>= all in exactly this package</i></li>
	 * <li>"package.prefix.." <i>= all below this package</i></li>
	 * <li>"package.prefix1, package.prefix2, package.prefix3"</li>
	 * </ul>
	 */
	public static final String PARAM_ADD_RELATED = "add-related";

	/**
	 * Invert the edge direction for derived edges (implements, extends) inside the graph that backs the diagram.
	 * <p/>
	 * When "true", implements &amp; extends arrows follow the opposite direction than composite arrows.
	 * This makes class diagrams usually easier to understand as it results in a layout of classes that follows the
	 * "general-to-specific" rule.
	 * Setting this option to "true" does not create invalid UML diagrams it only influences how the classes are positioned.
	 * <p/>
	 * (Default: true)
	 */
	public static final String PARAM_INVERT_DERIVED = "invert-derived";

	/**
	 * Toggles whether the package is shown as package vertex in the class diagram.
	 * (Default: "false")
	 */
	public static final String PARAM_SHOW_PACKAGE = "show-package";

	/**
	 * Toggles whether the classes are grouped by package.
	 */
	public static final String PARAM_GROUP_BY_PACKAGE = "group-by-package";

	/**
	 * Specifies a regular expression that matches classes which should be displayed without any details.
	 * Matches classes will only display the class name, which is the same as if all the {@code hide-*} options were enabled.
	 */
	public static final String PARAM_COLLAPSED = "collapsed";

	/**
	 * Specifies the layout of the class diagram. This can be anything that is supported by {@link GraphLoader},
	 * however in most cases only "hierarchical-vertical" and "hierarchical" are useful layout engines for class diagrams.
	 * (Default: "hierarchical")
	 */
	public static final String PARAM_LAYOUT = "layout";

	/**
	 * Specifies whether "navigable" edges should be shown. Navigable edges are constructed from return values of visible
	 * methods ('visible' means access level visibility).
	 */
	public static final String PARAM_SHOW_NAVIGABLE = "show-navigable";

	/**
	 * Specifies whether "composite" edges should be hidden.
	 */
	public static final String PARAM_HIDE_COMPOSITES = "hide-composites";

	/**
	 * Specifies whether collections are always hidden inside the class diagram.
	 * <br/>
	 * By default collections are only hidden when excluded (which is the case for all standard java implementations).
	 * Hiding a collection means that the man-in-the-middle (= collection class) is removed.
	 */
	public static final String PARAM_HIDE_COLLECTIONS = "hide-collections";

	/**
	 * Specifies whether "implements" edges should be hidden.
	 */
	public static final String PARAM_HIDE_IMPLEMENTS = "hide-implements";

	/**
	 * Specifies whether "extends" edges should be hidden.
	 */
	public static final String PARAM_HIDE_EXTENDS = "hide-extends";

	/**
	 * Specifies whether {@code <<creates>>} edges should be hidden.
	 */
	public static final String PARAM_HIDE_CREATES = "hide-creates";

	/**
	 * Specifies whether all classes that are created are shown in the graph (when {@code hide-creates} is "false").
	 * <br/>
	 * By default only those created classes are added that are also in composites,implements or extends.
	 */
	public static final String PARAM_ADD_ALL_CREATES = "add-all-creates";

	private static final String INPUT_PARAM_CLASSES = "classes";

	@Override
	protected boolean doTransformParameters(Map<String, Object> requestParams) {
		Object input = requestParams.get(PARAM_UML_CLASS_DIAGRAM);
		if (input != null) {
			requestParams.put(INPUT_PARAM_CLASSES, input);
			requestParams.put(IncludeMacroSignature.PARAM_SOURCE, "classpath:/templates/dim/uml-class-diagram.xdoc.vm");


			String name = makeUniqueName("uml-class-diagram.svg",
					requestParams.get(PARAM_UML_CLASS_DIAGRAM),
					requestParams.get(PARAM_ADD_RELATED),
					requestParams.get(PARAM_INVERT_DERIVED),
					requestParams.get(PARAM_SHOW_PACKAGE),
					requestParams.get(PARAM_GROUP_BY_PACKAGE),
					requestParams.get(PARAM_INCLUDES),
					requestParams.get(PARAM_EXCLUDES),
					requestParams.get(PARAM_DEFAULT_EXCLUDES),
					requestParams.get(PARAM_COLLAPSED),
					requestParams.get(PARAM_SHOW_NAVIGABLE),
					requestParams.get(PARAM_HIDE_COMPOSITES),
					requestParams.get(PARAM_HIDE_COLLECTIONS),
					requestParams.get(PARAM_HIDE_IMPLEMENTS),
					requestParams.get(PARAM_HIDE_EXTENDS),
					requestParams.get(PARAM_HIDE_CREATES),
					requestParams.get(PARAM_ADD_ALL_CREATES),
					requestParams.get(PARAM_LAYOUT)
			);
			requestParams.put(GraphLoader.PARAM_ALIAS, "create:" + name);

			return true;
		}
		return false;
	}
}
