/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions.utils;

import org.tinyjee.maven.dim.utils.AbstractSelectableJavaEntitiesList;

import java.util.Collection;
import java.util.Map;

/**
 * Implements {@link AbstractSelectableJavaEntitiesList} for {@link org.tinyjee.maven.dim.extensions.JavaSourceLoader}.
 * <p/>
 * See {@link AbstractSelectableJavaEntitiesList} for more information how to use this list type.
 *
 * @author Juergen_Kellerer, 2011-10-10
 */
public class JavaEntitiesList extends AbstractSelectableJavaEntitiesList<Map<?, ?>> {

	private static final long serialVersionUID = -1483709242125352523L;

	private final String qdoxEntityKey;

	public JavaEntitiesList(Collection<? extends Map<?, ?>> maps, String qdoxEntityKey) {
		super(maps);
		this.qdoxEntityKey = qdoxEntityKey;
	}

	public JavaEntitiesList(String qdoxEntityKey) {
		this.qdoxEntityKey = qdoxEntityKey;
	}

	@Override
	protected Object unwrap(Map<?, ?> element) {
		return element.get(qdoxEntityKey);
	}

	@Override
	@SuppressWarnings("unchecked")
	public JavaEntitiesList clone() {
		return (JavaEntitiesList) super.clone();
	}
}
