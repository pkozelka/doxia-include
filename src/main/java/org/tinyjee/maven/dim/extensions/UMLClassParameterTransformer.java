/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.extensions;

import org.tinyjee.maven.dim.spi.ExtensionInformation;

import java.util.Map;

/**
 * Generates an UML class of the specified java class and adds it to the document either as SVG or rendered as image (PNG).
 * <p/>
 * <b>Usage:</b><!-- START SNIPPET:usage -->
 * <code><pre>
 * %{include|uml-class=my.package.MyClass}
 * %{include|uml-class=my.package.MyClass|render=png}
 * </pre></code>
 * <!-- END SNIPPET:usage -->
 *
 * @author Juergen_Kellerer, 2012-07-08
 */
@ExtensionInformation(displayName = "UML Class", documentationUrl = "/templatesClassDiagram.html")
public class UMLClassParameterTransformer extends AbstractParameterTransformer {

	/**
	 * Enables the UML class inclusion with the fully qualified class name that was specified here.
	 * <p/>
	 * By default the class is included as SVG, add {@code render=png} to get it rendered as image.
	 */
	public static final String PARAM_UML_CLASS = "uml-class";

	/**
	 * Changes the minimum visibility of java entities, one of "private", "package-local", "protected", "public" (defaults to 'protected').
	 */
	public static final String PARAM_VISIBILITY = "visibility";

	/**
	 * Boolean parameter specifying whether properties and methods should include all reachable methods from superclasses.
	 * (Default: 'false')
	 */
	public static final String PARAM_INCLUDE_PARENTS = "include-parents";

	/**
	 * Is the size of the font inside the UML class.
	 * (Default: '8.5pt')
	 */
	public static final String PARAM_FONT_SIZE = "font-size";

	/**
	 * Is the target-width of a method signature measured in characters.
	 * (Default: '45')
	 */
	public static final String PARAM_TARGET_WIDTH = "method-target-width";

	/**
	 * Boolean parameter to hide any constant fields in the UML class.
	 */
	public static final String PARAM_HIDE_CONSTANTS = "hide-constants";

	/**
	 * Boolean parameter to hide any fields in the UML class.
	 */
	public static final String PARAM_HIDE_FIELDS = "hide-fields";

	/**
	 * Boolean parameter to hide any properties in the UML class.
	 */
	public static final String PARAM_HIDE_PROPERTIES = "hide-properties";

	/**
	 * Boolean parameter to hide any interface-methods (= abstract methods) in the UML class.
	 */
	public static final String PARAM_HIDE_INTERFACE_METHODS = "hide-interface-methods";

	/**
	 * Boolean parameter to hide any methods in the UML class.
	 */
	public static final String PARAM_HIDE_METHODS = "hide-methods";

	private static final String INPUT_PARAM_CLASS = "class";

	@Override
	protected boolean doTransformParameters(Map<String, Object> requestParams) {
		Object umlClass = requestParams.get(PARAM_UML_CLASS);
		if (umlClass != null) {
			requestParams.put(INPUT_PARAM_CLASS, umlClass);
			requestParams.put(SvgLoader.PARAM_ALIAS, "classpath:/templates/dim/uml-class.svg.vm");

			if (!requestParams.containsKey(SvgLoader.PARAM_ATTACH)) {
				requestParams.put(SvgLoader.PARAM_ATTACH, makeUniqueName("uml-class-" + umlClass + ".svg",
						requestParams.get(PARAM_VISIBILITY),
						requestParams.get(PARAM_INCLUDE_PARENTS),
						requestParams.get(PARAM_FONT_SIZE),
						requestParams.get(PARAM_TARGET_WIDTH),
						requestParams.get(PARAM_HIDE_CONSTANTS),
						requestParams.get(PARAM_HIDE_FIELDS),
						requestParams.get(PARAM_HIDE_PROPERTIES),
						requestParams.get(PARAM_HIDE_INTERFACE_METHODS),
						requestParams.get(PARAM_HIDE_METHODS)
				));
			}

			return true;
		}
		return false;
	}
}
