/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sh;

import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * {@link BrushResolver} defines an interface that may be implemented and registered via service loading to resolve
 * javascript brushes (= language support) for the syntax highlighting library by content or filename inspection.
 * <p/>
 * <b>Implementation</b>:<code><pre>
 * package my.package;
 * public class MyBrushResolver implements BrushResolver {
 *      public Collection&lt;URL&gt; resolveBrushes() {
 *          return Collections.singleton(getClass().getResource("/my/package/my-brush.js"));
 *      }
 *      public String detectBrushAlias(URL source, Map&lt;Integer, List&lt;String&gt;&gt; sourceContent) {
 *          return source.getPath().toLowerCase().endsWith(".my-extension") ? "my-brush-alias" : null;
 *      }
 * }
 * </pre></code>
 * <p/>
 * <b>Register the implementation using service loading</b>:<ul>
 * <li>
 * Create the following file:<code><pre>
 * META-INF/services/
 *    org.tinyjee.maven.dim.sh.BrushResolver
 * </pre></code></li>
 * <li>Add the fully qualified class name of your implementation to the file "org.tinyjee.maven.dim.sh.BrushResolver":<code><pre>
 * my.package.MyBrushResolver
 * </pre></code></li>
 * </ul>
 *
 * @since 1.0
 * @version 1.0
 * @author Juergen_Kellerer, 2010-09-03
 */
public interface BrushResolver {
	/**
	 * Returns a list of URLs that point to ".JS" or ".ZIP" files containing additional brush definitions.
	 *
	 * @return a list of URLs that point to ".JS" or ".ZIP" files containing additional brush definitions.
	 */
	Collection<URL> resolveBrushes();

	/**
	 * Auto Detects the brush alias name from the source URL or content.
	 *
	 * @param source        the source URL (.. to be used for extension matching).
	 * @param sourceContent the extracted content of the source
	 *                      (.. to be used for content based matching, e.g. shebang).
	 * @return a valid alias name of a brush that is defined inside the brushes returned by {@link #resolveBrushes()}
	 *         or 'null' if no alias can be resolved.
	 *         Note: If the content is HTML with a compiled / script language inside tags, the returned value is
	 *         "html/[brush-alias]" (e.g. JSP would map to "html/java").
	 */
	String detectBrushAlias(URL source, Map<Integer, List<String>> sourceContent);
}
