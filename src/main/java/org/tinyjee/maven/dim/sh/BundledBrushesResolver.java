/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sh;

import java.io.IOException;
import java.net.URL;
import java.util.*;

import static java.util.Locale.ENGLISH;
import static java.util.regex.Pattern.compile;

/**
 * Resolves bundled brushes for various languages to be used inside the syntax highlighting library.
 *
 * @author Juergen_Kellerer, 2010-09-03
 */
public class BundledBrushesResolver implements BrushResolver {

	static final Properties typeMapping = new Properties();

	static {
		try {
			final Class<?> cls = BundledBrushesResolver.class;
			typeMapping.load(cls.getResourceAsStream("/org/tinyjee/maven/dim/sh/type-to-brush.mapping.properties"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}


	/**
	 * {@inheritDoc}
	 */
	public Collection<URL> resolveBrushes() {
		final Class<?> cls = BundledBrushesResolver.class;
		return Arrays.asList(
				cls.getResource("/js-bundle/syntax-highlighter-default-brushes.zip"),
				cls.getResource("/js-bundle/shBatchBrush.js"),
				cls.getResource("/js-bundle/shClojureBrush.js"),
				cls.getResource("/js-bundle/shDotBrush.js"),
				cls.getResource("/js-bundle/shJavaPropertiesBrush.js"),
				cls.getResource("/js-bundle/shVelocityBrush.js")
		);
	}

	/**
	 * {@inheritDoc}
	 */
	public String detectBrushAlias(URL source, Map<Integer, List<String>> sourceContent) {
		final String aliasByContent = detectAliasByContent(sourceContent),
				aliasByExtension = source == null ? null : detectAliasByExtension(source.getFile());

		if (aliasByExtension != null && (
				aliasByContent == null || (aliasByExtension.startsWith("html/") && !aliasByContent.startsWith("html/")))) {
			return aliasByExtension;
		}

		return aliasByContent;
	}

	/**
	 * Returns the backing properties used to map types.
	 *
	 * @return the backing properties used to map types.
	 */
	protected Properties getTypeMappingProperties() {
		return typeMapping;
	}

	/**
	 * Detects the brush alias by file extension.
	 *
	 * @param file the file to detect the alias for.
	 * @return the brush alias, or 'null' if no mapping exists.
	 */
	protected String detectAliasByExtension(String file) {
		if (file == null) return null;
		for (int extensionIndex = file.indexOf('.'); extensionIndex != -1; extensionIndex = file.indexOf('.', extensionIndex + 1)) {
			String extension = file.substring(extensionIndex);
			if (extension.length() > 1) {
				extension = extension.toLowerCase(ENGLISH);
				final String typeMapping = getTypeMappingProperties().getProperty(extension);
				if (typeMapping != null) return typeMapping;
			}
		}
		return null;
	}

	/**
	 * Detects the brush alias by file content.
	 *
	 * @param content the file content.
	 * @return the brush alias, or 'null' if no mapping exists.
	 */
	protected String detectAliasByContent(Map<Integer, List<String>> content) {
		String firstLine = "";
		if (content != null && !content.isEmpty()) {
			List<String> lines = content.values().iterator().next();
			for (String line : lines) {
				if (line != null && line.trim().length() != 0) {
					firstLine = line;
					break;
				}
			}
		}

		for (Map.Entry<Object, Object> entry : getTypeMappingProperties().entrySet()) {
			String key = entry.getKey().toString();
			if (key.startsWith("^")) {
				boolean isRegExpMatch = key.startsWith("^RegExp") && compile(key.substring(7)).matcher(firstLine).matches();
				if (isRegExpMatch || firstLine.startsWith(key.substring(1)))
					return entry.getValue().toString();
			}
		}

		return null;
	}
}
