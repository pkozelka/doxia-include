/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sh;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.ContextAction;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.Script;
import org.tinyjee.maven.dim.spi.backport.ServiceLoader;
import org.tinyjee.maven.dim.utils.JavaScriptFacade;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implements a Java bridge to the Alex Gorbatchev's JavaScript library SyntaxHighlighter.
 * <p/>
 * See 'http://alexgorbatchev.com/SyntaxHighlighter/' for more details on the highlighting library.
 *
 * @author Juergen_Kellerer, 2010-09-03
 * @version 1.0
 */
public class HighlighterScriptFacade extends JavaScriptFacade {

	static final Iterable<BrushResolver> resolverLoader = ServiceLoader.load(BrushResolver.class);

	final Function highlightFunction;
	final Iterable<BrushResolver> brushResolvers = resolverLoader;

	/**
	 * Constructs a new thread bound HighlighterScriptFacade.
	 *
	 * @throws IOException In case of the initial scripts needed for the SyntaxHighlighter cannot be loaded.
	 */
	protected HighlighterScriptFacade() throws IOException {
		super();

		List<Script> scripts = new ArrayList<Script>();
		final Class<?> cls = HighlighterScriptFacade.class;
		compileScript(cls.getResource("/js-bundle/xregexp.js"), scripts);
		compileScript(cls.getResource("/js-bundle/syntax-highlighter.zip"), scripts);

		for (BrushResolver resolver : brushResolvers) {
			for (URL url : resolver.resolveBrushes())
				compileScript(url, scripts);
		}

		compileScript(cls.getResource("/org/tinyjee/maven/dim/sh/highlight.js"), scripts);

		for (Script script : scripts)
			execute(script);

		highlightFunction = (Function) getFromScope("highlightToHTML");
	}

	/**
	 * Highlights the given code using a HTML representation.
	 *
	 * @param brushName the name of the SyntaxHighlighter brush to use.
	 * @param code      the code to highlight.
	 * @param params    the params to pass to the SyntaxHighlighter.
	 * @return the highlighted HTML representation of the code.
	 */
	public String highlightToHTML(String brushName, String code, Map<String, String> params) {
		final Object[] args = buildCallArguments(brushName, code, params);

		Object result = contextFactory.call(new ContextAction() {
			public Object run(Context context) {
				return highlightFunction.call(context, scope, scope, args);
			}
		});

		return result == null ? null : result.toString();
	}

	private Object[] buildCallArguments(String brushName, String code, Map<String, String> params) {
		final Map<String, Object> rawParams = new HashMap<String, Object>();
		if (params != null) {
			for (final Map.Entry<String, String> entry : params.entrySet()) {
				final String key = entry.getKey();
				final Object result = contextFactory.call(new ContextAction() {
					public Object run(Context context) {
						return context.evaluateString(scope, entry.getValue(), key, 0, null);
					}
				});

				rawParams.put(key, result);
			}
		}

		return new Object[]{brushName, code, createObject(rawParams)};
	}

	public final Iterable<BrushResolver> getBrushResolvers() {
		return brushResolvers;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void finalize() throws Throwable {
		try {
			close();
		} finally {
			super.finalize();
		}
	}
}
