/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sh;

import java.io.IOException;
import java.lang.ref.SoftReference;

/**
 * Implements a thread local that holds an instance of {@link HighlighterScriptFacade} per thread.
 *
 * @author Juergen_Kellerer, 2010-09-03
 * @version 1.0
 */
public class HighlighterThreadLocal extends ThreadLocal<SoftReference<HighlighterScriptFacade>> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected SoftReference<HighlighterScriptFacade> initialValue() {
		try {
			return new SoftReference<HighlighterScriptFacade>(new HighlighterScriptFacade());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Returns a thread bound, script facade.
	 *
	 * @return a thread bound, script facade.
	 */
	public HighlighterScriptFacade getHighlighter() {

		HighlighterScriptFacade highlighter;
		do {
			SoftReference<HighlighterScriptFacade> ref = get();
			highlighter = ref == null ? null : ref.get();
			if (highlighter == null)
				remove(); // clear a disposed reference to force their re-creation.
			else
				break;
		} while (true);

		return highlighter;
	}
}
