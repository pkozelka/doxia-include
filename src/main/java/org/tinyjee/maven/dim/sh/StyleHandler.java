/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sh;

import org.codehaus.plexus.util.IOUtil;
import org.codehaus.plexus.util.StringUtils;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Handles the inclusion of theme dependent CSS styles for highlighted code.
 *
 * @author Juergen_Kellerer, 2010-09-03
 * @version 1.0
 */
public class StyleHandler {

	static final String BASE_DIR = "css/";
	static final String BASE_NAME = "shCore";
	static final String LB = System.getProperty("line.separator", "\n");
	static final Charset CSS_CHARSET = Charset.forName("UTF-8");

	static final String SCROLLBARS_PATCH = "" +
			".syntaxhighlighter {" + LB +
			"  overflow: visible !important;" + LB +
			"  overflow-y: hidden !important;" + LB +
			"  overflow-x: auto !important;" + LB +
			'}';

	static final String BORDER_COLOR = System.getProperty("dim.highlighted.border.color", "#e5e5e5");

	static final String BORDER_PATCH = "" +
			"div.syntaxhighlighter {" + LB +
			"  border: 1px solid " + BORDER_COLOR + ';' + LB +
			'}';


	final String themeName;
	final Map<String, byte[]> sources = new HashMap<String, byte[]>();

	/**
	 * Constructs a new style handler with the default style theme.
	 */
	public StyleHandler() {
		this(System.getProperty("org.tinyjee.doxia.include.sh.default.theme", "default"));
	}

	/**
	 * Constructs a new style handler with the given style theme.
	 *
	 * @param themeName the name of the supported style theme (name is not validated!).
	 */
	public StyleHandler(String themeName) {
		this.themeName = themeName.trim().toLowerCase();
		scanSources();
	}

	final void scanSources() {
		try {
			final Class<?> cls = StyleHandler.class;
			final ByteArrayOutputStream buffer = new ByteArrayOutputStream(4096);

			final InputStream in = cls.getResource("/js-bundle/syntax-highlighter.zip").openStream();
			final ZipInputStream zIn = new ZipInputStream(in);
			try {
				sources.clear();

				final String cssName = getCSSFileName();
				for (ZipEntry ze = zIn.getNextEntry(); ze != null; ze = zIn.getNextEntry()) {
					String name = ze.getName();
					if (!name.endsWith(".css"))
						continue;

					name = BASE_DIR + new File(name).getName();
					if (name.equals(cssName)) {
						buffer.reset();
						IOUtil.copy(zIn, buffer);
						sources.put(name, buffer.toByteArray());
					}
				}
			} finally {
				zIn.close();
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Returns the name of the CSS file.
	 *
	 * @return the name of the CSS file.
	 */
	public String getCSSFileName() {
		return BASE_DIR + BASE_NAME + Character.toUpperCase(themeName.charAt(0)) + themeName.substring(1) + ".css";
	}

	/**
	 * Returns true if this instance is handling the given style theme.
	 *
	 * @param theme the name of the supported style theme.
	 * @return true if this instance is handling the given style theme.
	 */
	public boolean isHandlingStyleForTheme(String theme) {
		return themeName.equalsIgnoreCase(theme);
	}

	/**
	 * Returns the CSS content of the CSS file.
	 *
	 * @param cssName the name of the cached file or 'null' to return the default for the given schema.
	 * @return the CSS content of the CSS file.
	 */
	public String getCSSContent(String cssName) {
		if (cssName == null) cssName = getCSSFileName();

		final CharBuffer buffer = CSS_CHARSET.decode(ByteBuffer.wrap(sources.get(cssName)));
		StringBuilder builder = new StringBuilder(buffer.length() + 128).append(buffer);

		builder.append(LB).append(SCROLLBARS_PATCH).append(LB);
		if (StringUtils.isNotEmpty(BORDER_COLOR))
			builder.append(LB).append(BORDER_PATCH).append(LB);

		return builder.toString();
	}
}
