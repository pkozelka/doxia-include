/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sh;

import org.apache.maven.doxia.logging.Log;
import org.apache.maven.doxia.sink.Sink;
import org.apache.maven.doxia.sink.SinkEventAttributeSet;
import org.codehaus.plexus.util.StringUtils;
import org.tinyjee.maven.dim.spi.FixedContentSource;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.SnippetExtractor;
import org.tinyjee.maven.dim.utils.XhtmlEntityResolver;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.*;

import static java.lang.String.valueOf;
import static org.tinyjee.maven.dim.IncludeMacroSignature.*;

/**
 * Performs code highlighting on the provided source before writing it to the Sink.
 *
 * @author Juergen_Kellerer, 2010-09-04
 * @version 1.0
 */
public class CodeHighlighter {

	private static final Log log = Globals.getLog();
	static final String LB = System.getProperty("line.separator", "\n");

	static final HighlighterThreadLocal highlighters = new HighlighterThreadLocal();

	StyleHandler styleHandler = new StyleHandler();

	static final String HTML_OPEN_TAG =
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" " + LB +
					"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"> " + LB +
					"<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\"> " + LB;
	static final String HTML_CLOSE_TAG = "</html>";

	final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	final TransformerFactory transformerFactory = TransformerFactory.newInstance();

	/**
	 * Highlights the given code using a HTML representation.
	 *
	 * @param brushName     the name of the SyntaxHighlighter brush to use.
	 * @param sourceContent the code to highlight.
	 * @param params        the params to pass to the SyntaxHighlighter.
	 * @return the highlighted HTML representation of the code.
	 */
	public String highlight(String brushName, String sourceContent, Map<String, String> params) {
		return highlighters.getHighlighter().highlightToHTML(brushName, sourceContent, params);
	}

	/**
	 * Hightlights the given source lines using HTML markup.
	 *
	 * @param sourceLines a map of first-line number to lines.
	 * @param firstLine   set to >= 0 to adjust the first line index.
	 * @param brushName   the name of the brush used for code highlighting, e.g. "java".
	 * @param params      any params to pass to the highlighting library.
	 * @return the highlighted html code.
	 */
	public String highlight(Map<Integer, List<String>> sourceLines, int firstLine, String brushName, Map<String, String> params) {
		final List<String> lines = new LinkedList<String>();
		final List<Integer> linesToRemove = new LinkedList<Integer>();

		int lineNumber = -1, delta = 0;
		for (Map.Entry<Integer, List<String>> entry : sourceLines.entrySet()) {
			if (lines.isEmpty()) {
				delta = firstLine == -1 ? 0 : entry.getKey() - firstLine;
				params.put("first-line", valueOf(entry.getKey() - delta));
			} else {
				for (int i = lineNumber, len = entry.getKey(); i < len; i++) {
					linesToRemove.add(i - delta);
					lines.add("");
				}
			}
			lineNumber = entry.getKey() + entry.getValue().size();
			lines.addAll(entry.getValue());
		}

		final String code = SnippetExtractor.getInstance().toString(lines);
		if (StringUtils.isEmpty(code))
			return null;

		String htmlResult = highlight(brushName, code, params);
		if (!linesToRemove.isEmpty())
			htmlResult = removeLines(htmlResult, linesToRemove);

		return htmlResult;
	}

	/**
	 * Attaches the cascading stylesheet required to do code highlighting to the sink.
	 *
	 * @param sink              the sink to operate on, may be 'null' to use the global default.
	 * @param requestParameters the request parameters that may modify the style selection.
	 * @return true if the sink supported attaching the styles.
	 * @throws IOException In case of writing the attachment failed.
	 */
	public boolean attachStylesheet(Sink sink, Map requestParameters) throws IOException {
		final String highlightTheme = (String) requestParameters.get(PARAM_HIGHLIGHT_THEME);
		if (highlightTheme != null && !styleHandler.isHandlingStyleForTheme(highlightTheme))
			styleHandler = new StyleHandler(highlightTheme);

		String cssFileName = styleHandler.getCSSFileName();
		return Globals.getInstance().operateOnSink(sink).attachCss(cssFileName, styleHandler.getCSSContent(cssFileName));
	}

	/**
	 * Processes the given source lines and writes the results into the sink.
	 *
	 * @param source            The source URL used to obtain the content (can be 'null').
	 * @param sourceLines       a map of first line number to lines mapping.
	 * @param requestParameters the original request params.
	 * @param sink              the target sink to write to.
	 * @throws IOException in case of the process fails.
	 */
	public void execute(URL source, Map requestParameters, Map<Integer, List<String>> sourceLines, Sink sink) throws IOException {
		if (source == null) source = FixedContentSource.UNKNOWN;
		final Map<String, String> params = buildHighlighterParams(source, sourceLines, requestParameters);
		final String brushName = findBrushName(source, sourceLines, requestParameters);

		final int firstLine = requestParameters.containsKey(PARAM_SET_FIRST_LINE) ?
				Integer.parseInt((String) requestParameters.get(PARAM_SET_FIRST_LINE)) : -1;

		if (log.isDebugEnabled()) log.debug("Highlighting source '" + source + "'");

		final String htmlResult = highlight(sourceLines, firstLine, brushName, params);
		if (htmlResult == null) {
			log.error("No content to highlight for source '" + source + "' with params '" + params + '\'');
			return;
		}

		if (Globals.getInstance().operateOnSink(sink).isHTMLCapableSink() && attachStylesheet(sink, requestParameters)) {
			sink.rawText(htmlResult);
		} else {
			// TODO: Render image instead (e.g. using Lobo or Swing).
			sink.verbatim(SinkEventAttributeSet.BOXED);
			sink.text("TODO: Image Rendering !!");
			sink.verbatim_();
		}
	}

	/**
	 * Finds the name of the code highlighter to use.
	 *
	 * @param source            the source URL.
	 * @param sourceLines       the source content.
	 * @param requestParameters the request parameters of the macro request.
	 * @return the name of the code highlighter to use or 'plain' if not found.
	 */
	public String findBrushName(URL source, Map<Integer, List<String>> sourceLines, Map requestParameters) {
		if (source == null) source = FixedContentSource.UNKNOWN;
		String brushName = findBrushAlias(source, sourceLines, requestParameters);
		int idx = brushName.indexOf('/');
		return idx == -1 ? brushName : brushName.substring(idx + 1);
	}

	/**
	 * Finds the name of the code highlighter to use.
	 *
	 * @param source            the source URL.
	 * @param sourceLines       the source content.
	 * @param requestParameters the request parameters of the macro request.
	 * @return the name of the code highlighter to use or 'plain' if not found.
	 */
	protected String findBrushAlias(URL source, Map<Integer, List<String>> sourceLines, Map requestParameters) {
		String brushAlias = requestParameters == null ? null : (String) requestParameters.get(PARAM_HIGHLIGHT_TYPE);

		if (brushAlias == null)
			for (BrushResolver resolver : highlighters.getHighlighter().getBrushResolvers()) {
				brushAlias = resolver.detectBrushAlias(source, sourceLines);
				if (brushAlias != null) break;
			}

		return brushAlias == null ? "plain" : brushAlias;
	}

	/**
	 * Returns true if the source content is HTML with embedded source code of another language.
	 *
	 * @param source            the source URL.
	 * @param sourceLines       the source content.
	 * @param requestParameters the request parameters of the macro request.
	 * @return true if the source content is HTML with embedded source code of another language.
	 */
	protected boolean isCodeSurroundedByHTML(URL source, Map<Integer, List<String>> sourceLines, Map requestParameters) {
		return findBrushAlias(source, sourceLines, requestParameters).startsWith("html/");
	}

	/**
	 * Builds a map of params that can be passed to the SyntaxHighlighter library.
	 *
	 * @param source            the source URL.
	 * @param sourceLines       the source content.
	 * @param requestParameters the request parameters of the macro request.
	 * @return a map of params that can be passed to the SyntaxHighlighter library.
	 */
	public Map<String, String> buildHighlighterParams(URL source, Map<Integer, List<String>> sourceLines, Map requestParameters) {
		if (source == null) source = FixedContentSource.UNKNOWN;
		final Map<String, String> params = new HashMap<String, String>();

		// Decide whether the code is embedded into HTML or XHTML.
		params.put("html-script", valueOf(isCodeSurroundedByHTML(source, sourceLines, requestParameters)));

		params.put("gutter", System.getProperty("org.tinyjee.maven.dim.defaultShowGutter", "true"));
		// Disable the gutter if explicitly requested.
		if ("false".equalsIgnoreCase(valueOf(requestParameters.get(PARAM_SHOW_GUTTER)).trim()))
			params.put("gutter", "false");

		// Pass through the setting of padded line numbers.
		Object padLineNumbers = requestParameters.get(PARAM_PAD_LINE_NUMBERS);
		if (padLineNumbers != null) params.put("pad-line-numbers", Integer.valueOf(padLineNumbers.toString()).toString());

		// Parse (legacy) highlighted lines
		Object highlightLines = requestParameters.get(PARAM_HIGHLIGHT);
		if (highlightLines != null) {
			StringBuilder buffer = new StringBuilder().append("[0");
			String[] lines = highlightLines.toString().split("\\s*[,;+]\\s*");
			for (String lineNumber : lines) {
				String[] parts = lineNumber.split("\\s*-\\s*");
				if (parts.length == 1)
					buffer.append(", ").append(Integer.parseInt(parts[0]));
				else if (parts.length == 2 && StringUtils.isNotEmpty(parts[0])) {
					for (int i = Integer.parseInt(parts[0]), len = Integer.parseInt(parts[1]); i <= len; i++)
						buffer.append(", ").append(i);
				}
			}
			params.put("highlight", buffer.append(']').toString());
		}

		// Generate highlighted lines using snippet selection
		highlightLines = requestParameters.get(PARAM_HIGHLIGHT_LINES);
		if (highlightLines != null && sourceLines != null) {
			@SuppressWarnings("unchecked")
			Map<String, Object> parameters = new HashMap<String, Object>(requestParameters);
			parameters.put("snippet", highlightLines);
			SortedSet<Integer> selectedLines;
			try {
				SnippetExtractor extractor = SnippetExtractor.getInstance();
				selectedLines = extractor.findSelectedLines(
						new FixedContentSource(extractor.toString(sourceLines, true), parameters)).get(0);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			final StringBuilder buffer = new StringBuilder().append("[0");
			for (Integer selectedLine : selectedLines)
				buffer.append(',').append(selectedLine);
			params.put("highlight", buffer.append(']').toString());
		}

		// Pass through any params that were prefixed with "sh-" (removing the prefix).
		for (Object entry : requestParameters.entrySet()) {
			Map.Entry e = (Map.Entry) entry;
			String key = valueOf(e.getKey());
			if (key.startsWith("sh-")) params.put(key.substring(3), (String) e.getValue());
		}

		return params;
	}

	protected String removeLines(String content, Collection<Integer> linesToRemove) {
		try {
			final Set<String> classesToRemove = new HashSet<String>(linesToRemove.size());
			for (Integer integer : linesToRemove) classesToRemove.add("number" + integer);

			content = HTML_OPEN_TAG + content + HTML_CLOSE_TAG;
			DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();
			builder.setEntityResolver(new XhtmlEntityResolver());
			Document document = builder.parse(new InputSource(new StringReader(content)));
			NodeList divs = document.getDocumentElement().getElementsByTagName("div");
			for (int i = divs.getLength() - 1; i >= 0; i--) {
				Element div = (Element) divs.item(i);
				for (String className : div.getAttribute("class").split("\\s+")) {
					if (classesToRemove.contains(className))
						div.getParentNode().removeChild(div);
				}
			}

			final StringWriter buffer = new StringWriter(content.length());
			final Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.METHOD, "html");
			transformer.transform(new DOMSource(document), new StreamResult(buffer));
			content = buffer.toString();
			content = content.substring(content.indexOf('<', content.indexOf("<html") + 5), content.lastIndexOf(HTML_CLOSE_TAG));
		} catch (Exception e) {
			log.error("Failed to remove hidden lines from highlighted content. " +
					"The result may contain content that should have been hidden.", e);
			log.error(content);
		}
		return content;
	}
}
