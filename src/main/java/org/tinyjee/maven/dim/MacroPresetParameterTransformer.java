/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim;

import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.RequestParameterTransformer;

import java.util.List;
import java.util.Map;

/**
 * Implements a parameter transformer that can be configured with MacroPresets coming from {@link InitializeMacroMojo}.
 *
 * @author Juergen_Kellerer, 2012-02-21
 */
public class MacroPresetParameterTransformer implements RequestParameterTransformer {

	private static final Log log = Globals.getLog();

	static final String KEY_PRESET = "preset";
	static final String DEFAULT_PRESET = "default";

	public void transformParameters(Map<String, Object> requestParams) {
		final boolean debug = log.isDebugEnabled();
		Object presetName = requestParams.get(KEY_PRESET), aliasValue = null;

		if (presetName == null) {
			final List<String> aliases = MacroPreset.getAliases(System.getProperties());
			for (String alias : aliases) {
				aliasValue = requestParams.get(alias);
				if (aliasValue != null) {
					presetName = alias;
					if (debug) log.debug("Detected an alias request for alias '" + alias + "'::value='" + aliasValue + '\'');
					break;
				}
			}

			if (presetName == null) presetName = DEFAULT_PRESET;
		}

		final boolean isDefaultPreset = DEFAULT_PRESET.equals(presetName);
		final String name = presetName.toString();

		if (!isDefaultPreset) {
			if (debug) {
				log.debug("Looking for after named properties preset '" + name + "', " +
						"properties before applying preset are: " + requestParams);
			}
		}

		final MacroPreset preset = MacroPreset.decodeFrom(name, System.getProperties());
		if (preset != null) {
			if (preset.isAlias()) requestParams.remove(preset.getName());

			final Map<String, String> properties = preset.getProperties();
			for (Map.Entry<String, String> entry : properties.entrySet()) {
				if (!requestParams.containsKey(entry.getKey())) {
					String value = entry.getValue();
					if (preset.isAlias()) {
						value = value.replace("${value}", String.valueOf(aliasValue));
					}
					requestParams.put(entry.getKey(), value);
				}
			}

			if (debug) log.debug("Properties after applying preset are: " + requestParams);
		} else {
			if (!isDefaultPreset) {
				log.error("Did not find a defined properties preset that is named: '" + name + '\'');
			}
		}
	}
}
