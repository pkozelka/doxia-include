/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Implements an abstract base for snippet selectors that implement everything using stream processing,
 * rather than trying to buffer the results in memory.
 *
 * @author Juergen_Kellerer, 2011-10-14
 */
public abstract class AbstractStreamingSnippetSelector extends AbstractSnippetSelector {

	/**
	 * Constructs a new instance with the given expression-prefix.
	 *
	 * @param expressionPrefix the prefix to use for detecting expressions belonging to this selector.
	 */
	protected AbstractStreamingSnippetSelector(String expressionPrefix) {
		super(expressionPrefix);
	}

	/**
	 * Is the actual base for line number iterators that directly operate on the content stream.
	 *
	 * @param <E> the type of the used expression.
	 */
	protected abstract static class AbstractStreamIterator<E> implements Iterator<Integer> {

		private final E expression;
		private final LineNumberReader content;

		private String line;
		private boolean eof;
		private boolean capturing;

		/**
		 * Constructs a new instance with the given expression around the specified input stream.
		 *
		 * @param expression the expression to use for selecting snippets.
		 * @param content    the input stream (as LineNumberReader).
		 */
		protected AbstractStreamIterator(E expression, LineNumberReader content) {
			this.content = content;
			this.expression = expression;
		}

		/**
		 * Returns true if the current line should be captured.
		 *
		 * @param alreadyCapturing true if the previous line was captured.
		 * @param expression       the expression to match the line with.
		 * @param line             the line to test.
		 * @param lineNumber       the line number of the given line.
		 * @return true if the current line should be captured.
		 */
		protected abstract boolean isCapturingLine(boolean alreadyCapturing, E expression, String line, int lineNumber);

		protected boolean doHasNext() throws IOException {
			while (!eof && line == null) {
				eof = (line = content.readLine()) == null;
				if (!eof) {
					capturing = isCapturingLine(capturing, expression, line, content.getLineNumber());
					if (!capturing) line = null;
				}
			}
			return line != null;
		}

		public boolean hasNext() {
			try {
				return doHasNext();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		public Integer next() {
			if (!hasNext()) throw new NoSuchElementException();
			try {
				return content.getLineNumber();
			} finally {
				line = null;
			}
		}

		public final void remove() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String toString() {
			return getClass().getSimpleName() + '{' +
					"expression=" + expression +
					", content=" + content +
					", line='" + line + '\'' +
					", eof=" + eof +
					", capturing=" + capturing +
					'}';
		}
	}

}
