/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.apache.maven.doxia.logging.Log;
import org.codehaus.plexus.util.IOUtil;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.SnippetExtractor;
import org.tinyjee.maven.dim.utils.ClassUtils;

import java.io.*;
import java.util.List;
import java.util.Map;

import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_SOURCE_CONTENT;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_SOURCE_IS_TEMPLATE;

/**
 * Adapts a source class to the source interface.
 *
 * @author Juergen_Kellerer, 2010-09-04
 */
public class SourceClassAdapter extends AbstractBufferedSource {

	private static final Log log = Globals.getLog();

	/**
	 * Defines the map key to use for retrieving the source content from the Map defined with the wrapped class.
	 * <p/>
	 * Note: A source class can decide to return an instance of <code>Map&lt;Integer, List&lt;String&gt;&gt;</code>
	 * when queried for the key defined by this constant.
	 */
	public static final String LEGACY_CONTENT_MAP_KEY = "content";

	final File basePath;
	final String classNameOrExpression;

	Object sourceInstance;

	/**
	 * Creates a new instance of SourceClassAdapter.
	 *
	 * @param basePath              the base path containing the "target/classes" folder.
	 * @param requestParams         the params of the macro request (are optionally passed to the object instance).
	 * @param classNameOrExpression the name of the class to adapt.
	 */
	@SuppressWarnings("unchecked")
	public SourceClassAdapter(File basePath, Map requestParams, String classNameOrExpression) {
		super(null, requestParams);
		this.basePath = basePath;
		this.classNameOrExpression = classNameOrExpression;
	}

	public synchronized Object getSourceInstance() {
		if (sourceInstance == null) {
			try {
				sourceInstance = ClassUtils.invoke(classNameOrExpression, basePath, parameters);
			} catch (RuntimeException e) {
				log.error("Failed creating a source class instance on basePath: '" + basePath + "'\n" +
						"Expression: '" + classNameOrExpression + "'\n" +
						"Call-Parameters:" + parameters, e);
				throw e;
			}
		}
		return sourceInstance;
	}

	@Override
	public synchronized Content getContent() {
		if (content == null) {
			final Object instance = getSourceInstance();
			if (instance instanceof Content) content = (Content) instance;
		}
		return super.getContent();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected void writeSourceContent(Writer writer) throws IOException {
		final Object instance = getSourceInstance();

		Reader sourceContent = null;

		if (instance instanceof Map) {
			parameters.clear();
			parameters.putAll((Map) instance);

			boolean hasLegacyContentKey = parameters.containsKey(LEGACY_CONTENT_MAP_KEY);
			Object content = parameters.containsKey(PARAM_SOURCE_CONTENT) ?
					parameters.get(PARAM_SOURCE_CONTENT) :
					(hasLegacyContentKey ? parameters.get(LEGACY_CONTENT_MAP_KEY) : null);

			if (content != null && !hasLegacyContentKey && !parameters.containsKey(PARAM_SOURCE_IS_TEMPLATE))
				parameters.put(PARAM_SOURCE_IS_TEMPLATE, true);

			if (content instanceof Map)
				content = SnippetExtractor.getInstance().toString((Map<Integer, List<String>>) content, false);
			if (content != null)
				sourceContent = new StringReader(content.toString());

		} else if (instance instanceof Reader) {
			sourceContent = (Reader) instance;
		}

		if (sourceContent == null) sourceContent = new StringReader(instance.toString());
		try {
			IOUtil.copy(sourceContent, writer);
		} finally {
			sourceContent.close();
		}
	}
}
