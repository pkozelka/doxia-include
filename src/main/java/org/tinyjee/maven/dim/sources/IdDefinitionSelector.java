/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import java.io.IOException;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

/**
 * Implements a selector that matches all snippet ID definitions triggered by a single expression "<b>snippet-ids</b>".
 * The purpose of this selector is to exclude any snippet id definitions from matched content.
 * <p/>
 * Example: "{@code %{include|snippet=#id-to-include, !snippet-ids}}".
 *
 * @author Juergen_Kellerer, 2011-10-16
 */
public class IdDefinitionSelector extends IdSnippetSelector {

	public IdDefinitionSelector() {
		super("snippet-ids");
	}

	@Override
	public boolean canSelectSnippetsWith(String expression, URL contentUrl, Map<String, Object> macroParameters) {
		return expressionPrefix.equalsIgnoreCase(expression);
	}

	@Override
	public Iterator<Integer> selectSnippets(String expression, URL contentUrl, LineNumberReader content,
	                                        Map<String, Object> macroParameters) throws IOException {
		assertExpressionIsValid(expression, contentUrl, macroParameters);

		return new AbstractStreamIterator<String>(expression, content) {
			@Override
			protected boolean isCapturingLine(boolean alreadyCapturing, String expression, String line, int lineNumber) {
				return isStart("", line) || isEnd("", line);
			}
		};
	}
}
