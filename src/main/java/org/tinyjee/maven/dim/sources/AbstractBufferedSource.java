/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.tinyjee.maven.dim.spi.AbstractSource;

import java.io.*;
import java.net.URL;
import java.util.Map;

/**
 * Base class for source implementations that buffer (generated) content in memory.
 *
 * @author Juergen_Kellerer, 2011-10-19
 */
public abstract class AbstractBufferedSource extends AbstractSource {

	protected Content content;

	protected AbstractBufferedSource(URL sourceUrl, Map<String, Object> parameters) {
		super(sourceUrl, parameters);
	}

	/**
	 * Is called when the source content has to be created.
	 *
	 * @param writer the writer to write the source content to.
	 * @throws IOException in case of writing failed.
	 */
	protected abstract void writeSourceContent(Writer writer) throws IOException;

	@Override
	public synchronized Content getContent() {
		if (content == null) {
			String bufferedContent = null;
			IOException exception = null;
			try {
				final StringWriter writer = new StringWriter(2048);
				try {
					writeSourceContent(writer);
				} finally {
					writer.close();
				}
				bufferedContent = writer.toString();
			} catch (IOException e) {
				exception = e;
			}

			content = new BufferedSourceContent(bufferedContent, exception);
		}

		return content;
	}

	public synchronized void reset() {
		content = null;
	}

	protected class BufferedSourceContent extends SourceContent {

		protected final IOException exception;
		protected final String bufferedContent;

		protected BufferedSourceContent(String bufferedContent, IOException exception) {
			this.bufferedContent = bufferedContent;
			this.exception = exception;
		}

		@Override
		public Reader openReader() throws IOException {
			if (exception != null) throw exception;
			return new StringReader(bufferedContent);
		}

		@Override
		public String toString() {
			return "BufferedSourceContent{" +
					"sourceUrl=" + sourceUrl +
					", parameters=" + parameters +
					", exception=" + exception +
					", bufferedContent='" + bufferedContent + '\'' +
					'}';
		}
	}
}
