/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import com.thoughtworks.qdox.model.*;
import org.tinyjee.maven.dim.utils.AbstractSelectableJavaEntitiesList;
import org.tinyjee.maven.dim.utils.JavaClassLoader;
import org.tinyjee.maven.dim.utils.SelectableJavaEntitiesList;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.net.URL;
import java.util.*;

import static java.lang.Boolean.parseBoolean;
import static java.lang.String.valueOf;
import static java.util.Collections.singletonMap;

/**
 * Implements an AOP expression like snippet selector using the expression prefix "<b>AJ:</b>".
 * <p/>
 * <b>Well formed content required</b>: This snippet selector requires well formed Java source input. Anything else will cause failures.
 * <p/>
 * Expression samples:<ul>
 * <li>{@code AJ:..toString()}</li>
 * <li>{@code AJ:..fieldName}</li>
 * <li>{@code AJ:public void my..MyClass.myMethod(..)}</li>
 * <li>{@code AJ:public void my..MyClass.myMethod(java.lang.String, ..)}</li>
 * <li>{@code AJ:public void my..MyClass.myMethod(..String, ..)}</li>
 * <li>{@code AJ:@SuppressWarnings(value="unchecked")}</li>
 * <li>{@code AJ:@my..Annotation(..value="something"..)}</li>
 * </ul>
 * <p/>
 * See also {@link AbstractSelectableJavaEntitiesList} for more details.
 * <p/>
 * The selector uses {@link AbstractSelectableJavaEntitiesList#selectAnnotated(String)} to retrieve matching elements of the given
 * java source in case of the expression starts with an '@', otherwise {@link AbstractSelectableJavaEntitiesList#selectMatching(String)}
 * is used to find matching java entities.
 * <p/>
 * Comments and bean properties are expanded if {@code "expand-snippets=true"} is set. Effectively this means when matching
 * a field, getter or setter all linked code parts are automatically included, as well as the javadoc comments.
 * <p/>
 * When {@code "fold-code-blocks=true"} is set, the snippet selector can be used to select an abstract view of a class
 * (= only declarations but no code blocks). This mode can be combined with {@code "expand-snippets=true"} which results in a
 * view of an ordinary class that is very similar to that of an interface. A typical macro call with this feature enabled looks like:
 * "{@code %{include|source=my.package.MyClass|snippet=af:public ..*|fold-code-blocks=true}}"
 *
 * @author Juergen_Kellerer, 2011-10-14
 */
public class AOPLikeSnippetSelector extends AbstractSnippetSelector {

	/**
	 * Is a option that changes the behaviour of the selector when true code blocks of
	 * methods are not selected (= collapsed).
	 *
	 * @since 1.2
	 */
	public static final String FOLD_CODE_BLOCKS = "fold-code-blocks";

	public AOPLikeSnippetSelector() {
		this("aj:");
	}

	protected AOPLikeSnippetSelector(String expressionPrefix) {
		super(expressionPrefix);
	}

	public Iterator<Integer> selectSnippets(String expression, URL contentUrl, LineNumberReader content,
	                                        Map<String, Object> macroParameters) throws IOException {
		assertExpressionIsValid(expression, contentUrl, macroParameters);
		expression = stripPrefix(expression).trim();

		final String contentString = bufferContent(content);
		final boolean expandSnippets = parseBoolean(valueOf(macroParameters.get(EXPAND_SNIPPETS)));
		final boolean foldCodeBlocks = parseBoolean(valueOf(macroParameters.get(FOLD_CODE_BLOCKS)));
		final AbstractSelectableJavaEntitiesList<AbstractJavaEntity> all = collectJavaEntities(contentUrl, contentString),
				matchingEntities = expression.startsWith("@") ? all.selectAnnotated(expression) : all.selectMatching(expression);

		return new LinesIterator(matchingEntities, contentUrl, contentString, expandSnippets, foldCodeBlocks);
	}

	protected String bufferContent(LineNumberReader content) throws IOException {
		StringBuilder builder = new StringBuilder(2 * 1024);
		for (String line = content.readLine(); line != null; line = content.readLine())
			builder.append(line).append('\n');
		return builder.toString();
	}

	protected SelectableJavaEntitiesList<AbstractJavaEntity> collectJavaEntities(URL contentUrl, String content)
			throws IOException {
		final JavaSource javaSource = JavaClassLoader.getInstance().addSource(contentUrl, new StringReader(content));
		final SelectableJavaEntitiesList<AbstractJavaEntity> allEntities = new SelectableJavaEntitiesList<AbstractJavaEntity>();
		for (JavaClass javaClass : javaSource.getClasses())
			add(javaClass, allEntities);

		return allEntities;
	}

	protected void add(JavaClass javaClass, SelectableJavaEntitiesList<AbstractJavaEntity> allEntities) {
		allEntities.add(javaClass);
		Collections.addAll(allEntities, javaClass.getFields());
		Collections.addAll(allEntities, javaClass.getMethods());

		for (JavaClass nested : javaClass.getNestedClasses())
			add(nested, allEntities);
	}

	protected static class LinesIterator implements Iterator<Integer> {

		final TokenAndBraceSnippetSelector tokenBraceSelector = new TokenAndBraceSnippetSelector();
		Iterator<Integer> sourceIterator;

		protected final AbstractSelectableJavaEntitiesList<AbstractJavaEntity> matchingEntities;
		protected final URL contentUrl;
		protected final String contentString;
		protected final boolean expandSnippets, foldCodeBlocks;

		protected LinesIterator(AbstractSelectableJavaEntitiesList<AbstractJavaEntity> matchingEntities,
		                        URL contentUrl, String contentString, boolean expandSnippets, boolean foldCodeBlocks) {
			this.matchingEntities = matchingEntities;
			this.contentString = contentString;
			this.expandSnippets = expandSnippets;
			this.contentUrl = contentUrl;
			this.foldCodeBlocks = foldCodeBlocks;
		}

		protected Iterator<Integer> getSourceIterator() {
			if (sourceIterator == null) {
				final Set<Integer> selectedLines = new TreeSet<Integer>();
				for (AbstractJavaEntity entity : matchingEntities) {
					try {
						selectLines(entity, selectedLines);

						if (expandSnippets) selectBeanProperty(entity, selectedLines);
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				}
				sourceIterator = selectedLines.iterator();
			}
			return sourceIterator;
		}

		protected void selectLines(AbstractJavaEntity entity, Set<Integer> selectedLines) throws IOException {
			if (entity == null) return;
			final int firstLine = entity.getLineNumber();

			if (foldCodeBlocks) {
				if (entity instanceof JavaField)
					selectedLines.add(firstLine);
				else
					selectDeclaration(firstLine, selectedLines);
			} else {
				if (entity instanceof JavaField) {
					for (int i = 0, lines = countLines(entity.getCodeBlock()); i < lines; i++)
						selectedLines.add(firstLine + i);
				} else {
					Iterator<Integer> iterator = tokenBraceSelector.selectSnippets("tb:[" + firstLine + "]{", contentUrl,
							new LineNumberReader(new StringReader(contentString)), singletonMap("case-sensitive", (Object) true));
					while (iterator.hasNext()) selectedLines.add(iterator.next());
				}
			}

			if (expandSnippets && entity.getComment() != null) {
				selectCommentAbove(firstLine, selectedLines);
			}
		}

		protected void selectDeclaration(int firstLine, Set<Integer> selectedLines) throws IOException {
			final LineNumberReader reader = new LineNumberReader(new StringReader(contentString));
			try {
				String line;
				while ((line = reader.readLine()) != null && (reader.getLineNumber() < firstLine || line.indexOf('{') == -1)) ;
				for (int number = firstLine, lastLine = Math.max(firstLine, reader.getLineNumber()); number <= lastLine; number++)
					selectedLines.add(number);
			} finally {
				reader.close();
			}
		}

		protected void selectCommentAbove(int firstLine, Set<Integer> selectedLines) throws IOException {
			String line;
			int lastCommentLineNumber = -1;
			final LineNumberReader reader = new LineNumberReader(new StringReader(contentString));
			try {
				while ((line = reader.readLine()) != null && reader.getLineNumber() < firstLine)
					if (line.trim().startsWith("/*")) lastCommentLineNumber = reader.getLineNumber();
			} finally {
				reader.close();
			}

			if (lastCommentLineNumber > -1) {
				for (int i = lastCommentLineNumber; i < firstLine; i++)
					selectedLines.add(i);
			}
		}

		protected int countLines(String text) {
			return new StringTokenizer(text, "\n").countTokens();
		}

		protected void selectBeanProperty(AbstractJavaEntity entity, Set<Integer> selectedLines) throws IOException {
			String propertyName = null;
			if (entity instanceof JavaField) {
				propertyName = entity.getName();
			} else if (entity instanceof JavaMethod) {
				propertyName = entity.getName();
				if ((propertyName.startsWith("set") || propertyName.startsWith("get")) && propertyName.length() > 3) {
					propertyName = propertyName.substring(3);
					propertyName = propertyName.length() == 4 ? propertyName.toLowerCase() :
							Character.toLowerCase(propertyName.charAt(0)) + propertyName.substring(1);
				}
			}

			if (propertyName != null) {
				BeanProperty beanProperty = entity.getParentClass().getBeanProperty(propertyName);
				if (beanProperty != null) {
					selectLines(beanProperty.getAccessor(), selectedLines);
					selectLines(beanProperty.getMutator(), selectedLines);
				}
			}
		}

		public boolean hasNext() {
			return getSourceIterator().hasNext();
		}

		public Integer next() {
			return getSourceIterator().next();
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}
