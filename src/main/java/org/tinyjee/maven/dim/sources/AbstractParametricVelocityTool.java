/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.tinyjee.maven.dim.spi.Globals;

import java.util.HashMap;
import java.util.Map;

/**
 * Is an abstract base for tools that
 *
 * @author Juergen_Kellerer, 2011-10-17
 */
public class AbstractParametricVelocityTool<E extends AbstractParametricVelocityTool> {

	// Thread-safe parameter store
	private final ThreadLocal<Map<String, String>> parameters = new ThreadLocal<Map<String, String>>() {
		@Override
		protected Map<String, String> initialValue() {
			return new HashMap<String, String>();
		}
	};

	/**
	 * Returns the current value of the internally stored parameter for the given key.
	 * <p/>
	 * If a parameter is not internally stored, the method retrieves the default value for the given key from the request parameters.
	 * If there's no default value, 'null' will be returned.
	 *
	 * @param key the name of the parameter to get.
	 * @return the value of the parameter, may be 'null' if the parameter does not exist.
	 */
	public String get(String key) {
		String value = parameters.get().get(key);
		if (value == null) {
			Object rawValue = Globals.getInstance().getRequest().getParameter(key);
			if (rawValue != null) value = String.valueOf(rawValue);
		}
		return value;
	}

	/**
	 * Sets a parameter that will be used with the next invocation.
	 *
	 * @param key   the name of the parameter to set.
	 * @param value the value to set.
	 * @return an instance of this tool to allow invocation chaining.
	 */
	@SuppressWarnings("unchecked")
	public E put(String key, String value) {
		parameters.get().put(key, value);
		return (E) this;
	}

	/**
	 * Removes a parameter form the internally maintained parameter list.
	 *
	 * @param key the name of the parameter to unset.
	 * @return an instance of this tool to allow invocation chaining.
	 */
	@SuppressWarnings("unchecked")
	public E remove(String key) {
		parameters.get().remove(key);
		return (E) this;
	}

	/**
	 * Clears all parameters form the internally maintained parameter list.
	 *
	 * @return an instance of this tool to allow invocation chaining.
	 */
	@SuppressWarnings("unchecked")
	public E clear() {
		parameters.get().clear();
		return (E) this;
	}

	/**
	 * Creates the next parameters to be used to invoke a command inside this tool.
	 *
	 * @return the next parameters to be used to invoke a command inside this tool.
	 */
	protected Map<String, String> nextInvocationParameters() {
		@SuppressWarnings("unchecked")
		Map<String, String> params = new HashMap<String, String>(Globals.getInstance().getRequest().getParameters());
		params.putAll(parameters.get());

		return params;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + '{' +
				"parameters=" + parameters +
				'}';
	}
}
