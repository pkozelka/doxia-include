/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.apache.velocity.tools.config.DefaultKey;

import java.util.*;

/**
 * Implements a custom velocity tool that provides a variables stack which may be used for recursive calls or
 * building dynamically increasing lists.
 * <p/>
 * Use for preserving variables in recursive calls:<code><pre>
 * #macro(myRecursiveMacro $input)
 *  $stack.push({"input": $input})
 *  ## ---
 *  #set($input = $math.add($input, 1))
 *  #if($input &lt; 32) #myRecursiveMacro($input) #end
 *  ## ---
 *  Counter inside method: $input
 *  Counter inside stack: $stack.get().input
 *  ## ---
 *  $stack.pop();
 * #end
 * #myRecursiveMacro(1)
 * </pre></code>
 * <p/>
 * Use for self extending lists:<code><pre>
 * $stack.push("first")
 * #foreach($element in $stack.iterate())
 *  Current element: $element
 *  #if($stack.size() &lt; 32) $stack.push("another #$stack.size()") #end
 * #end
 * </pre></code>
 *
 * @author Juergen_Kellerer, 2011-10-14
 */
@DefaultKey("stack")
public class VelocityVariablesStackTool {

	private static final String DEFAULT_STACK = "__default-stack";
	private final Map<String, List<Object>> stacks = new HashMap<String, java.util.List<Object>>();

	private List<Object> getStack(String name) {
		List<Object> stack = stacks.get(name);
		if (stack == null) stacks.put(name, stack = new ArrayList<Object>());

		return stack;
	}

	/**
	 * Adds the specified data on top of the default stack.
	 *
	 * @param data the data to add on top.
	 */
	public void push(Object data) {
		push(DEFAULT_STACK, data);
	}

	/**
	 * Adds the specified data on top of the named stack.
	 *
	 * @param name the name of the stack to use.
	 * @param data the data to add on top.
	 */
	public synchronized void push(String name, Object data) {
		getStack(name).add(data);
	}

	/**
	 * Gets the top data of the default stack or 'null' if there's no data in this stack.
	 *
	 * @return the top data of the default stack or 'null' if there's no data in this stack.
	 */
	public Object get() {
		return get(DEFAULT_STACK);
	}

	/**
	 * Gets the top data of the named stack or 'null' if there's no data in this stack.
	 *
	 * @param name the name of the stack to use.
	 * @return the top data of the named stack or 'null' if there's no data in this stack.
	 */
	public synchronized Object get(String name) {
		final List<Object> stack = getStack(name);
		return stack.isEmpty() ? null : stack.get(stack.size() - 1);
	}

	/**
	 * Removes the top data of the default stack.
	 */
	public void pop() {
		pop(DEFAULT_STACK);
	}

	/**
	 * Removes the top data of the named stack.
	 *
	 * @param name the name of the stack to use.
	 */
	public synchronized void pop(String name) {
		final List<Object> stack = getStack(name);
		if (!stack.isEmpty()) stack.remove(stack.size() - 1);
	}

	/**
	 * Returns the number of elements in the default stack.
	 *
	 * @return the number of elements in the default stack.
	 */
	public int size() {
		return size(DEFAULT_STACK);
	}

	/**
	 * Returns the number of elements in the named stack.
	 *
	 * @param name the name of the stack to use.
	 * @return the number of elements in the named stack.
	 */
	public synchronized int size(String name) {
		return getStack(name).size();
	}

	/**
	 * Iterates the default stack from bottom to top.
	 *
	 * @return a bottom to top iterable.
	 */
	public Iterable<Object> iterate() {
		return iterate(DEFAULT_STACK);
	}

	/**
	 * Iterates the named stack from bottom to top.
	 *
	 * @param name the name of the stack to use.
	 * @return a bottom to top iterable.
	 */
	public Iterable<Object> iterate(final String name) {
		return new Iterable<Object>() {
			public Iterator<Object> iterator() {
				return new Iterator<Object>() {

					int index;
					final List<Object> stack = getStack(name);

					public boolean hasNext() {
						return index < stack.size();
					}

					public Object next() {
						if (!hasNext()) throw new NoSuchElementException();
						return stack.get(index++);
					}

					public void remove() {
						throw new UnsupportedOperationException();
					}
				};
			}
		};
	}

	/**
	 * Iterates the default stack from top to bottom.
	 *
	 * @return a top to bottom iterable.
	 */
	public Iterable<Object> iterateParents() {
		return iterateParents(DEFAULT_STACK);
	}

	/**
	 * Iterates the named stack from top to bottom.
	 *
	 * @param name the name of the stack to use.
	 * @return a top to bottom iterable.
	 */
	public Iterable<Object> iterateParents(final String name) {
		return new Iterable<Object>() {
			public Iterator<Object> iterator() {
				return new Iterator<Object>() {

					final List<Object> stack = getStack(name);
					int index = stack.size();

					public boolean hasNext() {
						while (index >= stack.size()) index--;
						return index >= 0;
					}

					public Object next() {
						if (!hasNext()) throw new NoSuchElementException();
						return stack.get(index--);
					}

					public void remove() {
						throw new UnsupportedOperationException();
					}
				};
			}
		};
	}

	@Override
	public String toString() {
		return "VelocityVariablesStackTool{" +
				"stacks=" + stacks +
				'}';
	}
}
