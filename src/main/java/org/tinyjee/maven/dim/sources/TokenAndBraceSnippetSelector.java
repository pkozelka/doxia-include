/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.codehaus.plexus.util.StringUtils;
import org.tinyjee.maven.dim.spi.Globals;

import java.io.IOException;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import static java.lang.Boolean.parseBoolean;
import static java.lang.String.valueOf;
import static java.util.Collections.unmodifiableMap;
import static java.util.Locale.ENGLISH;

/**
 * Selects snippets using an activation token and open/close brace counting, using the expression prefix "<b>TB:</b>".
 * <p/>
 * This selector works with any C based language constructs of the form {@code Activation-TOKEN .. { ... content ...  }} as well as
 * other content that uses hierarchical braces of one of the supported formats (see below).
 * <p/>
 * Expression samples:<ul>
 * <li><code>tb:methodName{</code></li>
 * <li>{@code tb:activationToken[}</li>
 * <li>{@code tb:activationToken(}</li>
 * <li>{@code tb:activationToken<}</li>
 * <li>{@code tb:XmlCommentNextToThisToken<!--}</li>
 * <li>{@code tb:<!--}</li>
 * <li>{@code tb:/*}</li>
 * <li>{@code tb:#*}</li>
 * </ul>
 * <p/>
 * If the brace is omitted, the selector defaults to the braces <code>{}</code>.
 * <br/>
 * In case of only a brace but no token is specified, the selector matches all content that is surrounded by the given brace.
 * E.g. {@code <!--} matches all comments of an XML/HTML file.
 * <p/>
 * Activation tokens of the format {@code [line-number]} (e.g. {@code [132]}) activate the brace matching algorithm when
 * the given line number is reached instead of a string-match of the token is found.
 * <p/>
 * By default this selector works case-insensitive, unless {@code "case-sensitive=true"} is set.
 *
 * @author Juergen_Kellerer, 2011-10-16
 */
public class TokenAndBraceSnippetSelector extends AbstractStreamingSnippetSelector {

	public static final Map<String, String> BRACES;

	static {
		Map<String, String> braces = new LinkedHashMap<String, String>();
		braces.put("<!--", "-->");
		braces.put("/*", "*/");
		braces.put("#*", "*#");
		braces.put("[", "]");
		braces.put("(", ")");
		braces.put("<", ">");
		braces.put("{", "}"); // default must be last!
		BRACES = unmodifiableMap(braces);

	}

	public TokenAndBraceSnippetSelector() {
		this("tb:");
	}

	protected TokenAndBraceSnippetSelector(String expressionPrefix) {
		super(expressionPrefix);
	}

	public Iterator<Integer> selectSnippets(String expression, URL contentUrl, LineNumberReader content,
	                                        Map<String, Object> macroParameters) throws IOException {
		assertExpressionIsValid(expression, contentUrl, macroParameters);
		expression = stripPrefix(expression);
		final Map.Entry<String, String> braceStyle = findBraceStyle(expression);
		if (expression.endsWith(braceStyle.getKey()))
			expression = expression.substring(0, expression.length() - braceStyle.getKey().length());

		final int fixedLineNumber = extractFixedLineNumber(expression);
		final boolean caseSensitive = parseBoolean(valueOf(macroParameters.get(CASE_SENSITIVE)));

		return new AbstractStreamIterator<String>((caseSensitive ? expression : expression.toLowerCase(ENGLISH)), content) {
			int braceOpenCount;
			final String openingBrace = braceStyle.getKey();
			final String closingBrace = braceStyle.getValue();

			@Override
			protected boolean isCapturingLine(boolean alreadyCapturing, String expression, String line, int lineNumber) {
				if (alreadyCapturing) {
					if (braceOpenCount < 0) {
						braceOpenCount = 0;
						return false;
					}

					int openBraces = countTokens(line, openingBrace), closeBraces = countTokens(line, closingBrace);
					// Handle the case that the open brace is not in the same line.
					if (braceOpenCount == 0 && openBraces == 0) return true;

					braceOpenCount += openBraces - closeBraces;
					if (braceOpenCount <= 0) braceOpenCount = -1;
					return true;
				} else {
					boolean tokenIsPresent = fixedLineNumber == lineNumber || (
							StringUtils.isEmpty(expression) ? line.contains(openingBrace) :
									(caseSensitive ? line.contains(expression) : line.toLowerCase(ENGLISH).contains(expression))
					);
					return tokenIsPresent && isCapturingLine(true, expression, line, lineNumber);
				}
			}
		};
	}

	protected int extractFixedLineNumber(String expression) {
		if (expression.startsWith("[") && expression.endsWith("]")) {
			try {
				return Integer.parseInt(expression.substring(1, expression.length() - 1));
			} catch (NumberFormatException e) {
				Globals.getLog().warn("A line number token was used that contained an invalid numeric" +
						" expression: " + expression + ", will fallback to string based token processing.", e);
			}
		}
		return -1;
	}

	protected int countTokens(String line, String token) {
		int count = 0, position = -token.length();
		while ((position = line.indexOf(token, position + token.length())) > -1) count++;
		return count;
	}

	protected Map.Entry<String, String> findBraceStyle(String expression) {
		Map.Entry<String, String> defaultEntry = null;
		for (Map.Entry<String, String> entry : BRACES.entrySet()) {
			if (expression.endsWith(entry.getKey()))
				return entry;
			defaultEntry = entry;
		}
		return defaultEntry;
	}
}
