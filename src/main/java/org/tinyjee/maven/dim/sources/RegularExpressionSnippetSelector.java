/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.tinyjee.maven.dim.spi.Globals;

import java.io.IOException;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

import static java.lang.Boolean.parseBoolean;
import static java.lang.String.valueOf;
import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.UNICODE_CASE;

/**
 * Implements a simple {@link Pattern regular expression} based snippet selector using the expression prefix "<b>RE:</b>".
 * <p/>
 * Expression samples:<ul>
 * <li>{@code re:(INFO|WARN|ERROR)}</li>
 * <li>{@code %{include|source=build.log|snippet=RE:(?i)(INFO|WARN.*|ERROR)|snippet-grow-offset=5}}</li>
 * </ul>
 * <p/>
 * By default this selector works case-sensitive, unless {@code "case-sensitive=false"} is specified with the macro call
 * or the case in-sensitive flag "{@code (?i)}" is set in the regular expression (as used in the second example).
 *
 * @author Juergen_Kellerer, 2011-10-14
 */
public class RegularExpressionSnippetSelector extends AbstractStreamingSnippetSelector {

	/**
	 * Constructs a new regular expression based snippet selector.
	 */
	public RegularExpressionSnippetSelector() {
		this("re:");
	}

	protected RegularExpressionSnippetSelector(String expressionPrefix) {
		super(expressionPrefix);
	}

	protected Pattern compile(String expression, boolean caseSensitive) {
		try {
			String regex = stripPrefix(expression);
			return caseSensitive ? Pattern.compile(regex) : Pattern.compile(regex, CASE_INSENSITIVE | UNICODE_CASE);
		} catch (RuntimeException e) {
			Globals.getLog().error("Failed to parse regular expression pattern '" + expression + '\'', e);
			throw e;
		}
	}

	public Iterator<Integer> selectSnippets(String expression, URL contentUrl, LineNumberReader content,
	                                        Map<String, Object> macroParameters) throws IOException {
		assertExpressionIsValid(expression, contentUrl, macroParameters);

		Object rawCaseSensitive = macroParameters.get(CASE_SENSITIVE);
		final boolean caseSensitive = rawCaseSensitive == null || parseBoolean(valueOf(rawCaseSensitive));

		return new AbstractStreamIterator<Pattern>(compile(expression, caseSensitive), content) {
			@Override
			protected boolean isCapturingLine(boolean alreadyCapturing, Pattern expression, String line, int lineNumber) {
				return expression.matcher(line).find();
			}
		};
	}
}
