/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.maven.doxia.logging.Log;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.LogChute;
import org.apache.velocity.runtime.resource.Resource;
import org.apache.velocity.runtime.resource.loader.ResourceLoader;
import org.apache.velocity.tools.ToolContext;
import org.apache.velocity.tools.ToolManager;
import org.apache.velocity.tools.config.EasyFactoryConfiguration;
import org.codehaus.plexus.util.IOUtil;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.Source;
import org.tinyjee.maven.dim.spi.UrlFetcher;
import org.tinyjee.maven.dim.spi.VelocityConfigurator;
import org.tinyjee.maven.dim.utils.ReducedVerbosityMap;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.WeakHashMap;

import static org.apache.velocity.runtime.RuntimeConstants.RUNTIME_LOG_LOGSYSTEM;
import static org.apache.velocity.runtime.RuntimeConstants.VM_LIBRARY_DEFAULT;
import static org.codehaus.plexus.util.FileUtils.basename;
import static org.codehaus.plexus.util.IOUtil.copy;
import static org.tinyjee.maven.dim.DebugConstants.PRINT_NULL_REFERENCES;
import static org.tinyjee.maven.dim.DebugConstants.PRINT_VELOCITY;
import static org.tinyjee.maven.dim.spi.VelocityConfigurator.CONFIGURATORS;
import static org.tinyjee.maven.dim.utils.DebugUtils.dumpFileWithLineNumbers;
import static org.tinyjee.maven.dim.utils.DebugUtils.dumpReaderWithLineNumbers;

/**
 * Wraps a source content that contains a velocity template and executes it on using the Source interfaces.
 *
 * @author Juergen_Kellerer, 2010-09-04
 * @version 1.0
 */
public class TemplateSource extends AbstractBufferedSource {

	private static final Log log = Globals.getLog();

	static final String TEMPLATE_ENCODING = "UTF-8";
	static final String TEMPLATE_PARAM_SOURCE_URL = "sourceUrl";

	private static final ThreadLocal<VelocityEngine> engineEnvironment = new ThreadLocal<VelocityEngine>() {
		@Override
		protected VelocityEngine initialValue() {
			try {
				final VelocityEngine ve = new VelocityEngine();
				ve.setProperty(RUNTIME_LOG_LOGSYSTEM, new TemplateLogger());
				for (VelocityConfigurator configurator : CONFIGURATORS) configurator.configureEngine(ve);
				ve.init();

				return ve;
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	};

	private static final ThreadLocal<WeakHashMap<String, Template>> templateCache = new ThreadLocal<WeakHashMap<String, Template>>() {
		@Override
		protected WeakHashMap<String, Template> initialValue() {
			return new WeakHashMap<String, Template>();
		}
	};

	static final ToolManager VELOCITY_TOOL_MANAGER;

	static {
		EasyFactoryConfiguration config = new EasyFactoryConfiguration();
		for (VelocityConfigurator configurator : CONFIGURATORS) configurator.configureTools(config);
		config.autoLoad();

		ToolManager toolManager = new ToolManager(false, false);
		toolManager.configure(config);
		toolManager.setUserCanOverwriteTools(true);
		VELOCITY_TOOL_MANAGER = toolManager;
	}

	private static void deleteTemporaryFile(File file) {
		if (file != null && !file.delete()) {
			log.warn("Failed to delete temporary file: " + file + ", trying to delete on exit.");
			file.deleteOnExit();
		}
	}

	final Source source;
	final VelocityEngine engine = engineEnvironment.get();

	Template template;

	/**
	 * Constructs a new TemplateSource.
	 *
	 * @param sourceUrl the original source URL (used for naming purposes).
	 * @param source    the source containing the template source and context params.
	 */
	public TemplateSource(URL sourceUrl, Source source) {
		super(sourceUrl, null);
		this.source = source;
	}

	@Override
	protected void writeSourceContent(Writer writer) throws IOException {
		final Content sourceContent = source.getContent();
		parameters.putAll(sourceContent.getParameters());
		if (template == null) createTemplate(source, sourceContent);
		renderContent(writer);
	}

	private void renderContent(Writer writer) throws IOException {
		final ToolContext toolContext = VELOCITY_TOOL_MANAGER.createContext();
		if (sourceUrl != null) toolContext.put(TEMPLATE_PARAM_SOURCE_URL, sourceUrl);

		final Context context = new VelocityContext(parameters, toolContext);
		for (VelocityConfigurator configurator : CONFIGURATORS)
			configurator.configureContext(new ReducedVerbosityMap<String, Object>(parameters), context);

		template.merge(context, writer);
	}

	private void createTemplate(Source source, Content sourceContent) {
		final UrlSource urlSource = source instanceof UrlSource ? (UrlSource) source : null;
		File templateFile = null;
		try {
			String cacheKey, templateContent = IOUtil.toString(sourceContent.openReader());
			boolean staticTemplateFile = urlSource != null && !templateContent.contains("##import");

			cacheKey = templateContent;
			template = templateCache.get().get(cacheKey);

			if (template == null) {
				if (staticTemplateFile) {
					// Static template
					final String charset = UrlFetcher.getSourceCharset(urlSource.getUrl()).name();
					template = engine.getTemplate(urlSource.getUrl().toExternalForm(), charset);
				} else {
					// Dynamically built template...
					templateContent = applyImports(templateContent);
					templateFile = createTemporaryTemplateFile(templateContent);
					template = engine.getTemplate(templateFile.toURI().toASCIIString(), TEMPLATE_ENCODING);
				}

				if (PRINT_VELOCITY) {
					log.info("Successfully parsed template '" + (urlSource != null ? urlSource.getUrl() : templateFile) + '\'');
					dumpReaderWithLineNumbers(new StringReader(templateContent));
				}

				templateCache.get().put(cacheKey, template);
			} else {
				if (staticTemplateFile && template.isSourceModified()) template.process();
			}
		} catch (ParseErrorException e) {
			if (templateFile != null) {
				log.error("Failed parsing template '" + templateFile + '\'');
				dumpFileWithLineNumbers(templateFile, TEMPLATE_ENCODING);
			} else {
				log.error("Failed parsing template '" + (urlSource != null ? urlSource.getUrl() : null) + '\'');
			}
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			deleteTemporaryFile(templateFile);
		}
	}

	private static String applyImports(String templateContent) {
		int prevIndex = 0, index = 0;
		StringBuilder builder = null;
		do {
			index = templateContent.indexOf("##import", index);
			if (builder != null)
				builder.append(templateContent.substring(prevIndex, index == -1 ? templateContent.length() : index));

			if (index != -1) {
				if (builder == null) builder = new StringBuilder(templateContent.length()).append(templateContent.substring(prevIndex, index));

				prevIndex = index + 8;

				int rIndex = templateContent.indexOf('\r', index);
				index = templateContent.indexOf('\n', index);

				if (rIndex > -1 && rIndex < index) index = rIndex;
				else if (index == -1) index = templateContent.length();

				final String path = templateContent.substring(prevIndex, index).trim();
				try {
					builder.append(applyImports(Globals.getInstance().fetchText(path)));
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}

			prevIndex = index;

		} while (index != -1);

		return builder != null ? builder.toString() : templateContent;
	}

	private File createTemporaryTemplateFile(String templateContent) throws URISyntaxException, IOException {
		final boolean isLocalFilePath = sourceUrl != null && "file".equalsIgnoreCase(sourceUrl.getProtocol());
		File temporaryFolder = isLocalFilePath ? new File(sourceUrl.toURI()).getParentFile() : null;

		String temporaryNamePrefix = sourceUrl == null ? "inline-content-" : basename(new File(sourceUrl.getPath()).getName()) + '-';
		File templateFile = File.createTempFile(temporaryNamePrefix, ".vm", temporaryFolder);
		OutputStreamWriter output = new OutputStreamWriter(new FileOutputStream(templateFile), TEMPLATE_ENCODING);

		try {
			copy(templateContent, output);
		} finally {
			output.close();
		}

		return templateFile;
	}

	/**
	 * Resolves templates from Urls.
	 */
	public static class UrlResourceLoader extends ResourceLoader {
		@Override
		public void init(ExtendedProperties extendedProperties) {
		}

		@Override
		public InputStream getResourceStream(String sourceUrl) throws ResourceNotFoundException {
			try {
				return UrlFetcher.getSource(new URL(sourceUrl), false);
			} catch (Exception ignored) {
				try {
					return UrlFetcher.getSource(Globals.getInstance().resolvePath(sourceUrl), false);
				} catch (Exception e) {
					if (VM_LIBRARY_DEFAULT.equals(sourceUrl)) {
						log.debug("A global default VM library '" + sourceUrl + "' was not found. Will run without it.");
						return new ByteArrayInputStream(new byte[0]);
					}
					throw new ResourceNotFoundException(sourceUrl, e);
				}
			}
		}

		@Override
		public boolean isSourceModified(Resource resource) {
			try {
				return UrlFetcher.isModified(new URL(resource.getName()));
			} catch (MalformedURLException ignored) {
				return false;
			}
		}

		@Override
		public long getLastModified(Resource resource) {
			try {
				return UrlFetcher.getLastModified(new URL(resource.getName()));
			} catch (MalformedURLException ignored) {
				return 0;
			}
		}
	}

	/**
	 * Translates velocity logs to Maven/Doxia logs.
	 */
	private static class TemplateLogger implements LogChute {

		boolean logNullReference = PRINT_NULL_REFERENCES;

		public void init(RuntimeServices runtimeServices) throws Exception {
		}

		public void log(int level, String message) {
			log(level, message, null);
		}

		public void log(int level, String message, Throwable throwable) {
			if (message.startsWith("Null reference") && !logNullReference)
				return;

			switch (level) {
				case TRACE_ID:
				case DEBUG_ID:
					if (log.isDebugEnabled()) {
						if (throwable == null) log.debug(message);
						else log.debug(message, throwable);
					}
					break;
				case INFO_ID:
					// translating INFO without exception to DEBUG as Velocity is a bit to chatty and INFO-log is enabled by default.
					if (throwable == null) {
						if (log.isDebugEnabled()) log.debug(message);
					} else log.info(message, throwable);
					break;
				case WARN_ID:
					if (throwable == null) log.warn(message);
					else log.warn(message, throwable);
					break;
				case ERROR_ID:
					if (throwable == null) log.error(message);
					else log.error(message, throwable);
					break;
			}
		}

		public boolean isLevelEnabled(int level) {
			Log log = Globals.getLog();
			switch (level) {
				case TRACE_ID:
				case DEBUG_ID:
					return log.isDebugEnabled();
				case INFO_ID:
					return log.isInfoEnabled();
			}
			return true;
		}
	}
}
