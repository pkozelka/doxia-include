/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.apache.velocity.tools.config.EasyFactoryConfiguration;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.VelocityConfigurator;

import java.util.Map;

import static org.apache.velocity.runtime.RuntimeConstants.*;

/**
 * Is the default implementation for {@link VelocityConfigurator} which sets various defaults.
 *
 * @author Juergen_Kellerer, 2011-10-14
 */
public class DefaultVelocityConfigurator implements VelocityConfigurator {
	public void configureEngine(VelocityEngine engine) {
		// Register a custom resource loader that uses DIM's path / url resolve logic
		engine.setProperty(RESOURCE_LOADER, "dim-resource");
		engine.setProperty("dim-resource.resource.loader.class",
				"org.tinyjee.maven.dim.sources.TemplateSource$UrlResourceLoader");

		// Enable template-local macro definitions
		if (engine.getProperty(VM_PERM_ALLOW_INLINE) == null)
			engine.setProperty(VM_PERM_ALLOW_INLINE, true);
		if (engine.getProperty(VM_PERM_ALLOW_INLINE_REPLACE_GLOBAL) == null)
			engine.setProperty(VM_PERM_ALLOW_INLINE_REPLACE_GLOBAL, true);

		// Setup macro libraries
		String existingLibrary = (String) engine.getProperty(VM_LIBRARY);
		engine.setProperty(VM_LIBRARY, "classpath:/templates/dim/macro-library.vm," +
				VM_LIBRARY_DEFAULT + (existingLibrary == null ? "" : ',' + existingLibrary));
	}

	public void configureTools(EasyFactoryConfiguration toolsConfiguration) {
		toolsConfiguration.toolbox("application").
				tool("org.apache.velocity.tools.generic.AlternatorTool").
				tool("org.apache.velocity.tools.generic.ClassTool").
				tool("org.apache.velocity.tools.generic.ComparisonDateTool").
				tool("org.apache.velocity.tools.generic.ConversionTool").
				tool("org.apache.velocity.tools.generic.DateTool").
				tool("org.apache.velocity.tools.generic.DisplayTool").
				tool("org.apache.velocity.tools.generic.EscapeTool").
				tool("org.apache.velocity.tools.generic.FieldTool").
				tool("org.apache.velocity.tools.generic.MathTool").
				tool("org.apache.velocity.tools.generic.NumberTool").
				tool("org.apache.velocity.tools.generic.RenderTool").
				tool("org.apache.velocity.tools.generic.SortTool").
				tool("org.apache.velocity.tools.generic.ListTool").
				tool("org.apache.velocity.tools.generic.ResourceTool").
				tool("org.apache.velocity.tools.generic.MarkupTool").
				tool(VelocityHighlightingTool.class).
				tool(VelocitySourceClassInvocationTool.class).
				tool(VelocityVariablesStackTool.class);
	}

	public void configureContext(Map<String, Object> parameters, Context context) {
		// Note: The given parameters and tools configuration is already present, applying
		//       additional customizations:
		if (!context.containsKey("globals")) context.put("globals", Globals.getInstance());
		if (!context.containsKey("macroParameters")) context.put("macroParameters", parameters);
		if (!context.containsKey("context")) context.put("context", context);

		// 'sourceUrl' is set when the context is constructed and points to the actual template file,
		// there's no need to construct and set it here.
	}
}
