/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.apache.velocity.tools.config.DefaultKey;
import org.tinyjee.maven.dim.sh.CodeHighlighter;
import org.tinyjee.maven.dim.spi.SnippetExtractor;

import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.*;

/**
 * Implements a custom velocity tool that can be used to trigger code highlighting within templates.
 * <p/>
 * Usage within a template:<code><pre>
 * Highlighted content: $highlighter.highlight("public class MyClass {...}")
 * </pre></code>
 * <p/>
 * Quick Example - Embed the comment of a class with all code blocks (&lt;code&gt;&lt;pre&gt;) being highlighted:<code><pre>
 * %{include|source-content=$highlighter.highlightCodeBlocks($comment)|source-java=my.package.MyClass}
 * </pre></code>
 * <p/>
 * Reference:<ul>
 * <li><code>$highlighter.java("..java-code...")</code></li>
 * <li><code>$highlighter.js("..js-code...")</code></li>
 * <li><code>$highlighter.xml("..xml-code...")</code></li>
 * <li><code>$highlighter.css("..css-code...")</code></li>
 * <li><code>$highlighter.jsp("..jsp-code...")</code></li>
 * <li><code>$highlighter.highlight("..autodetect-code...")</code></li>
 * <li><code>$highlighter.highlight("..brush-name..e.g. 'java'", "..code...")</code></li>
 * <li><code>$highlighter.highlightCodeBlocks("Non-highlighted content.
 * &lt;code&gt;&lt;pre&gt;public class MyClass {...}&lt;/pre&gt;&lt;/code&gt;")</code></li>
 * <li><code>$highlighter.highlightCodeBlocks($comment)</code></li>
 * </ul>
 *
 * @author Juergen_Kellerer, 2011-10-09
 */
@DefaultKey("highlighter")
public class VelocityHighlightingTool extends AbstractParametricVelocityTool<VelocityHighlightingTool> {

	private static final Pattern[] codeBlocksPatterns = {
			compile("(<code>\\s*<pre>)(.+?)(</pre>\\s*</code>)", CASE_INSENSITIVE | MULTILINE | DOTALL),
			compile("(<pre>\\s*<code>)(.+?)(</code>\\s*</pre>)", CASE_INSENSITIVE | MULTILINE | DOTALL),
	};

	private final URL unknown;
	private CodeHighlighter highlighter;

	public VelocityHighlightingTool() throws MalformedURLException {
		unknown = new URL("file:///unknown");
	}

	private CodeHighlighter getHighlighter() {
		if (highlighter == null) highlighter = new CodeHighlighter();
		return highlighter;
	}

	private Map<String, String> nextInvocationParameters(Map<Integer, List<String>> content) {
		return getHighlighter().buildHighlighterParams(unknown, content, nextInvocationParameters());
	}

	/**
	 * Highlights the given code and attempts to auto-select the highlighting scheme.
	 *
	 * @param code the code to highlight.
	 * @return the highlighted code block using XHTML markup.
	 */
	public String highlight(String code) {
		return highlight(null, code);
	}

	/**
	 * Highlights the given code.
	 *
	 * @param brush a brush name to use for highlighting (= highlighting scheme)
	 * @param code  the code to highlight.
	 * @return the highlighted code block using XHTML markup.
	 */
	public String highlight(String brush, String code) {
		try {
			final CodeHighlighter codeHighlighter = getHighlighter();
			codeHighlighter.attachStylesheet(null, nextInvocationParameters());

			final Map<Integer, List<String>> content = SnippetExtractor.getInstance().convert(new StringReader(code));
			final Map<String, String> params = nextInvocationParameters(content);
			if (brush == null) brush = codeHighlighter.findBrushName(unknown, content, params);

			return codeHighlighter.highlight(brush, code, params);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Highlights code blocks (&lt;code&gt;) in the given XHTML content and attempts to auto-select the highlighting scheme.
	 *
	 * @param contentWithCodeBlocks the content with code blocks to highlight.
	 * @return the content with code blocks being highlighted.
	 */
	public String highlightCodeBlocks(String contentWithCodeBlocks) {
		return highlightCodeBlocks(null, contentWithCodeBlocks);
	}

	/**
	 * Highlights code blocks (&lt;code&gt;) in the given XHTML content.
	 *
	 * @param brush                 a brush name to use for highlighting (= highlighting scheme)
	 * @param contentWithCodeBlocks the content with code blocks to highlight.
	 * @return the content with code blocks being highlighted.
	 */
	public String highlightCodeBlocks(String brush, String contentWithCodeBlocks) {
		for (Pattern pattern : codeBlocksPatterns) {
			Matcher matcher = pattern.matcher(contentWithCodeBlocks);
			StringBuffer result = new StringBuffer(contentWithCodeBlocks.length() + 256);
			while (matcher.find())
				matcher.appendReplacement(result, highlight(brush, matcher.group(2)));
			matcher.appendTail(result);
			contentWithCodeBlocks = result.toString();
		}
		return contentWithCodeBlocks;
	}

	/**
	 * Shortcut to {@code highlight("java", code)}.
	 *
	 * @param code the code to highlight.
	 * @return the highlighted code.
	 */
	public String java(String code) {
		return highlight("java", code);
	}

	/**
	 * Shortcut to {@code highlight("js", code)}.
	 *
	 * @param code the code to highlight.
	 * @return the highlighted code.
	 */
	public String js(String code) {
		return highlight("js", code);
	}

	/**
	 * Shortcut to {@code highlight("css", code)}.
	 *
	 * @param code the code to highlight.
	 * @return the highlighted code.
	 */
	public String css(String code) {
		return highlight("css", code);
	}

	/**
	 * Shortcut to {@code highlight("xml", code)}.
	 *
	 * @param code the code to highlight.
	 * @return the highlighted code.
	 */
	public String xml(String code) {
		return highlight("xml", code);
	}

	/**
	 * Shortcut to {@code highlight("html/jsp", code)}.
	 *
	 * @param code the code to highlight.
	 * @return the highlighted code.
	 */
	public String jsp(String code) {
		return highlight("html/jsp", code);
	}
}
