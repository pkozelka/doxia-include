/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.codehaus.plexus.util.StringUtils;
import org.tinyjee.maven.dim.utils.CompositeIterator;

import java.io.IOException;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.*;

import static java.lang.Integer.parseInt;

/**
 * Implements a selector based on a line range definition to select snippets using the expression prefix "<b>lines:</b>".
 * <p/>
 * The primary purpose of this selector is to cut-off content that should not be in the output (e.g. using {@code !lines:100+}).
 * <p/>
 * Example expressions:<ul>
 * <li>'{@code lines:50-100}' (from 50 to 100)</li>
 * <li>'{@code lines:50+}' (from 50 to EOF)</li>
 * <li>'{@code lines:50-}' (from 1 to 50)</li>
 * <li>'{@code lines:1,3,10-12,1000+}' (selects 1, 3, 10, 11, 12 and 1000 to EOF)</li>
 * </ul>
 *
 * @author Juergen_Kellerer, 2011-10-16
 */
public class LineRangeSnippetSelector extends AbstractSnippetSelector {

	public LineRangeSnippetSelector() {
		this("lines:");
	}

	protected LineRangeSnippetSelector(String expressionPrefix) {
		super(expressionPrefix);
	}

	public Iterator<Integer> selectSnippets(String expression, URL contentUrl, LineNumberReader content,
	                                        Map<String, Object> macroParameters) throws IOException {
		assertExpressionIsValid(expression, contentUrl, macroParameters);
		expression = stripPrefix(expression).trim();

		int totalLines = -1;
		final List<IntegerIterator> iterators = new ArrayList<IntegerIterator>();
		for (String rangeExpressions : expression.split("\\s*[,;]+\\s*")) {
			final int firstLine, lastLine;

			if (rangeExpressions.endsWith("+")) {
				firstLine = parseInt(rangeExpressions.substring(0, rangeExpressions.length() - 1));
				lastLine = totalLines == -1 ? (totalLines = countTotalLines(content)) : totalLines;
			} else if (rangeExpressions.endsWith("-")) {
				firstLine = 1;
				lastLine = parseInt(rangeExpressions.substring(0, rangeExpressions.length() - 1));
			} else {
				String[] parts = rangeExpressions.split("-");
				if (parts.length > 2 || StringUtils.isEmpty(parts[0]) || (parts.length == 2 && StringUtils.isEmpty(parts[1]))) {
					throw new IllegalArgumentException("The given line number expression is invalid, use either 'START-END' or " +
							"'START-', 'START+', e.g. 'lines:25-84', 'lines:100+', 'lines:110-', etc.");
				}

				firstLine = parseInt(parts[0]);
				lastLine = parts.length == 1 ? firstLine : parseInt(parts[1]);
			}
			iterators.add(new IntegerIterator(firstLine, lastLine));
		}

		Collections.sort(iterators);

		@SuppressWarnings("unchecked")
		Iterator<Iterator<Integer>> iterator = (Iterator) iterators.iterator();
		return new CompositeIterator<Integer>(iterator);
	}

	protected int countTotalLines(LineNumberReader content) throws IOException {
		while (content.readLine() != null) ;
		return content.getLineNumber();
	}

	private static class IntegerIterator implements Iterator<Integer>, Comparable<IntegerIterator> {
		int lineNumber;
		private final int firstLine;
		private final int lastLine;

		private IntegerIterator(int firstLine, int lastLine) {
			this.firstLine = firstLine;
			this.lastLine = lastLine;
			lineNumber = firstLine;
		}

		public boolean hasNext() {
			return lineNumber <= lastLine;
		}

		public Integer next() {
			if (!hasNext()) throw new NoSuchElementException();
			try {
				return lineNumber;
			} finally {
				lineNumber++;
			}
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

		public int compareTo(IntegerIterator o) {
			return firstLine - o.firstLine;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (!(o instanceof IntegerIterator)) return false;
			IntegerIterator that = (IntegerIterator) o;
			return firstLine == that.firstLine && lastLine == that.lastLine;
		}

		@Override
		public int hashCode() {
			int result = firstLine;
			result = 31 * result + lastLine;
			return result;
		}
	}
}
