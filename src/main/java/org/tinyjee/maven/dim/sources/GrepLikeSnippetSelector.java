/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import java.io.IOException;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Boolean.parseBoolean;
import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;

/**
 * Implements a token based snippet selector working similar as the unix command 'grep' using the expression prefix "<b>grep:</b>".
 * <p/>
 * Expression samples:<ul>
 * <li>{@code grep:case-insensitive-token}</li>
 * <li>{@code grep:ERROR}</li>
 * <li>{@code %{include|source=build.log|snippet=GREP:Exception, GREP:org.tinyjee.dim|snippet-grow-offset=5}}</li>
 * <li>{@code %{include|source=build.log|snippet=GREP:Exception+3, GREP:org.tinyjee.dim+5}}</li>
 * </ul>
 * <p/>
 * By default this selector works case-insensitive, unless {@code "case-sensitive=true"} is set.
 * <p/>
 * The grep selector allows to define "{@code snippet-end-offset}" inline by adding "+lines" to the end of a statement.
 * In order to match "+5" use an escaped expression like "\+5".
 *
 * @author Juergen_Kellerer, 2011-10-14
 */
public class GrepLikeSnippetSelector extends AbstractStreamingSnippetSelector {

	private static final Pattern SNIPPET_END_OFFSET_PATTERN = Pattern.compile("(\\+[0-9]+)$");

	public GrepLikeSnippetSelector() {
		this("grep:");
	}

	protected GrepLikeSnippetSelector(String expressionPrefix) {
		super(expressionPrefix);
	}

	public Iterator<Integer> selectSnippets(String expression, URL contentUrl, LineNumberReader content,
	                                        Map<String, Object> macroParameters) throws IOException {
		assertExpressionIsValid(expression, contentUrl, macroParameters);
		final boolean caseSensitive = parseBoolean(valueOf(macroParameters.get(CASE_SENSITIVE)));
		expression = stripPrefix(expression);

		int endOffset = 0;
		final Matcher matcher = SNIPPET_END_OFFSET_PATTERN.matcher(expression);
		if (matcher.find()) {
			if (expression.charAt(matcher.start() - 1) == '\\')
				expression = expression.substring(0, matcher.start() - 1).concat(expression.substring(matcher.start()));
			else {
				expression = expression.substring(0, matcher.start());
				endOffset = parseInt(matcher.group(1).substring(1));
			}
		}

		final int addLines = endOffset;
		return new AbstractStreamIterator<String>(caseSensitive ? expression : expression.toLowerCase(), content) {
			int addCount = addLines;

			@Override
			protected boolean isCapturingLine(boolean alreadyCapturing, String expression, String line, int lineNumber) {
				boolean lineIsMatching = caseSensitive ? line.contains(expression) : line.toLowerCase().contains(expression);
				if (!lineIsMatching && alreadyCapturing && addCount > 0) {
					addCount--;
					return true;
				} else if (addCount == 0)
					addCount = addLines;
				return lineIsMatching;
			}
		};
	}
}
