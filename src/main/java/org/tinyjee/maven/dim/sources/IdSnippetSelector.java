/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import java.io.IOException;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/**
 * Implements the default ID based selector already known from the classic snippet macro that is part of Doxia.
 * <p/>
 * In difference to the snippet macro, IDs must be prefixed with "<b>#</b>" and are matched fully. Sub-id matching
 * is not allowed unless hierarchically built IDs use one of the defined delimiting chars {@link IdSnippetSelector#ID_DELIMITER_CHARS})
 * (e.g. "ParentId.ChildId").
 * <p/>
 * Snippets are processed on a line by line basis and multiple snippets with the same name may appear in one file.
 * In order to define a section using a snippet id, surround a block with:<br/>
 * <code>
 * // START SNIPPET: snippet-id<br/>
 * ... code to include ...<br/>
 * // END SNIPPET: snippet-id<br/>
 * </code>
 * Expression samples:<ul>
 * <li>{@code #snippet-id}</li>
 * <li>{@code #MySnippetIdToInclude}</li>
 * <li>{@code !#MySnippetIdToExclude}</li>
 * <li>{@code #FirstSnippet, #SecondSnippet, #ThirdSnippet}</li>
 * <li>{@code #ParentId} (Matches "ParentId" and "ParentId.[ANYTHING]")</li>
 * <li>{@code #ParentId.ChildId}</li>
 * </ul>
 * This selector is always case-insensitive.
 *
 * @author Juergen_Kellerer, 2011-10-14
 */
public class IdSnippetSelector extends AbstractStreamingSnippetSelector {

	/**
	 * Defines the chars that may surround an ID.
	 */
	public static final String ID_DELIMITER_CHARS = "\t .,;:=\"'";

	/**
	 * Constructs a new SnippetId selector.
	 */
	public IdSnippetSelector() {
		this("#");
	}

	protected IdSnippetSelector(String expressionPrefix) {
		super(expressionPrefix);
	}

	@Override
	public boolean canSelectSnippetsWith(String expression, URL contentUrl, Map<String, Object> macroParameters) {
		return super.canSelectSnippetsWith(expression, contentUrl, macroParameters) || expression.indexOf(' ') == -1;
	}

	public Iterator<Integer> selectSnippets(String expression, URL contentUrl, final LineNumberReader content,
	                                        Map<String, Object> macroParameters) throws IOException {
		assertExpressionIsValid(expression, contentUrl, macroParameters);

		return new AbstractStreamIterator<String>(stripPrefix(expression), content) {

			boolean previousLineWasStart;

			@Override
			protected boolean isCapturingLine(boolean alreadyCapturing, String snippetId, String line, int lineNumber) {
				if ((alreadyCapturing || previousLineWasStart) && isEnd(snippetId, line)) {
					previousLineWasStart = false;
					return false;
				}

				if (!alreadyCapturing) {
					try {
						alreadyCapturing = previousLineWasStart;
					} finally {
						previousLineWasStart = isStart(snippetId, line);
					}
				}

				return alreadyCapturing;
			}
		};
	}

	/**
	 * Determines if the given line is a start demarcator.
	 *
	 * @param snippetId the id of the snippet.
	 * @param line      the line.
	 * @return True, if the line is a start demarcator.
	 */

	protected boolean isStart(String snippetId, String line) {
		return isDemarcator(snippetId, "START", line);
	}

	/**
	 * Determines if the given line is a demarcator.
	 *
	 * @param snippetId the id of the snippet.
	 * @param what      Identifier for the demarcator.
	 * @param line      the line.
	 * @return True, if the line is a start demarcator.
	 */
	protected boolean isDemarcator(String snippetId, String what, String line) {
		final int idLength = snippetId.length(), snippetIdIndex = idLength == 0 ? 1 : line.indexOf(snippetId);
		if (snippetIdIndex > 0) {
			final boolean isValidSnippetId = (idLength == 0 || (
					(line.length() - snippetIdIndex == idLength ||
							ID_DELIMITER_CHARS.indexOf(line.charAt(snippetIdIndex + idLength)) != -1
					) && ID_DELIMITER_CHARS.indexOf(line.charAt(snippetIdIndex - 1)) != -1
			));

			final String upper = line.toUpperCase(Locale.ENGLISH);
			return isValidSnippetId && upper.contains(what.toUpperCase(Locale.ENGLISH)) && upper.contains("SNIPPET");
		} else
			return false;
	}

	/**
	 * Determines if the given line is an end demarcator.
	 *
	 * @param snippetId the id of the snippet.
	 * @param line      the line.
	 * @return True, if the line is an end demarcator.
	 */
	protected boolean isEnd(String snippetId, String line) {
		return isDemarcator(snippetId, "END", line);
	}
}
