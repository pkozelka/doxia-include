/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.tinyjee.maven.dim.utils.*;
import org.w3c.dom.Node;

import java.io.IOException;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.*;

import static java.lang.Boolean.parseBoolean;
import static java.lang.String.valueOf;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_SOURCE_CONTENT_TYPE;
import static org.tinyjee.maven.dim.utils.AbstractPositioningDocumentBuilder.getLineNumber;

/**
 * Implements a <a href="http://www.w3.org/TR/xpath/">XPath</a> expression based snippet selector supporting XML and
 * JSON formatted input, using the expression prefix "<b>XP:</b>".
 * <p/>
 * <b>Well formed content required</b>: This snippet selector requires well formed XML or JSON input. Anything else will cause failures.
 * <p/>
 * Example Expressions:<ul>
 * <li>{@code xp:/html/head/title}</li>
 * <li>{@code xp://title}</li>
 * <li>'{@code xp:/default:html/default:head}' (with namespace awareness enabled the prefix for the default XML-NS is 'default')</li>
 * </ul>
 * <p/>
 * Notes:<ul>
 * <li>XPath support for JSON is enabled if the source content's file extension is
 * "{@code .json}" or "{@code source-content-type=json}" is set. See {@link PositioningJsonDocumentBuilder json document builder}
 * for details how json translates to XML.</li>
 * <li>XML namespace awareness can be turned on by setting "{@code namespace-aware=true}" (does not exist in JSON).</li>
 * </ul>
 *
 * @author Juergen_Kellerer, 2011-10-14
 */
public class XPathSnippetSelector extends AbstractSnippetSelector {

	public static final String NAMESPACE_AWARE = "namespace-aware";

	public XPathSnippetSelector() {
		this("xp:");
	}

	protected XPathSnippetSelector(String expressionPrefix) {
		super(expressionPrefix);
	}

	public Iterator<Integer> selectSnippets(String expression, URL contentUrl, LineNumberReader content,
	                                        Map<String, Object> macroParameters) throws IOException {
		assertExpressionIsValid(expression, contentUrl, macroParameters);
		expression = stripPrefix(expression);

		boolean contentIsJson = contentUrl.getPath().toLowerCase().endsWith(".json") ||
				"json".equalsIgnoreCase(valueOf(macroParameters.get(PARAM_SOURCE_CONTENT_TYPE)));

		final AbstractPositioningDocumentBuilder documentBuilder = contentIsJson ? new PositioningJsonDocumentBuilder() :
				new PositioningDocumentBuilder(parseBoolean(valueOf(macroParameters.get(NAMESPACE_AWARE))));

		try {
			final Set<Integer> selectedLines = new TreeSet<Integer>();
			final XPathEvaluator evaluator = new XPathEvaluatorImplementation(documentBuilder.parse(contentUrl, content));

			for (Node node : evaluator.findNodes(expression)) {
				final int firstLine = getLineNumber(node), newLines = new StringTokenizer(evaluator.serialize(node), "\n").countTokens();
				for (int i = 0; i < newLines; i++)
					selectedLines.add(firstLine + i);
			}

			return selectedLines.iterator();
		} catch (IOException e) {
			throw e;
		} catch (Exception e) {
			IOException ioException = new IOException(e.getMessage());
			ioException.initCause(e);
			throw ioException; //NOSONAR - False positive, stack trace is preserved.
		}
	}
}
