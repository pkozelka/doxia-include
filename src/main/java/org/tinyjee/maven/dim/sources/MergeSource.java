/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.tinyjee.maven.dim.spi.Source;

import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Merges 2 sources to 1.
 *
 * @author Juergen_Kellerer, 2010-09-06
 */
public class MergeSource implements Source {

	final Source mainSource;
	final Source paramsSource;

	Content content;

	/**
	 * Constructs a merge source.
	 *
	 * @param mainSource   the source containing params and content.
	 * @param paramsSource the source containing only the params.
	 */
	public MergeSource(Source mainSource, Source paramsSource) {
		this.paramsSource = paramsSource;
		this.mainSource = mainSource;
	}

	/**
	 * {@inheritDoc}
	 */
	public URL getUrl() {
		URL u = mainSource.getUrl();
		return u == null ? paramsSource.getUrl() : u;
	}

	/**
	 * {@inheritDoc}
	 */
	public Content getContent() {
		if (content == null)
			content = new MergeContent(mainSource.getContent(), paramsSource.getContent());
		return content;
	}

	private static class MergeContent implements Content {

		final Content mainContent;
		final Content paramsContent;

		MergeContent(Content mainContent, Content paramsContent) {
			this.mainContent = mainContent;
			this.paramsContent = paramsContent;
		}

		/**
		 * {@inheritDoc}
		 */
		public Map<String, Object> getParameters() {
			Map<String, Object> p, merged = new HashMap<String, Object>();

			p = mainContent.getParameters();
			if (p != null) merged.putAll(p);

			p = paramsContent.getParameters();
			if (p != null) merged.putAll(p);

			return merged;
		}

		public Reader openReader() throws IOException {
			return mainContent.openReader();
		}
	}
}
