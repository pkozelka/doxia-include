/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.sources;

import org.tinyjee.maven.dim.spi.SnippetSelector;

import java.net.URL;
import java.util.Map;

import static java.util.Locale.ENGLISH;

/**
 * Implements an abstract base for snippet selectors that operate on a single expression prefix.
 *
 * @author Juergen_Kellerer, 2011-10-14
 */
public abstract class AbstractSnippetSelector implements SnippetSelector {

	/**
	 * The prefix to use for detecting expressions belonging to this selector.
	 */
	protected final String expressionPrefix;

	/**
	 * Constructs a new instance with the given expression-prefix.
	 *
	 * @param expressionPrefix the prefix to use for detecting expressions belonging to this selector.
	 */
	protected AbstractSnippetSelector(String expressionPrefix) {
		this.expressionPrefix = expressionPrefix.toLowerCase(ENGLISH);
	}

	public String[] getExpressionPrefixes() {
		return new String[]{expressionPrefix};
	}

	public boolean canSelectSnippetsWith(String expression, URL contentUrl, Map<String, Object> macroParameters) {
		return expression.toLowerCase(ENGLISH).startsWith(expressionPrefix);
	}

	/**
	 * Fails if the given expression cannot be used to select snippets.
	 *
	 * @param expression      the expression to test.
	 * @param contentUrl      the url to the content.
	 * @param macroParameters the parameters given with the macro call.
	 */
	protected void assertExpressionIsValid(String expression, URL contentUrl, Map<String, Object> macroParameters) {
		if (!canSelectSnippetsWith(expression, contentUrl, macroParameters))
			throw new IllegalStateException("Cannot select snippets for " + contentUrl + " using an expression of '" + expression + '\'');
	}

	/**
	 * Strips the prefix form the given snippet selection expression.
	 *
	 * @param expression the expression to process.
	 * @return a snippet selection expression without the prefix.
	 */
	protected String stripPrefix(String expression) {
		return expression.toLowerCase(ENGLISH).startsWith(expressionPrefix) ? expression.substring(expressionPrefix.length()) : expression;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + '{' +
				"expressionPrefix='" + expressionPrefix + '\'' +
				'}';
	}
}
