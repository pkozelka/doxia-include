/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim;

import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.model.Build;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Resource;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.util.*;

import static java.lang.String.valueOf;
import static java.lang.System.*;
import static org.codehaus.plexus.util.StringUtils.join;
import static org.tinyjee.maven.dim.spi.ResourceResolver.setModulePath;

/**
 * Initializes paths that are used by the include macro and allows to perform adjust some general settings.
 *
 * @author juergen_kellerer, 2011-01-27
 * @goal initialize
 * @inheritByDefault true
 * @executionStrategy always
 * @phase pre-site
 * @requiresDependencyResolution test
 * @threadSafe false
 */
public class InitializeMacroMojo extends AbstractMojo {

	/**
	 * Readonly don't try to set it.
	 *
	 * @parameter default-value="${project.basedir}"
	 * @required
	 * @readonly
	 */
	private File basedir;

	/**
	 * Readonly don't try to set it.
	 *
	 * @parameter default-value="${project}"
	 * @required
	 * @readonly
	 */
	private MavenProject project;

	/**
	 * Toggles the export of additional project paths to the MavenProject- and system
	 * properties that allow to use the following additional constructs to reference content:
	 * <p/>
	 * Supported anywhere in the build:<ul>
	 * <li>${dim.toproject.basedir}/path/to/project-root-element</li>
	 * <li>${dim.toproject.baseUri}/file-uri/to/project-root-element</li>
	 * <li>${dim.artifactId.basedir}/path/to/module-file</li>
	 * <li>${dim.artifactId.baseUri}/file-uri/to/module-file</li>
	 * <li>${dim.groupId_artifactId.basedir}/path/to/module-file</li>
	 * <li>${dim.groupId_artifactId.baseUri}/file-uri/to/module-file</li>
	 * </ul>
	 * <p/>
	 * Supported when using the include macro:<ul>
	 * <li>%{include|source=[top]:/path/to/project-root-element}</li>
	 * <li>%{include|source=[artifactId]:/path/to/module-file}</li>
	 * <li>%{include|source=[groupId:artifactId]:/path/to/module-file}</li>
	 * </ul>
	 *
	 * @parameter default-value="true"
	 */
	private boolean exportAdditionalProjectPaths = true;

	/**
	 * Toggles whether the project classpath should be exported and re-used for 'source-class' parameters.
	 * <p/>
	 * When 'true', source classes run under the combined runtime / test classpath as defined inside the
	 * maven dependencies.
	 *
	 * @parameter default-value="true"
	 */
	private boolean exportProjectClasspath = true;


	/**
	 * Specifies the default charset to use when reading input (may be overridden with the macro call).
	 *
	 * @parameter default-value="${project.build.sourceEncoding}"
	 */
	private String inputEncoding;

	/**
	 * Directory containing the 'site.xml' file and the source for apt, fml and xdoc docs.
	 *
	 * @parameter default-value="${project.basedir}/src/site"
	 */
	private String siteDirectory;

	/**
	 * Relative path to a directory where 'include' macro places any attached files like CSS, scripts or
	 * other sort of attachments that may be created through "<code>$globals.attachContent(...)</code>".
	 *
	 * @parameter default-value="attached-includes"
	 */
	private String attachedIncludesDirectory = "attached-includes";

	/**
	 * Toggles whether attached includes are directly created in the build directory (<code>target/site</code>).
	 * <p/>
	 * By default attached includes are created inside site sources using a path that is constructed via
	 * <code>${siteDirectory}/resources/${attachedIncludesDirectory}</code>. This is the most compatible way
	 * of attaching files (as it is supported with all goals defined in the site plugin) but it has the drawback
	 * that it requires an exclusion rule with VCS systems to avoid that the specified folder is checked-in.
	 * <p/>
	 * Setting this property to true changes the path creation strategy with attached includes to
	 * <code>${build.directory}/site/${attachedIncludesDirectory}</code>.
	 * <br/>
	 * Please note that site goals like "site:run" and "site:staging" will have issues with this mode.
	 *
	 * @parameter default-value="false"
	 */
	private boolean generateAttachedIncludesInBuildDirectory;

	/**
	 * Toggles whether the gutter is shown by default for included verbatim content.
	 *
	 * @parameter default-value="true"
	 */
	private boolean defaultShowGutter = true;

	/**
	 * Toggles whether the macro creates verbose output.
	 * <p/>
	 * You can toggle verbose output from the commandline using <code>-Ddim.verbose=true</code>
	 *
	 * @parameter default-value="${dim.verbose}"
	 */
	private boolean verbose;

	/**
	 * Property-presets can be used withing a Macro call via "preset=name". <b>Example Usage</b>:<pre>
	 * &lt;presets&gt;
	 *   &lt;preset&gt;
	 *     &lt;name&gt;plain-box&lt;/name&gt;
	 *     &lt;properties&gt;
	 *       &lt;verbatim&gt;true&lt;/verbatim&gt;
	 *       &lt;show-gutter&gt;false&lt;/show-gutter&gt;
	 *     &lt;/properties&gt;
	 *   &lt;/preset&gt;
	 * &lt;/presets&gt;
	 * </pre>
	 * The defined preset 'plain-box' is used with the following macro call:
	 * <pre>%{include|preset=plain-box|...</pre>
	 * Which is then similar to:
	 * <pre>%{include|verbatim=true|show-gutter=false|...</pre>
	 * <p/>
	 * <b>Default settings:</b> Preset names can be chosen freely, however the name "<code>default</code>" is
	 * preserved. When used, the properties defined inside the <i>default preset</i> are applied to <b>ALL</b>
	 * macro calls below the module that defined the preset, thus the following example definition sets
	 * the highlighting theme for all includes with verbatim content:
	 * <pre>
	 * &lt;presets&gt;
	 *   &lt;preset&gt;
	 *     &lt;name&gt;default&lt;/name&gt;
	 *     &lt;properties&gt;
	 *       &lt;highlight-theme&gt;eclipse&lt;/highlight-theme&gt;
	 *     &lt;/properties&gt;
	 *   &lt;/preset&gt;
	 * &lt;/presets&gt;
	 * </pre>
	 * <p/>
	 * <b>Aliasing:</b> Presets also define an additional flag that allows the definition of an alias which may
	 * be used to shortcut calls like <code>"preset=my-preset|somekey=value"</code> to <code>"my-preset=value"</code>
	 * <pre>
	 * &lt;presets&gt;
	 *   &lt;preset&gt;
	 *     &lt;name&gt;<b>my-preset</b>&lt;/name&gt;
	 *     <b>&lt;alias&gt;true&lt;/alias&gt;</b>
	 *     &lt;properties&gt;
	 *       &lt;somekey&gt;<b>${value}</b>&lt;/somekey&gt;
	 *     &lt;/properties&gt;
	 *   &lt;/preset&gt;
	 * &lt;/presets&gt;
	 * </pre>
	 * Note: "${value}" is dynamically replaced with the value used with the alias.
	 *
	 * @parameter
	 */
	private MacroPreset[] presets;

	/**
	 * Resets any project specific adjustments that break path resolution when they remain active on a
	 * POM that did not call the this Mojo.
	 */
	public static void reset() {
		Properties properties = getProperties();

		final String previousBaseDir = getProperty("org.tinyjee.maven.dim.previous.basedir");
		if (previousBaseDir != null) setProperty("basedir", previousBaseDir);

		for (Iterator<Object> iterator = properties.keySet().iterator(); iterator.hasNext(); ) {
			String key = (String) iterator.next();
			if (key.startsWith("org.tinyjee.maven.dim.project.")) iterator.remove();
		}
	}

	public void execute() throws MojoExecutionException, MojoFailureException {
		getLog().debug("Setting system property 'basedir' to " + basedir);
		setProperty("org.tinyjee.maven.dim.previous.basedir", getProperty("basedir", ""));
		setProperty("basedir", basedir.getAbsolutePath());

		// Note: This Mojo and the Macro won't run in the same Classloader, thus using system properties is the easiest way
		//       to exchange information with each other without adding any other dependencies (static fields are bound to CLs).

		if (presets != null) {
			for (MacroPreset preset : presets) preset.encodeTo(getProperties());
		}

		setProperty("dim.verbose", valueOf(verbose));
		setProperty("org.tinyjee.maven.dim.defaultShowGutter", valueOf(defaultShowGutter));
		setProperty("org.tinyjee.maven.dim.attachedIncludesDirectory", attachedIncludesDirectory);
		setProperty("org.tinyjee.maven.dim.generateAttachedIncludesInBuildDirectory", valueOf(generateAttachedIncludesInBuildDirectory));

		setProperty("org.tinyjee.maven.dim.project.inputEncoding", inputEncoding);
		setProperty("org.tinyjee.maven.dim.project.siteDirectory", siteDirectory);

		Build build = project.getBuild();
		setProperty("org.tinyjee.maven.dim.project.scriptSourceDirectory", build.getScriptSourceDirectory());
		setProperty("org.tinyjee.maven.dim.project.sourceDirectory", build.getSourceDirectory());
		setProperty("org.tinyjee.maven.dim.project.resourceDirectories", getResourcePaths(build.getResources()));
		setProperty("org.tinyjee.maven.dim.project.outputDirectory", build.getOutputDirectory());
		setProperty("org.tinyjee.maven.dim.project.testSourceDirectory", build.getTestSourceDirectory());
		setProperty("org.tinyjee.maven.dim.project.testResourceDirectories", getResourcePaths(build.getTestResources()));
		setProperty("org.tinyjee.maven.dim.project.testOutputDirectory", build.getTestOutputDirectory());
		setProperty("org.tinyjee.maven.dim.project.targetDirectory", build.getDirectory());

		if (exportProjectClasspath) {
			getLog().debug("Exporting the project class path.");
			try {
				exportProjectClassPath();
			} catch (DependencyResolutionRequiredException ignored) {
				getLog().warn("Failed exporting 'project.classpath' and 'project.test.classpath' as artifacts require resolving. " +
						"'doxia include macro' may fail on a 'source-class' that requires any non-JDK dependencies.");
			}
		}

		if (exportAdditionalProjectPaths) {
			getLog().debug("Exporting additional module paths to allow artifactId based path definitions.");
			HashSet<MavenProject> alreadyHandled = new HashSet<MavenProject>();
			for (MavenProject p = project; p != null && p.getBasedir() != null; p = p.getParent())
				exportProjectPaths(p, alreadyHandled);
		}
	}

	@SuppressWarnings("unchecked")
	private void exportProjectClassPath() throws DependencyResolutionRequiredException {
		Set<String> classPath = new LinkedHashSet<String>();
		classPath.addAll(project.getCompileClasspathElements());
		classPath.addAll(project.getRuntimeClasspathElements());
		setProperty("org.tinyjee.maven.dim.include.project.classpath", join(classPath.iterator(), File.pathSeparator));

		classPath.clear();
		classPath.addAll(project.getTestClasspathElements());
		setProperty("org.tinyjee.maven.dim.include.project.test.classpath", join(classPath.iterator(), File.pathSeparator));
	}

	private static void exportProjectPaths(MavenProject project, Set<MavenProject> alreadyHandled) {
		final File baseDir = project == null ? null : project.getBasedir();
		if (baseDir == null || alreadyHandled.contains(project)) return;

		final String baseUri = baseDir.toURI().toString();
		final String groupId = project.getGroupId();
		final String artifactId = project.getArtifactId();

		final boolean isTopMostProject = project.getParent() == null || project.getParent().getBasedir() == null;

		List dependencies = new ArrayList();
		dependencies.addAll(project.getDependencies());
		dependencies.addAll(project.getTestDependencies());

		if (isTopMostProject) setModulePath("top", "top", baseDir, dependencies);
		setModulePath(groupId, artifactId, baseDir, dependencies);

		List<Properties> allProperties = Arrays.asList(project.getProperties(), getProperties());
		for (Properties properties : allProperties) {
			properties.setProperty("dim." + groupId + '_' + artifactId + ".basedir", baseDir.toString());
			properties.setProperty("dim." + groupId + '_' + artifactId + ".baseUri", baseUri);
			properties.setProperty("dim." + artifactId + ".basedir", baseDir.toString());
			properties.setProperty("dim." + artifactId + ".baseUri", baseUri);
			if (isTopMostProject) {
				properties.setProperty("dim.top.basedir", baseDir.toString());
				properties.setProperty("dim.top.baseUri", baseUri);
			}
		}

		alreadyHandled.add(project);

		if (project.getCollectedProjects() != null) {
			for (Object projectReference : project.getCollectedProjects())
				exportProjectPaths((MavenProject) projectReference, alreadyHandled);
		}
	}

	private static String getResourcePaths(List resources) {
		List<String> paths = new ArrayList<String>();
		for (Object rawResource : resources) {
			Resource resource = (Resource) rawResource;
			paths.add(resource.getDirectory());
		}
		return join(paths.iterator(), File.pathSeparator);
	}

	public boolean isExportAdditionalProjectPaths() {
		return exportAdditionalProjectPaths;
	}

	public boolean isExportProjectClasspath() {
		return exportProjectClasspath;
	}

	public String getInputEncoding() {
		return inputEncoding;
	}

	public String getSiteDirectory() {
		return siteDirectory;
	}

	public String getAttachedIncludesDirectory() {
		return attachedIncludesDirectory;
	}

	public boolean isGenerateAttachedIncludesInBuildDirectory() {
		return generateAttachedIncludesInBuildDirectory;
	}

	public boolean isDefaultShowGutter() {
		return defaultShowGutter;
	}

	public boolean isVerbose() {
		return verbose;
	}

	public MacroPreset[] getPresets() {
		return presets == null ? new MacroPreset[0] : presets.clone();
	}
}
