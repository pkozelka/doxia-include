/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * Resets project specific paths that were previously set with 'initialize'.
 * <p/>
 * Using this mojo is optional but required in case of 'initialize' is not everywhere in the build but just for specific artifacts.
 *
 * @author Juergen_Kellerer, 2011-10-27
 * @goal reset
 * @inheritByDefault true
 * @executionStrategy always
 * @phase post-site
 * @threadSafe false
 */
public class ResetMojo extends AbstractMojo {
	public void execute() throws MojoExecutionException, MojoFailureException {
		InitializeMacroMojo.reset();
	}
}
