/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.spi.Globals;

import java.io.*;

/**
 * Collection of utilities to format output that is required to debug errors.
 *
 * @author juergen kellerer, 2012-07-08
 */
public class DebugUtils {

	private static final Log log = Globals.getLog();

	public static void dumpFileWithLineNumbers(File file, String charset) {
		try {
			dumpReaderWithLineNumbers(new InputStreamReader(new FileInputStream(file), charset));
		} catch (IOException e) {
			log.error("Failed dumping '" + file + "'.", e);
		}
	}

	public static void dumpReaderWithLineNumbers(Reader content) {
		try {
			LineNumberReader reader = new LineNumberReader(content);
			try {
				String line;
				while ((line = reader.readLine()) != null)
					System.out.printf("%03d:%s%n", reader.getLineNumber(), line);
			} finally {
				reader.close();
			}
		} catch (IOException e) {
			log.error(e);
		}
	}
}
