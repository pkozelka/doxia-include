/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

/**
 * Clones the contents that are written to this stream to all specified targets.
 *
 * @author Juergen_Kellerer, 2012-06-02
 */
public class CloningOutputStream extends OutputStream {

	private final OutputStream[] targets;

	/**
	 * Creates a new stream that clones everything that it receives to the specified targets.
	 *
	 * @param targets the clone targets.
	 */
	public CloningOutputStream(OutputStream... targets) {
		this.targets = targets;
	}

	@Override
	public void write(int singleByte) throws IOException {
		for (OutputStream target : targets) target.write(singleByte);
	}

	@Override
	public void write(byte[] bytes) throws IOException {
		for (OutputStream target : targets) target.write(bytes);
	}

	@Override
	public void write(byte[] bytes, int off, int len) throws IOException {
		for (OutputStream target : targets) target.write(bytes, off, len);
	}

	@Override
	public void close() throws IOException {
		IOException last = null;
		for (OutputStream target : targets) {
			try {
				target.close();
			} catch (IOException e) {
				last = e;
			}
		}
		if (last != null) throw last;
	}

	@Override
	public void flush() throws IOException {
		IOException last = null;
		for (OutputStream target : targets) {
			try {
				target.flush();
			} catch (IOException e) {
				last = e;
			}
		}
		if (last != null) throw last;
	}

	@Override
	public String toString() {
		return "CloningOutputStream{" +
				"targets=" + (targets == null ? null : Arrays.asList(targets)) +
				'}';
	}
}
