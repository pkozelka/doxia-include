/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.AbstractList;

/**
 * Adapts NodeList to Java Collections.
 *
 * @author Juergen_Kellerer, 2011-10-30
 */
public class NodeListAdapter extends AbstractList<Node> {

	private final NodeList nodeList;

	public NodeListAdapter(NodeList nodeList) {
		this.nodeList = nodeList;
	}

	@Override
	public Node get(int index) {
		return nodeList.item(index);
	}

	@Override
	public int size() {
		return nodeList == null ? 0 : nodeList.getLength();
	}
}
