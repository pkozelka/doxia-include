/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.tinyjee.maven.dim.spi.ExtensionInformation;
import org.tinyjee.maven.dim.spi.Globals;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Implements a linked hash map that has a reduced toString representation by filtering out some keys and values.
 *
 * @author Juergen_Kellerer, 2012-06-03
 */
public class ReducedVerbosityMap<K, V> extends LinkedHashMap<K, V> {

	private static final long serialVersionUID = 4833295279529710519L;

	private static final String KEY_SOURCE_CONTENT = "sourceContent";
	private static final int MAX_STRING_SIZE = 120;

	public static final String ABBREVIATED_SUFFIX = "..<truncated>";

	private final boolean debug = Globals.getLog().isDebugEnabled();

	public ReducedVerbosityMap() {
	}

	public ReducedVerbosityMap(Map<? extends K, ? extends V> map) {
		super(map);
	}

	@Override
	public String toString() {
		final Iterator<Map.Entry<K, V>> iterator = entrySet().iterator();
		if (!iterator.hasNext()) return "{}";

		final StringBuilder builder = new StringBuilder().append('{');
		for (; ; ) {
			Map.Entry<K, V> e = iterator.next();
			K key = e.getKey();
			V value = e.getValue();
			builder.append(key == this ? "(this Map)" : key);
			builder.append('=').append(value == this ? "(this Map)" :
					isValueToAbbreviate(key, value) ? abbreviate(value, MAX_STRING_SIZE, ABBREVIATED_SUFFIX) : value);
			if (!iterator.hasNext())
				return builder.append('}').toString();
			builder.append(", ");
		}
	}

	private String abbreviate(V value, int maxSize, String appendWhenAbbreviated) {
		ExtensionInformation extension = value == null ? null : value.getClass().getAnnotation(ExtensionInformation.class);
		if (extension != null) {
			return value.getClass().getName() + " { " + extension.toString() + " } ";
		} else {
			String stringValue = String.valueOf(value);
			if (stringValue.length() <= maxSize) return stringValue;

			int newLength = Math.max(0, maxSize - appendWhenAbbreviated.length());
			return stringValue.substring(0, newLength) + appendWhenAbbreviated;
		}
	}

	private boolean isValueToAbbreviate(K key, V value) {
		if (debug) return false;

		return KEY_SOURCE_CONTENT.equals(key) ||
				value instanceof String ||
				(value != null && value.getClass().isAnnotationPresent(ExtensionInformation.class));
	}
}
