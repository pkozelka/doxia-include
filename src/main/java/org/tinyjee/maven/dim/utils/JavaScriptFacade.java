/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.mozilla.javascript.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Implements a common base class for javascript facades that simplify the usage of the Rhino JavaScript interpreter.
 * <p/>
 * Note: Non of the facades is thread safe nor sharable between threads.
 *
 * @author Juergen_Kellerer, 2010-09-03
 */
public class JavaScriptFacade {

	protected static final Charset SCRIPT_ENCODING = Charset.forName("UTF-8");

	protected ScriptableObject scope;
	protected final ContextFactory contextFactory = new ContextFactory() {
		@Override
		protected Context makeContext() {
			Context context = super.makeContext();
			context.setOptimizationLevel(9);
			return context;
		}
	};

	/**
	 * Constructs a new JavaScriptFacade hosing a thread-bound script Context.
	 */
	protected JavaScriptFacade() {
		scope = (ScriptableObject) contextFactory.call(new ContextAction() {
			public Object run(Context context) {
				return context.initStandardObjects();
			}
		});
	}

	protected void close() {
	}

	/**
	 * Creates a JavaScript object.
	 *
	 * @param values the key, value pairs forming the JavaScript object.
	 * @return a JavaScript object.
	 */
	protected Scriptable createObject(final Map<String, Object> values) {
		return (Scriptable) contextFactory.call(new ContextAction() {
			public Object run(Context context) {
				Scriptable object = context.newObject(scope);
				for (Map.Entry<String, Object> entry : values.entrySet())
					object.put(entry.getKey(), object, entry.getValue());
				return object;
			}
		});
	}

	/**
	 * Compiles the given script or script bundle.
	 *
	 * @param resource an URL pointing to the JS or ZIP file.
	 * @param scripts  the list of scripts to add the compiled scripts to.
	 * @throws IOException in case of reading the URL content failed.
	 */
	protected void compileScript(URL resource, List<Script> scripts) throws IOException {
		if (resource.getFile().endsWith(".zip")) {
			ZipInputStream zIn = new ZipInputStream(resource.openStream());
			try {
				compileScripts(zIn, scripts);
			} finally {
				zIn.close();
			}
		} else
			scripts.add(compileScript(resource.getFile(), resource.openStream(), true));
	}

	/**
	 * Compiles all JS scripts contained in the given ZIP file.
	 *
	 * @param zIn     the ZipInputStream containing the scripts.
	 * @param scripts the list of scripts to add the compiled scripts to.
	 * @throws IOException in case of reading the ZIP content failed.
	 */
	protected void compileScripts(ZipInputStream zIn, List<Script> scripts) throws IOException {
		try {
			for (ZipEntry ze = zIn.getNextEntry(); ze != null; ze = zIn.getNextEntry()) {
				if (ze.getName().endsWith(".js"))
					scripts.add(compileScript(ze.getName(), zIn, false));
			}
		} finally {
			zIn.close();
		}
	}

	/**
	 * Compiles the given JS script contained in the stream and returns the compiled version.
	 *
	 * @param name  the name of the script source (used when compile errors are contained).
	 * @param in    the input stream containing the script.
	 * @param close whether the stream is closed or not at the end.
	 * @return the compiled Script instance.
	 * @throws IOException In case of reading the stream failed.
	 */
	protected Script compileScript(final String name, InputStream in, boolean close) throws IOException {
		final InputStreamReader streamReader = new InputStreamReader(in, SCRIPT_ENCODING);
		try {
			return (Script) contextFactory.call(new ContextAction() {
				public Object run(Context context) {
					try {
						return context.compileReader(streamReader, name, 0, null);
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				}
			});
		} catch (RuntimeException e) {
			if (e.getCause() instanceof IOException) throw (IOException) e.getCause();
			throw e;
		} finally {
			if (close) streamReader.close();
		}
	}

	/**
	 * Executes the script.
	 *
	 * @param script the script to execute.
	 * @return the return value of the script.
	 */
	protected Object execute(final Script script) {
		return contextFactory.call(new ContextAction() {
			public Object run(Context context) {
				return script.exec(context, scope);
			}
		});
	}

	/**
	 * Returns the object from the current scope.
	 *
	 * @param name the name of the object to retrieve.
	 * @return the object from the current scope.
	 */
	protected Object getFromScope(String name) {
		return scope.get(name, scope);
	}
}
