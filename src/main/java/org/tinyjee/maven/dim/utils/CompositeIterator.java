/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Simple iterator iterating over a given set of iterators.
 *
 * @author Juergen_Kellerer, 2011-10-23
 */
public class CompositeIterator<E> implements Iterator<E> {

	private Iterator<E> current;
	private final Iterator<Iterator<E>> iterators;

	/**
	 * Creates a new composite iterator over the given iterators.
	 *
	 * @param iterators the iterators to group.
	 */
	public CompositeIterator(Iterator<Iterator<E>> iterators) {
		this.iterators = iterators;
	}

	public boolean hasNext() {
		while ((current == null || !current.hasNext()) && iterators.hasNext())
			current = iterators.next();

		return current != null && current.hasNext();
	}

	public E next() {
		if (!hasNext()) throw new NoSuchElementException();
		return current.next();
	}

	public void remove() {
		current.remove();
	}

	@Override
	public String toString() {
		return "CompositeIterator{" +
				"current=" + current +
				", iterators=" + iterators +
				'}';
	}
}
