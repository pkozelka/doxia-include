/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.codehaus.plexus.util.SelectorUtils;

import java.io.Serializable;
import java.util.Comparator;
import java.util.StringTokenizer;

import static java.lang.Character.isDigit;

/**
 * Compares strings like humans would do (not caring on upper/lower case, whitespaces, number padding and some special characters).
 *
 * @author Juergen_Kellerer, 2012-06-03
 */
public class NumberAwareStringComparator implements Comparator<String>, Serializable {

	private static final long serialVersionUID = 4362901001755473215L;

	private static final String WS = " \t\n\r\f", DELIMITERS = ",:;.-_+*~!$%&/\\()=?'\"";

	boolean caseSensitive = true;

	public NumberAwareStringComparator() {
	}

	public NumberAwareStringComparator(boolean caseSensitive) {
		this.caseSensitive = caseSensitive;
	}

	public int compare(String text1, String text2) {
		text1 = text1 == null ? "" : text1.trim();
		text2 = text2 == null ? "" : text2.trim();

		if (text1.equals(text2)) return 0;

		String[] tuple = null;
		final StringTokenizer tokenizer1 = new StringTokenizer(text1, DELIMITERS);
		final StringTokenizer tokenizer2 = new StringTokenizer(text2, DELIMITERS);

		int result = 0;
		while (result == 0 && tokenizer1.hasMoreTokens() && tokenizer2.hasMoreTokens()) {
			String t1 = removeWhitespace(tokenizer1.nextToken()), t2 = removeWhitespace(tokenizer2.nextToken());

			if (t1.length() != 0 && t2.length() != 0) {
				final boolean startWithDigits = isDigit(t1.charAt(0)) && isDigit(t2.charAt(0)),
						endWithDigits = isDigit(t1.charAt(t1.length() - 1)) && isDigit(t2.charAt(t2.length() - 1));
				if (startWithDigits || endWithDigits) {
					Integer n1, n2;
					tuple = extractNumber(t1, tuple, startWithDigits);
					n1 = safeParseInt(tuple[0]);
					t1 = tuple[1];
					tuple = extractNumber(t2, tuple, startWithDigits);
					n2 = safeParseInt(tuple[0]);
					t2 = tuple[1];

					if (n1 != null && n2 != null) {
						if (!n1.equals(n2)) {
							result = n1 < n2 ? -1 : 1;
							continue;
						}
					}
				}
			}

			// Fallback to string comparison.
			result = caseSensitive ? t1.compareToIgnoreCase(t2) : t1.compareTo(t2);
		}

		if (result == 0)
			result = tokenizer1.hasMoreTokens() ? 1 : -1;

		return result;
	}

	private String[] extractNumber(String text, String[] tuple, boolean startsWithDigits) {
		if (tuple == null) tuple = new String[2];

		int index, length = text.length();
		if (startsWithDigits) {
			for (index = 0; index < length; index++) {
				if (!isDigit(text.charAt(index))) break;
			}
			tuple[0] = text.substring(0, index);
			tuple[1] = text.substring(index);
		} else {
			for (index = length - 1; index >= 0; index--) {
				if (!isDigit(text.charAt(index))) break;
			}
			tuple[0] = text.substring(index + 1);
			tuple[1] = text.substring(0, index + 1);
		}
		return tuple;
	}

	private static String removeWhitespace(String text) {
		for (int index = text.length() - 1; index >= 0; index--) {
			if (WS.indexOf(text.charAt(index)) != -1) {
				return SelectorUtils.removeWhitespace(text);
			}
		}
		return text;
	}

	private Integer safeParseInt(String numberCandidate) {
		try {
			return Integer.parseInt(numberCandidate);
		} catch (NumberFormatException e) {
			return null;
		}
	}
}
