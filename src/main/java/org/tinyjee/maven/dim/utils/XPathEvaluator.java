/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.List;

/**
 * Defines a simple data access interface that may be used to evaluate XPath expressions
 * against the loaded XML document.
 */
public interface XPathEvaluator {
	/**
	 * Searches the loaded document for the text content of the first node matched by the given XPath expression.
	 *
	 * @param xpathExpression The xpath expression used to select the text content. E.g. <code>"/html/head/title"</code>
	 * @return The first matched text content or 'null' if not found.
	 */
	String findText(String xpathExpression);

	/**
	 * Finds all DOM nodes that match the given XPath expression.
	 *
	 * @param xpathExpression The xpath expression used to select nodes, E.g. <code>//table</code>
	 * @return A list of all matched DOM nodes. If no nodes were matched, an empty list is returned instead.
	 */
	List<Node> findNodes(String xpathExpression);

	/**
	 * Serializes the given DOM node to XML.
	 *
	 * @param node the node to serialize.
	 * @return the XML string representing the serialized node.
	 */
	String serialize(Node node);

	/**
	 * Converts the given {@link NodeList} to a java list of {@link Node} instances.
	 *
	 * @param nodeList the node list to convert.
	 * @return a node list that uses the standard java collection framework.
	 */
	List<Node> asList(NodeList nodeList);

	/**
	 * Creates a new dependent evaluator that resolves nodes relative to the given node.
	 * <p/>
	 * The evaluator returned by this method shares internals like namespace resolution and xpath engine and
	 * may not be used concurrently (in multiple threads) with its creator.
	 *
	 * @param node the node to resolve against.
	 * @return a new evaluator that resolves nodes relative to the given node.
	 */
	XPathEvaluator newEvaluator(Node node);
}
