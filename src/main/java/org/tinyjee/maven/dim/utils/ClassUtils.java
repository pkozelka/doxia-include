/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.ResourceResolver;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * Is a shared utility for class instantiation.
 * <p/>
 * This utility is in particular capable of parsing and resolving expressions like:
 * <ul>
 * <li>my.package.MyClass</li>
 * <li>my.package.MyClass.getInstance()</li>
 * <li>my.package.MyClassFactory.getMyClass()</li>
 * <li>my.package.MyClassFactory.getInstance().getMyClass()</li>
 * </ul>
 * <p/>
 * Constructors and methods can have one of the signatures that are defined within
 * {@link #newInstance(Class, java.io.File, java.util.Map)} and {@link #invokeMethod(Object, String, java.io.File, java.util.Map)}.
 * <p/>
 * The primary usage of this utility is calling the method {@link #invoke(String, java.io.File, java.util.Map)}.
 *
 * @author Juergen_Kellerer, 2011-10-23
 */
public class ClassUtils {

	private static final Log log = Globals.getLog();

	/**
	 * Parses a callstack expression like 'className.methodName()' and returns the call elements.
	 *
	 * @param expression the expression to return.
	 * @return the parsed call expressions.
	 */
	public static Stack<String> parseCallStack(String expression) {
		Stack<String> callStack = new Stack<String>();
		StringBuilder currentCall = new StringBuilder();

		StringTokenizer tokenizer = new StringTokenizer(expression, ".$#", true);
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			if (isMethodExpression(token)) {
				callStack.push(currentCall.substring(0, currentCall.length() - 1));
				currentCall.setLength(0);
			}

			currentCall.append(token);
		}

		if (currentCall.length() > 0) callStack.push(currentCall.toString());

		return callStack;
	}

	/**
	 * Invokes the given expression which may be 'className', 'className.methodName()', 'className.methodName().anotherMethod()', etc. .
	 *
	 * @param expression the expression to invoke.
	 * @param basePath   the base path to use for resolving class loaders.
	 * @param parameters the macro parameters that may get optionally passed.
	 * @return the result of the invocation.
	 */
	public static Object invoke(String expression, File basePath, Map parameters) {
		return invoke(parseCallStack(expression), basePath, parameters);
	}

	/**
	 * Invokes the given call stack.
	 *
	 * @param callStack  the callStack to invoke.
	 * @param basePath   the base path to use for resolving class loaders.
	 * @param parameters the macro parameters that may get optionally passed.
	 * @return the result of the invocation.
	 */
	@SuppressWarnings("unchecked")
	public static Object invoke(Stack<String> callStack, File basePath, Map parameters) {
		Queue<String> calls = new LinkedList<String>(callStack);

		Object context = null;
		RuntimeException capturedInstantiationException = null;
		for (String callExpression = calls.poll(); callExpression != null; callExpression = calls.poll()) {
			if (isMethodExpression(callExpression)) {
				try {
					context = invokeMethod(context, extractName(callExpression), basePath, parameters);
				} catch (RuntimeException exception) {
					if (capturedInstantiationException != null) {
						log.error("Previously captured exception was: " +
								capturedInstantiationException.getMessage(), capturedInstantiationException);
					}
					throw exception;
				}
			} else {
				ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
				Class<?> aClass = ResourceResolver.resolveClass(basePath, callExpression);
				try {
					try {
						Thread.currentThread().setContextClassLoader(aClass.getClassLoader());
						context = newInstance(aClass, basePath, parameters);
					} finally {
						Thread.currentThread().setContextClassLoader(contextClassLoader);
					}
				} catch (RuntimeException exception) {
					if (!calls.isEmpty() && capturedInstantiationException == null) {
						// Maybe we tried to create an instance of a static factory, retrying on the class...
						capturedInstantiationException = exception;
						context = aClass;
					} else {
						throw exception;
					}
				}
			}
		}

		return context;
	}

	/**
	 * Creates a new instance of the given class.
	 * <p/>
	 * This method searches for one of the following constructors (in this order):
	 * <ul>
	 * <li>{@code Constructor(File.class, Map.class)}</li>
	 * <li>{@code Constructor(Map.class)}</li>
	 * <li>{@code Constructor(File.class)}</li>
	 * <li>{@code Constructor()}</li>
	 * </ul>
	 *
	 * @param cls        the class to instantiate.
	 * @param basePath   the base path to use for resolving class loaders.
	 * @param parameters the macro parameters that may get optionally passed.
	 * @return the result of the instantiation.
	 */
	public static Object newInstance(Class cls, File basePath, Map parameters) {
		try {
			try {
				return cls.getConstructor(File.class, Map.class).newInstance(basePath, parameters);
			} catch (NoSuchMethodException ignored) {
				try {
					return cls.getConstructor(Map.class).newInstance(parameters);
				} catch (NoSuchMethodException ignored1) {
					try {
						return cls.getConstructor(File.class).newInstance(basePath);
					} catch (NoSuchMethodException ignored2) {
						return cls.newInstance();
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof InvocationTargetException && e.getCause() instanceof Exception)
				e = (Exception) e.getCause();
			if (e instanceof RuntimeException)
				throw (RuntimeException) e;
			throw new RuntimeException("Creating class '" + cls.getCanonicalName() + "' failed with:\n -> " + e.getMessage(), e);
		}
	}

	/**
	 * Invokes the given method and returns the result.
	 * <p/>
	 * This method searches for one of the following method signatures recursively including super classes (in this order):
	 * <ul>
	 * <li>{@code methodName()}</li>
	 * <li>{@code methodName(File.class)}</li>
	 * <li>{@code methodName(Map.class)}</li>
	 * <li>{@code methodName(File.class, Map.class)}</li>
	 * </ul>
	 *
	 * @param context    the class or object that is used to start searching in.
	 * @param methodName the name of the method to invoke.
	 * @param basePath   the base path to use for resolving class loaders.
	 * @param parameters the macro parameters that may get optionally passed.
	 * @return the result of the invocation.
	 */
	public static Object invokeMethod(Object context, String methodName, File basePath, Map parameters) {
		final Class cls = context instanceof Class ? (Class) context : context.getClass();

		Method method = findMethod(cls, methodName);
		if (method == null)
			throw new RuntimeException("Failed calling '" + methodName + "' on '" + context + "' as no such method was found.");

		if (!method.isAccessible())
			method.setAccessible(true);

		if (Modifier.isStatic(method.getModifiers()))
			context = null;

		try {
			ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
			try {
				Thread.currentThread().setContextClassLoader(method.getDeclaringClass().getClassLoader());
				switch (method.getParameterTypes().length) {
					case 2:
						return method.invoke(context, basePath, parameters);
					case 1:
						if (method.getParameterTypes()[0] == File.class)
							return method.invoke(context, basePath);
						else
							return method.invoke(context, parameters);
					default:
						return method.invoke(context);
				}
			} finally {
				Thread.currentThread().setContextClassLoader(contextClassLoader);
			}
		} catch (Exception e) {
			if (e instanceof InvocationTargetException && e.getCause() instanceof Exception)
				e = (Exception) e.getCause();
			Object on = context == null ? method.getDeclaringClass() : context;
			throw new RuntimeException("Calling '" + methodName + "' on '" + on + "' failed with:\n -> " + e.getMessage(), e);
		}
	}

	private static Method findMethod(Class startClass, String methodName) {
		Method method = null;
		for (Class clazz = startClass; method == null && clazz != null; clazz = clazz.getSuperclass()) {
			try {
				method = clazz.getDeclaredMethod(methodName);
			} catch (NoSuchMethodException ignored) {
				try {
					method = clazz.getDeclaredMethod(methodName, File.class);
				} catch (NoSuchMethodException ignored1) {
					try {
						method = clazz.getDeclaredMethod(methodName, Map.class);
					} catch (NoSuchMethodException ignored2) {
						try {
							method = clazz.getDeclaredMethod(methodName, File.class, Map.class);
						} catch (NoSuchMethodException ignored4) {
							method = null;
						}
					}
				}
			}
		}
		return method;
	}

	private static String extractName(String callExpression) {
		return isMethodExpression(callExpression) ? callExpression.substring(0, callExpression.indexOf('(')) : callExpression;
	}

	private static boolean isMethodExpression(String token) {
		return token.contains("(");
	}

	private ClassUtils() {
	}
}
