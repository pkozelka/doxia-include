/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import com.thoughtworks.qdox.model.AbstractJavaEntity;

import java.util.Collection;

/**
 * Implements AbstractSelectableJavaEntitiesList for elements that directly derive from {@link AbstractJavaEntity}.
 *
 * @author Juergen_Kellerer, 2011-10-11
 */
public class SelectableJavaEntitiesList<E extends AbstractJavaEntity> extends AbstractSelectableJavaEntitiesList<E> {

	private static final long serialVersionUID = 2251042402549371225L;

	public SelectableJavaEntitiesList() {
		super();
	}

	public SelectableJavaEntitiesList(Collection<? extends E> collection) {
		super(collection);
	}

	@Override
	public AbstractJavaEntity unwrap(E element) {
		return element;
	}
}
