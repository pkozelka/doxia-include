/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.extensions.JavaSourceLoader;

import java.io.File;
import java.util.Map;

/**
 * Provided for backwards compatibility, extensions have been moved to an own package "org.tinyjee.maven.dim.extensions".
 *
 * @author Juergen_Kellerer, 2011-09-30
 */
@Deprecated
public class InterfaceScanner extends JavaSourceLoader {

	private static final long serialVersionUID = -6521845671951229575L;

	/**
	 * Implements the "{@link #LEGACY_PARAM_ALIAS}" alias functionality.
	 */
	public static class AliasHandler extends AbstractAliasHandler {
		/**
		 * Constructs the handler (Note: Is called by
		 * {@link org.tinyjee.maven.dim.spi.RequestParameterTransformer#TRANSFORMERS}).
		 */
		public AliasHandler() {
			super(LEGACY_PARAM_ALIAS, PARAM_JAVA_SOURCE, InterfaceScanner.class.getName());
		}

		@Override
		public void transformParameters(Map<String, Object> requestParams) {
			super.transformParameters(requestParams);
			if (requestParams.containsKey(LEGACY_PARAM_INTERFACE))
				requestParams.put(PARAM_JAVA_SOURCE, requestParams.get("interface"));
		}
	}

	public static final String LEGACY_PARAM_ALIAS = "source-interface-api-scan";
	public static final String LEGACY_PARAM_INTERFACE = "interface";
	public static final String LEGACY_PARAM_TYPE = "type";

	public InterfaceScanner(File baseDir, Map<String, Object> requestParams) {
		super(baseDir, requestParams);
		put(LEGACY_PARAM_TYPE, OUT_PARAM_JAVA_CLASS);
		Globals.getLog().warn("Deprecated source-class '" + getClass().getName() + "' is used. " +
				"Consider using 'org.tinyjee.maven.dim.extensions.JavaSourceLoader' instead. " +
				"(please replace '" + LEGACY_PARAM_ALIAS + "' with '" + PARAM_ALIAS + "')");
	}
}
