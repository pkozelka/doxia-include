/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Stack;

/**
 * Composites a given number of input streams.
 *
 * @author Juergen_Kellerer, 2011-10-27
 */
public class CompositeInputStream extends InputStream {

	final Stack<InputStream> streams = new Stack<InputStream>();

	/**
	 * Constructs a new stream out of the given streams.
	 *
	 * @param streams the streams to composite.
	 */
	public CompositeInputStream(Collection<InputStream> streams) {
		InputStream[] inputStreams = streams.toArray(new InputStream[streams.size()]);
		for (int i = inputStreams.length - 1; i >= 0; i--)
			this.streams.push(inputStreams[i]);
	}

	/**
	 * Constructs a new stream out of the given streams.
	 *
	 * @param streams the streams to composite.
	 */
	public CompositeInputStream(InputStream... streams) {
		this(Arrays.asList(streams));
	}

	@Override
	public int read() throws IOException {
		int r = -1;
		while (!streams.empty()) {
			InputStream inputStream = streams.peek();
			r = inputStream.read();
			if (r == -1)
				streams.pop().close();
			else
				break;
		}
		return r;
	}

	@Override
	public int read(byte[] buffer, int off, int len) throws IOException {
		int r = -1;
		while (!streams.empty()) {
			InputStream inputStream = streams.peek();
			r = inputStream.read(buffer, off, len);
			if (r == -1)
				streams.pop().close();
			else
				break;
		}
		return r;
	}

	@Override
	public void close() throws IOException {
		IOException first = null;
		while (!streams.empty()) {
			try {
				streams.pop().close();
			} catch (IOException e) {
				first = e;
			}
		}
		if (first != null) throw first;
	}
}
