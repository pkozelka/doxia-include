/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.tinyjee.maven.dim.spi.UrlFetcher;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.*;
import org.xml.sax.ext.Attributes2Impl;
import org.xml.sax.helpers.XMLFilterImpl;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.sax.SAXSource;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Field;
import java.net.URL;

import static java.util.Arrays.asList;

/**
 * Creates a {@link Document} builder that adds user-data into the parsed DOM nodes that covers line number and column information.
 * The purpose of this implementation is to get positions of selected elements in order to allow snippet selection via XPath.
 * <p/>
 * This implementation is roughly based on the examples found at:<ul>
 * <li>http://stackoverflow.com/questions/2798376/is-there-a-way-to-parse-xml-via-sax-dom-with-line-numbers-available-per-node</li>
 * <li>http://stackoverflow.com/questions/4915422/get-line-number-from-xml-node-java</li>
 * </ul>
 *
 * @author Juergen_Kellerer, 2011-10-12
 * @see PositioningJsonDocumentBuilder PositioningJsonDocumentBuilder
 */
public class PositioningDocumentBuilder extends AbstractPositioningDocumentBuilder {

	private final SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
	private final TransformerFactory transformerFactory = TransformerFactory.newInstance();

	/**
	 * Creates a new document builder that adds line and column number to the user data sections of resulting element nodes.
	 *
	 * @param namespaceAware whether XML processing is namespace aware.
	 */
	public PositioningDocumentBuilder(boolean namespaceAware) {
		saxParserFactory.setNamespaceAware(namespaceAware);
	}

	public SAXParserFactory getSaxParserFactory() {
		return saxParserFactory;
	}

	public TransformerFactory getTransformerFactory() {
		return transformerFactory;
	}

	/**
	 * Parses the given XML document.
	 *
	 * @param systemId the URL of the document to parse.
	 * @return a parsed document with line and column numbers being included for elements.
	 * @throws SAXException                 If SAX parses cannot be created.
	 * @throws IOException                  If reading fails.
	 * @throws TransformerException         If XML transformation is unavailable.
	 * @throws ParserConfigurationException If parsers are not configured correctly.
	 */
	@Override
	public Document parse(URL systemId) throws SAXException, IOException, TransformerException, ParserConfigurationException {
		return parse(systemId, null);
	}

	@Override
	public Document parse(URL systemIdUrl, Reader reader)
			throws SAXException, IOException, TransformerException, ParserConfigurationException {
		final String systemId = systemIdUrl.toString();
		final boolean namespaceAware = saxParserFactory.isNamespaceAware();

		XMLReader xmlReader = saxParserFactory.newSAXParser().getXMLReader();
		xmlReader.setEntityResolver(new XhtmlEntityResolver());

		InputSource inputSource = reader == null ? new InputSource(UrlFetcher.getSource(systemIdUrl, false)) : new InputSource(reader);
		inputSource.setSystemId(systemId);
		SAXSource saxSource = new SAXSource(new LocationXmlFilter(xmlReader, namespaceAware), inputSource);

		Transformer transformer = transformerFactory.newTransformer();
		DOMResult domResult = new DOMResult(null, systemId);
		transformer.transform(saxSource, domResult);

		Document document = (Document) domResult.getNode();
		document.setDocumentURI(systemId);

		postProcessDocument(document, namespaceAware);

		return document;
	}

	private static void postProcessDocument(Document document, boolean namespaceAware) {
		NodeList allElements = namespaceAware ? document.getElementsByTagNameNS("*", "*") : document.getElementsByTagName("*");
		for (int i = 0, length = allElements.getLength(); i < length; i++) {
			final Node element = allElements.item(i);
			final NamedNodeMap attributes = element.getAttributes();
			if (attributes != null) {
				Node node;
				if (namespaceAware)
					node = attributes.removeNamedItemNS(LocationXmlFilter.LOCATION_NS, LocationXmlFilter.LOCATION_ATTRIBUTE_LOCAL);
				else
					node = attributes.removeNamedItem(LocationXmlFilter.LOCATION_ATTRIBUTE_LOCAL);

				if (node != null) {
					String location = node.getNodeValue();
					String[] locationParts = location == null ? null : location.split(":");
					if (locationParts != null && locationParts.length > 1) {
						element.setUserData(KEY_LINE_NUMBER, Integer.parseInt(locationParts[0]), null);
						element.setUserData(KEY_COLUMN_NUMBER, Integer.parseInt(locationParts[1]), null);
					}
				}
			}
		}
	}

	private static class LocationXmlFilter extends XMLFilterImpl {

		static final String LOCATION_NS = LocationXmlFilter.class.getName();
		static final String LOCATION_ATTRIBUTE_QUALIFIED = "XdimlocX:dim-element-location";
		static final String LOCATION_ATTRIBUTE_LOCAL = "dim-element-location";
		static final String CDATA = "CDATA";

		private final boolean namespaceAware;

		LocationXmlFilter(XMLReader xmlReader, boolean namespaceAware) {
			super(xmlReader);
			this.namespaceAware = namespaceAware;
		}

		private Locator locator;

		@Override
		public void setDocumentLocator(Locator locator) {
			super.setDocumentLocator(locator);
			this.locator = locator;
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			// Add extra attribute to elements to hold location
			String location = locator.getLineNumber() + ":" + locator.getColumnNumber();
			Attributes2Impl modifiedAttributes = new JRE5SafeAttributes2Impl(attributes);
			if (namespaceAware)
				modifiedAttributes.addAttribute(LOCATION_NS, LOCATION_ATTRIBUTE_LOCAL, LOCATION_ATTRIBUTE_QUALIFIED, CDATA, location);
			else
				modifiedAttributes.addAttribute("", "", LOCATION_ATTRIBUTE_LOCAL, CDATA, location);

			super.startElement(uri, localName, qName, modifiedAttributes);
		}


		private static class JRE5SafeAttributes2Impl extends Attributes2Impl {

			private static final Field declaredField, specifiedField;

			static {
				try {
					declaredField = Attributes2Impl.class.getDeclaredField("declared");
					specifiedField = Attributes2Impl.class.getDeclaredField("specified");
					for (Field field : asList(declaredField, specifiedField)) field.setAccessible(true);
				} catch (NoSuchFieldException e) {
					throw new RuntimeException(e);
				}
			}

			JRE5SafeAttributes2Impl(Attributes attributes) {
				super(attributes);
			}

			@Override
			public void addAttribute(String uri, String localName, String qName, String type, String value) {
				try {
					super.addAttribute(uri, localName, qName, type, value);
				} catch (Exception ignored) {
					// In JRE 1.5 this method is completely broken and has to be patched with the implementation found in JDK 1.6
					try {
						boolean[] specified = (boolean[]) specifiedField.get(this), declared = (boolean[]) declaredField.get(this);
						int length = getLength();
						if (specified == null) {
							specified = new boolean[length];
							declared = new boolean[length];
						} else if (length > specified.length) {
							boolean[] flags;

							flags = new boolean[length];
							System.arraycopy(declared, 0, flags, 0, declared.length);
							declared = flags;

							flags = new boolean[length];
							System.arraycopy(specified, 0, flags, 0, specified.length);
							specified = flags;
						}

						specified[length - 1] = true;
						declared[length - 1] = !"CDATA".equals(type);

						specifiedField.set(this, specified);
						declaredField.set(this, declared);
					} catch (IllegalAccessException e) {
						throw new RuntimeException(e); //NOSONAR - The original exception 'ignored' is ignored as this
						// is a workaround to a bug in JDK 1.5 that does always happen and should not produce any output.
						// It is still not very secure as we may end up catching something un-expected.
					}
				}
			}
		}
	}
}
