/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import com.thoughtworks.qdox.JavaDocBuilder;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaPackage;
import com.thoughtworks.qdox.model.JavaSource;
import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.ResourceResolver;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.ref.SoftReference;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.util.*;

import static java.util.Arrays.asList;
import static org.tinyjee.maven.dim.spi.ResourceResolver.findAllSources;

/**
 * Is a singleton that loads java classes at source level using qdox.
 *
 * @author Juergen_Kellerer, 2011-10-11
 */
public class JavaClassLoader {

	private static final Log log = Globals.getLog();

	/**
	 * Finds the smallest common java package name for the currently processed module.
	 *
	 * @return the smallest common java package name for the currently processed module.
	 */
	public static String findCommonPackageName() {
		File startDir = null;
		try {
			startDir = Globals.getInstance().resolveLocalPath("src/main/java");
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Did not find an entry point for getting the package name within the current module.", e);
			return null;
		}
		return doFindCommonPackageName(startDir);
	}

	private static String doFindCommonPackageName(File startDir) {
		File[] contents = startDir.listFiles();
		File result = firstDirectory(contents);
		for (; contents != null && contents.length < 2; contents = firstDirectory(contents).listFiles())
			result = firstDirectory(contents);

		if (result == null) return "";

		try {
			int prefixLength = startDir.getCanonicalPath().length();
			return result.getCanonicalPath().substring(prefixLength + 1).replace(File.separatorChar, '.');
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private static File firstDirectory(File[] content) {
		if (content != null) {
			for (File file : content) {
				if (file.isDirectory()) return file;
			}
		}
		return null;
	}

	private static final JavaClassLoader instance = new JavaClassLoader();

	public static JavaClassLoader getInstance() {
		return instance;
	}

	private SoftReference<SharedBuildHolder> sharedBuilderReference = new SoftReference<SharedBuildHolder>(null);
	private final Set<String> sourceUrls = new LinkedHashSet<String>();
	private final Set<String> sourceTrees = new LinkedHashSet<String>();

	private JavaClassLoader() {
	}

	public synchronized JavaSource addSource(File basePath, String pathOrUrl) throws IOException {
		return addSource(basePath, pathOrUrl, false);
	}

	public synchronized JavaSource addSource(File basePath, String pathOrUrl, boolean doNotLoadSourceTree) throws IOException {
		return addSource(ResourceResolver.findSource(basePath, pathOrUrl), doNotLoadSourceTree);
	}

	public synchronized JavaSource addSource(URL sourceUrl, Reader sourceContent) {
		String sourceInfo = sourceUrl.toExternalForm();
		sourceUrls.add(sourceInfo);
		return getSharedBuilder(false).addSource(sourceContent, sourceInfo);
	}

	public synchronized JavaSource addSource(URL sourceUrl, boolean doNotLoadSourceTree) throws IOException {
		JavaSource javaSource = getSource(sourceUrl);
		if (javaSource != null)
			return javaSource;

		try {
			if (!doNotLoadSourceTree && "file".equals(sourceUrl.getProtocol())) {
				// The builder does not de-duplicate already added source, thus we must use a fresh builder here
				javaSource = createNewBuilder(true).addSource(sourceUrl);

				final String packagePath = javaSource.getPackageName().replace('.', '/');
				final String url = sourceUrl.toString();

				final String rootUrl;
				if ("".equals(packagePath)) {
					rootUrl = sourceUrl.toURI().resolve(".").toASCIIString();
				} else if (url.contains(packagePath)) {
					rootUrl = url.substring(0, url.indexOf(packagePath));
				} else {
					if (log.isDebugEnabled()) log.debug("Not loading source tree for single java source not located below a corresponding package path.");
					rootUrl = null;
				}

				if (rootUrl != null) {
					final File sourceFolder = new File(new URL(rootUrl).toURI());
					addSourceTree(sourceFolder);

					javaSource = getSource(sourceUrl);
					if (javaSource == null) {
						log.warn("Failed getting java source " + sourceUrl + " after adding source path " + sourceFolder +
								", will load the source directly which may lead to duplicates when selecting JavaClasses later.");
					}
				}
			}

			if (javaSource == null) {
				sourceUrls.add(sourceUrl.toString());
				javaSource = getSharedBuilder(false).addSource(sourceUrl);
			}

			return javaSource;
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

	public synchronized JavaPackage[] addPackageSource(File basePath, String packageName) throws IOException {
		final boolean debug = log.isDebugEnabled();

		if (packageName == null || "".equals(packageName)) packageName = findCommonPackageName();
		if (packageName == null || "".equals(packageName)) return new JavaPackage[0];

		// Quick initial check it it's already loaded.
		for (JavaPackage javaPackage : getSharedBuilder(false).getPackages()) {
			if (javaPackage != null && packageName.equals(javaPackage.getName())) return new JavaPackage[]{javaPackage};
		}

		// Converting the package to a path.
		final String relativePathPart = packageName.replace('.', File.separatorChar);
		// Adds support for expression like "[module-name]:some.package":
		if (packageName.indexOf(':') != -1) packageName = packageName.substring(packageName.lastIndexOf(':') + 1);

		for (URL packageUrl : findAllSources(basePath, relativePathPart)) {
			if (!"file".equalsIgnoreCase(packageUrl.getProtocol())) {
				if (debug)
					log.debug("Cannot resolve non-local path '" + packageUrl + "' when adding a package source for '" + packageName + '\'');
				continue;
			}
			try {
				final String canonicalPath = new File(packageUrl.toURI()).getCanonicalPath();
				File packageSourcePath = new File(canonicalPath.substring(0, canonicalPath.length() - packageName.length()));
				addSourceTree(packageSourcePath);
			} catch (URISyntaxException e) {
				throw new RuntimeException(e);
			}
		}

		final LinkedList<JavaPackage> compatiblePackages = new LinkedList<JavaPackage>();
		final SortedSet<JavaPackage> packages = getPackages();
		for (JavaPackage javaPackage : packages) {
			if (packageName.equals(javaPackage.getName())) return new JavaPackage[]{javaPackage};
			if (javaPackage.getName().startsWith(packageName)) compatiblePackages.add(javaPackage);
		}

		int targetDepth = Integer.MAX_VALUE;
		for (JavaPackage javaPackage : compatiblePackages)
			targetDepth = Math.min(packageDepth(javaPackage.getName()), targetDepth);

		for (Iterator<JavaPackage> iterator = compatiblePackages.iterator(); iterator.hasNext(); )
			if (packageDepth(iterator.next().getName()) != targetDepth) iterator.remove();

		if (debug) log.debug("Didn't find exact match, returning the smallest compatible package '" + compatiblePackages + "' instead.");

		return compatiblePackages.toArray(new JavaPackage[compatiblePackages.size()]);
	}

	private static int packageDepth(String name) {
		int depth = 0, index = -1;
		while ((index = name.indexOf('.', index + 1)) != -1) depth++;
		return depth;
	}

	public synchronized void addSourceTree(File sourceFolder) {
		if (sourceTrees.add(sourceFolder.toString())) {
			if (log.isDebugEnabled()) log.debug("Adding java source folder " + sourceFolder);
			getSharedBuilder(false).addSourceTree(sourceFolder);
		}
	}

	public JavaSource getSource(URL sourceUrl) {
		try {
			final String urlCharset = Globals.getInstance().getCharset().name();
			final String urlString = sourceUrl.toExternalForm(), decodedUrlString = URLDecoder.decode(urlString, urlCharset);
			for (JavaSource javaSource : getSources()) {
				URL otherUrl = javaSource.getURL();
				String otherUrlString = otherUrl == null ? "" : otherUrl.toExternalForm();
				if (otherUrlString.equals(urlString) || otherUrlString.equals(decodedUrlString))
					return javaSource;
			}
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
		return null;
	}

	public synchronized JavaSource[] getSources() {
		return getSharedBuilder(true).getSources();
	}

	public SortedSet<JavaClass> getClasses() {
		final JavaClass[] classes;
		synchronized (this) {
			classes = getSharedBuilder(true).getClasses();
		}

		return new TreeSet<JavaClass>(asList(classes));
	}

	public JavaClass resolveClass(String name) {
		return getSharedBuilder(false).getClassByName(name);
	}

	public SortedSet<JavaPackage> getPackages() {
		final JavaPackage[] packages;
		synchronized (this) {
			packages = getSharedBuilder(true).getPackages();
		}

		final TreeSet<JavaPackage> javaPackages = new TreeSet<JavaPackage>(new Comparator<JavaPackage>() {
			public int compare(JavaPackage o1, JavaPackage o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		for (JavaPackage javaPackage : packages) {
			if (javaPackage != null) javaPackages.add(javaPackage);
		}

		return javaPackages;
	}

	public synchronized void clear() {
		sourceTrees.clear();
		sourceUrls.clear();
		sharedBuilderReference = new SoftReference<SharedBuildHolder>(null);
	}

	public long getLastModifiedTime() {
		final SharedBuildHolder buildHolder = sharedBuilderReference.get();
		if (buildHolder == null) return System.currentTimeMillis();
		return buildHolder.getCreated();
	}

	JavaDocBuilder getSharedBuilder(boolean needsImmediateUpToDateCheck) {
		JavaDocBuilder builder;
		SharedBuildHolder holder = sharedBuilderReference.get();

		if (!needsImmediateUpToDateCheck)
			scheduleUpToDateCheck();
		else if (holder != null && !holder.isBuilderUpToDate())
			holder = null;

		if (holder == null) {
			builder = createNewBuilder(false);
			sharedBuilderReference = new SoftReference<SharedBuildHolder>(new SharedBuildHolder(builder));
		} else
			builder = holder.getBuilder();

		return builder;
	}

	void scheduleUpToDateCheck() {
		final SharedBuildHolder holder = sharedBuilderReference.get();
		if (holder != null) holder.resetLastUpToDateCheck();
	}

	synchronized JavaDocBuilder createNewBuilder(boolean forceEmpty) {
		JavaDocBuilder javaDocBuilder = new JavaDocBuilder();
		// Setting the encoding
		javaDocBuilder.setEncoding(Globals.getInstance().getCharset().name());

		// Setting a delegating class loader that may be used to resolve imports (e.g. import something.*)
		javaDocBuilder.getClassLibrary().addClassLoader(new URLClassLoader(new URL[0], null) {
			@Override
			public Class<?> loadClass(String name) throws ClassNotFoundException {
				return Globals.getInstance().getClassLoader().loadClass(name);
			}
		});

		if (!forceEmpty) {
			try {
				for (String sourceUrl : sourceUrls)
					javaDocBuilder.addSource(new URL(sourceUrl));
				for (String sourceTree : sourceTrees)
					javaDocBuilder.addSourceTree(new File(sourceTree));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		return javaDocBuilder;
	}

	/**
	 * Holds a shared builder with the option to do a file sync. check on retrieve.
	 */
	private static class SharedBuildHolder {

		private static final int MS_BETWEEN_CHECKS = 2000;

		private final JavaDocBuilder builder;

		private long lastUpToDateCheck, created = System.currentTimeMillis();
		private final Map<String, Long> sourceModifiedTimes = new HashMap<String, Long>();

		private SharedBuildHolder(JavaDocBuilder builder) {
			if (builder == null) throw new IllegalArgumentException("The wrapped builder cannot be set to 'null'");
			this.builder = builder;

			isBuilderUpToDate();
		}

		JavaDocBuilder getBuilder() {
			return builder;
		}

		long getCreated() {
			return created;
		}

		synchronized void resetLastUpToDateCheck() {
			lastUpToDateCheck = 0;
		}

		synchronized boolean isBuilderUpToDate() {
			final long time = System.currentTimeMillis();
			if (time - lastUpToDateCheck > MS_BETWEEN_CHECKS) {
				final JavaSource[] sources = builder.getSources();
				for (JavaSource javaSource : sources) {
					final URL url = javaSource.getURL();
					try {
						if (url != null && "file".equalsIgnoreCase(url.getProtocol())) {
							final String key = url.toString();
							final long sourceLastModified = url.openConnection().getLastModified();
							final Long capturedLastModified = sourceModifiedTimes.get(key);

							if (capturedLastModified == null)
								sourceModifiedTimes.put(key, sourceLastModified);
							else if (capturedLastModified != sourceLastModified)
								return false;
						}
					} catch (IOException e) {
						log.warn("Failed to get last modified date for URL " + url, e);
					}
				}
				lastUpToDateCheck = time;
			}
			return true;
		}
	}
}
