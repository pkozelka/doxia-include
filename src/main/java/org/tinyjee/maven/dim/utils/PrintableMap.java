/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.spi.Globals;

import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Extends LinkedHashMap with the ability to print the map's contents as formatted string.
 * <p/>
 * Contents are automatically printed to the build log when a non-existing key is queried or
 * when {@link #printContent()} is called.
 *
 * @author Juergen_Kellerer, 2011-09-30
 */
public class PrintableMap<K, V> extends ReducedVerbosityMap<K, V> implements Iterable<V> {

	private static final long serialVersionUID = 6815282986526871197L;
	private static final String NEWLINE = System.getProperty("line.separator", "\n");

	private final String name;
	private boolean detailsWerePrinted;

	public PrintableMap(String name) {
		this.name = name;
	}

	public PrintableMap(Map<? extends K, ? extends V> map, String name) {
		super(map);
		this.name = name;
	}

	/**
	 * Returns the value to which the specified key is mapped, or {@code null} if this map contains no mapping for the key.
	 * <p/>
	 * If the key is not found within the map, this map implementation logs a warning including the failed key and the real
	 * content that was contained in this map instance.
	 *
	 * @param key the key to lookup.
	 * @return the value to which the specified key is mapped, or {@code defaultValue} if this map contains no mapping for the key.
	 */
	@Override
	public V get(Object key) {
		final V value = super.get(key);
		if (value == null && !containsKey(key)) logMissingKeyAccess(key);

		return value;
	}

	/**
	 * Returns the value to which the specified key is mapped, or {@code defaultValue} if this map contains no non-null value for the key.
	 * <p/>
	 * Unlike the standard get method, this method does not print a warning when the key was not found but returns the given
	 * default value instead.
	 *
	 * @param key          the key to lookup.
	 * @param defaultValue the value to return if the specified key is not contained in this map or maps to 'null'.
	 * @return the value to which the specified key is mapped, or {@code defaultValue} if this map contains no value for the key.
	 * @since 1.2
	 */
	public V get(Object key, V defaultValue) {
		final V value = super.get(key);
		return value == null ? defaultValue : value;
	}

	void logMissingKeyAccess(Object key) {
		final Log logger = Globals.getLog();
		final String mapContent = getContentAsString();
		logger.warn(NEWLINE +
				NEWLINE +
				"--------------------------------------------------------------------------------" + NEWLINE +
				"Didn't find the requested key '" + key + "' " + NEWLINE +
				"inside this '" + name + "' map." + NEWLINE +
				NEWLINE +
				"--- Contained values are: " + NEWLINE +
				(detailsWerePrinted ?
						"{ ..Skipping map contents, they were already sent to the logs before.. }" :
						mapContent) +
				NEWLINE +
				"--------------------------------------------------------------------------------" + NEWLINE +
				"Visit \"http://doxia-include.sf.net/\" for further information & reference." + NEWLINE +
				NEWLINE +
				"(Use $map.get(\"key\", \"defaultValue\") or $map.containsKey(\"keyToCheck\") to" + NEWLINE +
				"avoid this log message.)" + NEWLINE +
				"--------------------------------------------------------------------------------" + NEWLINE);
		detailsWerePrinted = true;
	}

	/**
	 * Prints the content of this map to the build log using {@link #getContentAsString()}.
	 */
	public void printContent() {
		Globals.getLog().info(getContentAsString());
	}

	/**
	 * Returns a formatted string representation of this map's content that may be used for debugging.
	 *
	 * @return a formatted string representation of this map's content that may be used for debugging.
	 */
	public String getContentAsString() {
		String mapContent = toString();
		final StringTokenizer tokenizer = new StringTokenizer(mapContent, "{},", true);

		int indentDepth = 0;
		final StringBuilder buffer = new StringBuilder(mapContent.length());
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			switch (token.charAt(0)) {
				case '{':
					buffer.append(token);
					indentDepth++;
					break;
				case ',':
					buffer.append(token);
					break;
				case '}':
					indentDepth--;
				default:
					buffer.append(NEWLINE);
					for (int i = 0; i < indentDepth; i++) buffer.append("   ");
					buffer.append(token.trim());
			}
		}
		mapContent = buffer.toString();
		return mapContent;
	}

	/**
	 * Makes this map iterable like a collection and is an alias to {@code values().iterator()}.
	 *
	 * @return an iterator over all values.
	 * @since 1.2
	 */
	public Iterator<V> iterator() {
		return values().iterator();
	}
}
