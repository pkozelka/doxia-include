/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.namespace.NamespaceContext;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringWriter;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Implements {@link XPathEvaluator}.
 *
 * @author Juergen_Kellerer, 2011-10-12
 */
public class XPathEvaluatorImplementation implements XPathEvaluator {

	public static final String DEFAULT_PREFIX = "default";

	public static String serializeNode(Node node, boolean omitDeclaration, boolean standalone) {
		return serializeNode(node, omitDeclaration, standalone, "xml", 0);
	}

	public static String serializeNode(Node node, boolean omitDeclaration, boolean standalone, String method, int maxLineWidth) {
		try {
			final StringWriter buffer = new StringWriter(4096);
			final Transformer transformer = TransformerFactory.newInstance().newTransformer();

			if (omitDeclaration) transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			if (standalone) transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
			transformer.setOutputProperty(OutputKeys.METHOD, method);

			transformer.transform(new DOMSource(node), new StreamResult(buffer));

			if (maxLineWidth > 0) {
				final StringBuffer source = buffer.getBuffer();
				final String lineBreak = source.indexOf("\r\n") != -1 ? "\r\n" : source.indexOf("\r") != -1 ? "\r" : "\n";
				final StringBuilder output = applyMaxLineWith(source, maxLineWidth, lineBreak);
				return output.toString();
			}

			return buffer.toString();
		} catch (TransformerException e) {
			throw new RuntimeException(e);
		}
	}

	static StringBuilder applyMaxLineWith(CharSequence source, int maxLineWith, String lineBreak) {
		final StringBuilder output = new StringBuilder(source.length());
		int leadingWsStart = 0, leadingWsEnd = 0, possibleBreakPosition = 0;
		int currentLineStart = 0;
		boolean foundNonWsChars = false;
		char isInQuotes = ' ';
		for (int pos = 0, len = source.length(); pos < len; pos++) {
			final char c = source.charAt(pos);
			switch (c) {
				case '\r':
				case '\n':
					if (isInQuotes == ' ') {
						foundNonWsChars = false;
						possibleBreakPosition = leadingWsStart = leadingWsEnd = currentLineStart = pos + 1;
						break;
					}
				case ' ':
				case '\t':
					if (isInQuotes == ' ') {
						if (foundNonWsChars) possibleBreakPosition = pos;
						else leadingWsEnd = pos;
					}
					break;
				case '\'':
				case '"':
					foundNonWsChars = true;
					if (isInQuotes == ' ') isInQuotes = c;
					else if (isInQuotes == c) isInQuotes = ' ';
					break;
				default:
					foundNonWsChars = true;
			}

			if (isInQuotes == ' ' &&
					possibleBreakPosition > currentLineStart &&
					possibleBreakPosition < pos &&
					pos - currentLineStart > maxLineWith) {
				int insertPosition = output.length() - (pos - possibleBreakPosition);
				if (leadingWsEnd > leadingWsStart)
					output.insert(insertPosition, source.subSequence(leadingWsStart, leadingWsEnd + 1));
				output.insert(insertPosition, lineBreak);

				possibleBreakPosition = currentLineStart = (possibleBreakPosition + 1);
			}

			output.append(c);
		}
		return output;
	}

	private final Node context;
	private final XPath xPath;

	/**
	 * Creates a new evaluator around the given document.
	 *
	 * @param document the document to evaluate on.
	 */
	public XPathEvaluatorImplementation(final Document document) {
		context = document;
		xPath = XPathFactory.newInstance().newXPath();
		xPath.setNamespaceContext(new NamespaceContext() {
			public String getNamespaceURI(String prefix) {
				return DEFAULT_PREFIX.equals(prefix) ? document.lookupNamespaceURI(null) : document.lookupNamespaceURI(prefix);
			}

			public String getPrefix(String namespaceURI) {
				return namespaceURI.equals(document.lookupNamespaceURI(null)) ? DEFAULT_PREFIX : document.lookupPrefix(namespaceURI);
			}

			public Iterator getPrefixes(String namespaceURI) {
				return Collections.singleton(getPrefix(namespaceURI)).iterator();
			}
		});
	}

	private XPathEvaluatorImplementation(XPath xPath, Node context) {
		this.xPath = xPath;
		this.context = context;
	}

	public String findText(String xpathExpression) {
		try {
			return (String) xPath.evaluate(xpathExpression, context, XPathConstants.STRING);
		} catch (XPathExpressionException e) {
			throw new RuntimeException(e);
		}
	}

	public List<Node> findNodes(String xpathExpression) {
		try {
			final NodeList nodeList = (NodeList) xPath.evaluate(xpathExpression, context, XPathConstants.NODESET);
			return asList(nodeList);
		} catch (XPathExpressionException e) {
			throw new RuntimeException(e);
		}
	}

	public String serialize(Node node) {
		return serializeNode(node, true, true);
	}

	public List<Node> asList(final NodeList nodeList) {
		return new NodeListAdapter(nodeList);
	}

	public XPathEvaluator newEvaluator(Node node) {
		return new XPathEvaluatorImplementation(xPath, node);
	}
}
