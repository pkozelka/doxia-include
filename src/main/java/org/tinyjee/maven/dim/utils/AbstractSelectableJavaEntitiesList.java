/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import com.thoughtworks.qdox.model.*;
import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.spi.Globals;

import java.lang.reflect.Field;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Implements a base class for selectable lists of qdox based java entities ({@link com.thoughtworks.qdox.model.AbstractJavaEntity})
 * using an AOP-like expressions syntax to select classes, methods and fields.
 * <p/>
 * Lists of this type may contain, classes, methods, fields or any other element that derives from the AbstractJavaEntity.
 * <p/>
 * Expression samples:<ul>
 * <li><code>MyClass</code></li>
 * <li><code>My*</code></li>
 * <li><code>my.package.MyClass</code></li>
 * <li><code>..MyClass</code></li>
 * <li><code>my.*.MyClass</code></li>
 * <li><code>my..MyClass</code></li>
 * <li><code>public void my..MyClass.myMethod(..)</code></li>
 * <li><code>public void my..MyClass.myMethod(java.lang.String, ..)</code></li>
 * <li><code>public void my..MyClass.myMethod(..String, ..)</code></li>
 * <li><code>..MyClass.myOtherMethod()</code></li>
 * <li><code>.myOtherMethod</code></li>
 * <li><code>@my..Annotation</code></li>
 * <li><code>@my..Annotation(..value="something"..)</code></li>
 * </ul>
 * <p/>
 * The matching is implemented using regular expressions with the logic: ".." translates to ".*($|\.|\$)" outside an
 * argument list, to ".*" inside an argument list and "*" translates to "[^\s\.\$]*". All other characters are escaped and
 * matched as specified with the exception that modifiers (e.g. public) and the character "{@literal @}" get special treatment
 * to get rid of ordering problems.<br/>
 * The following example illustrates how java entities are converted to strings that are then matched with regular expressions:
 * <ul>
 * <li><b>Classes:</b> {@code [modifiers] (enum|interface|@interface|class) fully.qualified.ClassName}<br/>
 * e.g. "{@code public abstract class org.tinyjee.maven.dim.utils.AbstractSelectableJavaEntitiesList}"</li>
 * <li><b>Methods:</b>
 * {@code [modifiers] fully.qualified.ClassName.methodName([fully.qualified.ClassName]) throws fully.qualified.ClassName}<br/>
 * e.g. "{@code public org.tinyjee.maven.dim.utils.AbstractSelectableJavaEntitiesList.selectMatching(java.lang.String)}"</li>
 * <li><b>Fields:</b> {@code [modifiers] fully.qualified.ClassName.fieldName}<br/>
 * e.g. "{@code private static final long org.tinyjee.maven.dim.utils.AbstractSelectableJavaEntitiesList.serialVersionUID}"</li>
 * </ul>
 * <p/>
 * Note: The selection logic that this list implements is similar to the logic used in AOP but it is not the same in order reduce the
 * overall complexity.
 *
 * @author Juergen_Kellerer, 2011-10-10
 */
public abstract class AbstractSelectableJavaEntitiesList<E> extends SelectableArrayList<E> {

	private static final Log log = Globals.getLog();
	private static final long serialVersionUID = -1293142697836984789L;

	protected AbstractSelectableJavaEntitiesList() {
	}

	protected AbstractSelectableJavaEntitiesList(Collection<? extends E> collection) {
		super(collection);
	}

	/**
	 * Unwraps the given list element to something that can be converted to a valid {@code AbstractJavaEntity}
	 * inside {@link #unwrapToEntity(Object)}.
	 *
	 * @param element the list element to unwrap.
	 * @return the raw JavaEntity or compatible instance of the behind the list element.
	 */
	protected abstract Object unwrap(E element);

	/**
	 * Converts the given list element to {@code AbstractJavaEntity}.
	 *
	 * @param element the element to convert.
	 * @return the corresponding {@code AbstractJavaEntity} or 'null' if not available.
	 */
	protected final AbstractJavaEntity unwrapToEntity(E element) {
		Object rawEntity = unwrap(element);
		if (rawEntity instanceof BeanProperty) {
			BeanProperty property = (BeanProperty) rawEntity;
			final JavaMethod method = property.getAccessor() == null ? property.getMutator() : property.getAccessor();
			final JavaClass javaClass = method == null ? null : method.getParentClass();
			if (javaClass != null) {
				final JavaField fakeField = new JavaField(property.getType(), property.getName());
				fakeField.setAnnotations(method.getAnnotations());
				fakeField.setParentClass(javaClass);
				fakeField.setModifiers(method.getModifiers());
				rawEntity = fakeField;
			}
		}

		return rawEntity instanceof AbstractJavaEntity ? (AbstractJavaEntity) rawEntity : null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public AbstractSelectableJavaEntitiesList<E> clone() {
		return (AbstractSelectableJavaEntitiesList<E>) super.clone();
	}

	/**
	 * Returns a sorted clone of this list. Sorting is applied by name.
	 *
	 * @return a sorted clone of this list. Sorting is applied by name.
	 */
	public AbstractSelectableJavaEntitiesList<E> sort() {
		return sort(false);
	}

	/**
	 * Returns a sorted clone of this list. Sorting is applied by name.
	 *
	 * @param descending true if sorting is descending, false for ascending.
	 * @return a sorted clone of this list. Sorting is applied by name.
	 */
	public AbstractSelectableJavaEntitiesList<E> sort(final boolean descending) {
		AbstractSelectableJavaEntitiesList<E> clone = clone();
		Collections.sort(clone, new Comparator<E>() {
			public int compare(E element1, E element2) {
				AbstractJavaEntity j1 = unwrapToEntity(element1), j2 = unwrapToEntity(element2);
				String name1 = j1 == null ? "" : j1.getName(), name2 = j2 == null ? "" : j2.getName();
				return descending ? name2.compareTo(name1) : name1.compareTo(name2);
			}
		});
		return clone;
	}

	/**
	 * Converts this list to a map view where elements are mapped against their simple name.
	 * <p/>
	 * If this list contains methods, the methods are mapped with simple name AND full call signature (2 keys for 1 element).
	 * If java methods are overloaded, the last occurrence will be accessible via the simple name.
	 *
	 * @return A map view of this list.
	 */
	public PrintableMap<String, E> asMap() {
		final PrintableMap<String, E> mapView = new PrintableMap<String, E>("Named-Java-Entities");
		for (E element : this) {
			final AbstractJavaEntity javaEntity = unwrapToEntity(element);
			if (javaEntity != null) {
				mapView.put(javaEntity.getName(), element);
				if (javaEntity instanceof JavaMethod) {
					String callSignature = ((JavaMethod) javaEntity).getCallSignature();
					mapView.put(callSignature, element);
				}
			}
		}

		return mapView;
	}

	/**
	 * Selects all java entities that match the given AOP-like expression.
	 * <p/>
	 * Note: Prefixing the expression with "!" inverts the meaning.
	 *
	 * @param entityExpression an expression that matches entities contained in this list.
	 * @return all java entities that match the given AOP-like expression.
	 */
	@Override
	public AbstractSelectableJavaEntitiesList<E> selectMatching(final String entityExpression) {
		if (entityExpression.startsWith("!")) return selectNonMatching(entityExpression.substring(1));
		return (AbstractSelectableJavaEntitiesList<E>) select(new SimpleEntitySelector(entityExpression));
	}

	/**
	 * Selects all java entities that do not match the given AOP-like expression.
	 * <p/>
	 * Note: Prefixing the expression with "!" inverts the meaning.
	 *
	 * @param entityExpression an expression that matches entities contained in this list.
	 * @return all java entities that do not match the given AOP-like expression.
	 */
	@Override
	public AbstractSelectableJavaEntitiesList<E> selectNonMatching(String entityExpression) {
		if (entityExpression.startsWith("!")) return selectMatching(entityExpression.substring(1));
		return (AbstractSelectableJavaEntitiesList<E>) select(createInvertSelector(new SimpleEntitySelector(entityExpression)));
	}

	/**
	 * Selects all java entities that derive from a class that is matching the given AOP-like expression.
	 *
	 * @param classNameExpression an expression matching a super class or interface.
	 * @return all java entities that derive from a class that is matching the given AOP-like expression.
	 */
	public AbstractSelectableJavaEntitiesList<E> selectDerived(String classNameExpression) {
		return (AbstractSelectableJavaEntitiesList<E>) select(new DerivedEntitySelector(classNameExpression));
	}

	/**
	 * Selects all java entities that do not derive from a class that is matching the given AOP-like expression.
	 *
	 * @param classNameExpression an expression matching a super class or interface that must not be present in selected classes.
	 * @return all java entities that do not derive from a class that is matching the given AOP-like expression.
	 */
	public AbstractSelectableJavaEntitiesList<E> selectNonDerived(String classNameExpression) {
		return (AbstractSelectableJavaEntitiesList<E>) select(createInvertSelector(new DerivedEntitySelector(classNameExpression)));
	}

	/**
	 * Selects all java entities that are annotated with an annotation that is matching the given AOP-like expression.
	 *
	 * @param annotationNameExpression an expression matching an annotation (optionally including key &amp; value pairs in method braces).
	 * @return all java entities that are annotated with an annotation that is matching the given AOP-like expression.
	 */
	public AbstractSelectableJavaEntitiesList<E> selectAnnotated(String annotationNameExpression) {
		return (AbstractSelectableJavaEntitiesList<E>) select(new AnnotatedEntitySelector(annotationNameExpression));
	}

	/**
	 * Selects all java entities that are not annotated with an annotation that is matching the given AOP-like expression.
	 *
	 * @param annotationNameExpression an expression matching an annotation (optionally including key &amp; value pairs in method braces).
	 * @return all java entities that are not annotated with an annotation that is matching the given AOP-like expression.
	 */
	public AbstractSelectableJavaEntitiesList<E> selectNonAnnotated(String annotationNameExpression) {
		return (AbstractSelectableJavaEntitiesList<E>) select(createInvertSelector(new AnnotatedEntitySelector(annotationNameExpression)));
	}

	static final Field javaClassIsAnnotationField, typeNameField;

	static {
		try {
			javaClassIsAnnotationField = JavaClass.class.getDeclaredField("isAnnotation");
			javaClassIsAnnotationField.setAccessible(true);
			typeNameField = Type.class.getDeclaredField("name");
			typeNameField.setAccessible(true);
		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Returns true if the given java class is an annotation.
	 *
	 * @param javaClass the class to test.
	 * @return true if the given java class is an annotation.
	 */
	public static boolean isAnnotation(JavaClass javaClass) {
		try {
			if ((Boolean) javaClassIsAnnotationField.get(javaClass)) {
				return true;
			} else {
				for (Type type : javaClass.getImplements()) {
					if ("java.lang.annotation.Annotation".equals(fastGetName(type, false)))
						return true;
				}
			}
			return false;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String fastGetName(Type type, boolean mustBeFullyQualified) {
		if (typeNameField == null || mustBeFullyQualified) return type.getFullyQualifiedName();

		try {
			return (String) typeNameField.get(type);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}


	private abstract class AbstractEntitySelector implements Selector<E> {

		final Log logger = Globals.getLog();
		final Pattern pattern;

		AbstractEntitySelector(String entityExpression) {
			pattern = entityExpression == null ? null : compileAOPLikeExpression(entityExpression);
		}

		public boolean accept(E element) {
			AbstractJavaEntity javaEntity = unwrapToEntity(element);
			return javaEntity != null && accept(javaEntity);
		}

		abstract boolean accept(AbstractJavaEntity entity);

		boolean matches(Object entity) {
			String entityString = entityToString(entity);
			boolean matches = pattern.matcher(entityString).matches();
			if (logger.isDebugEnabled()) logger.debug(pattern.pattern() + (matches ? " matches " : " does NOT match ") + entityString);
			return matches;
		}

		String entityToString(Object entity) {
			if (entity instanceof JavaClass) {
				return toString((JavaClass) entity);
			} else if (entity != null) {
				try {
					return entity.toString();
				} catch (NullPointerException e) {
					logger.warn("Failed converting entity " + entity.getClass() + " to string for matching a selection pattern, " +
							"caused by an internal state error.", e);
				}
			}
			return "";
		}

		String toString(JavaClass entity) {
			StringBuilder result = new StringBuilder();
			appendModifiers(entity, result);
			result.append(entity.isEnum() ? "enum " :
					(entity.isInterface() ? "interface " :
							(isAnnotation(entity) ? "@interface " : "class ")));
			result.append(entity.getFullyQualifiedName());
			return result.toString();
		}

		void appendModifiers(AbstractJavaEntity entity, StringBuilder result) {
			for (String modifier : entity.getModifiers()) result.append(modifier).append(' ');
		}

		@Override
		public String toString() {
			return getClass().getSimpleName() + '{' +
					"pattern=" + pattern +
					'}';
		}
	}

	private class SimpleEntitySelector extends AbstractEntitySelector {
		private SimpleEntitySelector(String entityExpression) {
			super(entityExpression);
		}

		@Override
		boolean accept(AbstractJavaEntity entity) {
			return matches(entity);
		}
	}


	private class DerivedEntitySelector extends AbstractEntitySelector {
		DerivedEntitySelector(String classNameExpression) {
			super(classNameExpression);
		}

		@Override
		boolean accept(AbstractJavaEntity entity) {
			if (entity instanceof JavaClass) {
				List<Type> typesToCheck = new ArrayList<Type>(Arrays.asList(((JavaClass) entity).getImplements()));
				typesToCheck.add(((JavaClass) entity).getSuperClass());

				for (Type type : typesToCheck) {
					if (type != null && (matches(type) || accept(type.getJavaClass())))
						return true;
				}
			}
			return false;
		}
	}

	private class AnnotatedEntitySelector extends AbstractEntitySelector {
		AnnotatedEntitySelector(String annotationNameExpression) {
			super(annotationNameExpression.endsWith(")") ? annotationNameExpression : annotationNameExpression + "(..)");
		}

		@Override
		boolean accept(AbstractJavaEntity entity) {
			for (Annotation annotation : entity.getAnnotations()) {
				if (matches(annotation))
					return true;
			}
			return false;
		}
	}

	static Pattern compileAOPLikeExpression(String expression) {
		expression = expression.trim();
		final StringBuilder pattern = new StringBuilder(expression.length() + 16).append("^.*");

		// Adjust expression for annotations without package name definition.
		if (expression.startsWith("@")) {
			int packageIndex = expression.indexOf('.'), braceIndex = expression.indexOf('(');
			if (packageIndex == -1 || (braceIndex != -1 && packageIndex > braceIndex))
				expression = "@.." + expression.substring(1);
		}

		String[] modifiers = expression.split("\\s+");
		if (modifiers.length > 1) {
			// We must match all modifiers without considering the order.
			for (int j = modifiers.length - 1; j > 0; j--) {
				pattern.append('(');
				for (int i = 0, len = modifiers.length - 1; i < len; i++) {
					if (i > 0) pattern.append('|');
					pattern.append(Pattern.quote(modifiers[i]));
				}
				pattern.append(')');
			}
			pattern.append(".*");
			expression = modifiers[modifiers.length - 1];
		}

		final StringTokenizer tokenizer = new StringTokenizer(expression.replace("..", "~"), "~*()", true);
		int openBraceCount = 0;
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			switch (token.charAt(0)) {
				case '*':
					pattern.append("[^\\s\\.\\$]*"); // '*' matches anything except whitespace, '.' and '$'
					break;
				case '~':
					if (openBraceCount > 0)
						pattern.append(".*"); // '..' matches anything inside method braces
					else
						pattern.append("(|.*($|\\.|\\$))"); // '..' matches either nothing or 'anything($|.|END)'
					break;
				case '(':
					openBraceCount++;
					pattern.append("\\(");
					break;
				case ')':
					openBraceCount--;
					pattern.append("\\)");
					break;
				default:
					pattern.append(Pattern.quote(token));
			}
		}

		// If the pattern ends with an argument list, we add ".*" to match any attached "throws" clause.
		if (pattern.charAt(pattern.length() - 1) == ')') pattern.append(".*");

		Pattern compiledPattern = Pattern.compile(pattern.append('$').toString());
		if (log.isDebugEnabled()) {
			log.debug("Compiled AOP like expression '" + expression + "' to the following regular expression: " + compiledPattern.pattern());
		}

		return compiledPattern;
	}
}
