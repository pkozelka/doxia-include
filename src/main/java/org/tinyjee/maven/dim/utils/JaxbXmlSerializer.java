/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.codehaus.plexus.util.StringUtils;
import org.w3c.dom.Document;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.dom.DOMResult;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Implements a helper class that can serialize JAXB annotated classes between class tree and DOM document.
 *
 * @author Juergen_Kellerer, 2011-10-23
 */
public class JaxbXmlSerializer<E> {

	public final String defaultSchemaBasename;
	public String namespace;

	private final JAXBContext jaxbContext;
	private final DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();


	/**
	 * Constructs a new XmlSerializer for the given types.
	 *
	 * @param namespace             the namespace to use.
	 * @param defaultSchemaBasename the base filename to use when creating a XSD schema.
	 * @param types                 the types to include in this serializer.
	 */
	public JaxbXmlSerializer(String namespace, String defaultSchemaBasename, Class<? extends E>... types) {
		this.defaultSchemaBasename = defaultSchemaBasename;

		if (namespace == null) {
			for (Class<? extends E> type : types) {
				namespace = type.isAnnotationPresent(XmlRootElement.class) ? type.getAnnotation(XmlRootElement.class).namespace() : null;
				if ("##default".equals(namespace)) namespace = null;
				if (namespace != null) break;
			}
			if (namespace == null) {
				for (Class<? extends E> type : types) {
					namespace = type.isAnnotationPresent(XmlType.class) ? type.getAnnotation(XmlType.class).namespace() : null;
					if ("##default".equals(namespace)) namespace = null;
					if (namespace != null) break;
				}
			}
			if (namespace == null) namespace = "";
		}

		this.namespace = namespace;

		try {
			jaxbContext = JAXBContext.newInstance(types);
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Constructs a new XmlSerializer for the given type.
	 *
	 * @param type the type to serialize and de-serialize.
	 */
	public JaxbXmlSerializer(Class<E> type) {
		this(type, null, type.getSimpleName().replaceAll("(?!^)([A-Z]+)", "-$1").toLowerCase());
	}

	/**
	 * Constructs a new XmlSerializer for the given type.
	 *
	 * @param type                  the type to serialize and de-serialize.
	 * @param defaultSchemaBasename the base filename to use when creating a XSD schema.
	 */
	public JaxbXmlSerializer(Class<E> type, String defaultSchemaBasename) {
		this(type, null, defaultSchemaBasename);
	}

	/**
	 * Constructs a new XmlSerializer for the given type.
	 *
	 * @param type                  the type to serialize and de-serialize.
	 * @param namespace             the namespace to use.
	 * @param defaultSchemaBasename the base filename to use when creating a XSD schema.
	 */
	@SuppressWarnings("unchecked")
	public JaxbXmlSerializer(Class<E> type, String namespace, String defaultSchemaBasename) {
		this(namespace, defaultSchemaBasename, type);
	}

	public JAXBContext getJaxbContext() {
		return jaxbContext;
	}

	/**
	 * Generates the XSD schema and returns a map of all generated documents.
	 *
	 * @return a list of all files that were generated.
	 * @throws Exception in case of schema creation failed.
	 */
	public Map<String, Document> generateSchema() throws Exception {
		return generateSchema(defaultSchemaBasename);
	}

	/**
	 * Generates the XSD schema and returns a map of all generated documents.
	 *
	 * @param basename the basename to use for naming the XSD files.
	 * @return a list of all files that were generated.
	 * @throws Exception in case of schema creation failed.
	 */
	public Map<String, Document> generateSchema(final String basename) throws Exception {
		final Map<String, Document> schemaFiles = new LinkedHashMap<String, Document>();
		final DocumentBuilder builder = builderFactory.newDocumentBuilder();

		jaxbContext.generateSchema(new SchemaOutputResolver() {
			int fileId;

			@Override
			public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException {
				String xsdFile = basename + (++fileId > 1 ? "-" + fileId : "") + ".xsd";
				Document xsdDocument = builder.newDocument();
				xsdDocument.setDocumentURI(xsdFile);
				schemaFiles.put(xsdFile, xsdDocument);
				return new DOMResult(xsdDocument, xsdDocument.getDocumentURI());
			}
		});

		return schemaFiles;
	}

	/**
	 * De-serializes the next type from the given document.
	 *
	 * @param document the document to read from.
	 * @return the next de-serialized type contained in the stream.
	 * @throws Exception in case of reading or de-serializing fails.
	 */
	@SuppressWarnings("unchecked")
	public E deserialize(Document document) throws Exception {
		return (E) jaxbContext.createUnmarshaller().unmarshal(document);
	}

	/**
	 * Serializes the given instance and returns a DOM document of the serialized content.
	 *
	 * @param instance      the instance to save.
	 * @param includeSchema Whether the XSD schema shall be referenced and created inside the output directory.
	 * @return The serialized document.
	 * @throws Exception in case of writing or serializing failed.
	 */
	public Document serialize(E instance, boolean includeSchema) throws Exception {
		Marshaller marshaller = jaxbContext.createMarshaller();

		if (includeSchema) {
			if (namespace == null || StringUtils.isEmpty(namespace))
				marshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, defaultSchemaBasename + ".xsd");
			else
				marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, namespace + ' ' + defaultSchemaBasename + ".xsd");
		}

		Document document = builderFactory.newDocumentBuilder().newDocument();
		marshaller.marshal(instance, document);
		return document;
	}
}
