/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import java.io.IOException;
import java.net.URL;

/**
 * This is a simple DTD resolver that attempts to load DTDs from the classpath first
 * before delegating to a web query.
 * <p/>
 * It is doing so by translating any HTTP systemIds to local classpath URLs using the
 * common translation rule between HTTP-url and CP url.
 *
 * @author Juergen_Kellerer, 2011-09-28
 */
public class XhtmlEntityResolver implements EntityResolver {
	public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
		if (systemId.startsWith("http")) {
			URL systemIdUrl = new URL(systemId);

			StringBuilder newPath = new StringBuilder(systemId.length());
			String[] domainParts = systemIdUrl.getHost().split("\\.");
			for (int i = domainParts.length - 1; i > 0; i--) newPath.append('/').append(domainParts[i]);
			newPath.append(systemIdUrl.getPath());

			URL resource = getClass().getResource(newPath.toString());
			if (resource != null) {
				InputSource inputSource = new InputSource(resource.toExternalForm());
				inputSource.setPublicId(publicId);
				return inputSource;
			}
		}

		return null;
	}
}
