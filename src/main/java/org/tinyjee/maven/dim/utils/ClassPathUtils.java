/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

/**
 * Utility for classpath operations.
 *
 * @author juergen_kellerer, 2012-06-06
 */
public class ClassPathUtils {

	private ClassPathUtils() {
	}

	public static List<String> listPaths(final ClassLoader classLoader) {
		List<String> urls = new ArrayList<String>();
		for (Map.Entry<URL, URLClassLoader> entry : listUrls(classLoader)) {
			final String path = entry.getKey().toString();
			if (!urls.contains(path)) urls.add(path);
		}

		StringTokenizer tokenizer = new StringTokenizer(System.getProperty("java.class.path", ""), File.pathSeparator);
		while (tokenizer.hasMoreTokens()) {
			final String path = new File(tokenizer.nextToken()).toURI().toString();
			if (!urls.contains(path)) urls.add(path);
		}

		return urls;
	}

	/**
	 * Returns an iterable over all URLs inside the classpath loading hierarchy.
	 *
	 * @param classLoader the classloader to start with.
	 * @return an iterable over all URLs inside the classpath loading hierarchy.
	 */
	public static Iterable<Map.Entry<URL, URLClassLoader>> listUrls(final ClassLoader classLoader) {
		return new Iterable<Map.Entry<URL, URLClassLoader>>() {
			public Iterator<Map.Entry<URL, URLClassLoader>> iterator() {
				return new URLIterator(classLoader);
			}
		};
	}

	private static class URLIterator implements Iterator<Map.Entry<URL, URLClassLoader>> {

		URLClassLoader loader;
		Iterator<URL> currentIterator;
		private final ClassLoader classLoader;

		URLIterator(ClassLoader classLoader) {
			this.classLoader = classLoader;
			loader = findLoader(classLoader);
			currentIterator = newIterator();
		}

		private Iterator<URL> newIterator() {
			return loader == null ? Collections.<URL>emptyList().iterator() : Arrays.asList(loader.getURLs()).iterator();
		}

		private URLClassLoader findLoader(ClassLoader classLoader) {
			while (!(classLoader instanceof URLClassLoader)) {
				if (classLoader == null) break;
				classLoader = classLoader.getParent();
			}
			return (URLClassLoader) classLoader;
		}

		public boolean hasNext() {
			if (loader == null) return false;

			if (!currentIterator.hasNext()) {
				loader = findLoader(loader.getParent());
				currentIterator = newIterator();
			}

			return currentIterator.hasNext();
		}

		public Map.Entry<URL, URLClassLoader> next() {
			if (!hasNext()) throw new NoSuchElementException();

			final URL url = currentIterator.next();

			return new Map.Entry<URL, URLClassLoader>() {
				public URL getKey() {
					return url;
				}

				public URLClassLoader getValue() {
					return loader;
				}

				public URLClassLoader setValue(URLClassLoader value) {
					return null;
				}
			};
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}
