/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.io.Reader;
import java.net.URL;

/**
 * Base class to {@link Document} builders that add element location information into the generated element nodes.
 *
 * @author Juergen_Kellerer, 2011-10-13
 */
public abstract class AbstractPositioningDocumentBuilder {
	/**
	 * Is a user-data key (for {@link org.w3c.dom.Node#getUserData(String)}) that may be used to return the line number of an element node.
	 */
	public static final String KEY_LINE_NUMBER = "lineNumber";
	/**
	 * Is a user-data key (for {@link org.w3c.dom.Node#getUserData(String)}) that may be used to return the column number of an element node.
	 */
	public static final String KEY_COLUMN_NUMBER = "columnNumber";

	/**
	 * Returns the line number of the specified node if known, -1 otherwise.
	 *
	 * @param node a node that was created using this builder.
	 * @return the line number of the specified node if known, -1 otherwise.
	 */
	public static int getLineNumber(Node node) {
		return extractNumber(node, KEY_LINE_NUMBER);
	}

	/**
	 * Returns the column number of the specified node if known, -1 otherwise.
	 *
	 * @param node a node that was created using this builder.
	 * @return the column number of the specified node if known, -1 otherwise.
	 */
	public static int getColumnNumber(Node node) {
		return extractNumber(node, KEY_COLUMN_NUMBER);
	}

	private static int extractNumber(Node node, String key) {
		Object data = node.getUserData(key);
		return data == null ? -1 : (Integer) data;
	}

	/**
	 * Parses the given URL to a {@link Document}.
	 *
	 * @param systemId the URL of the document to parse.
	 * @return the parsed document.
	 * @throws Exception In case of parsing failed.
	 */
	public abstract Document parse(URL systemId) throws Exception;

	/**
	 * Parses the given URL to a {@link Document}.
	 *
	 * @param systemId the URL of the document to parse.
	 * @param reader   a reader containing the content to parse.
	 * @return the parsed document.
	 * @throws Exception In case of parsing failed.
	 */
	public abstract Document parse(URL systemId, Reader reader) throws Exception;
}
