/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonLocation;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.tinyjee.maven.dim.spi.UrlFetcher;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Pattern;

/**
 * Creates a {@link Document} builder that parses JSON instead of XML and adds user-data into the parsed DOM nodes that covers
 * line number and column information.
 * The purpose of this implementation is to apply XPath queries on JSON to drive snippet selection.
 * <p/>
 * Translations Rules:<ul>
 * <li>JSON-Objects translate to XML elements with child nodes. The root element is named "json".</li>
 * <li>JSON-Fields translate to XML elements with text content or child nodes if the value is an JSON-Object.</li>
 * <li>JSON-Arrays translate to multiple siblings of XML elements that are assembled as if they were a JSON-Field.</li>
 * <li>The resulting XML is composed of element and text nodes. Attributes are not created.</li>
 * </ul>
 * <p/>
 * Example - Input:<br/>
 * <code><pre>
 * {
 *     "name" : {
 *         "first" : "Joe",
 *         "last" : "Sixpack"
 *     },
 *     "gender" : "MALE",
 *     "verified" : false,
 *     "flags" : ["123", 123, "xyz", {
 *         "composite": 987,
 *         "inner flags": [1,{
 *             "innermost":"abc",
 *             "innermost-object" : {
 *                 "x":1
 *             }
 *         },3]
 *     }],
 *     "userImage" : "Rm9vYmFyIQ=="
 * }
 * </pre></code>
 * Example - Result:<br/>
 * <pre><code>
 * &lt;json&gt;
 *    &lt;name&gt;
 *        &lt;first&gt;Joe&lt;/first&gt;
 *        &lt;last&gt;Sixpack&lt;/last&gt;
 *    &lt;/name&gt;
 *    &lt;gender&gt;MALE&lt;/gender&gt;
 *    &lt;verified&gt;false&lt;/verified&gt;
 *    &lt;flags&gt;123&lt;/flags&gt;&lt;flags&gt;123&lt;/flags&gt;&lt;flags&gt;xyz&lt;/flags&gt;&lt;flags&gt;
 *        &lt;composite&gt;987&lt;/composite&gt;
 *        &lt;inner_flags&gt;1&lt;/inner_flags&gt;&lt;inner_flags&gt;
 *            &lt;innermost&gt;abc&lt;/innermost&gt;
 *            &lt;innermost-object&gt;
 *                &lt;x&gt;1&lt;/x&gt;
 *            &lt;/innermost-object&gt;
 *        &lt;/inner_flags&gt;&lt;inner_flags&gt;3&lt;/inner_flags&gt;
 *    &lt;/flags&gt;
 *    &lt;userImage&gt;Rm9vYmFyIQ==&lt;/userImage&gt;
 * &lt;/json&gt;
 * </code></pre>
 *
 * @author Juergen_Kellerer, 2011-10-12
 * @see PositioningDocumentBuilder PositioningDocumentBuilder
 */
public class PositioningJsonDocumentBuilder extends AbstractPositioningDocumentBuilder {

	public static final String ROOT_ELEMENT_TAG_NAME = "json";
	private final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	private final JsonFactory jsonFactory = new JsonFactory();

	{
		jsonFactory.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, true);

		// Make JSON parsing permissive
		jsonFactory.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
		jsonFactory.configure(JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS, true);
		jsonFactory.configure(JsonParser.Feature.ALLOW_NUMERIC_LEADING_ZEROS, true);
		jsonFactory.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
		jsonFactory.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
		jsonFactory.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

		documentBuilderFactory.setNamespaceAware(false);
	}

	public JsonFactory getJsonFactory() {
		return jsonFactory;
	}

	@Override
	public Document parse(URL systemId) throws IOException, ParserConfigurationException {
		return parse(systemId, null);
	}

	@Override
	public Document parse(URL systemId, Reader reader) throws IOException, ParserConfigurationException {
		final JsonParser parser = jsonFactory.createJsonParser(reader == null ? UrlFetcher.getReadableSource(systemId) : reader);
		final Document document = documentBuilderFactory.newDocumentBuilder().newDocument();
		final Element rootElement = document.createElement(ROOT_ELEMENT_TAG_NAME);
		document.setDocumentURI(systemId.toString());
		document.appendChild(rootElement);
		new DocumentTreeBuilder(document, rootElement, parser).buildDocumentTree();

		return document;
	}

	private static class DocumentTreeBuilder {

		private static final Pattern NON_TAG_NAME_CHARACTERS = Pattern.compile("[\\s<>&']+");

		private final Document document;
		private final Element rootElement;
		private final JsonParser parser;

		private final Stack<Element> elementStack = new Stack<Element>();
		private final Stack<Element> arrayStack = new Stack<Element>();
		private final Map<String, Element> stackElementOnArrayCreation = new HashMap<String, Element>();

		private JsonLocation currentLocation;
		private int lineNumber;
		private int lastLineNumber;
		private String currentName;

		private DocumentTreeBuilder(Document document, Element rootElement, JsonParser parser) {
			this.document = document;
			this.rootElement = rootElement;
			this.parser = parser;
		}

		void buildDocumentTree() throws IOException {

			Element previousField = null;
			for (JsonToken currentToken = parser.nextToken(); currentToken != null; currentToken = parser.nextToken()) {
				currentLocation = parser.getCurrentLocation();
				lineNumber = currentLocation.getLineNr();
				currentName = parser.getCurrentName();

				switch (currentToken) {
					case START_OBJECT:
						if (previousField != null) {
							previousField.getParentNode().removeChild(previousField);
							previousField = null;
						}
						if (!elementStack.isEmpty() &&
								elementStack.peek().equals(stackElementOnArrayCreation.get(getArrayStackMappingKey()))) {
							elementStack.push(addArrayElement());
						} else
							elementStack.push(addElement());

						break;
					case END_OBJECT:
						Element element = elementStack.pop();
						if (element != null && element.hasChildNodes())
							addWhitespaces(element, elementStack.size());

						break;
					case START_ARRAY:
						arrayStack.push(previousField);
						recordArrayStackToElementMapping();
						previousField = null;
						break;
					case END_ARRAY:
						stackElementOnArrayCreation.remove(getArrayStackMappingKey());
						arrayStack.pop();
						break;
					case FIELD_NAME:
						previousField = addElement();
						break;
					case VALUE_FALSE:
					case VALUE_TRUE:
					case VALUE_NULL:
					case VALUE_NUMBER_FLOAT:
					case VALUE_NUMBER_INT:
					case VALUE_STRING:
						if (previousField != null) {
							previousField.setTextContent(parser.getText());
							previousField = null;
						} else {
							Element arrayElement = addArrayElement();
							arrayElement.setTextContent(parser.getText());
							addWhitespaces(arrayElement.getParentNode(), elementStack.size());
						}
						break;
				}

				lastLineNumber = lineNumber;
			}
		}

		private void recordArrayStackToElementMapping() {
			stackElementOnArrayCreation.put(getArrayStackMappingKey(), elementStack.peek());
		}

		private String getArrayStackMappingKey() {
			int size = arrayStack.size();
			return size + ":" + (size == 0 ? "" : arrayStack.peek().getTagName());
		}

		private Element addArrayElement() {
			Element arrayElement = arrayStack.peek();
			String textContent = arrayElement.getTextContent();
			if (textContent != null && textContent.length() != 0) {
				arrayElement = document.createElement(arrayElement.getTagName());
				fillInCurrentLocation(arrayElement);
				arrayStack.pop().getParentNode().appendChild(arrayElement);
				arrayStack.push(arrayElement);
			}

			recordArrayStackToElementMapping();

			return arrayElement;
		}

		private Element addElement() {
			Node parentNode = elementStack.isEmpty() ? null : elementStack.peek();
			String tagName = currentName == null ? "anonymous" : currentName;
			tagName = NON_TAG_NAME_CHARACTERS.matcher(tagName).replaceAll("_");
			Element element = parentNode == null ? rootElement : document.createElement(tagName);
			fillInCurrentLocation(element);

			addWhitespaces(parentNode, elementStack.size());
			if (parentNode != null) parentNode.appendChild(element);

			return element;
		}

		private void fillInCurrentLocation(Element element) {
			element.setUserData(KEY_LINE_NUMBER, lineNumber, null);
			element.setUserData(KEY_COLUMN_NUMBER, currentLocation.getColumnNr(), null);
		}

		private void addWhitespaces(Node parentNode, int indentCount) {
			if (parentNode != null) {
				int newLines = lineNumber - lastLineNumber;
				for (; newLines > 0; newLines--) {
					StringBuilder builder = new StringBuilder().append('\n');
					for (int j = indentCount; j > 0; j--)
						builder.append("    ");
					Text textNode = document.createTextNode(builder.toString());
					parentNode.appendChild(textNode);
				}
			}
		}
	}
}
