/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.spi.Globals;
import org.tinyjee.maven.dim.spi.RequestParameterTransformer;

import java.util.Map;

import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_SOURCE_CLASS;

/**
 * Implements an handler that allows the definition of aliases for request parameters.
 */
public abstract class AbstractAliasHandler implements RequestParameterTransformer {

	final String triggerParameter, targetParameter, sourceClass;

	/**
	 * Constructs a new AbstractAliasHandler for the given values.
	 *
	 * @param triggerParameter the parameter to trigger.
	 * @param targetParameter  the parameter to transfer the trigger value to.
	 * @param sourceClass      the source class to set if the trigger was present.
	 */
	protected AbstractAliasHandler(String triggerParameter, String targetParameter, String sourceClass) {
		this.triggerParameter = triggerParameter;
		this.targetParameter = targetParameter;
		this.sourceClass = sourceClass;
	}

	/**
	 * {@inheritDoc}
	 */
	public void transformParameters(Map<String, Object> requestParams) {
		Log logger = Globals.getLog();
		Object parameterValue = requestParams.get(triggerParameter);
		if (parameterValue != null) {
			if (sourceClass != null) {
				if (requestParams.containsKey(PARAM_SOURCE_CLASS)) {
					logger.warn("The request parameter '" + PARAM_SOURCE_CLASS + "' is already set to '" +
							requestParams.get(PARAM_SOURCE_CLASS) + "'. Will not set the parameter to '" + sourceClass + "' " +
							"which may break subsequent template execution and may lead to unexpected results.");
				} else
					requestParams.put(PARAM_SOURCE_CLASS, sourceClass);
			}

			if (requestParams.containsKey(targetParameter) && !parameterValue.equals(requestParams.get(targetParameter))) {
				logger.warn("The request parameter '" + targetParameter + "' is already set to '" +
						requestParams.get(targetParameter) + "', overwriting it with '" + parameterValue + "'. " +
						"This may may lead to unexpected results.");
			}

			requestParams.put(targetParameter, parameterValue);
		}
	}
}
