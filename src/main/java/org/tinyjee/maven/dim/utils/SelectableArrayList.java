/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.utils;

import org.apache.maven.doxia.logging.Log;
import org.tinyjee.maven.dim.spi.Globals;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Implements an array list that allows sub-list selections via regular expressions and / or selectors.
 *
 * @author Juergen_Kellerer, 2011-10-09
 */
public class SelectableArrayList<E> extends ArrayList<E> {

	private static final Log log = Globals.getLog();
	private static final long serialVersionUID = 7547555631109691023L;

	/**
	 * Defines a selector to be used when selecting a sub-list.
	 *
	 * @param <E> the type of element that the selector operates on.
	 */
	public interface Selector<E> {
		boolean accept(E element);
	}

	public SelectableArrayList() {
	}

	public SelectableArrayList(int initialCapacity) {
		super(initialCapacity);
	}

	public SelectableArrayList(Collection<? extends E> collection) {
		super(collection);
	}

	@Override
	@SuppressWarnings("unchecked")
	public SelectableArrayList<E> clone() {
		return (SelectableArrayList) super.clone();
	}

	/**
	 * Appends all elements to a copy of the list and returns the new list.
	 *
	 * @param elements the elements to add.
	 * @return a copy of the list with all specified elements being added.
	 * @since 1.2
	 */
	public SelectableArrayList<E> append(E... elements) {
		return append(Arrays.asList(elements));
	}

	/**
	 * Appends all elements to a copy of the list and returns the new list.
	 *
	 * @param elements the elements to add.
	 * @return a copy of the list with all specified elements being added.
	 * @since 1.2
	 */
	@SuppressWarnings("unchecked")
	public SelectableArrayList<E> append(Collection<E> elements) {
		return append(new Collection[]{elements});
	}

	/**
	 * Appends all elements to a copy of the list and returns the new list.
	 *
	 * @param elements the elements to add.
	 * @return a copy of the list with all specified elements being added.
	 * @since 1.2
	 */
	public SelectableArrayList<E> append(Collection<E>... elements) {
		final SelectableArrayList<E> clone = clone();
		for (Collection<E> other : elements) clone.addAll(other);
		return clone;
	}

	/**
	 * Removes all elements from a copy of the list and returns the new list.
	 *
	 * @param elements the elements to remove.
	 * @return a copy of the list with all specified elements being removed.
	 * @since 1.2
	 */
	@SuppressWarnings("unchecked")
	public SelectableArrayList<E> remove(Collection<E> elements) {
		final SelectableArrayList<E> clone = clone();
		clone.removeAll(elements);
		return clone;
	}

	/**
	 * Returns a copy of this list in reverse order.
	 *
	 * @return a copy of this list in reverse order.
	 * @since 1.2
	 */
	public SelectableArrayList<E> reverse() {
		final SelectableArrayList<E> clone = clone();
		Collections.reverse(clone);
		return clone;
	}

	/**
	 * Selects a sub list of all elements that were matched by the given selector.
	 *
	 * @param selector the selector to use for matching the elements.
	 * @return a sub list of matched elements.
	 */
	public SelectableArrayList<E> select(Selector<E> selector) {
		// Using clone to copy the list to allow extending this class.
		SelectableArrayList<E> clone = clone();

		// Note: clear + add should be more effective on ArrayList than iterating and removing.
		clone.clear();
		for (E e : this) {
			if (selector.accept(e)) clone.add(e);
		}

		if (clone.isEmpty() && !isEmpty()) {
			if (log.isDebugEnabled()) log.debug("No elements were matched when selecting from " + size() + " elements by selector: " + selector);
		}

		return clone;
	}

	/**
	 * Selects all elements whose string representation matches the given regular expression.
	 *
	 * @param regularExpression the regular expression to match.
	 * @return A sub list of all elements that match the given regular expression.
	 */
	public SelectableArrayList<E> selectMatching(String regularExpression) {
		return select(createRegularExpressionSelector(regularExpression));
	}

	/**
	 * Selects all elements whose string representation matches the one of given regular expressions.
	 *
	 * @param regularExpressions the regular expressions to match using OR.
	 * @return A sub list of all elements that matches one of the given regular expressions.
	 */
	@SuppressWarnings("unchecked")
	public SelectableArrayList<E> selectMatching(String... regularExpressions) {
		List<Selector<E>> selectors = new ArrayList<Selector<E>>(regularExpressions.length);
		for (String regularExpression : regularExpressions)
			selectors.add(createRegularExpressionSelector(regularExpression));
		return select(createOrSelector(selectors.toArray(new Selector[selectors.size()])));
	}

	/**
	 * Selects all elements whose string representation does NOT match the given regular expression.
	 *
	 * @param regularExpression the regular expression to match.
	 * @return A sub list of all elements that do NOT match the given regular expression.
	 */
	public SelectableArrayList<E> selectNonMatching(String regularExpression) {
		return select(createInvertSelector(createRegularExpressionSelector(regularExpression)));
	}

	/**
	 * Creates a selector that combines the given selectors with OR (accepts the element if one of the given selectors accepts it).
	 *
	 * @param selectors the selectors that match the actual content.
	 * @return a selector that combines the given selectors with OR.
	 */
	public Selector<E> createOrSelector(final Selector<E>... selectors) {
		return new Selector<E>() {
			public boolean accept(E element) {
				for (Selector<E> selector : selectors)
					if (selector.accept(element)) return true;
				return false;
			}

			@Override
			public String toString() {
				return "OrSelector{" +
						"selectors=" + Arrays.toString(selectors) +
						'}';
			}
		};
	}

	/**
	 * Creates a selector that inverts the given selector.
	 *
	 * @param other the selector to invert.
	 * @return a selector that inverts the given selector.
	 */
	public Selector<E> createInvertSelector(final Selector<E> other) {
		return new Selector<E>() {
			public boolean accept(E element) {
				return !other.accept(element);
			}

			@Override
			public String toString() {
				return "InvertSelector{" +
						"selector=" + other +
						'}';
			}
		};
	}

	/**
	 * Creates a selector that matches the given regular expression against the string representation of the element to select.
	 *
	 * @param regularExpression the regular expression to use for selecting.
	 * @return a selector that matches the given regular expression against the string representation of the element to select.
	 */
	public Selector<E> createRegularExpressionSelector(final String regularExpression) {
		return new Selector<E>() {
			final Pattern pattern = Pattern.compile(regularExpression);

			public boolean accept(E element) {
				return pattern.matcher(String.valueOf(element)).matches();
			}

			@Override
			public String toString() {
				return "RegularExpressionSelector{" +
						"pattern=" + pattern +
						'}';
			}
		};
	}
}
