/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim;

/**
 * Is a summary of system properties that control debug logging.
 *
 * @author Juergen_Kellerer, 2012-07-22
 */
public interface DebugConstants {
	/**
	 * Specifies whether debug logging is enabled.
	 */
	boolean ENABLE_DEBUG = Boolean.getBoolean("dim.verbose");

	/**
	 * Enabled logging of any loaded java AST (JavaSourceLoader extension).
	 */
	boolean PRINT_JAVA_AST = Boolean.getBoolean("dim.print.java-ast");

	/**
	 * Enabled logging of any processed JavaDoc comments (JavaSourceLoader extension).
	 */
	boolean PRINT_JAVADOC = Boolean.getBoolean("dim.print.javadoc");

	/**
	 * Enables logging of any loaded XML document (XmlLoader extension).
	 */
	boolean PRINT_XML = Boolean.getBoolean("dim.print.xml");

	/**
	 * Enables logging of null references within velocity templates.
	 */
	boolean PRINT_NULL_REFERENCES = Boolean.getBoolean("dim.print.null-references");

	/**
	 * Enables printing of velocity templates after successful parsing.
	 */
	boolean PRINT_VELOCITY = Boolean.getBoolean("dim.print.velocity");
}
