/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim;

/**
 * Defines the signature of the include macro.
 *
 * @author Juergen_Kellerer, 2010-09-06
 * @version 1.1
 */
public interface IncludeMacroSignature {
	/**
	 * Sets the content to include using a source path, source URL or the fully qualified java class name.
	 * <p/>
	 * Examples: <code><pre>
	 * source=path/to/file
	 * source=my.package.MyClass
	 * source=/absolute/path/to/file
	 * source=http://somehost/someservice#.xml
	 * source=classpath:/bundled/template.vm
	 * </pre></code>
	 * <p/>
	 * Relative source paths are resolved against multiple base locations including <b>site</b>, <b>java (test)source</b> and
	 * <b>resource</b> directories. Non existing absolute paths starting with the project's base directory are stripped and retried
	 * using the relative paths resolver.
	 * <p/>
	 * When remote content is specified using URLs with schemes like <b>http</b> or <b>ftp</b>, the content is downloaded
	 * using a network connection and cached locally for the time of a single build (= subsequent requests to the same remote
	 * source will not download the source again).
	 * <p/>
	 * Executing the goal "{@code initialize}" within the maven build enables the following additional syntax for referencing a
	 * module file location: <code><pre>
	 * [artifactId]:path/to/file
	 * [groupId:artifactId]:path/to/file
	 * [top]:path/to/file/in/project/root
	 * </pre></code>
	 * It is also possible to specify multiple artifacts delimited by semicolon (e.g. {@code [aid1;aid2]}) which means that the path of
	 * module {@code aid1} is looked-up first and if the reference is missing module {@code aid2} is used as fallback before breaking the build.
	 * This can be useful in templates when references are dynamically built.
	 * <p/>
	 * <i>With "{@code site:run}" it may be required to use "{@code mvn pre-site post-site site:run}" to use the module reference syntax.</i>
	 * <p/>
	 * Finally sources can also be loaded from the current module's <b>classpath</b> or the <b>classpath</b> used by <code>maven-site-plugin</code>
	 * using: <code><pre>
	 * {@code classpath:/path/to/file}
	 * </pre></code>
	 * The classpath is constructed out of the module <code>sources</code> and <code>test-sources</code> folder with the parent
	 * classloader being defined by the dependency list of the <code>maven-site-plugin</code>.
	 * When the goal "{@code initialize}" is executed the classpath includes all dependencies that are defined inside
	 * the <code>pom.xml</code> of the currently executed module.
	 * <p/>
	 * Limited support exists for using classpaths of modules other than the currently executed module. Use
	 * "{@code classpath:[artifactId]:/path/to/file}" to reference a file inside the classpath of module "artifactId".
	 * Classpaths that are used with this option may be missing some dependencies as no transitive dependency resolution is performed.
	 */
	String PARAM_SOURCE = "source";

	/**
	 * Sets a <b>class</b> or <b>factory-method</b> to use as dynamic source for content, request parameters or both.
	 * <p/>
	 * A <b>class</b> that is set using this parameter must implement the interface <code>java.utils.Map</code>.
	 * All entries of this map are treated as if they had been specified as request parameters,
	 * on the macro call (therefore all parameters except 'source-class' can be set dynamically).
	 * <p/>
	 * The source class may choose to use one of the 4 constructor signatures: <code>DefaultNoArg</code>,
	 * <code>Constructor(java.io.File basePath)</code>, <code>Constructor(java.utils.Map requestProperties)</code>
	 * or <code>Constructor(java.io.File basePath, java.utils.Map requestProperties)</code>.
	 * <p/>
	 * The default behaviour of a source class should be to provide properties for a velocity template that is specified by
	 * either setting the "source" key to a bundled template or require the user to set the parameter when using the macro with
	 * this class.<br/>
	 * Alternatively it can also directly provide the content to include (instead of loading the template specified with
	 * <code>"source"</code>). To do so the map key <code>"source-content"</code> must either return a <code>String</code> or
	 * <code>Map&lt;Integer, List&lt;String&gt;&gt;</code> (firstLine=>lines) containing the content to include.
	 * <p/>
	 * The classpath that is used to load the class is similar to the classpath used when loading sources.
	 * (see <code>"source"</code> parameter for more information)
	 * <p/>
	 * Simple Example:<code><pre>
	 * package my.package;
	 * public class MySource extends HashMap&lt;String, Object&gt; {
	 *     public MySource(java.io.File basePath, java.utils.Map requestProperties) {
	 *         putAll(requestProperties);
	 *         put("verbatim", false);
	 *         put("source-content", "&lt;b&gt;Hello World!&lt;/b&gt;");
	 *     }
	 * }
	 * </pre></code>
	 * Macro Call:<code><pre>
	 * %{include|source-class=my.package.MySource}
	 * </pre></code>
	 * <p/>
	 * In addition to the specification above the parameter can also use <b>factory methods</b> that are referenced with:<code><pre>
	 * %{include|source-class=my.package.MySourceFactory.newSourceInstance()}
	 * </pre></code>
	 * Factory methods can implement the same method signature like the constructors, thus macro parameters are also accessible.
	 * Whatever signature is chosen it must NOT be exposed in the expression used with 'source-class' (it is always an empty brace "()").
	 * <br/>
	 * Using a factory method it is also possible to return a 'String' containing the content to include directly, thus the following expression
	 * includes the string value of the constructed class:<code><pre>
	 * %{include|source-class=my.package.MySourceFactory.newSourceInstance().toString()}
	 * </pre></code>
	 */
	String PARAM_SOURCE_CLASS = "source-class";

	/**
	 * Sets the source content inline with the macro call. (This feature may become more useful when
	 * <a href="http://jira.codehaus.org/browse/DOXIA-445">DOXIA-445</a> gets implemented. Voting for it may help.)
	 * <p/>
	 * The purpose of this parameter is to allow the inline definition of templates used to render output of source classes.
	 * Unless specified, "<code>source-is-template</code>" is set to "<code>true</code>" when this parameter is set with the
	 * macro call (it is not implicitly set when the parameter was set by a source class).
	 * <p/>
	 * Example: Including the main java doc comment of a class (using the JavaSourceLoader extension)<code><pre>
	 * %{include|source-content=$comment|source-java=my.package.MyClass}
	 * </pre></code>
	 */
	String PARAM_SOURCE_CONTENT = "source-content";

	/**
	 * Sets the source path using <code>java.io.File</code> instead of evaluating it against URL
	 * or File paths.
	 * <p/>
	 * Under normal conditions it's easier to set the <code>"source"</code> parameter instead.
	 * However there may be occasions where it's necessary to force that the path is treated
	 * as a file path.
	 */
	String PARAM_FILE = "file";

	/**
	 * Toggles whether the source content is treated as a Velocity template.
	 * (<i>Default: auto (= true if file extension ends with ".vm")</i>)
	 */
	String PARAM_SOURCE_IS_TEMPLATE = "source-is-template";

	/**
	 * Sets the content type of the source.
	 * (<i>Default: auto (= detected by file extensions ".apt", ".fml", ".xdoc")</i>)
	 * <p/>
	 * Setting this property has no effect for verbatim content. With non-verbatim
	 * content this information is used to select a doxia parser that transcodes the
	 * the content when including it into the site. Using a parser allows to use <i>xdoc</i>
	 * templates inside <i>apt</i> documents and vice versa.
	 * <p/>
	 * Values that will select a parser are "<b>apt</b>", "<b>fml</b>", "<b>xdoc</b>",
	 * any other values will have no effect.
	 */
	String PARAM_SOURCE_CONTENT_TYPE = "source-content-type";

	/**
	 * Defines the charset to use when loading the source content.
	 * (<i>Default: context specific</i>, used to be <i>"UTF-8" in pre 1-1-alpha</i>)
	 * <p/>
	 * When no charset is specified the following actions apply:<ul>
	 * <li>If auto detection is enabled, the detected charset is used.</li>
	 * <li>If source is using the HTTP protocol, any given charset is used.</li>
	 * <li>The build charset is used when the goal {@code initialize} was executed.</li>
	 * <li>The systems default charset is used if nothing else is defined.</li>
	 * </ul>
	 */
	String PARAM_CHARSET = "charset";

	/**
	 * Toggles whether charset detection is used when loading source content.
	 * (<i>Default: true</i>)
	 * <p/>
	 * This value can have multiple values:<ul>
	 * <li>false: Disable any auto detection.</li>
	 * <li>true: Enable auto detection, externally specified charsets override detected values.</li>
	 * <li>force: Use auto detection result even if confidence is low.</li>
	 * </ul>
	 */
	String PARAM_CHARSET_AUTODETECT = "charset-autodetect";

	/**
	 * Toggles whether the content is verbatim (included as source) or non-verbatim (included
	 * as markup and interpreted).
	 * (<i>Default: auto</i>)
	 */
	String PARAM_VERBATIM = "verbatim";

	/**
	 * Sets the snippet-id to use for extracting snippets out of the specified source.
	 * <p/>
	 * Snippets are processed on a line by line basis and multiple snippets with the same
	 * name may appear in one file. In order to define a section using a snippet id,
	 * surround a block with:<br/>
	 * <code>
	 * // START SNIPPET: snippet-id<br/>
	 * ... code to include ...<br/>
	 * // END SNIPPET: snippet-id<br/>
	 * </code>
	 * <p/>
	 * Note: From version 1.1 this parameter is deprecated, use {@code snippet} instead.
	 * Calls like "{@code %{include|id=SnippetId|..}}" translate to {@code %{include|snippet=#SnippetId|..}}
	 */
	@Deprecated
	String PARAM_SNIPPET_ID = "id";

	/**
	 * Sets one or more comma separated expression to select the content snippets to include.
	 * <p/>
	 * Snippet selection allows to define what content is included or excluded from the loaded or generated source.
	 * This operation is always the last thing to happen before including and highlighting content and can therefore be
	 * combined with any other options that the macro offers.
	 * <p/>
	 * The general format of this parameter is "{@code prefix:expression1, prefix:expression2}, .." to <b>include</b>
	 * matching content and "{@code !prefix:expression1, !prefix:expression2}, .." to <b>exclude</b> matching content.
	 * <p/>
	 * <span id="START-SNIPPET: snippet-selection-examples">
	 * <b>Examples</b>:<ul>
	 * <li>Include snippet by ID: '{@code snippet=#SnippetId}'</li>
	 * <li>Exclude all snippet id definitions: '{@code snippet=!snippet-ids}'</li>
	 * <li>Include by ID, exclude any definitions: '{@code snippet=#SnippetId, !snippet-ids}'</li>
	 * <li>'grep' like syntax: '{@code snippet=grep:Token,!grep:NotToken}'</li>
	 * <li>'<a href="http://download.oracle.com/javase/6/docs/api/java/utils/regex/Pattern.html">Regular Expression</a>'
	 * syntax: '{@code snippet=re:([0-9,.;\s]+)$}' (all lines ending with numeric expressions)</li>
	 * <li>'<a href="http://www.w3.org/TR/xpath/">XPath</a>' syntax: '{@code snippet=xp:/html/head/title}'</li>
	 * <li>'AOP'-like syntax: '{@code snippet=aj:..InnerClass, !aj:..toString()}'</li>
	 * <li>'Token &amp; Brace' syntax: '<code>snippet=tb:"json-key"{, !tb:"inner-array"[</code>'</li>
	 * <li>Line range: '{@code snippet=lines:50-100}'</li>
	 * </ul>
	 * </span><span id="END-SNIPPET: snippet-selection-examples"/>
	 * <p/>
	 * Some snippet selectors (e.g. XPath or AOP) require that the source content is well formed (XML/JSON or Java in this example)
	 * and may fail if applied to other content.
	 * <br/>
	 * A complete overview on available expressions can be seen by looking after implementations of
	 * {@link org.tinyjee.maven.dim.spi.SnippetSelector}.
	 *
	 * @since 1.1-alpha
	 */
	String PARAM_SNIPPET = "snippet";

	/**
	 * Optional parameter that can be used to grow snippet selections by the given amount of lines.
	 *
	 * @since 1.1-alpha
	 */
	String PARAM_SNIPPET_GROW_OFFSET = "snippet-grow-offset";

	/**
	 * Optional parameter that can be used to grow the start of snippet selections by the given amount of lines.
	 *
	 * @since 1.1-alpha
	 */
	String PARAM_SNIPPET_START_OFFSET = "snippet-start-offset";

	/**
	 * Optional parameter that can be used to grow the end of snippet selections by the given amount of lines.
	 *
	 * @since 1.1-alpha
	 */
	String PARAM_SNIPPET_END_OFFSET = "snippet-end-offset";

	/**
	 * Optional boolean parameter that toggles whether growing is applied to exclusions as well.
	 * (<i>Default: false</i>)
	 *
	 * @since 1.1-alpha
	 */
	String PARAM_SNIPPET_GROW_EXCLUDES = "snippet-grow-excludes";

	/**
	 * Toggles whether the gutter including line numbers is shown for verbatim content.
	 * (<i>Default: true</i>)
	 */
	String PARAM_SHOW_GUTTER = "show-gutter";

	/**
	 * Sets the line number to use for the first line (if gutter is shown).
	 * (<i>Default: auto</i>)
	 */
	String PARAM_SET_FIRST_LINE = "set-first-line";

	/**
	 * Pads all line numbers by the specified amount of zeros (if gutter is shown).
	 */
	String PARAM_PAD_LINE_NUMBERS = "pad-line-numbers";

	/**
	 * Sets a list of lines to highlight inside verbatim content.
	 * <p/>
	 * Lines can be specified as a comma separated list of line numbers, as a range
	 * using x-y or as a mixture of the 2.<br/>
	 * Example usage: <code>highlight=1,5,10,20-25</code>
	 * <p/>
	 * As of version <i>1.1-beta</i> this option is deprecated and was replaced by "{@code highlight-lines}".
	 * The example usage from above translates to "{@code highlight-lines=lines:1,5,10,20-25}"
	 */
	@Deprecated
	String PARAM_HIGHLIGHT = "highlight";

	/**
	 * Defines lines to highlight inside verbatim content using the same selection logic as used within the "{@code snippet}" parameter.
	 * Selected lines are highlighted by the use of a background color.
	 * <p/>
	 * Examples:<ul>
	 * <li>"{@code highlight-lines=grep:org.tinyjee.dim+3}"</li>
	 * <li>"{@code highlight-lines=xp://license}"</li>
	 * <li>"{@code highlight-lines=lines:1,5,10,20-25}"</li>
	 * </ul>
	 *
	 * @since 1.1-beta
	 */
	String PARAM_HIGHLIGHT_LINES = "highlight-lines";

	/**
	 * Sets the highlighter (brush) to use when highlighting verbatim content.
	 * (<i>Default: auto ; uses source content or extension to detect</i>)
	 * <p/>
	 * DIM bundles the brushes <b>bat</b>, <b>dot</b>, <b>clojure</b>, <b>properties</b> and <b>velocity</b> in
	 * <b>addition</b> to what is available by <b>default</b> inside the syntax highlighting library.
	 * A set of known brushes includes: <b>java</b>, <b>js</b>, <b>jsp</b>, <b>cpp</b>, <b>csharp</b>,
	 * <b>sql</b>, <b>xml</b>, <b>html</b>, etc. Check the documentation of SyntaxHighlighter to get a complete list.
	 * <p/>
	 * Highlighting of verbatim content can be disabled by setting this property to <b>none</b>.
	 * <p/>
	 * Specifying the highlight type as "<code>html/[brushname]</code>" allows to highlight HTML with embedded scripts.
	 * A common use case is "<code>html/js</code>" which highlights any HTML-like code with embedded javascript (Note: files with
	 * an extension of ".xhtml" or ".html" default to this highlighting type).
	 */
	String PARAM_HIGHLIGHT_TYPE = "highlight-type";

	/**
	 * Sets the highlighting color theme to use.
	 * (<i>Default: default</i>)
	 * <p/>
	 * Possible values: "<code>default</code>", "<code>django</code>", "<code>eclipse</code>",
	 * "<code>emacs</code>", "<code>fadeToGrey</code>", "<code>MDUltra</code>",
	 * "<code>midnight</code>", "<code>RDark</code>".
	 * (See: <a href="http://alexgorbatchev.com/SyntaxHighlighter/manual/themes/">
	 * http://alexgorbatchev.com/SyntaxHighlighter/manual/themes/</a>)
	 * <br/>
	 * The theme can be set only once per page. The first highlighted inclusion will set the theme for all others.
	 */
	String PARAM_HIGHLIGHT_THEME = "highlight-theme";

	/**
	 * Forces the re-loading of source content when set to <code>true</code>.
	 * <p/>
	 * Content is cached for the lifetime of the build to avoid double
	 * downloads of remote resources.
	 */
	String PARAM_NO_CACHE = "no-cache";

	/**
	 * Sets the path of the site source directory.
	 * <p/>
	 * Setting this property is not required if the site directory uses standard paths or is located directly below the
	 * module's base directory.
	 * <p/>
	 * As of version <b>1.1-beta</b> this option is deprecated and was replaced by running the "initialize" goal
	 * using the macro as build plugin.
	 */
	@Deprecated
	String PARAM_SITE_DIRECTORY = "site-directory";
}
