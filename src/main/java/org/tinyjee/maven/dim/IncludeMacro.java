/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim;

import org.apache.maven.doxia.logging.Log;
import org.apache.maven.doxia.logging.SystemStreamLog;
import org.apache.maven.doxia.macro.AbstractMacro;
import org.apache.maven.doxia.macro.MacroExecutionException;
import org.apache.maven.doxia.macro.MacroRequest;
import org.apache.maven.doxia.macro.manager.MacroManager;
import org.apache.maven.doxia.module.apt.AptParser;
import org.apache.maven.doxia.module.fml.FmlParser;
import org.apache.maven.doxia.module.xdoc.XdocParser;
import org.apache.maven.doxia.parser.AbstractParser;
import org.apache.maven.doxia.parser.ParseException;
import org.apache.maven.doxia.parser.Parser;
import org.apache.maven.doxia.sink.Sink;
import org.apache.maven.doxia.sink.SinkEventAttributeSet;
import org.codehaus.plexus.PlexusConstants;
import org.codehaus.plexus.PlexusContainer;
import org.codehaus.plexus.context.Context;
import org.codehaus.plexus.context.ContextException;
import org.codehaus.plexus.personality.plexus.lifecycle.phase.Contextualizable;
import org.codehaus.plexus.util.FileUtils;
import org.codehaus.plexus.util.IOUtil;
import org.codehaus.plexus.util.StringUtils;
import org.tinyjee.maven.dim.sh.CodeHighlighter;
import org.tinyjee.maven.dim.sources.MergeSource;
import org.tinyjee.maven.dim.sources.SourceClassAdapter;
import org.tinyjee.maven.dim.sources.TemplateSource;
import org.tinyjee.maven.dim.sources.UrlSource;
import org.tinyjee.maven.dim.spi.*;

import java.io.*;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import static java.lang.Boolean.parseBoolean;
import static java.lang.String.valueOf;
import static java.util.Locale.ENGLISH;
import static org.tinyjee.maven.dim.IncludeMacroSignature.*;
import static org.tinyjee.maven.dim.spi.Globals.getAttachedIncludesDirectory;
import static org.tinyjee.maven.dim.spi.GlobalsStack.initializeGlobals;
import static org.tinyjee.maven.dim.spi.GlobalsStack.reInitializeGlobals;
import static org.tinyjee.maven.dim.spi.RequestParameterTransformer.TRANSFORMERS;
import static org.tinyjee.maven.dim.spi.ResourceResolver.isAbsolute;

/**
 * Implements a more feature rich alternative to the bundled "Snippet" macro.
 *
 * @author Juergen_Kellerer, 2010-09-03
 * @version 1.0
 * @plexus.component role="org.apache.maven.doxia.macro.Macro" role-hint="include"
 */
public class IncludeMacro extends AbstractMacro implements Contextualizable {

	private static final Log log = Globals.getLog();

	/**
	 * Specifies the folder name of the folder where resources are put.
	 */
	public static final String RESOURCES_FOLDER_NAME = "resources";

	/**
	 * Specifies the extension used to detect velocity templates.
	 */
	public static final String VELOCITY_TEMPLATE_EXTENSION = ".vm";

	private CodeHighlighter highlighter;
	private PlexusContainer container;

	public CodeHighlighter getHighlighter() {
		if (highlighter == null) highlighter = new CodeHighlighter();
		return highlighter;
	}

	public void contextualize(Context context) throws ContextException {
		container = (PlexusContainer) context.get(PlexusConstants.PLEXUS_KEY);
	}

	private static void importSourceParameters(Source source, Map<String, String> textParameters) {
		if (source != null) {
			for (Map.Entry<String, Object> entry : source.getContent().getParameters().entrySet()) {
				if (entry.getValue() != null && !(entry.getValue() instanceof Map))
					textParameters.put(entry.getKey(), entry.getValue().toString());
				else
					textParameters.remove(entry.getKey());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public void execute(Sink sink, MacroRequest request) throws MacroExecutionException {
		final Log logger = getLogger();
		if (DebugConstants.ENABLE_DEBUG) logger.setLogLevel(Log.LEVEL_DEBUG);

		initializeGlobals(container, null, sink, request, logger, null);
		try {
			final File basePath = request.getBasedir();
			final Map parameters = request.getParameters();

			for (RequestParameterTransformer transformer : TRANSFORMERS) transformer.transformParameters(parameters);
			final Map<String, String> textParameters = extractTextParameters(parameters);

			final File attachedIncludesDirectory = findSiteAttachedIncludesDirectory(basePath, textParameters);
			reInitializeGlobals(container, textParameters.get(PARAM_CHARSET), sink, request, logger, attachedIncludesDirectory);

			Source source = findSource(basePath, textParameters, parameters);
			if (source == null) {
				final StringBuilder builder = new StringBuilder(1024).append(
						"No source was found, one of the parameters 'source', 'file', " +
								"'source-content' or 'source-class' is required, the parameters present at the macro call were:");
				for (Map.Entry<String, String> entry : textParameters.entrySet())
					builder.append('\n').append(entry.getKey()).append('=').append(entry.getValue());
				throw new MacroExecutionException(builder.toString());
			}

			importSourceParameters(source, textParameters);
			processSource(textParameters, basePath, source, sink);
			populateSourceParameters(source, parameters);
		} catch (RuntimeException e) {
			throw new MacroExecutionException(e.getMessage(), e);
		} finally {
			GlobalsStack.removeGlobals();
		}
	}

	/**
	 * Finds the source implementation given the provided input.
	 *
	 * @param basePath       the base path the macro operates on.
	 * @param textParameters the given parameters converted as text.
	 * @param parameters     the original given parameters
	 * @return the source as resolved by the input parameters or 'null' if no source was specified by the given input.
	 * @throws MacroExecutionException in case of the given input parameters were invalid.
	 */
	protected Source findSource(File basePath, Map<String, String> textParameters, Map<String, Object> parameters) throws MacroExecutionException {
		URL sourceUrl = findSourceURL(basePath, textParameters);
		Source source = null, contentSource = null;

		final String sourceContent = textParameters.get(PARAM_SOURCE_CONTENT);
		if (!StringUtils.isEmpty(sourceContent)) {
			if (!textParameters.containsKey(PARAM_SOURCE_IS_TEMPLATE)) textParameters.put(PARAM_SOURCE_IS_TEMPLATE, valueOf(true));
			if (!textParameters.containsKey(PARAM_VERBATIM)) textParameters.put(PARAM_VERBATIM, valueOf(false));
			parameters.putAll(textParameters);

			contentSource = new FixedContentSource(sourceContent, parameters);
		}

		final String sourceClass = textParameters.get(PARAM_SOURCE_CLASS);
		if (sourceClass != null)
			source = new SourceClassAdapter(basePath, parameters, sourceClass);

		if (sourceUrl == null && source != null) {
			importSourceParameters(source, textParameters);
			sourceUrl = findSourceURL(basePath, textParameters);

			final Globals globals = GlobalsStack.getGlobals();
			Sink sink = globals.getSink();
			MacroRequest request = globals.getRequest();
			Log logger = globals.getLogger();
			File attachedIncludesDirectory = globals.getAttachedIncludesPath();
			reInitializeGlobals(container, textParameters.get(PARAM_CHARSET), sink, request, logger, attachedIncludesDirectory);
		}

		if (sourceUrl != null) {
			contentSource = new UrlSource(sourceUrl, parameters);
		}

		if (contentSource != null) {
			if (source != null)
				source = new MergeSource(contentSource, source);
			else
				source = contentSource;
		}

		final String rawSourceIsTemplate = textParameters.get(PARAM_SOURCE_IS_TEMPLATE);
		final boolean sourceHasTemplateExtension = sourceUrl != null &&
				sourceUrl.getPath().toLowerCase(ENGLISH).endsWith(VELOCITY_TEMPLATE_EXTENSION);

		if (sourceHasTemplateExtension || parseBoolean(rawSourceIsTemplate)) {
			final boolean templateIsDisabled = rawSourceIsTemplate != null && !parseBoolean(rawSourceIsTemplate);

			// Removing the ".VM" extension from the source to allow getting the right results in extension based type detection.
			final boolean velocitySourceCanBeHighlighted = templateIsDisabled && !"plain".equals(getHighlighter().
					findBrushName(sourceUrl, Collections.<Integer, List<String>>emptyMap(), textParameters));

			if (sourceHasTemplateExtension && !velocitySourceCanBeHighlighted) {
				try {
					sourceUrl = new URL(sourceUrl.toExternalForm().replace(VELOCITY_TEMPLATE_EXTENSION, ""));
				} catch (MalformedURLException e) {
					throw new MacroExecutionException(e.getMessage(), e);
				}
			}

			// Allow to force disabling template processing in order to include templates themselves.
			if (!templateIsDisabled) source = new TemplateSource(sourceUrl, source);
		}
		return source;
	}

	/**
	 * Attempts to find the URL that locates the source to include.
	 *
	 * @param basePath   the base path to resolve files against.
	 * @param parameters the given macro parameters.
	 * @return a URL pointing to the source to include or 'null' if a source was not specified.
	 * @throws MacroExecutionException In case of a source was specified but not found.
	 */
	protected URL findSourceURL(File basePath, Map<String, String> parameters) throws MacroExecutionException {
		URL sourceUrl = null;
		String rawSource = parameters.get(PARAM_SOURCE);
		if (!StringUtils.isEmpty(rawSource)) {
			try {
				sourceUrl = ResourceResolver.findSource(basePath, rawSource);
			} catch (IllegalArgumentException e) {
				// Check if source is a fully qualified class name
				if (!rawSource.contains("/") && !rawSource.contains("\\")) {
					try {
						sourceUrl = ResourceResolver.findSource(basePath, rawSource.replace('.', '/') + ".java");
					} catch (IllegalArgumentException ignored) {
					}
				} else if (!rawSource.matches("^[a-zA-Z]{2,}:[/]{1,3}.+")) { // exclude URLs from this retry operation.
					// Check if source starts with base path, if so we strip the base path and retry the resolution process.
					String baseUri, sourceUri;
					try {
						baseUri = basePath.getCanonicalFile().toURI().toASCIIString();
						sourceUri = rawSource.startsWith(baseUri) ?
								rawSource : new File(rawSource).getCanonicalFile().toURI().toASCIIString();
					} catch (IOException ioException) {
						throw new MacroExecutionException("Failed to create canonical paths of source '" + rawSource +
								"' or base path '" + basePath + "' on the attempt to find a common path prefix for the 2 paths.\n" +
								"The source resolution failed in the second attempt as a previous lookup " +
								"already failed with:\n" + e.getMessage(), ioException);
					}

					if (sourceUri.startsWith(baseUri)) {
						try {
							sourceUrl = ResourceResolver.findSource(basePath, sourceUri.substring(baseUri.length()));
						} catch (IllegalArgumentException almostIgnored) {
							Globals.getLog().info(almostIgnored.getMessage());
						}
					}
				}

				if (sourceUrl == null) throw new MacroExecutionException("Cannot resolve 'source':" + rawSource, e);
			}
		}

		String file = parameters.get(PARAM_FILE);
		if (!StringUtils.isEmpty(file)) {
			try {
				sourceUrl = (isAbsolute(file) ? new File(file) : new File(basePath, file)).toURI().toURL();
			} catch (MalformedURLException e) {
				throw new MacroExecutionException(file, e);
			}
		}

		return sourceUrl;
	}

	/**
	 * Processes the given source to the Sink.
	 *
	 * @param textParameters a text-only version of the request params.
	 * @param basePath       the module's base path.
	 * @param source         the effective source.
	 * @param sink           the sink to write the output to.
	 * @throws MacroExecutionException in case of the processing failed.
	 */
	protected void processSource(Map<String, String> textParameters, File basePath,
	                             Source source, Sink sink) throws MacroExecutionException {
		final Source.Content content = source.getContent();
		final Parser parser = getContentParser(source.getUrl(), textParameters);
		final SnippetExtractor extractor = SnippetExtractor.getInstance();

		final boolean verbatim = textParameters.containsKey(PARAM_VERBATIM) ?
				parseBoolean(textParameters.get(PARAM_VERBATIM)) :
				parser == null;

		try {
			SortedMap<Integer, List<String>> snippets = extractor.isSnippetSelectionRequired(source) ?
					extractor.selectSnippets(source) :
					null;

			if (verbatim) {
				try {
					if ("none".equalsIgnoreCase(textParameters.get(PARAM_HIGHLIGHT_TYPE))) {
						sink.verbatim(SinkEventAttributeSet.BOXED);
						processRawContent(source, snippets, sink);
						sink.verbatim_();
					} else {
						// The highlighter always needs snippets.
						if (snippets == null) snippets = extractor.selectSnippets(source);
						getHighlighter().execute(source.getUrl(), textParameters, snippets, sink);
					}
				} catch (Exception e) {
					throw new MacroExecutionException(valueOf(source), e);
				}
			} else {
				if (parser == null) {
					processRawContent(source, snippets, sink);
				} else {
					final Reader input = snippets == null ? content.openReader() : new StringReader(extractor.toString(snippets, false));
					try {
						parser.parse(input, sink);
					} catch (ParseException e) {
						boolean debug = log.isDebugEnabled();
						throw new MacroExecutionException("Failed to parse content [\n" + IOUtil.toString(content.openReader()) + "\n] " +
								"loaded from " + (debug ? source : source.getUrl()) + " with parser " + parser.getClass(), e);
					} finally {
						input.close();
					}
				}
			}
		} catch (RuntimeException e) {
			throw new MacroExecutionException(valueOf(source), e);
		} catch (IOException e) {
			throw new MacroExecutionException(valueOf(source), e);
		}
	}

	/**
	 * Sends raw text to the sink.
	 *
	 * @param source   the source to process.
	 * @param snippets optional snippet selection.
	 * @param sink     the sink to write the raw content to.
	 * @throws IOException in case of writing failed.
	 */
	protected void processRawContent(Source source, SortedMap<Integer, List<String>> snippets, Sink sink) throws IOException {
		if (snippets != null) {
			for (List<String> lines : snippets.values()) {
				for (String line : lines) {
					sink.rawText(line);
					sink.rawText(SnippetExtractor.LB);
				}
			}
		} else {
			BufferedReader reader = new BufferedReader(source.getContent().openReader());
			try {
				String line;
				while ((line = reader.readLine()) != null) {
					sink.rawText(line);
					sink.rawText(SnippetExtractor.LB);
				}
			} finally {
				reader.close();
			}
		}
	}

	/**
	 * Finds the directory to use for attaching file includes to the site.
	 *
	 * @param basePath       the current project's base path.
	 * @param textParameters any given macro parameters that may influence the selection process.
	 * @return A path to place CSS, Script and arbitrary attached content.
	 */
	protected File findSiteAttachedIncludesDirectory(File basePath, Map<String, String> textParameters) {
		final boolean useTargetOutput = Boolean.getBoolean("org.tinyjee.maven.dim.generateAttachedIncludesInBuildDirectory") ||
				Boolean.getBoolean("dim.generateAttachedIncludesInBuildDirectory");

		File path = null;
		if (useTargetOutput) {
			String targetPath = System.getProperty("org.tinyjee.maven.dim.project.targetDirectory", "target");
			path = new File(basePath, targetPath + File.separator + "site");
			if (!path.isDirectory()) {
				log.warn("The selected path '" + path + "' is not a directory. Cannot use generation of " +
						"attached includes inside the build target path, will inline scripts and CSS if needed.");
				path = null;
			} else {
				if (log.isDebugEnabled()) {
					log.debug("Using '" + path + "' to copy attached resources. Specify " +
							"'-Ddim.generateAttachedIncludesInBuildDirectory=false' if you need to use the more compatible " +
							"'copy-to-resource' folder operational mode.");
				}
			}
		} else {
			File sitePath = findSiteSourceDirectory(basePath, textParameters);
			if (sitePath != null) {
				path = new File(sitePath, RESOURCES_FOLDER_NAME);
				if (!path.isDirectory() && !path.mkdirs()) {
					log.warn("Failed creating resource path '" + path + "'.");
					path = null;
				}
			}
		}

		final String attachedIncludesDirectory = getAttachedIncludesDirectory();
		if (path != null && !StringUtils.isEmpty(attachedIncludesDirectory)) {
			path = new File(path, attachedIncludesDirectory);
			if (!path.isDirectory() && !path.mkdirs()) {
				log.warn("Failed creating attached includes path '" + path + "'.");
				path = null;
			}
		}

		if (path == null) {
			log.warn("No path could be determined to be used to attach includes content as files. Attaching arbitrary " +
					"content will fail, while scripts and CSS will be inlined, when needed.");
		}

		return path;
	}

	/**
	 * Finds the directory where the site sources are located.
	 *
	 * @param baseDir the base directory ot the module.
	 * @param params  the parameters of the macro request.
	 * @return the directory where the site sources are located.
	 */
	protected File findSiteSourceDirectory(File baseDir, Map<String, String> params) {
		String siteDirectory = params.get(PARAM_SITE_DIRECTORY);
		if (siteDirectory == null)
			siteDirectory = System.getProperty("org.tinyjee.maven.dim.project.siteDirectory");

		File proposedSiteDir = StringUtils.isEmpty(siteDirectory) ? null :
				isAbsolute(siteDirectory) ? new File(siteDirectory) : new File(baseDir, siteDirectory);
		return ResourceResolver.findSiteSourceDirectory(baseDir, proposedSiteDir);
	}

	/**
	 * Extracts all text only parameters from the given map of request parameters.
	 *
	 * @param requestParameters the request parameters.
	 * @return A map containing only strings.
	 */
	protected Map<String, String> extractTextParameters(Map requestParameters) {
		final Map<String, String> textParameters = new HashMap<String, String>(requestParameters.size());
		for (Object o : requestParameters.entrySet()) {
			Map.Entry entry = (Map.Entry) o;
			if (!(entry.getValue() instanceof Map))
				textParameters.put((String) entry.getKey(), valueOf(entry.getValue()));
		}

		return textParameters;
	}

	/**
	 * Populates all source parameters into the given target map.
	 *
	 * @param source           the source to get the parameters from.
	 * @param targetParamaters the target map to put them to.
	 */
	@SuppressWarnings("unchecked")
	protected void populateSourceParameters(Source source, Map targetParamaters) {
		try {
			Map<String, Object> parameters = new LinkedHashMap<String, Object>(source.getContent().getParameters());
			if (!parameters.containsKey("sourceUrl")) parameters.put("sourceUrl", source.getUrl());
			targetParamaters.putAll(parameters);
		} catch (UnsupportedOperationException ignored) {
			if (log.isDebugEnabled()) {
				log.debug("Cannot export source parameters to the given target of " +
						targetParamaters + " as writing into the map is not supported by the given implementation.");
			}
		}
	}

	/**
	 * Returns a content parser for the given source or 'null' if not no parser exists.
	 *
	 * @param source the source url to retrieve the parser for (can be 'null').
	 * @param params the request params.
	 * @return a content parser for the given source or 'null' if not no parser exists.
	 */
	protected Parser getContentParser(URL source, Map<String, String> params) {
		String type = params.get(PARAM_SOURCE_CONTENT_TYPE);
		if (type == null && source != null)
			type = FileUtils.extension(source.getFile());

		Parser parser = null;
		if (type != null) {
			if ("apt".equalsIgnoreCase(type))
				parser = new AptParser();
			else if ("xdoc".equalsIgnoreCase(type))
				parser = new XdocParser();
			else if ("fml".equalsIgnoreCase(type))
				parser = new FmlParser();

			if (parser != null && container != null) {
				try {
					MacroManager macroManager = (MacroManager) container.lookup(MacroManager.ROLE);
					try {
						final Field macroManagerField = AbstractParser.class.getDeclaredField("macroManager");
						macroManagerField.setAccessible(true);
						macroManagerField.set(parser, macroManager);
					} finally {
						container.release(macroManager);
					}
				} catch (Exception e) {
					log.error("Failed to configure parser, recursive macro execution will not work.", e);
				}
			}
		}

		return parser;
	}

	/**
	 * Is a helper method to retrieve a Doxia logger using reflection
	 * (to be compatible with earlier versions of Doxia).
	 *
	 * @return a Doxia logger.
	 */
	protected Log getLogger() {
		// Using reflection as getLog() was not available in earlier versions of the API.
		try {
			return (Log) getClass().getMethod("getLog").invoke(this);
		} catch (Exception ignored) {
			return new SystemStreamLog();
		}
	}
}
