/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim;

import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.model.Build;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.util.*;

import static java.lang.System.setProperty;
import static org.codehaus.plexus.util.StringUtils.join;
import static org.tinyjee.maven.dim.spi.ResourceResolver.setModulePath;

/**
 * Sets the system property "basedir" to the current value of the maven property "basedir".
 * Using this plugin solves an issue with Doxia where macros cannot refer to correct path in multimodule builds.
 * However when builds are run in parallel, this fix is likely to fail.
 *
 * @author juergen_kellerer, 2011-01-27
 * @goal set-basedir
 * @inheritByDefault true
 * @executionStrategy always
 * @phase initialize
 * @requiresDependencyResolution test
 * @deprecated Use goal "initialize" instead.
 * @see InitializeMacroMojo use InitializeMacroMojo (goal "initialize") instead.
 */
@Deprecated
public class SetBaseDirPropertyMojo extends AbstractMojo {

	/**
	 * @parameter default-value="${project.basedir}"
	 * @required
	 * @readonly
	 */
	private File basedir;

	/**
	 * {@inheritDoc}
	 */
	public void execute() throws MojoExecutionException, MojoFailureException {
		getLog().debug("Setting system property 'basedir' to " + basedir);
		setProperty("basedir", basedir.getAbsolutePath());
	}
}
