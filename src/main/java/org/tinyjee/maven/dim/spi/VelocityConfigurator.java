/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.apache.velocity.tools.config.EasyFactoryConfiguration;
import org.tinyjee.maven.dim.spi.backport.ServiceLoader;

import java.util.Map;

/**
 * {@link VelocityConfigurator} defines an interface that may be implemented and registered via service loading
 * to customize the configuration of the velocity template engine and context before the templates are rendered.
 * <p/>
 * <b>Implementation</b>:<code><pre>
 * package my.package;
 * public class MyVelocityConfigurator implements VelocityConfigurator {
 *      public void configureEngine(VelocityEngine engine) {
 *          engine.setProperty(VM_PERM_INLINE_LOCAL, true);
 *          ...
 *      }
 *      ...
 * }
 * </pre></code>
 * <p/>
 * <b>Register the implementation using service loading</b>:<ul>
 * <li>
 * Create the following file:<code><pre>
 * META-INF/services/
 *    org.tinyjee.maven.dim.spi.VelocityConfigurator
 * </pre></code></li>
 * <li>Add the fully qualified class name of your implementation to the file "org.tinyjee.maven.dim.spi.VelocityConfigurator":<code><pre>
 * my.package.MyVelocityConfigurator
 * </pre></code></li>
 * </ul>
 *
 * @version 1.1
 * @since 1.1
 * @author Juergen_Kellerer, 2011-10-14
 */
public interface VelocityConfigurator {

	/**
	 * Is an iterable over all configurators within the classpath.
	 */
	Iterable<VelocityConfigurator> CONFIGURATORS = ServiceLoader.load(VelocityConfigurator.class);

	/**
	 * Is called after applying default settings but before the engine is initialized.
	 *
	 * @param engine an uninitialized engine that may be configured.
	 */
	void configureEngine(VelocityEngine engine);

	/**
	 * Is called when configuring velocity tools using {@link EasyFactoryConfiguration}.
	 *
	 * @param toolsConfiguration the instance to use for configuring tools.
	 */
	void configureTools(EasyFactoryConfiguration toolsConfiguration);

	/**
	 * Is called right before a template is executed.
	 *
	 * @param requestParameters the macro request parameters as they would also be seen by the template.
	 * @param templateContext   the pre-configured template context.
	 */
	void configureContext(Map<String, Object> requestParameters, Context templateContext);
}
