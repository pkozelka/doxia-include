/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi.backport;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Minimum efforts backport of ServiceLoader found in Java 1.6 to be used in Java 1.5.
 * <p/>
 * (May not behave in the same way and is meant to support adapter bindings only)
 * <p/>
 * Note: This implementation searches inside "META-INF/services/typeName" and "typePackage/typeName.services".
 * The latter is not standards compliant, but it works on Android where getting something into META-INF is
 * quite an issue.
 *
 * @author Juergen_Kellerer, 2011-09-27
 */
public class ServiceLoader {

	private ServiceLoader() {
	}

	/**
	 * Loads the services that are specified by the given service type.
	 *
	 * @param serviceType the service type to load.
	 * @param <T>         the expected java type.
	 * @return a collection of service implementations.
	 */
	public static <T> Iterable<T> load(Class<T> serviceType) {
		return new LazyIterable<T>(serviceType);
	}

	private static final class LazyIterable<T> implements Iterable<T> {

		private static final String PREFIX = "META-INF/services/";

		private final Class<T> serviceType;
		private final AtomicReference<List<T>> implementations = new AtomicReference<List<T>>();

		private LazyIterable(Class<T> serviceType) {
			this.serviceType = serviceType;
		}

		public Iterator<T> iterator() {
			if (implementations.get() == null) {
				try {
					// we perform eager initialization of all instances to simplify the implementation.
					List<T> instances = new ArrayList<T>();
					Set<Class<?>> classes = new HashSet<Class<?>>();
					String[] resourceNames = {PREFIX + serviceType.getName(), serviceType.getName().replace('.', '/') + ".services"};
					ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
					if (classLoader == null)
						classLoader = serviceType.getClassLoader();

					for (String resourceName : resourceNames)
						for (Enumeration<URL> urls = classLoader.getResources(resourceName); urls.hasMoreElements(); ) {
							BufferedReader reader = new BufferedReader(new InputStreamReader(urls.nextElement().openStream(), "utf-8"));
							try {
								String implementation;
								while ((implementation = reader.readLine()) != null) {
									int commentIndex = (implementation = implementation.trim()).indexOf('#');
									if (commentIndex != -1)
										implementation = implementation.substring(0, commentIndex).trim();
									if (implementation.length() != 0) {
										final Class<?> implementationClass = classLoader.loadClass(implementation);
										if (classes.add(implementationClass))
											instances.add(serviceType.cast(implementationClass.newInstance()));
									}
								}
							} finally {
								reader.close();
							}
						}

					implementations.compareAndSet(null, instances);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}

			return implementations.get().iterator();
		}
	}
}
