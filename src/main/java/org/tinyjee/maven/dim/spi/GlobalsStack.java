/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import org.apache.maven.doxia.logging.Log;
import org.apache.maven.doxia.macro.MacroRequest;
import org.apache.maven.doxia.sink.Sink;
import org.codehaus.plexus.PlexusContainer;

import java.io.File;
import java.util.Stack;

/**
 * Holds instances of {@link Globals} and manages the aspects of thread and recursive execution safety.
 *
 * @author Juergen_Kellerer, 2012-03-11
 */
public class GlobalsStack {

	private static final ThreadLocal<Stack<Globals>> threadLocal = new InheritableThreadLocal<Stack<Globals>>() {
		@Override
		protected Stack<Globals> initialValue() {
			return new Stack<Globals>();
		}
	};

	private static final Globals DEFAULT_GLOBALS = new Globals(null, null, null, null, null, null);

	/**
	 * Initializes a new instance on top of the current thread's stack of Global instances.
	 *
	 * @param plexusContainer      the plexus container the macro is running in.l
	 * @param charset              specifies the user supplied charset, may be 'null' to use builtin defaults.
	 * @param sink                 sets the output sink to write to, may be 'null' if not yet known.
	 * @param request              sets the incoming macro request, may be 'null' if not relevant.
	 * @param logger               sets the logger to use for logging, may be 'null' to use builtin defaults.
	 * @param attachedIncludesPath sets the path to use for storing attached content, may be 'null' to use builtin defaults.
	 * @return the instances that was placed on top of the stack.
	 */
	public static Globals initializeGlobals(PlexusContainer plexusContainer,
	                                        String charset, Sink sink, MacroRequest request, Log logger, File attachedIncludesPath) {
		Globals globals = new Globals(plexusContainer, sink, request, logger, charset, attachedIncludesPath);
		threadLocal.get().push(globals);
		return globals;
	}

	/**
	 * ReInitializes (= re-creates) the top instance of the current thread's stack of Global instances.
	 *
	 * @param plexusContainer      the plexus container the macro is running in.l
	 * @param charset              specifies the user supplied charset, may be 'null' to use builtin defaults.
	 * @param sink                 sets the output sink to write to, may be 'null' if not yet known.
	 * @param request              sets the incoming macro request, may be 'null' if not relevant.
	 * @param logger               sets the logger to use for logging, may be 'null' to use builtin defaults.
	 * @param attachedIncludesPath sets the path to use for storing attached content, may be 'null' to use builtin defaults.
	 * @return the instances that was re-initialized.
	 */
	public static Globals reInitializeGlobals(PlexusContainer plexusContainer,
	                                          String charset, Sink sink, MacroRequest request, Log logger, File attachedIncludesPath) {
		threadLocal.get().pop();
		return initializeGlobals(plexusContainer, charset, sink, request, logger, attachedIncludesPath);
	}

	/**
	 * Removes the top instances of the current thread's stack of Global instances.
	 */
	public static void removeGlobals() {
		Stack<Globals> globalsStack = threadLocal.get();
		globalsStack.pop();
		if (globalsStack.empty()) threadLocal.remove();
	}

	/**
	 * Returns a thread local, recursion safe instance.
	 *
	 * @return a thread local, recursion safe instance.
	 */
	public static Globals getGlobals() {
		Stack<Globals> globalsStack = threadLocal.get();
		return globalsStack.empty() ? DEFAULT_GLOBALS : globalsStack.peek();
	}

	private GlobalsStack() {
	}
}
