/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * Implements a Source that returns fixed content.
 *
 * @author Juergen_Kellerer, 2011-10-23
 */
public class FixedContentSource extends AbstractSource {

	/**
	 * Defines an unknown url that may be used as substitution for 'null' in services that are not null-pointer safe.
	 */
	public static final URL UNKNOWN;

	static {
		try {
			UNKNOWN = new URL("file:///unknown");
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}

	protected final String content;

	/**
	 * Constructs a new instance of FixedContentSource with the given content.
	 *
	 * @param content    The fixed content to wrap into a source implementation.
	 * @param parameters the macro parameters assigned with this source.
	 */
	public FixedContentSource(String content, Map<String, Object> parameters) {
		this(null, content, parameters);
	}

	/**
	 * Constructs a new instance of FixedContentSource with the given content.
	 *
	 * @param contentURL The url of the fixed content, if known. Can be set to 'null' if not known.
	 * @param content    The fixed content to wrap into a source implementation.
	 * @param parameters the macro parameters assigned with this source.
	 */
	public FixedContentSource(URL contentURL, String content, Map<String, Object> parameters) {
		super(contentURL, parameters);
		this.content = content;
	}

	@Override
	public Content getContent() {
		return new SourceContent() {
			@Override
			public Reader openReader() throws IOException {
				return new StringReader(content);
			}
		};
	}
}
