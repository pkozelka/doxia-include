/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.iterators.TransformIterator;
import org.apache.maven.doxia.logging.Log;
import org.apache.maven.model.Dependency;
import org.codehaus.plexus.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.net.*;
import java.util.*;
import java.util.regex.Pattern;

import static java.io.File.pathSeparator;
import static java.lang.String.valueOf;
import static java.lang.System.getProperty;
import static java.util.Arrays.asList;
import static java.util.Collections.addAll;
import static org.codehaus.plexus.util.StringUtils.join;
import static org.codehaus.plexus.util.StringUtils.split;

/**
 * A collection of utility methods used to resolve resources.
 *
 * @author Juergen_Kellerer, 2010-09-06
 * @version 1.0
 */
public final class ResourceResolver {

	private static final Log log = Globals.getLog();
	private static volatile List<String> currentClassLoaderKey;
	private static SoftReference<URLClassLoader> currentClassLoader = new SoftReference<URLClassLoader>(null);

	static final Pattern SPLITTER = Pattern.compile("\\s*[,;]+\\s*", Pattern.MULTILINE);

	/**
	 * Defines the default search path when looking after includes in the site directory.
	 * <p/>
	 * Paths beginning with "src/main/java", "src/test/java", "src/main/resources", "src/test/resources",
	 * "site/" and "target/" are adjusted with the absolute paths if {@link org.tinyjee.maven.dim.InitializeMacroMojo}
	 * was called.
	 */
	static final String[] DEFAULT_SITE_SOURCE_SEARCH_PATH = SPLITTER.split(getProperty(
			"org.tinyjee.maven.dim.siteSourceSearchPath", "" +
			"src/site, " +
			"src/site/resources, " +
			"site, " +
			"site/resources"));

	/**
	 * Defines the default search path when looking after sources.
	 * <p/>
	 * Paths beginning with "src/main/java", "src/test/java", "src/main/resources", "src/test/resources",
	 * "site/" and "target/" are adjusted with the absolute paths if {@link org.tinyjee.maven.dim.InitializeMacroMojo}
	 * was called.
	 */
	static final String[] DEFAULT_SOURCE_SEARCH_PATH = SPLITTER.split(getProperty(
			"org.tinyjee.maven.dim.sourceSearchPath", "" +
			"src/main/java, " +
			"src/main/resources, " +
			"src/test/java, " +
			"src/test/resources, " +
			"src/main, " +
			"src, " +
			"target"));

	static final String CLASSES_PATH = getProperty(
			"org.tinyjee.maven.dim.include.classesPath",
			"target/classes/");

	static final String TEST_CLASSES_PATH = getProperty(
			"org.tinyjee.maven.dim.include.testClassesPath",
			"target/test-classes/");

	/**
	 * Sets additional search paths that are used to support source definitions like '[artifactId]:/some-path' or
	 * '[groupId:artifactId]:/some-path'.
	 * <p/>
	 * This method is primarily called by {@link org.tinyjee.maven.dim.InitializeMacroMojo} and therefore path resolution
	 * depends on the Mojo being executed.
	 *
	 * @param groupId      the group id of the module to set.
	 * @param artifactId   the artifact id of the module to set.
	 * @param basePath     the base path of the module.
	 * @param dependencies the declared dependencies of the module.
	 */
	public static void setModulePath(String groupId, String artifactId, File basePath, List dependencies) {
		// Note: As the initialization Mojo and the Macro run in different classloaders,
		//       we must exchange information through system properties as static fields won't work here.
		String absolutePath = basePath.getAbsolutePath();
		System.setProperty("dim." + artifactId + ".basedir", absolutePath);
		System.setProperty("dim." + groupId + '_' + artifactId + ".basedir", absolutePath);

		final TransformIterator iterator = new TransformIterator(dependencies.iterator(), new Transformer() {
			public Object transform(Object input) {
				if (input instanceof Dependency) {
					final Dependency dependency = (Dependency) input;
					return dependency.getGroupId() + ':' + dependency.getArtifactId() + ':' + dependency.getVersion() + ':' + dependency.getScope();
				}
				return input;
			}
		});

		final String dependenciesString = join(iterator, ",");
		System.setProperty("dim." + artifactId + ".dependencies", dependenciesString);
		System.setProperty("dim." + groupId + '_' + artifactId + ".dependencies", dependenciesString);
	}

	/**
	 * Returns a module path for the given module name or 'null' if not known.
	 *
	 * @param moduleName the name of the module, either "groupId:artifactId" or "artifactId".
	 * @return a module path for the given module name or 'null' if not known.
	 */
	public static String getModulePath(String moduleName) {
		return getProperty("dim." + moduleName.replace(':', '_') + ".basedir");
	}

	/**
	 * Returns declared dependencies of the format: groupdId:artifactId:version:scope
	 *
	 * @param moduleName the module name to lookup the dependencies for.
	 * @return declared dependencies of the format: groupdId:artifactId:version:scope
	 */
	public static List<String> getModuleDependencies(String moduleName) {
		return asList(split(getProperty("dim." + moduleName.replace(':', '_') + ".dependencies", ""), ","));
	}

	/**
	 * Returns all currently defined module paths.
	 *
	 * @return all currently defined module paths.
	 */
	public static Map<String, String> getModulePaths() {
		Map<String, String> modulePaths = new TreeMap<String, String>();
		for (Object entry : System.getProperties().entrySet()) {
			String key = valueOf(((Map.Entry) entry).getKey()), value = valueOf(((Map.Entry) entry).getValue());
			if (key.startsWith("dim.") && key.endsWith(".basedir"))
				modulePaths.put('[' + key.substring(4, key.length() - 8).replace('_', ':') + ']', value);
		}
		return modulePaths;
	}

	static File[] canonicalizePath(File basePath, String path) {
		File[] result = new File[1];
		if (isAbsolute(path)) {
			result[0] = new File(path);
		} else {
			String base;
			try {
				base = basePath.getCanonicalPath();
			} catch (IOException ignored) {
				base = basePath.getAbsolutePath();
			}
			result = resolveDefaultProjectPaths("siteDirectory", "src/site", base, path, result);
			if (result[0] == null) result = resolveDefaultProjectPaths("sourceDirectory", "src/main/java", base, path, result);
			if (result[0] == null) result = resolveDefaultProjectPaths("resourceDirectories", "src/main/resources", base, path, result);
			if (result[0] == null) result = resolveDefaultProjectPaths("outputDirectory", "target/classes", base, path, result);
			if (result[0] == null) result = resolveDefaultProjectPaths("testSourceDirectory", "src/test/java", base, path, result);
			if (result[0] == null) result = resolveDefaultProjectPaths("testResourceDirectories", "src/test/resources", base, path, result);
			if (result[0] == null) result = resolveDefaultProjectPaths("testOutputDirectory", "target/test-classes", base, path, result);
			if (result[0] == null) result = resolveDefaultProjectPaths("targetDirectory", "target", base, path, result);
			if (result[0] == null) result[0] = new File(basePath, path);
		}

		for (int i = 0; i < result.length; i++) {
			try {
				result[i] = result[i].getCanonicalFile();
			} catch (IOException e) {
				log.warn("Failed to retrieve the canonical path of " + result[i] + '.');
				if (log.isDebugEnabled()) log.debug(e.getMessage(), e);
			}
		}
		return result;
	}

	static File[] resolveDefaultProjectPaths(String projectPathKey, String projectPathPrefix,
	                                         String basePath, String path, File[] result) {
		path = path.replace('\\', '/');
		if (path.equals(projectPathPrefix) || path.startsWith(projectPathPrefix + '/')) {
			final String projectPath = getProperty("org.tinyjee.maven.dim.project." + projectPathKey);
			if (!StringUtils.isEmpty(projectPath)) {
				final List<String> paths = new ArrayList<String>(asList(split(projectPath, pathSeparator)));
				for (Iterator<String> iterator = paths.iterator(); iterator.hasNext(); ) {
					final String pp = iterator.next();
					if (!isPathBelowBase(basePath, pp)) {
						if (log.isDebugEnabled()) {
							log.debug("Ignoring project path '" + projectPathKey + ':' + pp + "' is not below the given " +
									"search base path of '" + basePath + '\'');
						}
						iterator.remove();
					}
				}

				if (!paths.isEmpty()) {
					result = result.length == paths.size() ? result : new File[paths.size()];
					for (int i = 0, len = paths.size(); i < len; i++)
						result[i] = new File(paths.get(i) + File.separator + path.substring(projectPathPrefix.length()));
				}
			}
		}

		return result;
	}

	private static boolean isPathBelowBase(String basePath, String path) {
		if (!isAbsolute(basePath)) return false;
		if (!isAbsolute(path)) return false;
		path = path.replace('\\', '/').trim();
		basePath = basePath.replace('\\', '/').trim();
		boolean caseSensitive = File.separatorChar == '/';
		return caseSensitive ? path.startsWith(basePath) : path.toLowerCase().startsWith(basePath.toLowerCase());
	}

	/**
	 * Builds a list of paths to search in.
	 *
	 * @param basePath      the base path to lookup the search paths against (if not absolute).
	 * @param proposedPaths an optional list of proposed paths (!!these paths are user input!!).
	 * @return an ordered list of paths to use for searching resources.
	 */
	public static List<File> buildDefaultSearchPaths(File basePath, String... proposedPaths) {
		final List<File> searchPaths = new ArrayList<File>();
		final File[] basePaths = {basePath, new File(".")};
		final List<String[]> partialPaths = new ArrayList<String[]>();

		if (proposedPaths != null && proposedPaths.length > 0) {
			for (int i = 0; i < proposedPaths.length; i++)
				proposedPaths[i] = toFilePath(proposedPaths[i]);
			partialPaths.add(proposedPaths);
		}

		partialPaths.add(DEFAULT_SITE_SOURCE_SEARCH_PATH);
		partialPaths.add(DEFAULT_SOURCE_SEARCH_PATH);
		partialPaths.add(new String[]{""});

		for (File path : basePaths) {
			if (path == null)
				continue;

			for (String[] paths : partialPaths) {
				for (String pp : paths) {
					if (pp == null) continue;

					for (File searchPath : canonicalizePath(path, pp)) {
						if (searchPath.exists() && !searchPaths.contains(searchPath))
							searchPaths.add(searchPath.getAbsoluteFile());
					}
				}
			}
		}

		return searchPaths;
	}

	/**
	 * Returns true if the given filePath string is absolute.
	 *
	 * @param filePath the filePath to check.
	 * @return true if the given filePath string is absolute.
	 */
	public static boolean isAbsolute(String filePath) {
		if (filePath == null)
			return false;
		if (filePath.startsWith("/") || filePath.startsWith("\\"))
			return true;

		return File.pathSeparatorChar != ':' && filePath.length() > 1 && filePath.charAt(1) == ':';
	}

	/**
	 * Converts the given source to a filePath if possible.
	 *
	 * @param source the source to convert.
	 * @return the filePath of the given source or 'null' if the source is an URL like 'ftp://' or 'http://'.
	 */
	public static String toFilePath(String source) {
		if (source != null) {
			// Normalize to unix paths
			source = source.replace('\\', '/');

			if (!isAbsolute(source)) {
				// Resolve file URIs
				try {
					URI sourceURI = new URI(source);
					if (sourceURI.getScheme() != null) {
						if ("file".equalsIgnoreCase(sourceURI.getScheme()))
							source = sourceURI.getPath();
						else
							source = null;
					}
				} catch (URISyntaxException ignored) {
					if (log.isDebugEnabled()) log.debug(source + " is not a file path, processing it as URL.");
				}
			}
		}

		return source;
	}

	/**
	 * Finds the given source within the specified search paths and adds the results as file
	 * URLs to the results list.
	 *
	 * @param searchPaths the paths to search in.
	 * @param source      the source path to search for.
	 * @param results     a list to append the results to.
	 */
	public static void findMatchingPaths(List<File> searchPaths, String source, List<URL> results) {
		source = toFilePath(source);
		if (source == null) return;

		for (File searchPath : searchPaths) {
			File file = new File(searchPath, source);
			if (file.exists())
				try {
					URL url = file.toURI().toURL();
					if (!results.contains(url)) results.add(url);
				} catch (MalformedURLException e) {
					throw new RuntimeException(e);
				}
		}
	}

	/**
	 * Resolves the non-file URLs and module path expressions that match the source.
	 *
	 * @param basePath the basePath of the maven project (e.g. containing the target folder).
	 * @param source   the source URI or module path expression.
	 * @param results  the result list to write to.
	 */
	public static void findMatchingURLs(File basePath, String source, List<URL> results) {
		if (isAbsolute(source)) return;

		try {
			String moduleReference = extractModuleReference(source);
			if (moduleReference != null) {
				final Set<String> moduleNames = extractModuleNames(moduleReference);
				final Set<String> paths = new LinkedHashSet<String>();

				for (String name : moduleNames) {
					String path = getModulePath(name);
					if (path == null) continue;
					paths.add(path);
				}

				if (!paths.isEmpty()) {
					final List<File> searchPaths = new ArrayList<File>();
					for (String path : paths) {
						searchPaths.addAll(buildDefaultSearchPaths(new File(path)));
					}
					findMatchingPaths(searchPaths, removeModuleReference(source, moduleReference), results);
					assertHasResult(source, results, "below the paths", searchPaths);
				} else {
					assertHasResult(source, results,
							"below the module base paths ('" + moduleReference + "' does not exist)", getModulePaths().entrySet());
				}
			} else {
				URI sourceURI = new URI(source);
				String scheme = sourceURI.getScheme();
				if (scheme != null && !"file".equalsIgnoreCase(scheme)) {
					if ("classpath".equalsIgnoreCase(scheme)) {
						URLClassLoader ucl = resolveClassLoader(basePath);
						String path = sourceURI.getPath();
						if (path.startsWith("/")) path = path.substring(1);
						for (Enumeration<URL> e = ucl.getResources(path); e.hasMoreElements(); ) {
							URL url = e.nextElement();
							if (!results.contains(url)) results.add(url);
						}

						assertHasResult(source, results,
								"inside Maven's site building classpath nor below the paths", asList(ucl.getURLs()));
					} else {
						results.add(sourceURI.toURL());
					}
				}
			}
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			if (log.isDebugEnabled()) log.debug(source + " points to an invalid or broken classpath.", e);
		} catch (Exception ignored) {
			if (log.isDebugEnabled()) log.debug(source + " is not an URI, not resolving it as URL.");
		}
	}

	private static String removeModuleReference(String source, String moduleName) {
		return source.substring(moduleName.length() + 3);
	}

	private static String extractModuleReference(String source) {
		return source.startsWith("[") && source.contains("]:") ? source.substring(1, source.indexOf("]:")) : null;
	}

	private static Set<String> extractModuleNames(String moduleName) {
		final Set<String> moduleNames = new LinkedHashSet<String>();
		moduleNames.add(moduleName.trim());
		for (String name : split(moduleName, ";")) {
			moduleNames.add(name.trim());
		}
		return moduleNames;
	}

	/**
	 * Finds the given source relative to the specified basePath.
	 * <p/>
	 * Note: Sources are resolved using absolute or relative file paths (the latter is using a list
	 * of paths to find the resource inside) OR sources can use any supported URI scheme including
	 * FTP and HTTP and lastly using the special scheme "CLASSPATH" in order to use the module's
	 * classpath to resolve the source.
	 *
	 * @param basePath the basePath of the maven project (e.g. containing the target folder).
	 * @param source   the source path or URI.
	 * @return the resolved URL.
	 * @throws IllegalArgumentException In case of the source was not resolvable.
	 */
	public static URL findSource(File basePath, String source) {
		if (isAbsolute(source)) {
			String filePath = toFilePath(source);
			File absoluteFile = filePath == null ? null : new File(filePath);
			if (absoluteFile != null && absoluteFile.exists()) {
				try {
					return absoluteFile.toURI().toURL();
				} catch (MalformedURLException e) {
					throw new IllegalStateException(e);
				}
			}
		}

		final List<URL> results = findAllSources(basePath, source);

		if (results.size() > 1) {
			int firstSrcFileIndex = -1, firstFileIndex = -1;
			for (int i = 0; i < results.size(); i++) {
				URL url = results.get(i);
				if ("file".equalsIgnoreCase(url.getProtocol())) {
					if (firstFileIndex == -1) firstFileIndex = i;
					if (firstSrcFileIndex == -1 && url.getPath().contains("/src/")) firstSrcFileIndex = i;
				}
			}

			if (firstFileIndex > -1) {
				int index = firstSrcFileIndex == -1 ? firstFileIndex : firstSrcFileIndex;
				results.set(index, results.set(0, results.get(index)));
				log.info("Found multiple matching sources " + results + " for " + source + ", using the first match.");
			}

			log.warn("Found matching sources @ " + results + " for " + source + ", using the first match.");
		} else if (log.isDebugEnabled())
			log.debug("Found matching source @ " + results + " for " + source + '.');

		return results.get(0);
	}

	/**
	 * Finds the given source relative to the specified basePath.
	 *
	 * @param basePath the basePath of the maven project (e.g. containing the target folder).
	 * @param source   the source path or URI.
	 * @return all URLs that contain this source.
	 */
	public static List<URL> findAllSources(File basePath, String source) {
		final List<URL> results = new ArrayList<URL>();

		findMatchingURLs(basePath, source, results);

		if (results.isEmpty()) {
			List<File> searchPaths = buildDefaultSearchPaths(basePath);
			findMatchingPaths(searchPaths, source, results);
			assertHasResult(source, results, "below the paths", searchPaths);
		}

		return results;
	}

	private static void assertHasResult(String source, Collection<?> results, String where, Collection<?> searchPaths) {
		if (results.isEmpty()) {
			final StringBuilder builder = buildNotFoundMessage(source, where, searchPaths);
			throw new IllegalArgumentException(builder.toString());
		}
	}

	private static StringBuilder buildNotFoundMessage(String source, String where, Collection<?> searchPaths) {
		final String separator = getProperty("line.separator");
		final StringBuilder builder = new StringBuilder().append("Didn't find ");

		builder.append('\'').append(source).append("' ").append(where).append(": ");
		for (Object path : searchPaths) builder.append(separator).append("-- ").append(path);
		return builder;
	}

	/**
	 * Resolves the specified class.
	 *
	 * @param basePath  the basePath of the maven project (e.g. containing the target folder).
	 * @param className the name of the class to resolve.
	 * @return The class.
	 * @throws IllegalArgumentException In case of the class was not found.
	 */
	public static Class<?> resolveClass(File basePath, String className) {
		try {
			return Class.forName(className);
		} catch (ClassNotFoundException ignored) {
			File[] basePaths = null;

			final String moduleReference = extractModuleReference(className);
			if (moduleReference != null) {
				className = removeModuleReference(className, moduleReference);

				final Set<String> moduleNames = extractModuleNames(moduleReference);
				final Set<String> paths = new LinkedHashSet<String>();

				for (String name : moduleNames) {
					String path = getModulePath(name);
					if (path == null) continue;

					paths.add(path);

					// Adding module dependencies as well.
					for (String dependency : getModuleDependencies(name)) {
						final String[] strings = split(dependency, ":");
						if (strings.length > 1) {
							final String modulePath = getModulePath(strings[0] + '_' + strings[1]);
							if (modulePath != null) {
								paths.add(modulePath);
							}
						}
					}
				}

				assertHasResult(className, paths,
						"below the module base paths ('" + moduleReference + "' does not exist)", getModulePaths().entrySet());

				int i = 0;
				basePaths = new File[paths.size()];
				for (String path : paths) basePaths[i++] = new File(path);
			} else
				basePaths = new File[]{basePath};

			final URLClassLoader loader = resolveClassLoader(basePaths);
			try {
				return Class.forName(className, true, loader);
			} catch (ClassNotFoundException e1) {
				final String where = moduleReference == null ? "inside Maven's site building classpath nor below the paths" :
						"inside Maven's site building classpath, the module path '[" +
								moduleReference + "]:" + getModulePath(moduleReference) + "', nor below the paths";
				String message = buildNotFoundMessage(className, where, asList(loader.getURLs())).toString();
				throw new IllegalArgumentException(message, e1);
			}
		}
	}

	/**
	 * Resolves a class loader for the given base path.
	 *
	 * @param basePaths the basePaths of the maven project (e.g. containing the target folder).
	 * @return a URLClassLoader that is capable of loading all classes defined inside the current module.
	 *         (without transitive dependencies)
	 */
	public static synchronized URLClassLoader resolveClassLoader(File... basePaths) {
		if (basePaths == null || basePaths.length == 0 || basePaths[0] == null)
			return (URLClassLoader) ResourceResolver.class.getClassLoader();

		// TODO: Implement ".class"-file modified checking as used in UrlFetcher.

		final List<String> clKey = classLoaderKey(basePaths);
		URLClassLoader ulc = currentClassLoader.get();
		if (ulc == null || currentClassLoaderKey == null || !currentClassLoaderKey.equals(clKey)) {
			try {
				Set<String> classpath = new LinkedHashSet<String>();
				for (File basePath : basePaths) {
					classpath.add(getProperty("org.tinyjee.maven.dim.project.outputDirectory", new File(basePath, CLASSES_PATH).getAbsolutePath()));
					classpath.add(getProperty("org.tinyjee.maven.dim.project.testOutputDirectory", new File(basePath, TEST_CLASSES_PATH).getAbsolutePath()));

					// Always add default paths as well
					classpath.add(new File(basePath, CLASSES_PATH).getAbsolutePath());
					classpath.add(new File(basePath, TEST_CLASSES_PATH).getAbsolutePath());
				}

				String projectClassPath = getProperty("org.tinyjee.maven.dim.include.project.classpath");
				if (projectClassPath != null) addAll(classpath, split(projectClassPath, pathSeparator));

				String projectTestClassPath = getProperty("org.tinyjee.maven.dim.include.project.test.classpath");
				if (projectTestClassPath != null) addAll(classpath, split(projectTestClassPath, pathSeparator));

				if (log.isDebugEnabled())
					log.debug("Creating a new classloader to load classes via 'source-class', using the classpath:" + classpath);

				int i = 0;
				URL[] urls = new URL[classpath.size()];
				for (String file : classpath) urls[i++] = new File(file).toURI().toURL();
				ulc = new URLClassLoader(urls, ResourceResolver.class.getClassLoader());

				currentClassLoader = new SoftReference<URLClassLoader>(ulc);
				currentClassLoaderKey = clKey;
			} catch (Exception e) {
				throw new RuntimeException("Failed to build search path to lookup .class files.", e);
			}
		}

		return ulc;
	}

	static List<String> classLoaderKey(File... basePath) {
		String[] paths = new String[basePath.length];
		for (int i = 0; i < paths.length; i++) paths[i] = basePath[i].getAbsolutePath();

		Arrays.sort(paths);

		return asList(paths);
	}

	/**
	 * Finds the source directory for the project site.
	 *
	 * @param baseDir         the base directory of the project.
	 * @param proposedSiteDir an optional proposal where the site directory may be.
	 * @return the source directory for the project site or 'null' if it wasn't found.
	 */
	public static File findSiteSourceDirectory(File baseDir, File proposedSiteDir) {
		final List<File> paths = buildDefaultSearchPaths(baseDir, proposedSiteDir == null ? null : proposedSiteDir.getAbsolutePath());

		for (File file : paths) {
			if ("site".equals(file.getName())) {
				if (log.isDebugEnabled()) log.debug("Using '" + file + "/resources', to append CSS style sheets.");
				return file;
			}
		}

		if (log.isDebugEnabled()) log.debug("Didn't find site directory below " + paths + ", will inline CSS styles.");
		return null;
	}

	private ResourceResolver() {
	}
}
