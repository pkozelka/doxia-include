/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import org.tinyjee.maven.dim.utils.ReducedVerbosityMap;

import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.util.Collections;
import java.util.Map;

import static java.util.Collections.unmodifiableMap;

/**
 * Base implementation for sources.
 *
 * @author Juergen_Kellerer, 2011-10-19
 */
public abstract class AbstractSource implements Source {

	protected final URL sourceUrl;
	protected final Map<String, Object> parameters;

	/**
	 * Constructs a new instance of AbstractSource.
	 *
	 * @param sourceUrl  the source 'url' or null if the source is not bound to an url.
	 * @param parameters the macro parameters assigned with this source.
	 */
	protected AbstractSource(URL sourceUrl, Map<String, Object> parameters) {
		this.sourceUrl = sourceUrl;
		this.parameters = new ReducedVerbosityMap<String, Object>(parameters == null ? Collections.<String, Object>emptyMap() : parameters);
	}

	public URL getUrl() {
		return sourceUrl;
	}

	public Content getContent() {
		return new SourceContent();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + '{' +
				"sourceUrl=" + sourceUrl +
				", parameters=" + parameters +
				'}';
	}

	/**
	 * Base implementation for source content.
	 */
	protected class SourceContent implements Content {
		public Map<String, Object> getParameters() {
			return unmodifiableMap(parameters);
		}

		public Reader openReader() throws IOException {
			return sourceUrl == null ? null : UrlFetcher.getReadableSource(sourceUrl);
		}

		@Override
		public String toString() {
			return "SourceContent{" +
					"super=" + AbstractSource.this.toString() +
					'}';
		}
	}
}
