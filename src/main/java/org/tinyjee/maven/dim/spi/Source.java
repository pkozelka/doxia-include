/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * Defines an abstract interface for sources that may be a mixture of source content (multiple snippets)
 * and assigned meta data (parameters).
 *
 * @author Juergen_Kellerer, 2010-09-04
 */
public interface Source {
	/**
	 * Defines actual source content.
	 */
	interface Content {
		/**
		 * Returns the parameters assigned with the content.
		 *
		 * @return the parameters assigned with the content.
		 */
		Map<String, Object> getParameters();

		/**
		 * Returns the content as text stream.
		 *
		 * @return a reader containing the content to include.
		 * @throws IOException in case of reading the sections failed.
		 */
		Reader openReader() throws IOException;
	}

	/**
	 * Returns the underlying source URL or 'null' if this Source doesn't refer to an URL.
	 *
	 * @return the underlying source URL or 'null' if this Source doesn't refer to an URL.
	 */
	URL getUrl();

	/**
	 * Retrieves the source content.
	 *
	 * @return the source content.
	 */
	Content getContent();
}
