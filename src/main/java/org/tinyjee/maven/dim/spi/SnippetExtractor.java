/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import org.apache.maven.doxia.logging.Log;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Boolean.parseBoolean;
import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;
import static java.util.Arrays.asList;
import static org.tinyjee.maven.dim.IncludeMacroSignature.*;

/**
 * Implements a singleton that uses snippet selectors and sources to extract portions of content and format it as a map of selected lines,
 * indexed by their starting line numbers.
 *
 * @author Juergen_Kellerer, 2011-10-20
 */
public final class SnippetExtractor {

	private static final Log log = Globals.getLog();
	/**
	 * Defines the system dependent line break to use.
	 */
	public static final String LB = System.getProperty("line.separator", "\n");

	/**
	 * Defines the indentation chars to look after.
	 */
	private static final String INDENT_CHARS = System.getProperty("org.tinyjee.maven.dim.indentChars", "\t ");

	/**
	 * Defines the increment width that should be measured for the relevant char.
	 */
	private static final int[] INDENT_WIDTH;

	static {
		final String[] widths = System.getProperty("org.tinyjee.maven.dim.indentWidths", "4, 1").split("\\s*,\\s*");
		INDENT_WIDTH = new int[widths.length];
		for (int i = 0; i < widths.length; i++) INDENT_WIDTH[i] = parseInt(widths[i]);
	}

	private static final Pattern EXPRESSION_SPLIT_PATTERN;

	static {
		StringBuilder splitPattern = new StringBuilder();
		for (SnippetSelector selector : SnippetSelector.SELECTORS) {
			for (String prefix : selector.getExpressionPrefixes()) {
				splitPattern.append(splitPattern.length() == 0 ? '(' : '|');
				splitPattern.append("(^|\\s*,)[\\s!]*").append(Pattern.quote(prefix));
			}
		}
		EXPRESSION_SPLIT_PATTERN = Pattern.compile(splitPattern.append(')').toString(), Pattern.CASE_INSENSITIVE);
	}

	private static final SnippetExtractor instance = new SnippetExtractor();

	/**
	 * Returns a singleton instance of this class.
	 *
	 * @return a singleton instance of this class.
	 */
	public static SnippetExtractor getInstance() {
		return instance;
	}

	/**
	 * Splits a given snippet expression into the individual sub-expression parts.
	 *
	 * @param expression the expression to split.
	 * @return the expression parts.
	 */
	public String[] splitExpression(String expression) {
		int index = 0;
		String expressionPrefix = "";
		ArrayList<String> matchList = new ArrayList<String>();
		Matcher matcher = EXPRESSION_SPLIT_PATTERN.matcher(expression);
		while (matcher.find()) {
			String match = expressionPrefix.concat(expression.substring(index, matcher.start()));
			if (match.length() != 0) matchList.add(match);
			index = matcher.end();

			expressionPrefix = matcher.group(1).trim();
			if (expressionPrefix.startsWith(","))
				expressionPrefix = expressionPrefix.substring(1).trim();
		}

		if (index == 0) return new String[]{expression};

		// Add remaining
		if (index < expression.length())
			matchList.add(expressionPrefix.concat(expression.substring(index, expression.length())));
		else if (expressionPrefix.length() > 0)
			matchList.add(expressionPrefix);

		// Construct result
		return matchList.toArray(new String[matchList.size()]);
	}

	/**
	 * Returns true if the given source require snippet selection.
	 *
	 * @param source the source to test.
	 * @return true if the given source require snippet selection.
	 */
	public boolean isSnippetSelectionRequired(Source source) {
		Map<String, Object> parameters = source.getContent().getParameters();
		return parameters.get(PARAM_SNIPPET) != null || hasLegacySnippetId(parameters);
	}

	/**
	 * Selects the snippets of the given source.
	 *
	 * @param source the source to select the snippets in.
	 * @return a map of first-line-number to lines mapping.
	 * @throws IOException in case of reading the source failed.
	 */
	public SortedMap<Integer, List<String>> selectSnippets(Source source) throws IOException {
		final SortedMap<Integer, List<String>> result;

		if (isSnippetSelectionRequired(source)) {
			final List<SortedSet<Integer>> includesAndExcludes = findSelectedLines(source);
			final Reader reader = source.getContent().openReader();
			try {
				result = extractSnippets(reader, includesAndExcludes.get(0), includesAndExcludes.get(1));
				unIndent(result, parseBoolean(valueOf(source.getContent().getParameters().get("unindent-blocks"))));
			} finally {
				reader.close();
			}
		} else {
			Reader contentReader = source.getContent().openReader();
			try {
				result = convert(contentReader);
			} finally {
				contentReader.close();
			}
		}

		return result;
	}

	/**
	 * Low level method that extracts the snippets of the given source content.
	 *
	 * @param reader        the reader containing the source.
	 * @param includedLines the line numbers to include, if empty all is included (as long as exclusions exist).
	 * @param excludedLines the line numbers to exclude.
	 * @return a map of first-line-number to lines mapping.
	 * @throws IOException in case of reading the source failed.
	 */
	public SortedMap<Integer, List<String>> extractSnippets(
			Reader reader, Set<Integer> includedLines, Set<Integer> excludedLines) throws IOException {

		// TODO: If we have many lines in the sets, we may want to create a streaming map here.

		final boolean isExcludeOnlyMode = includedLines.isEmpty() && !excludedLines.isEmpty();
		final SortedMap<Integer, List<String>> result = new TreeMap<Integer, List<String>>();
		final LineNumberReader lines = toLineNumberReader(reader);

		String line;
		Integer lastIndex = null;
		while ((line = lines.readLine()) != null) {
			Integer lineNumber = lines.getLineNumber();
			if ((isExcludeOnlyMode || includedLines.contains(lineNumber)) && !excludedLines.contains(lineNumber)) {
				if (lastIndex == null) lastIndex = lineNumber;

				List<String> snippetLines = result.get(lastIndex);
				if (snippetLines == null)
					result.put(lastIndex, snippetLines = new ArrayList<String>(64));

				snippetLines.add(line);
			} else
				lastIndex = null;
		}

		return result;
	}

	/**
	 * Low level method that converts the complete source to the snippet map format.
	 * <p/>
	 * This is a convenience method to calling {@code extractSnippets(contentReader, emptySet(), singleton(-1))}.
	 *
	 * @param contentReader the reader containing the source.
	 * @return a map of first-line-number to lines mapping.
	 * @throws IOException in case of reading the source failed.
	 */
	public SortedMap<Integer, List<String>> convert(Reader contentReader) throws IOException {
		return extractSnippets(contentReader, Collections.<Integer>emptySet(), Collections.singleton(-1));
	}

	/**
	 * Flattens the given content to pure text using system specific line breaks.
	 *
	 * @param content            the content to write.
	 * @param addEmptyLineBreaks If true, will add any additional line breaks to re-construct the number of lines of the original document.
	 * @return a String of containing all content.
	 */
	public String toString(Map<Integer, List<String>> content, boolean addEmptyLineBreaks) {
		int lastLineNumber = 1;
		StringBuilder out = new StringBuilder(255);
		for (Map.Entry<Integer, List<String>> entry : content.entrySet()) {
			if (addEmptyLineBreaks) {
				int currentLineNumber = entry.getKey();
				if (lastLineNumber < currentLineNumber) {
					while (++lastLineNumber < currentLineNumber) out.append(LB);
				}
			}

			toString(entry.getValue(), out);
		}
		return out.toString();
	}

	/**
	 * Flattens the given lines to pure text using system specific line breaks.
	 *
	 * @param lines the lines to write.
	 * @return a String of containing all content.
	 */
	public String toString(List<String> lines) {
		return toString(lines, new StringBuilder(100 * lines.size())).toString();
	}

	/**
	 * Flattens the given lines to pure text using system specific line breaks.
	 *
	 * @param lines the lines to write.
	 * @param out   The StringBuilder instance to write to.
	 * @return The StringBuilder instance.
	 */
	public StringBuilder toString(List<String> lines, StringBuilder out) {
		for (String line : lines) {
			if (out.length() > 0) out.append(LB);
			out.append(line);
		}
		return out;
	}

	/**
	 * Removes unnecessary indents.
	 *
	 * @param snippetMap the snippets.
	 * @param blockScope true to process every snippet separately, false to detect indent to remove using a global scope.
	 */
	public void unIndent(SortedMap<Integer, List<String>> snippetMap, boolean blockScope) {
		final Map<Integer, Integer> minIndents = new HashMap<Integer, Integer>(snippetMap.size());
		if (blockScope) {
			for (Map.Entry<Integer, List<String>> entry : snippetMap.entrySet())
				minIndents.put(entry.getKey(), minIndent(entry.getValue()));
		} else {
			int minIndent = Integer.MAX_VALUE;
			for (List<String> lines : snippetMap.values()) minIndent = Math.min(minIndent, minIndent(lines));
			for (Integer key : snippetMap.keySet()) minIndents.put(key, minIndent);
		}

		for (Map.Entry<Integer, List<String>> entry : snippetMap.entrySet()) {
			int minIndent = minIndents.get(entry.getKey());
			for (ListIterator<String> iterator = entry.getValue().listIterator(); iterator.hasNext(); )
				iterator.set(unIndent(iterator.next(), minIndent));
		}
	}


	boolean hasLegacySnippetId(Map<String, Object> parameters) {
		return parameters.get(PARAM_SNIPPET_ID) != null &&
				parameters.get(PARAM_SOURCE) != null && !"true".equals(parameters.get(PARAM_SOURCE_IS_TEMPLATE));
	}

	String extractRawExpression(Map<String, Object> parameters) {
		String rawExpression = (String) parameters.get(PARAM_SNIPPET);
		if (hasLegacySnippetId(parameters)) {
			if (parameters.containsKey(PARAM_SNIPPET)) {
				log.warn("Both snippet selection parameter, 'id' and 'snippet' found. Ignoring the value of 'id' as it was deprecated.");
			} else {
				String id = (String) parameters.get(PARAM_SNIPPET_ID);
				log.warn("Found deprecated snippet selection parameter 'id', please use 'snippet=#" + id + "' instead.");
				rawExpression = '#' + id;
			}
		}

		if (rawExpression == null) throw new IllegalArgumentException("No snippet expression was defined in " + parameters + '.');
		return rawExpression;
	}

	/**
	 * Finds the selected lines given the specified source.
	 *
	 * @param source the source to process.
	 * @return a list with 2 elements: included and excluded line numbers.
	 * @throws IOException in case of reading the source failed.
	 */
	public List<SortedSet<Integer>> findSelectedLines(Source source) throws IOException {
		final boolean debug = log.isDebugEnabled();
		final Source.Content content = source.getContent();
		final Map<String, Object> parameters = content.getParameters();

		URL sourceUrl = source.getUrl() == null ? FixedContentSource.UNKNOWN : source.getUrl();
		String rawExpression = extractRawExpression(parameters);
		String[] expressions = splitExpression(rawExpression);
		boolean hadIncludes = false;
		if (debug) log.debug("Extracting snippets using '" + rawExpression + "', parsed as " + Arrays.toString(expressions));

		SortedSet<Integer> includedLines = new TreeSet<Integer>(), excludedLines = new TreeSet<Integer>();
		for (String expression : expressions) {
			boolean isExclusion = expression.startsWith("!");
			if (isExclusion) expression = expression.substring(1);

			for (SnippetSelector selector : SnippetSelector.SELECTORS) {
				if (selector.canSelectSnippetsWith(expression, sourceUrl, parameters)) {
					if (debug) {
						log.debug("About to " + (isExclusion ? "exclude" : "select") +
								" snippets with " + selector + " using expression " + expression);
					}
					final Reader reader = content.openReader();
					try {
						Iterator<Integer> linesIterator = selector.selectSnippets(
								expression, sourceUrl, toLineNumberReader(reader), parameters);

						Set<Integer> removeFrom = isExclusion ? includedLines : excludedLines,
								addTo = isExclusion ? excludedLines : includedLines;
						while (linesIterator.hasNext()) {
							Integer line = linesIterator.next();
							removeFrom.remove(line);
							addTo.add(line);
						}

						hadIncludes |= !includedLines.isEmpty();
					} finally {
						reader.close();
					}

					if (debug) {
						log.debug("Selected lines after processing selector:\n" +
								" included:" + includedLines + "\n excluded:" + excludedLines);
					}
				}
			}
		}

		// Clearing excluded lines if we had includes before to fix the case https://sourceforge.net/p/doxia-include/tickets/11/
		if (hadIncludes && includedLines.isEmpty()) excludedLines.clear();

		growSelectedLineOffsets(parameters, includedLines);
		if (parseBoolean(valueOf(parameters.get(PARAM_SNIPPET_GROW_EXCLUDES))))
			growSelectedLineOffsets(parameters, excludedLines);

		@SuppressWarnings("unchecked")
		List<SortedSet<Integer>> sets = asList(includedLines, excludedLines);
		return sets;
	}

	void growSelectedLineOffsets(Map<String, Object> parameters, SortedSet<Integer> selectedLines) {
		int growStart = toInt(parameters.get(PARAM_SNIPPET_START_OFFSET), toInt(parameters.get(PARAM_SNIPPET_GROW_OFFSET), 0));
		int growEnd = toInt(parameters.get(PARAM_SNIPPET_END_OFFSET), toInt(parameters.get(PARAM_SNIPPET_GROW_OFFSET), 0));
		if ((growStart > 0 || growEnd > 0) && !selectedLines.isEmpty()) {
			Integer currentLine = selectedLines.first();
			for (Integer lineNumber : selectedLines.toArray(new Integer[selectedLines.size()])) {
				if (!lineNumber.equals(currentLine) && lineNumber != currentLine + 1) {
					for (int i = 1; i <= growEnd; i++) selectedLines.add(currentLine + i);
					for (int i = 1; i <= growStart; i++) selectedLines.add(lineNumber - i);
				}
				currentLine = lineNumber;
			}
			for (int i = 1, line = selectedLines.first(); i <= growStart; i++) selectedLines.add(line - i);
			for (int i = 1, line = selectedLines.last(); i <= growEnd; i++) selectedLines.add(line + i);
		}
	}

	int toInt(Object value, int defaultValue) {
		return value == null ? defaultValue : value instanceof Number ? ((Number) value).intValue() : parseInt(valueOf(value));
	}

	LineNumberReader toLineNumberReader(Reader reader) {
		return reader instanceof LineNumberReader ? (LineNumberReader) reader : new LineNumberReader(reader);
	}

	static int minIndent(List<String> lines) {
		int minIndent = Integer.MAX_VALUE;
		for (String line : lines)
			minIndent = Math.min(minIndent, detectIndentWith(line));
		return minIndent;
	}

	static int detectIndentWith(String line) {
		for (int indent = 0, i = 0, len = line.length(); i < len; i++) {
			int idx = INDENT_CHARS.indexOf(line.charAt(i));
			if (idx == -1)
				return indent;
			indent += INDENT_WIDTH[idx];
		}
		return Integer.MAX_VALUE;
	}

	static String unIndent(String line, int indentWidth) {
		int indent = 0;
		for (int i = 0, len = line.length(); i < len; i++) {
			int index = INDENT_CHARS.indexOf(line.charAt(i));
			if (index == -1) break;

			indent += INDENT_WIDTH[index];
			if (indent == indentWidth)
				return line.substring(i + 1);
		}
		return line;
	}

	private SnippetExtractor() {
	}
}
