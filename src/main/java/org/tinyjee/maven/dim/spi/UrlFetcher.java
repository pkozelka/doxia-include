/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import org.apache.maven.doxia.logging.Log;
import org.codehaus.plexus.util.StringUtils;
import org.mozilla.intl.chardet.nsDetector;
import org.mozilla.intl.chardet.nsICharsetDetectionObserver;
import org.tinyjee.maven.dim.utils.CompositeInputStream;

import java.io.*;
import java.lang.ref.SoftReference;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static java.lang.Boolean.parseBoolean;
import static java.lang.String.valueOf;
import static java.nio.ByteBuffer.wrap;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_CHARSET_AUTODETECT;
import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_NO_CACHE;

/**
 * Abstracts url fetching, charset handling and caching.
 *
 * @author Juergen_Kellerer, 2011-10-13
 */
public class UrlFetcher {

	private static final Log log = Globals.getLog();
	/**
	 * Defines the max size of the URL content cache.
	 */
	public static final int MAX_CACHE_SIZE = 64;

	/**
	 * Defines the maximum amount of bytes to keep inside URL content cache per cached element.
	 */
	public static final int MAX_CACHED_CONTENT_SIZE = (int) Math.max(1024 * 1024, Math.min(Integer.MAX_VALUE, 0.05D * Runtime.getRuntime().maxMemory()));

	/**
	 * Defines a static URL content cache using soft references (to avoid OutOfMemory errors).
	 */
	private static final Map<String, UrlContent> cache = new LinkedHashMap<String, UrlContent>(MAX_CACHE_SIZE, 0.75f, true) {
		@Override
		protected boolean removeEldestEntry(Map.Entry<String, UrlContent> eldest) {
			return size() > MAX_CACHE_SIZE;
		}
	};

	private static final Charset US_ASCII = Charset.forName("US-ASCII");
	private static final Map<String, byte[]> BYTE_ORDER_MARKS = new LinkedHashMap<String, byte[]>();

	static {
		BYTE_ORDER_MARKS.put("UTF-8", new byte[]{(byte) 0xEF, (byte) 0xBB, (byte) 0xBF});
		BYTE_ORDER_MARKS.put("UTF-16BE", new byte[]{(byte) 0xFE, (byte) 0xFF});
		BYTE_ORDER_MARKS.put("UTF-16LE", new byte[]{(byte) 0xFF, (byte) 0xFE});
		BYTE_ORDER_MARKS.put("UTF-32BE", new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0xFE, (byte) 0xFF});
		BYTE_ORDER_MARKS.put("UTF-32LE", new byte[]{(byte) 0xFF, (byte) 0xFE, (byte) 0x00, (byte) 0x00});
	}


	private static final class UrlContent {

		private final URL sourceUrl;

		private volatile boolean sourceIsOverSizeLimit;
		private volatile String sourceCharset;

		private volatile long cacheLastModified;
		private SoftReference<byte[]> cachedContent;

		private UrlContent(URL sourceUrl) {
			this.sourceUrl = sourceUrl;
		}

		synchronized InputStream openContent(boolean noCache) throws IOException {
			final boolean debug = log.isDebugEnabled();

			InputStream contentStream = null;

			if (sourceIsOverSizeLimit) {
				if (debug) log.debug("Returning un-cached, buffered input stream for oversized content from " + sourceUrl);
				contentStream = new BufferedInputStream(sourceUrl.openStream(), 128 * 1024);
			} else {
				byte[] bytes = cachedContent == null || noCache ? null : cachedContent.get();

				if (bytes != null && isModified()) {
					log.info("The source " + sourceUrl + " was modified, flushing the cached content.");
					bytes = null;
				}

				if (bytes == null) {
					if (debug) log.debug("Starting to buffer content from " + sourceUrl);
					URLConnection urlConnection = sourceUrl.openConnection();
					cacheLastModified = urlConnection.getLastModified();
					sourceCharset = urlConnection.getContentEncoding();
					if (sourceCharset != null) {
						if (debug) log.debug("Found source charset is " + sourceCharset);
					}

					InputStream inputStream = urlConnection.getInputStream();
					try {
						int r = 0;
						byte[] transferBuffer = new byte[128 * 1024];
						ByteArrayOutputStream buffer = new ByteArrayOutputStream();
						while ((r = inputStream.read(transferBuffer)) != -1) {
							buffer.write(transferBuffer, 0, r);

							if (buffer.size() > MAX_CACHED_CONTENT_SIZE) {
								sourceIsOverSizeLimit = true;
								if (debug) log.debug("Source " + sourceUrl + " exceeds max buffer size of " + MAX_CACHED_CONTENT_SIZE + " bytes.");
								contentStream = new CompositeInputStream(new ByteArrayInputStream(buffer.toByteArray()), new BufferedInputStream(inputStream));
								inputStream = null;
								break;
							}
						}

						if (contentStream == null) {
							bytes = buffer.toByteArray();
							cachedContent = new SoftReference<byte[]>(bytes);
						}
					} finally {
						if (inputStream != null) inputStream.close();
					}
				}

				if (bytes != null) {
					if (debug) log.debug("Returning cached content from " + sourceUrl);
					contentStream = new ByteArrayInputStream(bytes);
				}
			}

			return contentStream;
		}

		boolean isModified() {
			return cacheLastModified != getLastModified();
		}

		long getLastModified() {
			if ("file".equalsIgnoreCase(sourceUrl.getProtocol())) {
				final Log log = Globals.getLog();
				try {
					return new File(sourceUrl.toURI()).lastModified();
				} catch (URISyntaxException ignored) {
					log.warn("Failed to look after last-modification for source " + sourceUrl + " could not convert the URL to a file");
				}
			}
			return cacheLastModified;
		}

		@Override
		public String toString() {
			return "UrlContent{" +
					"sourceUrl=" + sourceUrl +
					", cacheLastModified=" + new Date(cacheLastModified) +
					", sourceIsOverSizeLimit=" + sourceIsOverSizeLimit +
					", sourceCharset=" + sourceCharset +
					'}';
		}
	}

	private static synchronized UrlContent getUrlContent(URL sourceUrl) {
		final String urlString = sourceUrl.toExternalForm();
		UrlContent urlContent = cache.get(urlString);
		if (urlContent == null) cache.put(urlString, urlContent = new UrlContent(sourceUrl));

		return urlContent;
	}

	/**
	 * Returns the content stream of the given source URL.
	 *
	 * @param sourceUrl the source URL to open.
	 * @return the content stream of the given source URL.
	 * @throws java.io.IOException in case of the IO transfer failed.
	 */
	public static InputStream getSource(URL sourceUrl) throws IOException {
		return getSource(sourceUrl, false);
	}

	/**
	 * Returns the content stream of the given source URL.
	 *
	 * @param sourceUrl the source URL to open.
	 * @param noCache   whether the content shall be cached / retrieved from the cache or not.
	 * @return the content stream of the given source URL.
	 * @throws java.io.IOException in case of the IO transfer failed.
	 */
	public static InputStream getSource(URL sourceUrl, boolean noCache) throws IOException {
		return getUrlContent(sourceUrl).openContent(noCache);
	}

	/**
	 * Returns the last modification date of the specified url.
	 *
	 * @param sourceUrl the url to retrieve the last modified date for.
	 * @return The last modified date or 0 if unknown.
	 */
	public static long getLastModified(URL sourceUrl) {
		return getUrlContent(sourceUrl).getLastModified();
	}

	/**
	 * Returns true if the given url was modified between the last read and this call.
	 *
	 * @param sourceUrl the source url to check.
	 * @return true if the given url was modified between the last read and this call.
	 */
	public static boolean isModified(URL sourceUrl) {
		return getUrlContent(sourceUrl).isModified();
	}

	/**
	 * Returns the content text stream of the given source URL.
	 *
	 * @param sourceUrl the source URL to open.
	 * @return the content stream of the given source URL.
	 * @throws java.io.IOException in case of the IO transfer failed.
	 */
	public static BufferedReader getReadableSource(URL sourceUrl) throws IOException {
		boolean noCache = parseBoolean(valueOf(getRawRequestParameter(PARAM_NO_CACHE)));
		return getReadableSource(sourceUrl, noCache);
	}

	/**
	 * Returns the content text stream of the given source URL.
	 *
	 * @param sourceUrl the source URL to open.
	 * @param noCache   whether the content shall be cached / retrieved from the cache or not.
	 * @return the content stream of the given source URL.
	 * @throws java.io.IOException in case of the IO transfer failed.
	 */
	public static BufferedReader getReadableSource(URL sourceUrl, boolean noCache) throws IOException {
		Charset sourceCharset = getSourceCharset(sourceUrl);
		return new BufferedReader(new InputStreamReader(getSource(sourceUrl, noCache), sourceCharset));
	}

	/**
	 * Utility method that converts the given text stream to a string.
	 *
	 * @param reader      the reader to convert.
	 * @param closeReader whether the reader should be closed after reading.
	 * @return the text contained in the reader.
	 * @throws IOException in case of the IO transfer failed.
	 */
	public static String readerToString(Reader reader, boolean closeReader) throws IOException {
		try {
			StringBuilder builder = new StringBuilder(512);
			int count;
			char[] chars = new char[512];
			while ((count = reader.read(chars)) > 0) builder.append(chars, 0, count);

			return builder.toString();
		} finally {
			if (closeReader) reader.close();
		}
	}

	/**
	 * Returns the charset that is used to decode the content that is referenced by the given URL.
	 * <p/>
	 * Depending on the request parameters, this method reads a portion of the content to auto-detect what charset fits best.
	 *
	 * @param sourceUrl the url to retrieve the charset for.
	 * @return the charset that is used to decode the content that is referenced by the given URL.
	 */
	public static Charset getSourceCharset(URL sourceUrl) {
		final boolean debug = log.isDebugEnabled();
		final Globals globals = Globals.getInstance();
		final UrlContent urlContent = getUrlContent(sourceUrl);

		Charset charset = null;
		String sourceCharset = urlContent.sourceCharset;
		try {
			if (sourceCharset != null) charset = Charset.forName(sourceCharset);
		} catch (Exception e) {
			log.warn("Failed retrieving charset for name '" + sourceCharset + "', will attempt to use auto detection instead.");
			if (debug) log.debug(e.getMessage(), e);
		}

		if (charset == null) {
			charset = globals.getCharset();
			if (!globals.getCharsetIsUserSupplied() && !"false".equalsIgnoreCase(getCharsetAutoDetectParameter())) {
				try {
					if (debug) log.debug("Starting charset auto detection on " + sourceUrl);
					InputStream inputStream = urlContent.openContent(false);
					try {
						charset = autoDetectCharset(inputStream, charset);
						if (charset != null) urlContent.sourceCharset = charset.name();
						if (debug) log.debug("Auto detection returned the charset '" + charset + "' for " + sourceUrl);
					} finally {
						inputStream.close();
					}
				} catch (IOException e) {
					log.warn("Failed auto detecting charset for url '" + sourceUrl + "' using '" + charset + "' instead.", e);
				}
			}
		}

		return charset;
	}

	private static String getCharsetAutoDetectParameter() {
		Object rawValue = getRawRequestParameter(PARAM_CHARSET_AUTODETECT);
		return rawValue == null ? null : valueOf(rawValue);
	}

	private static Object getRawRequestParameter(String name) {
		Globals globals = Globals.getInstance();
		return globals.getRequest() == null ? null : globals.getRequest().getParameter(name);
	}

	private static Charset autoDetectCharset(InputStream inputStream, Charset fallback) throws IOException {
		final boolean debug = log.isDebugEnabled();
		final AtomicReference<Charset> exactMatch = new AtomicReference<Charset>();
		nsDetector detector = new nsDetector();
		detector.Init(new nsICharsetDetectionObserver() {
			public void Notify(String charset) {
				if (debug) log.debug("Detected charset " + charset);
				exactMatch.set(Charset.forName(charset));
			}
		});

		boolean isAscii = true;
		String charsetFromBom = null;
		final byte[] buffer = new byte[1024];

		int read, totalRead = 0, maxRead = 128 * 1024;
		while ((read = inputStream.read(buffer, 0, buffer.length)) != -1) {
			// Detect byte order mark of the UTF family
			if (charsetFromBom == null && read > 0) {
				charsetFromBom = "";
				for (Map.Entry<String, byte[]> entry : BYTE_ORDER_MARKS.entrySet()) {
					if (wrap(entry.getValue()).equals(wrap(buffer, 0, Math.min(read, entry.getValue().length))))
						charsetFromBom = entry.getKey();
				}
			}

			// Check if the stream is only ascii.
			if (isAscii) isAscii = detector.isAscii(buffer, read);
			// DoIt if non-ascii and not done yet.
			if (!isAscii && detector.DoIt(buffer, read, false))
				break;

			// Limiting the amount of bytes to read to avoid having too much slowdown.
			if ((totalRead += read) > maxRead) break;
		}
		detector.DataEnd();

		if (isAscii)
			return US_ASCII;

		Charset charset = exactMatch.get();
		String[] pc = detector.getProbableCharsets();
		final List<String> probableCharsets = pc == null ? Collections.<String>emptyList() : Arrays.asList(pc);

		if (!StringUtils.isEmpty(charsetFromBom)) {
			if (probableCharsets.contains(charsetFromBom) || (charset != null && charset.name().equals(charsetFromBom))) {
				if (debug) log.debug("Found UTF-BOM, assuming charset is " + charsetFromBom);
				charset = Charset.forName(charsetFromBom);
			}
		}

		if (charset == null) {
			charset = fallback;
			if ("force".equalsIgnoreCase(getCharsetAutoDetectParameter()) && !probableCharsets.isEmpty()) {
				if (debug) log.debug("Could not detect correct charset for source, using first of probably charset " + probableCharsets);
				charset = Charset.forName(probableCharsets.get(0));
			} else {
				if (debug) log.debug("Charset detection did not find the right charset, using '" + charset + "' as fallback.");
			}
		}

		return charset;
	}

	private UrlFetcher() {
	}
}
