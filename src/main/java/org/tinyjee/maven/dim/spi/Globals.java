/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import org.apache.maven.doxia.logging.Log;
import org.apache.maven.doxia.logging.SystemStreamLog;
import org.apache.maven.doxia.macro.MacroRequest;
import org.apache.maven.doxia.sink.Sink;
import org.codehaus.plexus.PlexusContainer;
import org.codehaus.plexus.util.FileUtils;
import org.codehaus.plexus.util.IOUtil;
import org.codehaus.plexus.util.StringUtils;
import org.tinyjee.maven.dim.utils.CloningOutputStream;

import java.io.*;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.zip.GZIPOutputStream;

import static java.util.Collections.synchronizedMap;
import static java.util.regex.Pattern.compile;

/**
 * Is a global, thread-local singleton that may be used to exchange global settings, functions and shared instances between
 * classes during the runtime of a macro call.
 * <p/>
 * On top of this, the class acts also as the scripting API that may be used in Velocity templates (via {@code $globals})
 * and JSR-223 scripts for accessing certain functionality like attaching and loading content.
 *
 * @author Juergen_Kellerer, 2011-10-13
 */
public class Globals {

	static final String LB = System.getProperty("line.separator", "\n");

	private static final Map<Sink, Set<String>> attachedResources = synchronizedMap(new WeakHashMap<Sink, Set<String>>());
	private static final Map<Sink, AtomicInteger> localIdSequences = synchronizedMap(new WeakHashMap<Sink, AtomicInteger>());
	private static final AtomicInteger idSequence = new AtomicInteger();

	private static final Log logProxy = (Log) Proxy.newProxyInstance(
			Globals.class.getClassLoader(), new Class[]{Log.class}, new InvocationHandler() {
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			return method.invoke(getInstance().getLogger(), args);
		}
	});

	/**
	 * Returns a thread local, recursion safe instance.
	 *
	 * @return a thread local, recursion safe instance.
	 */
	public static Globals getInstance() {
		return GlobalsStack.getGlobals();
	}

	/**
	 * Returns a logger to use for logging to the build log.
	 * <p/>
	 * In difference to {@code getInstance().getLogger()} this method return a static log proxy that does not suffer
	 * from timing issues, thus it is save to create a logger constant using "{@code private static final Log logger = Globals.getLog();}".
	 *
	 * @return a logger to use for logging to the build log.
	 */
	public static Log getLog() {
		return logProxy;
	}

	/**
	 * Returns the relative path (from web root) to the directory hosting the attached includes.
	 *
	 * @return the relative path (from web root) to the directory hosting the attached includes.
	 */
	public static String getAttachedIncludesDirectory() {
		return System.getProperty("org.tinyjee.maven.dim.attachedIncludesDirectory", "attached-includes").replace('\\', '/');
	}

	/**
	 * Returns the relative path (from basedir) to the directory that is created by the maven site plugin to run a site (site:run).
	 *
	 * @return the relative path (from basedir) to the directory that is created by the maven site plugin to run a site (site:run).
	 */
	public static String getSiteWebAppDirectory() {
		return System.getProperty("org.tinyjee.maven.dim.siteWebAppDirectory", "target/site-webapp").replace('\\', '/');
	}

	private Charset charset = Charset.defaultCharset();
	private boolean charsetIsUserSupplied;
	private final PlexusContainer plexusContainer;
	private final MacroRequest request;
	private final File attachedIncludesPath;
	private final List<File> attachedIncludedwebAppPath = new ArrayList<File>();
	private final Sink sink;
	private Log logger;

	Globals(PlexusContainer plexusContainer, Sink sink, MacroRequest request, Log logger, String charset, File attachedIncludesPath) {
		this.plexusContainer = plexusContainer;
		this.sink = sink;
		this.request = request;
		this.logger = logger;
		this.attachedIncludesPath = attachedIncludesPath;

		if (charsetIsUserSupplied = charset != null) {
			this.charset = Charset.forName(charset);
		} else {
			String inputEncoding = System.getProperty("org.tinyjee.maven.dim.project.inputEncoding");
			if (!StringUtils.isEmpty(inputEncoding))
				this.charset = Charset.forName(inputEncoding);
		}

		File[] possibleWebAppDirs = {new File(getBasedir(), getSiteWebAppDirectory()), new File(getSiteWebAppDirectory())};
		for (File webAppDir : possibleWebAppDirs) {
			if (webAppDir.isDirectory()) {
				webAppDir = new File(webAppDir, getAttachedIncludesDirectory());
				if (!attachedIncludedwebAppPath.contains(webAppDir) && (webAppDir.isDirectory() || webAppDir.mkdirs()))
					attachedIncludedwebAppPath.add(webAppDir);
			}
		}
	}

	/**
	 * Returns the charset to use when reading text.
	 *
	 * @return the charset to use when reading text.
	 */
	public Charset getCharset() {
		return charset;
	}

	/**
	 * Returns true if the user supplied a charset with the macro call.
	 *
	 * @return true if the user supplied a charset with the macro call.
	 */
	public boolean getCharsetIsUserSupplied() {
		return charsetIsUserSupplied;
	}

	/**
	 * Returns a logger to use for logging to the build log.
	 *
	 * @return a logger to use for logging to the build log.
	 */
	public Log getLogger() {
		if (logger == null) logger = new SystemStreamLog();
		return logger;
	}

	/**
	 * Returns the next value of the monotonically increasing integer ID (IDs are consistent only for the lifetime of a
	 * single module's site build).
	 * <p/>
	 * This method is primarily meant to assign unique ids to page elements (e.g. for javascript, svg, etc).
	 *
	 * @return an increasing integer ID.
	 */
	public int nextId() {
		return idSequence.incrementAndGet();
	}

	/**
	 * Returns the next page local value of the increasing integer ID (local IDs are consistent only within a single page).
	 * <p/>
	 * This method is primarily meant to assign unique ids to page elements (e.g. for javascript, svg, etc).
	 *
	 * @return an increasing integer ID.
	 */
	public int nextLocalId() {
		AtomicInteger idSequence;
		synchronized (localIdSequences) {
			idSequence = localIdSequences.get(sink);
			if (idSequence == null) localIdSequences.put(sink, idSequence = new AtomicInteger());
		}
		return idSequence.incrementAndGet();
	}

	/**
	 * The current sink, may be 'null'.
	 *
	 * @return the current sink, may be 'null'.
	 */
	public Sink getSink() {
		return sink;
	}

	/**
	 * Creates a globals instance for the given sink.
	 * <p/>
	 * If sink is 'null' or the same as the current, the method returns 'this'.
	 *
	 * @param sink the sink to return a globals instance for.
	 * @return a globals instance that operates on the given sink.
	 */
	public Globals operateOnSink(Sink sink) {
		if (sink == null || sink == this.sink)
			return this;

		final Globals globals = new Globals(plexusContainer, sink, request, logger, null, attachedIncludesPath);
		globals.charset = charset;
		globals.charsetIsUserSupplied = charsetIsUserSupplied;
		return globals;
	}

	/**
	 * Returns true if the sink produces some sort of HTML.
	 *
	 * @return true if the sink produces some sort of HTML.
	 */
	public boolean isHTMLCapableSink() {
		try {
			return sink instanceof org.apache.maven.doxia.markup.HtmlMarkup;
		} catch (NoClassDefFoundError ignored) {
			// Old Doxia API, use String-Search as fallback.
			String[] names = {"XhtmlSink", "XhtmlBaseSink"};
			for (Class<?> cls = sink.getClass(); cls != null; cls = cls.getSuperclass())
				for (String name : names)
					if (cls.getName().contains(name))
						return true;
			return false;
		}
	}

	/**
	 * Return the current macro request, may be 'null'.
	 *
	 * @return the current macro request, may be 'null'.
	 */
	public MacroRequest getRequest() {
		return request;
	}

	/**
	 * Returns the current base directory the Macro is operating against.
	 *
	 * @return the current base directory the Macro is operating against.
	 */
	public File getBasedir() {
		return request == null ? new File(".").getAbsoluteFile() : request.getBasedir();
	}

	/**
	 * Returns the file path to use for storing attached includes.
	 *
	 * @return the file path to use for storing attached includes, or 'null' if attaching includes as file is not supported.
	 */
	public File getAttachedIncludesPath() {
		return attachedIncludesPath;
	}

	/**
	 * Returns the classloader of the currently built module.
	 *
	 * @return the classloader of the currently built module.
	 */
	public ClassLoader getClassLoader() {
		return ResourceResolver.resolveClassLoader(getBasedir());
	}

	/**
	 * Attempts to resolve the given path and returns true if the path could be resolved.
	 *
	 * @param path the path to resolve.
	 * @return true if the path can be resolved.
	 */
	public boolean isResolvable(String path) {
		try {
			ResourceResolver.findSource(getBasedir(), path);
			return true;
		} catch (RuntimeException ignored) {
			return false;
		}
	}

	/**
	 * Resolves the given path to an URL that can be fetched via {@link UrlFetcher}.
	 * <p/>
	 * Note: Under the hood this method delegates to {@link ResourceResolver#findSource(java.io.File, String)}.
	 *
	 * @param path the path to resolve.
	 * @return an URL that can be fetched.
	 */
	public URL resolvePath(String path) {
		return ResourceResolver.findSource(getBasedir(), path);
	}

	/**
	 * Resolves the given local path and returns the corresponding file.
	 *
	 * @param localPath the local path to resolve.
	 * @return the resolved file or 'null' if the path couldn't be resolved to a local file or directory.
	 */
	public File resolveLocalPath(String localPath) {
		final URL url = resolvePath(localPath);
		try {
			return "file".equalsIgnoreCase(url.getProtocol()) ? new File(url.toURI()) : null;
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Fetches the given url and converts the content to a string.
	 *
	 * @param url the url to fetch.
	 * @return url content as string.
	 * @throws java.io.IOException In case of IO failed.
	 */
	public byte[] fetchUrl(URL url) throws IOException {
		final InputStream inputStream = UrlFetcher.getSource(url, false);
		return IOUtil.toByteArray(inputStream);
	}

	/**
	 * Fetches the given url and converts the content to a string.
	 *
	 * @param path the path to fetch.
	 * @return the content as byte array.
	 * @throws java.io.IOException In case of IO failed.
	 */
	public byte[] fetch(String path) throws IOException {
		return fetchUrl(resolvePath(path));
	}

	/**
	 * Fetches the given url and converts the content to a string.
	 *
	 * @param url the url to fetch.
	 * @return url content as string.
	 * @throws java.io.IOException In case of IO failed.
	 */
	public String fetchUrlText(URL url) throws IOException {
		return UrlFetcher.readerToString(UrlFetcher.getReadableSource(url), true);
	}

	/**
	 * Fetches the given path and converts the content to a string.
	 *
	 * @param path the path to fetch.
	 * @return the content as string.
	 * @throws java.io.IOException In case of IO failed.
	 */
	public String fetchText(String path) throws IOException {
		return fetchUrlText(resolvePath(path));
	}

	/**
	 * Calls include macro recursively with the given list of parameters and returns the result of the operation.
	 *
	 * @param parameters        the parameters to use with the include macro call.
	 * @param inheritParameters Specifies whether existing parameters of an outer call that is currently
	 *                          in progress should be inherited or not.
	 * @return the result of the operation.
	 */
	@SuppressWarnings("unchecked")
	public IncludeMacroResult callIncludeMacro(Map<String, Object> parameters, boolean inheritParameters) {
		final Map<String, Object> params = new LinkedHashMap<String, Object>(parameters.size());

		if (inheritParameters && request != null) params.putAll(request.getParameters());
		params.putAll(parameters);

		return IncludeMacroResult.createInstance(plexusContainer, params);
	}

	/**
	 * Calls include macro recursively with the given list of parameters and returns the result of the operation.
	 *
	 * @param parameters the parameters to use with the include macro call.
	 * @return the result of the operation.
	 */
	public IncludeMacroResult call(Map<String, Object> parameters) {
		return callIncludeMacro(parameters, false);
	}

	/**
	 * Returns a set of all resources that were attached to the current sink.
	 *
	 * @return a set of all resources that were attached to the current sink.
	 */
	public Set<String> getAttachedResources() {
		synchronized (attachedResources) {
			Set<String> resources = attachedResources.get(sink);
			if (resources == null) attachedResources.put(sink, resources = new HashSet<String>());
			return resources;
		}
	}

	/**
	 * Attempts to attach the given CSS (cascading style sheet).
	 *
	 * @param fileName   a unique name to reference the content. If the name is empty the css will be inlined.
	 * @param cssContent the content to attach, either in-lined or using the filename reference.
	 *                   If the css content starts with "classpath:", it is interpreted as a classpath URL to include
	 *                   instead of the real content.
	 * @return a status whether attaching the cascading style sheets is supported by the underlying sink.
	 * @throws java.io.IOException In case of disk IO failed.
	 */
	public boolean attachCss(String fileName, String cssContent) throws IOException {
		final Set<String> resources = getAttachedResources();
		if (resources.contains(fileName)) {
			getLog().debug("Will not attach a CSS of name '" + fileName + "' twice within the same output.");
			return true;
		}

		if (isHTMLCapableSink()) {
			StringBuilder html = new StringBuilder(64).append("<style type=\"text/css\">").append(LB);
			String link = StringUtils.isEmpty(fileName) ? null : attachContent(fileName, cssContent);
			if (link == null) {
				if (cssContent.startsWith("classpath:"))
					cssContent = IOUtil.toString(UrlFetcher.getReadableSource(resolvePath(cssContent)));
				html.append(cssContent);
			} else
				html.append("   @import url('").append(link).append("');");
			html.append(LB).append("</style>").append(LB);

			sink.rawText(html.toString());
			if (!StringUtils.isEmpty(fileName)) resources.add(fileName);
			return true;
		}

		return false;
	}

	/**
	 * Attempts to attach the given client side java script.
	 *
	 * @param fileName      a unique name to reference the script.
	 * @param scriptContent the content to attach, either in-lined or using the filename reference.
	 *                      If the script content starts with "classpath:", it is interpreted as a classpath URL to include
	 *                      instead of the real content.
	 * @return a status whether attaching the script is supported by the underlying sink.
	 * @throws java.io.IOException In case of disk IO failed.
	 */
	public boolean attachJs(String fileName, String scriptContent) throws IOException {
		return attachScript(fileName, scriptContent, null, null);
	}

	/**
	 * Attempts to attach the given client side script.
	 *
	 * @param fileName       a unique name to reference the script. If the name is empty the script will be inlined.
	 * @param scriptContent  the content to attach, either in-lined or using the filename reference.
	 *                       If the script content starts with "classpath:", it is interpreted as a classpath URL to include
	 *                       instead of the real content.
	 * @param scriptLanguage the script language, defaults to "javascript".
	 * @param contentType    the content type, defaults to "text/javascript".
	 * @return a status whether attaching the script is supported by the underlying sink.
	 * @throws java.io.IOException In case of disk IO failed.
	 */
	public boolean attachScript(String fileName, String scriptContent, String scriptLanguage, String contentType) throws IOException {
		final Set<String> resources = getAttachedResources();
		if (resources.contains(fileName)) {
			getLog().debug("Will not attach a Script of name '" + fileName + "' twice within the same output.");
			return true;
		}

		if (isHTMLCapableSink()) {
			if (scriptLanguage == null) scriptLanguage = "javascript";
			if (contentType == null) contentType = "text/javascript";

			StringBuilder html = new StringBuilder(64).append("<script " +
					"type=\"").append(contentType).append("\" " +
					"language=\"").append(scriptLanguage).append('"');

			String link = StringUtils.isEmpty(fileName) ? null : attachContent(fileName, scriptContent);
			if (link == null) {
				if (scriptContent.startsWith("classpath:"))
					scriptContent = IOUtil.toString(UrlFetcher.getReadableSource(resolvePath(scriptContent)));
				html.append('>').append(LB).append(scriptContent).append(LB);
			} else
				html.append(" src=\"").append(link).append("\">");
			html.append("</script>").append(LB);

			sink.rawText(html.toString());
			if (!StringUtils.isEmpty(fileName)) resources.add(fileName);
			return true;
		}

		return false;
	}


	/**
	 * Attempts to attach the given content as a file and returns the relative link to the content on success.
	 * <p/>
	 * Content can only be attached when building sites, as there has to be a resolvable resource directory
	 * that is used to put the attached content.
	 * <p/>
	 * Files of the same name are overwritten if they already exist in the output directory.
	 * <p/>
	 * When content is stored, it is saved in 2 versions, uncompressed and gzipped. The latter is provided to reduce
	 * transfer sizes on supporting web-servers and is created if the compressed size is smaller than 95% of the uncompressed size.
	 *
	 * @param fileName a unique name to reference the content.
	 * @param content  the content to attach.
	 *                 If the content starts with "classpath:", it is interpreted as a classpath URL to include
	 *                 instead of the real content.
	 * @return A relative link to the content or 'null' if attaching content is not supported by the underlying environment.
	 * @throws java.io.IOException In case of disk IO failed.
	 */
	public String attachContent(String fileName, String content) throws IOException {
		if (attachedIncludesPath == null) return null;
		if (StringUtils.isEmpty(content)) throw new IllegalArgumentException("Cannot attach 'null'. No content was specified.");

		if (content.startsWith("classpath:"))
			return attachBinaryContent(fileName, content);

		final Log log = getLog();
		Charset charset = getCharset(), utf8 = Charset.forName("UTF-8");

		// Handle charset for XML, it always defaults to UTF-8, overriding any user supplied charset.
		if (content.trim().startsWith("<")) {
			Matcher matcher = compile("\\<\\?xml.*encoding=['\"]{1}(.+)['\"]{1}").matcher(content);
			if (matcher.find())
				charset = Charset.forName(matcher.group(1));
			else if (content.contains("/>") || !content.contains("<html"))
				charset = utf8;
		}

		CharsetEncoder charsetEncoder = getCharset().newEncoder();
		if (!charsetEncoder.canEncode(content)) {
			log.warn("The selected charset of '" + charset.name() + "' cannot be used to encode the specified content. " +
					"Will change the charset to '" + utf8 + '\'');
			charset = utf8;
			charsetEncoder = charset.newEncoder();
		}

		if (log.isDebugEnabled()) log.debug("Selected charset for '" + fileName + "' using '" + charset + '\'');
		try {
			ByteBuffer buffer = charsetEncoder.encode(CharBuffer.wrap(content));
			final ByteArrayInputStream stream = new ByteArrayInputStream(buffer.array(), buffer.arrayOffset(), buffer.remaining());
			return attachBinaryContent(fileName, stream);
		} catch (RuntimeException e) {
			if (log.isDebugEnabled()) log.debug("Content that will not be stored:" + LB + content);
			throw e;
		}
	}

	/**
	 * Overloads {@link #attachBinaryContent(URL)}.
	 *
	 * @param url the url or path to fetch the binary content from.
	 * @return A relative link to the content or 'null' if attaching content is not supported by the underlying environment.
	 * @throws java.io.IOException In case of disk IO failed.
	 */
	public String attachBinaryContent(String url) throws IOException {
		return attachBinaryContent(resolvePath(url));
	}

	/**
	 * Overloads {@link #attachBinaryContent(String, URL)}.
	 *
	 * @param fileName a unique name to reference the content.
	 * @param url      the url or path to fetch the binary content from.
	 * @return A relative link to the content or 'null' if attaching content is not supported by the underlying environment.
	 * @throws java.io.IOException In case of disk IO failed.
	 */
	public String attachBinaryContent(String fileName, String url) throws IOException {
		return attachBinaryContent(fileName, resolvePath(url));
	}

	/**
	 * Overloads {@link #attachBinaryContent(String, URL)}.
	 *
	 * @param sourceUrl an URL to fetch the binary content that should be attached.
	 * @return A relative link to the content or 'null' if attaching content is not supported by the underlying environment.
	 * @throws java.io.IOException In case of disk IO failed.
	 */
	public String attachBinaryContent(URL sourceUrl) throws IOException {
		return attachBinaryContent(sourceUrl.getPath(), sourceUrl);
	}

	/**
	 * Overloads {@link #attachBinaryContent(String, java.io.InputStream)}.
	 *
	 * @param fileName  a unique name to reference the content.
	 * @param sourceUrl an URL to fetch the binary content that should be attached.
	 * @return A relative link to the content or 'null' if attaching content is not supported by the underlying environment.
	 * @throws java.io.IOException In case of disk IO failed.
	 */
	public String attachBinaryContent(String fileName, URL sourceUrl) throws IOException {
		return attachBinaryContent(fileName, UrlFetcher.getSource(sourceUrl, false));
	}

	/**
	 * Overloads {@link #attachBinaryContent(String, java.io.InputStream)}.
	 *
	 * @param fileName      a unique name to reference the content.
	 * @param binaryContent the content to attach.
	 * @return A relative link to the content or 'null' if attaching content is not supported by the underlying environment.
	 * @throws java.io.IOException In case of disk IO failed.
	 */
	public String attachBinaryContent(String fileName, byte[] binaryContent) throws IOException {
		return attachBinaryContent(fileName, new ByteArrayInputStream(binaryContent));
	}

	/**
	 * Attempts to attach the given content as a file and returns the relative link to the content on success.
	 * <p/>
	 * Content can only be attached when building sites, as there has to be a resolvable resource directory
	 * that is used to put the attached content.
	 * <p/>
	 * Files of the same name are overwritten if they already exist in the output directory.
	 * <p/>
	 * When content is stored, it is saved in 2 versions, uncompressed and gzipped. The latter is provided to reduce
	 * transfer sizes on supporting web-servers and is created if the compressed size is smaller than 95% of the uncompressed size.
	 *
	 * @param fileName      a unique name to reference the content.
	 * @param binaryContent the content to attach.
	 * @return A relative link to the content or 'null' if attaching content is not supported by the underlying environment.
	 * @throws java.io.IOException In case of disk IO failed.
	 */
	public String attachBinaryContent(String fileName, InputStream binaryContent) throws IOException {
		if (attachedIncludesPath == null) return null;
		if (binaryContent == null) throw new IllegalArgumentException("Cannot attach 'null'. Binary content is not specified.");
		if (StringUtils.isEmpty(fileName)) {
			throw new IllegalArgumentException("Cannot attach binary content '" + binaryContent + "' when filename is empty.");
		}

		final Log log = getLog();
		final boolean debug = log.isDebugEnabled();

		final File path = attachedIncludesPath.getCanonicalFile();
		final File targetFile;
		try {
			targetFile = createTargetFile(path, fileName);
		} catch (IOException e) {
			IOException ioException = new IOException(
					"Failed building target path to attach file " + fileName + " under base path " + path);
			ioException.initCause(e);
			throw ioException;
		}

		if (debug) log.debug("About to store content to '" + targetFile + '\'');
		final File compressedTargetFile = new File(targetFile + ".gz");
		final OutputStream outputStream = new CloningOutputStream(
				new FileOutputStream(targetFile), new GZIPOutputStream(new FileOutputStream(compressedTargetFile)));
		try {
			IOUtil.copy(binaryContent, outputStream, 64 * 1024);
		} finally {
			outputStream.close();
		}

		if (!attachedIncludedwebAppPath.isEmpty()) {
			if (debug) log.debug("Detected maven site webapp path, deploying attached includes there as well.");
			for (File attachedIncludedPath : attachedIncludedwebAppPath) {
				final File webAppPath = attachedIncludedPath.getCanonicalFile(),
						webTargetFile = createTargetFile(webAppPath, fileName), webCompressedTargetFile = new File(webTargetFile + ".gz");

				FileUtils.copyFile(targetFile, webTargetFile);
				FileUtils.copyFile(compressedTargetFile, webCompressedTargetFile);
			}
		}

		removeCompressedFileIfRatioIsTooLow(targetFile, compressedTargetFile);

		getAttachedResources().add(fileName);

		final URI baseUri = createBaseUri(path);
		return baseUri.relativize(targetFile.toURI()).toASCIIString();
	}

	private void removeCompressedFileIfRatioIsTooLow(File targetFile, File compressedTargetFile) {
		final Log log = getLog();
		final boolean debug = log.isDebugEnabled();
		final double compressionRatio = (double) compressedTargetFile.length() / (double) targetFile.length();
		if (compressionRatio > 0.95) {
			if (debug) {
				log.debug("The size of '" + compressedTargetFile + "' is " + Math.round(100 * compressionRatio) +
						"% the size of the uncompressed file, removing the compressed version as it is no improvement.");
			}
			if (!compressedTargetFile.delete()) compressedTargetFile.deleteOnExit();
		} else {
			if (debug) {
				log.debug("Generated a compressed version of the attached content '" + compressedTargetFile + "' having " +
						Math.round(100 * compressionRatio) + "% the size of the uncompressed file.");
			}
		}
	}

	private static URI createBaseUri(File path) {
		final String attachedIncludesDirectory = getAttachedIncludesDirectory();
		if (!StringUtils.isEmpty(attachedIncludesDirectory)) {
			String pathString = path.getAbsolutePath().replace('\\', '/');
			if (pathString.endsWith(attachedIncludesDirectory))
				pathString = pathString.substring(0, pathString.length() - attachedIncludesDirectory.length());
			return new File(pathString).toURI();
		} else
			return path.toURI();
	}

	static File createTargetFile(File path, String fileName) throws IOException {
		final Log log = getLog();
		// Detect URIs and extract the path portion
		if (fileName.matches("^[a-zA-Z]{2,}:[/]{1,3}.+")) {
			if (log.isDebugEnabled()) {
				log.debug("The given filename '" + fileName + "' seems to be an URI, trying to extract the URI path for use as " +
						"filename instead of the complete URI.");
			}
			fileName = URI.create(fileName).getPath();
			if (fileName.contains("!/")) fileName = fileName.substring(fileName.indexOf("!/") + 2);
		}

		// Convert absolute to relative paths (linux and windows.. Note: Must be that order!)
		if (fileName.startsWith("/")) fileName = fileName.substring(1);
		if (fileName.matches("^[a-zA-Z]{1}:.+")) fileName = fileName.substring(2);

		final File targetFile = new File(path, fileName).getCanonicalFile(), targetDirectory = targetFile.getParentFile();
		if (!targetFile.getAbsolutePath().startsWith(path.getAbsolutePath()))
			throw new IllegalArgumentException("Cannot store target file " + targetFile + " as it is not located below " + path);
		if (targetDirectory == null || (!targetDirectory.isDirectory() && !targetDirectory.mkdirs())) {
			log.warn("Cannot create target directory '" + targetDirectory + "' to store content for file '" + fileName + '\'');
			throw new IllegalArgumentException("Cannot store target file " + targetFile + " as the parent " +
					"directory does not exist and cannot be created. Parent directory is " + targetDirectory);
		}
		return targetFile;
	}

	@Override
	public String toString() {
		return "Globals{" +
				"charset=" + charset +
				", charsetIsUserSupplied=" + charsetIsUserSupplied +
				", request=" + request +
				", attachedIncludesPath=" + attachedIncludesPath +
				", sink=" + sink +
				", logger=" + logger +
				'}';
	}
}
