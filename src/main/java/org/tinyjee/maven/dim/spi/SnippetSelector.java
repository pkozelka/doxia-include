/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import org.tinyjee.maven.dim.spi.backport.ServiceLoader;

import java.io.IOException;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

/**
 * {@link SnippetSelector} defines an interface that may be implemented and registered via service loading
 * to add new snippet selection mechanisms.
 * <p/>
 * <b>Implementation</b>:<code><pre>
 * package my.package;
 * public class MySnippetSelector implements SnippetSelector {
 *      public String[] getExpressionPrefixes() {
 *          return new String[] {"my:"};
 *      }
 *      public boolean canSelectSnippetsWith(String expression, URL contentUrl, Map<String, Object> macroParameters) {
 *          return expression.toLowerCase().startsWith("my:");
 *      }
 *      public Iterator&lt;Integer&gt; selectSnippets(String expression, URL contentUrl, LineNumberReader content,
 *                                                    Map&lt;String, Object&gt; macroParameters) throws IOException {
 *          List&lt;Integer&gt; output = new LinkedList&lt;Integer&gt;();
 *          for (String line = content.readLine(); line != null; line = content.readLine()) {
 *              if (... EXPRESSION MATCHES LINE ...) output.add(content.getLineNumber());
 *          }
 *          return output.iterator();
 *      }
 * }
 * </pre></code>
 * <p/>
 * <b>Register the implementation using service loading</b>:<ul>
 * <li>
 * Create the following file:<code><pre>
 * META-INF/services/
 *    org.tinyjee.maven.dim.spi.SnippetSelector
 * </pre></code></li>
 * <li>Add the fully qualified class name of your implementation to the file "org.tinyjee.maven.dim.spi.SnippetSelector":<code><pre>
 * my.package.MySnippetSelector
 * </pre></code></li>
 * </ul>
 *
 * @version 1.1
 * @since 1.1
 * @author Juergen_Kellerer, 2011-10-14
 */
public interface SnippetSelector {

	// START SNIPPET: SnippetSelectorParams
	/**
	 * Is an optional boolean parameter that toggles whether snippet selection is case sensitive.
	 * Whether this parameter is used by selectors is implementation specific. Some selectors may not support to change this value.
	 */
	String CASE_SENSITIVE = "case-sensitive";
	/**
	 * Is a boolean parameters that may be used by snippet selectors to expand the included content to additional regions
	 * if matching an element that has belongings in other locations of the same file.
	 * <p/>
	 * E.g. when matching a field, getter or setter a snippet selector may choose to include all 3 code locations if only
	 * one of them was originally matched.
	 */
	String EXPAND_SNIPPETS = "expand-snippets";
	// END SNIPPET: SnippetSelectorParams

	/**
	 * Is an iterable over all selectors within the classpath.
	 */
	Iterable<SnippetSelector> SELECTORS = ServiceLoader.load(SnippetSelector.class);

	/**
	 * Returns all prefixes that are explicitly supported by this snippet selector.
	 * @return all prefixes that are explicitly supported by this snippet selector.
	 */
	String[] getExpressionPrefixes();

	/**
	 * Returns true if the expression is supported by this snippet selector.
	 *
	 * @param expression      a single snippet expression.
	 * @param contentUrl      the resolved URL of the content to include (might be 'null' if the source does not provide a Url).
	 * @param macroParameters the parameters used with the macro call.
	 * @return true if the snippet selector supports the given expression.
	 */
	boolean canSelectSnippetsWith(String expression, URL contentUrl, Map<String, Object> macroParameters);

	/**
	 * Selects snippets in the given content and returns an iterator over selected line numbers.
	 * <p/>
	 * Note: The caller reads the returned iterator fully before closing the content stream.
	 * This way snippet selectors can truly implement streaming selection (in difference to the provided example).
	 *
	 * @param expression      a single snippet expression.
	 * @param contentUrl      the resolved URL of the content to include (might be 'null' if the source does not provide a Url).
	 * @param content         a reader providing the content. Implementations must read content from this reader as the Url
	 *                        may point to an outdated static source that does not reflect the content to include.
	 * @param macroParameters the parameters used with the macro call.
	 * @return an iterator containing all selected line numbers.
	 * @throws java.io.IOException In case of reading the URL failed.
	 */
	Iterator<Integer> selectSnippets(final String expression, final URL contentUrl, final LineNumberReader content,
	                                 final Map<String, Object> macroParameters) throws IOException;
}
