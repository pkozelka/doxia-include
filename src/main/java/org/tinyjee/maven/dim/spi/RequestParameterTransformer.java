/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import org.tinyjee.maven.dim.spi.backport.ServiceLoader;

import java.util.Map;

/**
 * {@link RequestParameterTransformer} defines an interface that may be implemented and registered via service loading
 * to plug parameter transformers that adjust macro parameters on the fly before any other operation is triggered.
 * <p/>
 * <b>Implementation</b>:<code><pre>
 * package my.package;
 * public class MyRequestParameterTransformer implements RequestParameterTransformer {
 *      public void transformParameters(Map&lt;String, Object&gt; requestParams) {
 *          Object value = requestParams.get("my-input-param");
 *          if (value != null) {
 *              requestParams.put("myText", value);
 *              requestParams.put("source", "classpath:my-template.vm");
 *          }
 *      }
 * }
 * </pre></code>
 * <p/>
 * <b>Register the implementation using service loading</b>:<ul>
 * <li>
 * Create the following file:<code><pre>
 * META-INF/services/
 *    org.tinyjee.maven.dim.spi.RequestParameterTransformer
 * </pre></code></li>
 * <li>Add the fully qualified class name of your implementation to the file
 * "org.tinyjee.maven.dim.spi.RequestParameterTransformer":<code><pre>
 * my.package.MyRequestParameterTransformer
 * </pre></code></li>
 * </ul>
 *
 * @version 1.1
 * @since 1.0
 * @author Juergen_Kellerer, 2010-09-08
 */
public interface RequestParameterTransformer {

	/**
	 * Is an iterable over all transformers within the classpath.
	 */
	Iterable<RequestParameterTransformer> TRANSFORMERS = ServiceLoader.load(RequestParameterTransformer.class);

	/**
	 * Transforms the specified request parameters.
	 *
	 * @param requestParams the request params to modify.
	 */
	void transformParameters(Map<String, Object> requestParams);
}
