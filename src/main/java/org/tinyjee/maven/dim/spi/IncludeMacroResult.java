/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.tinyjee.maven.dim.spi;

import org.apache.maven.doxia.macro.Macro;
import org.apache.maven.doxia.macro.MacroRequest;
import org.apache.maven.doxia.macro.manager.MacroManager;
import org.apache.maven.doxia.sink.Sink;
import org.apache.maven.doxia.sink.XhtmlBaseSink;
import org.codehaus.plexus.PlexusContainer;
import org.tinyjee.maven.dim.IncludeMacroSignature;

import java.io.StringWriter;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.tinyjee.maven.dim.IncludeMacroSignature.PARAM_VERBATIM;

/**
 * Is a macro result wrapper that allows programmatic execution of include macro by creating an instance of this class.
 *
 * @author Juergen_Kellerer, 2012-03-11
 */
public final class IncludeMacroResult extends FixedContentSource {
	/**
	 * Creates a new include macro result for the given plexus container and macro parameters.
	 *
	 *
	 * @param container  the plexus-container that manages all instances.
	 * @param parameters the parameters of the macro call.
	 * @return a new instance wrapping the result of the macro execution.
	 */
	public static IncludeMacroResult createInstance(PlexusContainer container, Map<String, Object> parameters) {
		if (container == null)
			throw new IllegalStateException("Cannot create an instance of 'include' macro as the plexus container is 'null'");

		try {
			MacroManager macroManager = (MacroManager) container.lookup(MacroManager.ROLE);
			try {
				// Default to "non-verbatim" to avoid getting the syntax highlighter enabled (use verbatim=true is this is really required)
				parameters = new HashMap<String, Object>(parameters == null ? Collections.<String, Object>emptyMap() : parameters);
				if (!parameters.containsKey(PARAM_VERBATIM)) parameters.put(PARAM_VERBATIM, false);

				final MacroRequest request = new MacroRequest(parameters, Globals.getInstance().getBasedir());
				final StringWriter outputBuffer = new StringWriter();
				final Sink output = new XhtmlBaseSink(outputBuffer);

				final Macro includeMacro = macroManager.getMacro("include");
				includeMacro.execute(output, request);

				Object sourceUrl = request.getParameter("sourceUrl");
				return new IncludeMacroResult(sourceUrl instanceof URL ? (URL) sourceUrl : null, outputBuffer.toString(), parameters);
			} finally {
				container.release(macroManager);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private IncludeMacroResult(URL url, String content, Map<String, Object> parameters) {
		super(url, content, parameters);
	}

	/**
	 * Returns the text content that was produced with the macro call.
	 *
	 * @return the text content that was produced with the macro call.
	 */
	public String getText() {
		return content;
	}

	/**
	 * Returns the parameters that were produced with the macro call.
	 *
	 * @return the parameters that were produced with the macro call.
	 */
	public Map<String, Object> getParameters() {
		return getContent().getParameters();
	}

	@Override
	public String toString() {
		return "IncludeMacroResult{" +
				"sourceUrl=" + sourceUrl +
				", parameters=" + parameters +
				", content=" + content +
				'}';
	}
}
