var loader = request.get("graphLoader"), builder = request.get("graphBuilder");
var toMap = require('java-utils').convertToJava;

loader.put("cell-size-min", "100x35");
loader.put("layout", "organic");

builder.addVertex("a");
builder.addVertex("b", toMap({"label": "Vertex B", "style": "fontStyle=bold"}));
builder.addVertex("c", toMap({"style": "shape=ellipse;strokeColor=red;strokeWidth=3"}));

builder.addEdge("a", "c");
builder.addEdge("b", "c");
