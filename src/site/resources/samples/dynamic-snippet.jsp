<%-- START SNIPPET: ${id} --%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
    <body>
<%-- END SNIPPET: ${id} --%>

 		<!-- START SNIPPET: first -->
    	<h2><% out.println("First Snippet"); %></h2>
    	<!-- END SNIPPET: first -->

 		<!-- START SNIPPET: second -->
    	<h2><% out.println("Second Snippet"); %></h2>
    	<!-- END SNIPPET: second -->

<%-- START SNIPPET: ${id} --%>
    </body>
</html>
<%-- END SNIPPET: ${id} --%>