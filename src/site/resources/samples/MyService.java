
package samples;

import java.util.UUID;

public class MyService {
	public UUID prepareNewJob() {
		...
	}

	public void addWork(UUID jobId,
	                    Callable work) {
		...
	}

	public Future startJob(UUID jobId) {
		...
	}
}
