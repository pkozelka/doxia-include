/*
 * Copyright 2012 - Doxia :: Include Macro - Juergen Kellerer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package samples;

import java.lang.Object;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

public class MyServiceTest {
	@Test
	public testStandardWorkFlow() {
		List<String> names = Arrays.asList("First", "Second", "Third");
		List<Callable> myWorkList = new ArrayList<Callable>();
		
		for (final String name : names) {
			myWorkList.add(new Callable() {
				public String call() throws Exception {
					return name;
				}
			});
		}

		//START SNIPPET: UsageExample
		// Instantiate Service
		MyService service = new MyService();

		// Prepare a Job
		UUID jobId = service.prepareNewJob();
		verify: {
			assertFalse(service.jobsAndWork.containsKey(jobId));
		}

		// Add Work
		for (Callable work : myWorkList) 
			service.addWork(jobId, work);
		verify: {
			assertEquals(myWorkList, service.jobsAndWork.get(jobId));
		}

		// Start It
		Future<List> futureResult = service.startJob(jobId);
		List resultOfWork = futureResult.get();
		verify: {
			assertEquals(names, resultOfWork);
		}


		//END SNIPPET: UsageExample
	}
}
