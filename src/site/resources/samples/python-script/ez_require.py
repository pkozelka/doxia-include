import site, sys, os

def installOrUpdate(packageNames):
    print "<pre class='easy_install_output'>Running 'easy_install':"
    print ""

    ## Install "easy_install" (if required)
    try:
        from pkg_resources import require
    except ImportError:
        print "-- Creating a local copy of 'easy_install' to install itself.."
        from ez_setup import use_setuptools

        use_setuptools()
        ezEgg = sys.path[0]

        from pkg_resources import require

        if "setuptools" in ezEgg and ".egg" in ezEgg:
            print "-- Installing '", ezEgg, "' in site-packages"
            from setuptools.command import easy_install

            easy_install.main([ezEgg])

            print "-- Removing locally stored egg '", ezEgg, "'"
            sys.path.remove(ezEgg)
            require("setuptools") # reloading setuptools from site-packages
            os.unlink(ezEgg)
            print ""


    ## Installing or updating the named packages
    from setuptools.command import easy_install

    for name in packageNames:
        print "-- Installing or updating '", name, "'"
        easy_install.main(["-U", name])

    print "</pre>"


def ez_require(names=[]):
    first = True
    while True:
        try:
            from pkg_resources import require

            for name in names: require(name)
            break
        except:
            if first: first = False; installOrUpdate(names)
            else: raise

