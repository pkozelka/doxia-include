# Conditionally install pygments
from ez_require import ez_require
ez_require(["pygments>=1.5"])

# Fetching the snippet to highlight using a recursive macro call
params = {"source": request["src"], "verbatim": "false"}
if request.containsKey("snippet"):
    params["snippet"] = request["snippet"]

macroResult = globals.callIncludeMacro(params, False)
source = str(macroResult.url)
code = macroResult.text

# Init Pygments
from pygments import highlight
from pygments.lexers import guess_lexer_for_filename
from pygments.formatters import HtmlFormatter

lexer = guess_lexer_for_filename(source, code)
formatter = HtmlFormatter(linenos=False, cssclass="pygments-highlight")

# Highlight the Snippet
print "<style type='text/css'>"
print formatter.get_style_defs('.pygments-highlight')
print "</style>"
print highlight(code, lexer, formatter)
