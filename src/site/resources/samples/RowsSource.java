package net.sf;

import java.util.*;
import static java.util.Arrays.*;

public class RowsSource extends HashMap<String, Object> {
	public RowsSource(File basePath, Map requestParameters) {
		List<List<String>> rows = new ArrayList<List<String>>();
		rows.add(asList("a", "Parameter A"));
		rows.add(asList("b", "Parameter B"));
		put("rows", rows);
	}
}