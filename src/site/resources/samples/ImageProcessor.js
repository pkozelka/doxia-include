importPackage(java.awt);

// 'image' is an instance of BufferedImage
var image = request.get("image"), g = image.createGraphics();
try {
	var color = request.get("color");
	g.setPaint(color != null ? Color.decode(color) : Color.GREEN);
	g.drawRect(0, 0, image.width - 1, image.height - 1);
} finally {
	g.dispose();
}

